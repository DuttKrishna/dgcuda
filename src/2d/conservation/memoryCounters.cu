#include "operations.cuh"

class memoryCounters{

	public:
		size_t etree_memory, stree_memory;
		size_t emesh_memory, smesh_memory;
		size_t ebuffer_memory, sbuffer_memory;
		size_t integration_memory;
		size_t limiter_memory;
		size_t geometry_memory;
	    size_t miscellaneous_memory;
	    size_t visualization_memory;
	    size_t indicator_memory;

	    memoryCounters(); 
	    size_t total();
};

memoryCounters::memoryCounters( )
{
    etree_memory = 0, stree_memory=0;
	emesh_memory=0, smesh_memory=0;
	limiter_memory = 0;
	ebuffer_memory=0, sbuffer_memory=0;
	integration_memory=0;
	geometry_memory=0;
	miscellaneous_memory=0;
	visualization_memory = 0;
}

size_t memoryCounters::total( )
{
return etree_memory + stree_memory+
emesh_memory + smesh_memory +
ebuffer_memory + sbuffer_memory +
integration_memory + 
limiter_memory +
geometry_memory + 
miscellaneous_memory + 
visualization_memory ;
}


