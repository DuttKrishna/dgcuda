/* time_integrator.cu
 *
 * time integration functions.
 */
// #ifndef TIMEINTEGRATOR_H_GUARD
// #define TIMEINTEGRATOR_H_GUARD
void checkCudaError(const char*);
// #endif

extern int local_N;
extern int limiter;
#include "./cub-1.5.1/cub/cub.cuh"
#include "operations.cuh"
#include "plot.cuh"
#include "error_estimate.cuh"

void print_angular_momentum(double *d_a_m,double **C,
                            double *V1x, double *V1y,
                            double *V2x, double *V2y,
                            double *V3x, double *V3y,
                            double *in_J, 
                            int num_elem);

void write_U(int, int, int, double);
void write_C(int local_num_elem, int local_n_p) {
    int idx;
    FILE *out_file;
    char out_filename[100];
    
    // copy over coefficients
    double *c = (double *) malloc(local_num_elem * sizeof(double));
    
    // write to file
    sprintf(out_filename, "output/C.txt");
    out_file  = fopen(out_filename , "w");
    for(int n = 0; n < 4; n ++){
        for(int i = 0; i < local_n_p; i ++){
            cudaMemcpy(c, h_c[n*local_n_p + i], local_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
            for (idx = 0; idx < local_num_elem ; idx++) {
                fprintf(out_file, "%lf\n", c[idx]);
            }
        }
    }
    
    // close file and free c
    fclose(out_file);
    free(c);
}

__device__ void *d_temp_storage_reduce;
__device__ double *d_in;
__device__ double *d_max;
__device__ double *d_min_ratio;
__device__ double *d_max_lambda;

__device__ double *d_detector;
__device__ double *d_length;
__device__ int *d_limit;
__device__ double *d_a_m;


__device__ double *d_abs;
__device__ double *d_max_abs;

size_t temp_storage_bytes_reduce;

#include "tree.cuh"

void allocateTimestepper(int max) {
  cudaMalloc( (void **) &d_max, sizeof(double));
  cudaMalloc( (void **) &d_min_ratio, sizeof(double));
  cudaMalloc( (void **) &d_max_lambda, sizeof(double));
  cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max, max);
  cudaMalloc((void **) &d_temp_storage_reduce, temp_storage_bytes_reduce);

  cudaMalloc((void **) &d_abs, max * sizeof(double));
  cudaMalloc((void **) &d_max_abs, sizeof(double));
  cudaMalloc((void **) &d_detector, max * sizeof(double));
  cudaMalloc((void **) &d_limit, max * sizeof(int));
  set_value<<<get_blocks(max,512), 512>>>(d_limit, 0, max);

  cudaMalloc((void **) &d_a_m, max * sizeof(double));
} 


int resumeLimit(int curr_num_elem) {
        // read the coefficient file
        char *c_filename = "output/resume/limit.bin";
        FILE *c_file = fopen(c_filename, "rb");

        int *limit = (int *) malloc(curr_num_elem * sizeof(int));

        fread(limit ,sizeof(int),curr_num_elem,c_file);
        cudaMemcpy(d_limit, limit, curr_num_elem * sizeof(int) , cudaMemcpyHostToDevice);

        fclose(c_file);
        free(limit);
} 

__global__ void abs(double *out, double **in, double *J, double dt) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;
    double max_abs = -1.;
    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
              max_abs = fabs(in[n*n_p + i][idx]) * dt / J[idx] > max_abs ? fabs(in[n*n_p + i][idx]) : max_abs;
            }
        }
        out[idx] = max_abs;
    }
}



/* tempstorage for RK
 * 
 * I need to store u + alpha * k_i into some temporary variable called kstar
 */






__global__ void rk_tempstorage(double **c, double **kstar, double **k, double alpha) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] + alpha * k[n*n_p + i][idx];
            }
        }
    }
}

__global__ void rk_tempstorage(double **c, double **kstar, double**k, double alpha, double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] + alpha * k[n*n_p + i][idx]/J[idx];
            }
        }
    }
}

__global__ void rk2_ssp_final(double **out, double **c1, double **c2, double **k, 
                              double alpha1, double alpha2, double alpha3,
                              double dt, double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                out[n*n_p + i][idx] = alpha1*c1[n*n_p + i][idx] + alpha2*c2[n*n_p + i][idx] + alpha3 * dt * k[n*n_p + i][idx]/J[idx];
            }
        }
    }
}



__global__ void intermediate1(double **kstar, double **c, double alpha, double dt, double**k1,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] + alpha * dt * k1[n*n_p + i][idx]/J[idx];
            }
        }
    }
}

__global__ void intermediate2(double **kstar, double **c, double alpha1, double alpha2, double**k1, double**k2, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx])/J[idx];
            }
        }
    }
}

__global__ void intermediate3(double **kstar, double **c, double alpha1, double alpha2,double alpha3, double**k1, double**k2, double**k3, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx]+ alpha3*k3[n*n_p + i][idx])/J[idx];
            }
        }
    }
}

__global__ void intermediate4(double **kstar, double **c, double alpha1, double alpha2,double alpha3,double alpha4, double**k1, double**k2, double**k3,double**k4, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx]+ alpha3*k3[n*n_p + i][idx]+ alpha4*k4[n*n_p + i][idx])/J[idx];
            }
        }
    }
}

__global__ void intermediate5(double **kstar, double **c, double alpha1, double alpha2,double alpha3,double alpha4,double alpha5, double**k1, double**k2, double**k3,double**k4, double**k5, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx]+ alpha3*k3[n*n_p + i][idx]+ alpha4*k4[n*n_p + i][idx]+ alpha5*k5[n*n_p + i][idx])/J[idx];
            }
        }
    }
}

__global__ void intermediate6(double **kstar, double **c, double alpha1, double alpha2,double alpha3,double alpha4,double alpha5,double alpha6, double**k1, double**k2, double**k3,double**k4, double**k5, double**k6, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx]+ alpha3*k3[n*n_p + i][idx]+ alpha4*k4[n*n_p + i][idx]+ alpha5*k5[n*n_p + i][idx]+ alpha6*k6[n*n_p + i][idx])/J[idx];
            }
        }
    }
}

__global__ void intermediate7(double **kstar, double **c, double alpha1, double alpha2,double alpha3,double alpha4,double alpha5,double alpha6,double alpha7, double**k1, double**k2, double**k3,double**k4, double**k5, double**k6,double**k7, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx]+ alpha3*k3[n*n_p + i][idx]+ alpha4*k4[n*n_p + i][idx]+ alpha5*k5[n*n_p + i][idx]+ alpha6*k6[n*n_p + i][idx]+ alpha7*k7[n*n_p + i][idx])/J[idx];
            }
        }
    }
}

__global__ void intermediate8(double **kstar, double **c, double alpha1, double alpha2,double alpha3,double alpha4,double alpha5,double alpha6,double alpha7, double alpha8, double**k1, double**k2, double**k3,double**k4, double**k5, double**k6,double**k7,double**k8, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] +  dt * (alpha1*k1[n*n_p + i][idx] + alpha2*k2[n*n_p + i][idx]+ alpha3*k3[n*n_p + i][idx]+ alpha4*k4[n*n_p + i][idx]+ alpha5*k5[n*n_p + i][idx]+ alpha6*k6[n*n_p + i][idx]+ alpha7*k7[n*n_p + i][idx]+ alpha8*k8[n*n_p + i][idx])/J[idx];
            }
        }
    }
}



__global__ void intermediate2(double **u2, double **u0, double**u1, double**k2, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                u2[n*n_p + i][idx] = 0.75*u0[n*n_p + i][idx] + 0.25*u1[n*n_p + i][idx] + 0.25*dt * k2[n*n_p + i][idx] /J[idx];
            }
        }
    }
}
__global__ void rk3_final( double **u0, double **u2, double**k3, double dt,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                u0[n*n_p + i][idx] = (1./3.)*u0[n*n_p + i][idx] + (2./3.)*u2[n*n_p + i][idx] + (2./3.) *dt * k3[n*n_p + i][idx] /J[idx];
            }
        }
    }
}

__global__ void rk3_final(double **kstar, double **c, double alpha, double beta, double gamma, double dt, double**k1,double**k2,double**k3,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] + dt * (alpha * k1[n*n_p + i][idx] + beta * k2[n*n_p + i][idx] + gamma * k3[n*n_p + i][idx] )/J[idx];
            }
        }
    }
}

__global__ void rk4_final(double **kstar, double **c, double alpha, double beta, double gamma,double delta, double dt, double**k1,double**k2,double**k3,double**k4,  double *J) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < num_elem) {
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                kstar[n*n_p + i][idx] = c[n*n_p + i][idx] + dt * (alpha * k1[n*n_p + i][idx] + beta * k2[n*n_p + i][idx] + gamma * k3[n*n_p + i][idx] + delta * k4[n*n_p + i][idx] )/J[idx];
            }
        }
    }
}


__global__ void detector_scale(double *detector, double *circles, int order, double *length, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < max) {
      if(length[idx] < 1e-8)
        detector[idx] = 0.;
      else
        detector[idx] = abs(detector[idx]) / pow(circles[idx], (order+1.)/2.) / length[idx];
    }
}

__global__ void rhs_detector(double *detector, int *flag, 
                             double **rhs, double *circles, 
                             int order, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;

    if (idx < max) {
        detector[idx] = (abs(rhs[1][idx])  ) / pow(circles[idx], (order+1.)/2.) ;

        // printf("%lf \n", detector[idx]);
        if(detector[idx] > 1)
            flag[idx] = 1;
        else
            flag[idx] = 0;
    }



}

__global__ void rhs_detector(double *detector, int *flag, 
                             double **C, double **rhs, double *circles, 
                             int *curr_s1, int *curr_s2, int *curr_s3,
                             int *spos, int *d_schild1, int *d_schild2,
                             int *soriginal, double *slength,
                             int order, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;
    int s[6], sID[3], orig, sc;
    double len;
    double max_rhs, norm, d1, d2;

    if (idx < max) {

      len = 0.;

  // load the sides of the elements
      sID[0] = curr_s1[idx];
      sID[1] = curr_s2[idx];
      sID[2] = curr_s3[idx];

      for(int i = 0; i < 3; i++) {
          sc = d_schild1[sID[i]];
          if(sc > -1){
              orig = soriginal[spos[sc]];
              len+=slength[orig];
          }
          else{
              orig = soriginal[spos[ sID[i] ]];
              len+=slength[orig];
          }
      }

      norm = C[0][idx];
      // norm = sqrt(C[3][idx]*C[3][idx] + C[6][idx]*C[6][idx] );
      // norm = abs(rhs[1][idx] - volume[idx]);
      // norm = norm > 1e-10 ? norm : 1.;
      d1 = abs(rhs[1][idx]) / pow(circles[idx], (order+1.)/2.) / len  / norm;
      // d2 = abs(rhs[2][idx]) / pow(circles[idx], (order+1.)/2.) / len  / norm;
      // detector[idx] = (d1 > d2) ? d1 : d2;
      detector[idx] = d1;

      if(detector[idx] > 1)
          flag[idx] = 1;
      else
          flag[idx] = 0;
    }



}



__global__ void rhs_detector(double *detector, int *flag, 
                             double *volume,
                             double **C, double **rhs, double *circles, 
                             int *curr_s1, int *curr_s2, int *curr_s3,
                             int *spos, int *d_schild1, int *d_schild2,
                             int *soriginal, double *slength,
                             int order, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;
    int s[6], sID[3], orig, sc;
    double len;
    double max_rhs, norm, d1, d2;

    if (idx < max) {

      len = 0.;

  // load the sides of the elements
      sID[0] = curr_s1[idx];
      sID[1] = curr_s2[idx];
      sID[2] = curr_s3[idx];

      for(int i = 0; i < 3; i++) {
          sc = d_schild1[sID[i]];
          if(sc > -1){
              orig = soriginal[spos[sc]];
              len+=slength[orig];
          }
          else{
              orig = soriginal[spos[ sID[i] ]];
              len+=slength[orig];
          }
      }

      norm = C[0][idx];
      d1 = abs(rhs[1][idx]) / pow(circles[idx], (order+1.)/2.) / len  / norm;
      detector[idx] = d1;

      if(detector[idx] > 1)
          flag[idx] = 1;
      else
          flag[idx] = 0;
    }



}




void write_limit(int curr_num_elem) {
    double *limit;
    int *limit2;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    limit = (double *) malloc(curr_num_elem * sizeof(double));
    limit2 = (int *) malloc(curr_num_elem * sizeof(int));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // evaluate and write to file
      cudaMemcpy(limit, d_detector, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

      out_file  = fopen("output/limit1.pos" , "w");
      fprintf(out_file, "View \"U0\" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit[i] > 1., limit[i] > 1., limit[i] > 1.);
      }
      fprintf(out_file,"};");
      fclose(out_file);


      out_file  = fopen("output/limit2.pos" , "w");
      fprintf(out_file, "View \"U0 \" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.15lf,%.15lf,%.15lf};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit[i] , limit[i] , limit[i] );
      }
      fprintf(out_file,"};");
      fclose(out_file);


      cudaMemcpy(limit2, d_limit, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
      out_file  = fopen("output/limit3.pos" , "w");
      fprintf(out_file, "View \"U0 \" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit2[i] , limit2[i] , limit2[i] );
      }
      fprintf(out_file,"};");
      fclose(out_file);


    free(limit);
    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}

void write_limit(int curr_num_elem, int num) {
    double *limit;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    limit = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // evaluate and write to file
      cudaMemcpy(limit, d_detector, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

      // out_file  = fopen("output/limit1.pos" , "w");
      // fprintf(out_file, "View \"U0\" {\n");
      // for (i = 0; i < curr_num_elem; i++) {
      //     fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
      //                            V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
      //                            limit[i] > 0.1, limit[i] > 0.1, limit[i] > 0.1);
      // }
      // fprintf(out_file,"};");
      // fclose(out_file);
      char name[100];
      sprintf(name, "output/limit%i.pos", num);

      out_file  = fopen(name , "w");
      fprintf(out_file, "View \"U0 \" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.15lf,%.15lf,%.15lf};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit[i] , limit[i] , limit[i] );
      }
      fprintf(out_file,"};");
      fclose(out_file);



    free(limit);
    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}

void dump_limit(int curr_num_elem, int num) {
    double *limit;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    limit = (double *) malloc(curr_num_elem * sizeof(int));
    cudaMemcpy(limit, d_limit, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);

    sprintf(out_filename, "output/dump/limit-%i.bin", num);
    out_file  = fopen(out_filename , "wb");
    fwrite(limit, sizeof(int), curr_num_elem, out_file);
    fclose(out_file);

    free(limit);
}








int continue_while(double t, double endtime,
                   double timestep, double total_timesteps,
                   int refine, int refinecycles, 
                   int convergence, double conv, double tol)
{
  if(refine == 1)
    return t < endtime && timestep < total_timesteps;
  else if(refinecycles > -1)
    return (timestep < total_timesteps) || (t < endtime);
  else if(endtime > 0 && total_timesteps == -1)
    return (t < endtime);
  else if ( total_timesteps > -1  && endtime < 0)
    return (timestep < total_timesteps);
  else if (convergence ){
    if(endtime > 0)
      return conv > tol && t < endtime;
    if(endtime < 0)
      return conv > tol;
    }
}
double rk6(int local_num_elem, int local_num_sides, 
               int local_n, int local_n_p,
               double endtime, int total_timesteps, 
               int verbose, int convergence, int video, double tol, double tprev, int benchmark,
               int refine,int num, int refinecycles,
               int *num_color) {  
    int n_threads = 256;
    int vidnum = 0;
    double dt, t;
    double conv, max_conv;
    double max_dt, max_lambda, min_ratio, min_h; 
    FILE *out_file;


    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
    int n_blocks_rk    = (local_num_elem / n_threads) 
                       + ((local_num_elem % n_threads) ? 1 : 0);
    int n_blocks_conv    = (local_n_p*local_N*local_num_elem / n_threads) 
                       + ((local_n_p*local_N*local_num_elem % n_threads) ? 1 : 0);

    t = tprev;
    int timestep = 0;
    double CFL = (11.)*(1.+4./49.);
    conv = 1.;
    double limiting_time = 0., start, end;

    if(convergence){
        out_file  = fopen("output/conv.txt" , "w");
    }

      cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_circles, d_max, local_num_elem);
      cudaMemcpy(&min_h, d_max, sizeof(double), cudaMemcpyDeviceToHost);
      if(verbose)
        printf("\nminimum h is: %e\n", min_h);

    while(continue_while(t, endtime,
                         timestep, total_timesteps,
                         refine, refinecycles,
                         convergence, conv, tol) ){
        eval_global_lambda<<<n_blocks_elem, n_threads>>>(d_c, d_lambda, 
                                                         d_curr_V1x, d_curr_V1y,
                                                         d_curr_V2x, d_curr_V2y,
                                                         d_curr_V3x, d_curr_V3y,
                                                         t);

        cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max_lambda, local_num_elem);
        cudaMemcpy(&max_lambda, d_max_lambda, sizeof(double), cudaMemcpyDeviceToHost);

        divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, d_circles, d_lambda, local_num_elem); 
        cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_min_ratio, local_num_elem);
        cudaMemcpy(&min_ratio, d_min_ratio, sizeof(double), cudaMemcpyDeviceToHost);
        dt =  min_ratio / CFL; 

        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime  && endtime > 0) {
            dt = endtime - t;
        } 


        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


// F2 = func(t+h/4    , y_0+(h/4)*F1);
        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  1./6., dt, d_k1, d_curr_J); // evaluate intermediate solution 1


        // stage 2
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k2, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k2, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
// F3 = func(t+h/4    , y_0+(h/8)*F1+(h/8)*F2);
        intermediate2<<<n_blocks_rk, n_threads>>>(d_temp, d_c,   4./75., 16./75., d_k1,d_k2, dt,d_curr_J); // evaluate intermediate solution 2


        // stage 3
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k3, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k3, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }

// F4 = func(t+h/2    , y_0-(h/2)*F2+h*F3);
        intermediate3<<<n_blocks_rk, n_threads>>>(d_temp, d_c, 5./6., -8./3., 5./2., d_k1,d_k2,d_k3, dt,d_curr_J); // evaluate intermediate solution 2

        // stage 4
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k4, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k4, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        // F5 = func(t+(h*3/4), y_0+(h*3/16)*F1+(h*9/16)*F4);

        intermediate4<<<n_blocks_rk, n_threads>>>(d_temp, d_c, -165./64., 55./6., -425./64., 85./96., d_k1,d_k2,d_k3,d_k4, dt,d_curr_J); // evaluate intermediate solution 2
        
        // stage 5
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k5, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k5, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
// F6 = func(t+h      , y_0-(h*3/7)*F1+(h*2/7)*F2+(h*12/7)*F3-(h*12/7)*F4+(h*8/7)*F5);
        intermediate5<<<n_blocks_rk, n_threads>>>(d_temp, d_c, 12./5., -8., 4015./612., -11./36., 88./255.,  d_k1,d_k2,d_k3,d_k4,d_k5, dt,d_curr_J); // evaluate intermediate solution 2
        

        // stage 6
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k6, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k6, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        // y = y_0 + (h/90)*(7*F1+32*F3+12*F4+32*F5+7*F6);
        intermediate6<<<n_blocks_rk, n_threads>>>(d_temp, d_c, -8263./15000., 124./75., -643./680., -81./250., 2484./10625., 0., d_k1,d_k2,d_k3,d_k4,d_k5,d_k6, dt,d_curr_J); // evaluate intermediate solution 2

        // stage 7
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k7, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k7, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


        intermediate7<<<n_blocks_rk, n_threads>>>(d_temp, d_c, 3501./1720., -300./43., 297275./52632., -319./2322., 24068./84065., 0., 3850./26703., d_k1,d_k2,d_k3,d_k4,d_k5,d_k6,d_k7, dt,d_curr_J); // evaluate intermediate solution 2

        // stage 8
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k8, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k8, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }

        intermediate8<<<n_blocks_rk, n_threads>>>(d_c, d_c, 3./40., 0., 875./2244., 23./72., 264./1955., 0., 125./11592., 43./616., d_k1,d_k2,d_k3,d_k4,d_k5,d_k6,d_k7,d_k8, dt,d_curr_J); // evaluate intermediate solution 2

        if (verbose == 1 && !convergence) {
            printf("\r(%i) t = %lf, max lambda %lf", timestep, t, max_lambda);
        } else if (convergence == 1)  {
            printf("\r(%i) t = %lf, convergence = %e", timestep, t, conv);
        }
        else {
            printf("\r(%i) t = %lf", timestep, t);
        } 

        if (video > 0) {
            if (timestep % video == 0) {
                write_U(local_num_elem, vidnum, total_timesteps, t);
                write_Um(local_num_elem, total_timesteps, total_timesteps);
                // write_physical(local_num_elem, total_timesteps, total_timesteps);
                vidnum++;
            }
        }



        timestep++;
        t+=dt;

    }
    
    if(convergence){
      fclose(out_file);
    }

    // if (benchmark) {
    //     printf("\n Time spent in limiter as %lf\n", limiting_time);
    // }
    // write_C(local_num_elem, local_n_p);

    return t;
}
double rk5(int local_num_elem, int local_num_sides, 
               int local_n, int local_n_p,
               double endtime, int total_timesteps, 
               int verbose, int convergence, int video, double tol, double tprev, int benchmark,
               int refine,int num, int refinecycles,
               int *num_color) {  
    int n_threads = 256;
    int vidnum = 0;
    double dt, t;
    double conv, max_conv;
    double max_dt, max_lambda, min_ratio, min_h; 
    FILE *out_file;


    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
    int n_blocks_rk    = (local_num_elem / n_threads) 
                       + ((local_num_elem % n_threads) ? 1 : 0);
    int n_blocks_conv    = (local_n_p*local_N*local_num_elem / n_threads) 
                       + ((local_n_p*local_N*local_num_elem % n_threads) ? 1 : 0);

    t = tprev;
    int timestep = 0;
    double CFL = (9.)*(1.+4./36.);
    conv = 1.;
    double limiting_time = 0., start, end;

    if(convergence){
        out_file  = fopen("output/conv.txt" , "w");
    }

      cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_circles, d_max, local_num_elem);
      cudaMemcpy(&min_h, d_max, sizeof(double), cudaMemcpyDeviceToHost);
      if(verbose)
        printf("\nminimum h is: %e\n", min_h);

    while(continue_while(t, endtime,
                         timestep, total_timesteps,
                         refine, refinecycles,
                         convergence, conv, tol) ){
        eval_global_lambda<<<n_blocks_elem, n_threads>>>(d_c, d_lambda, 
                                                         d_curr_V1x, d_curr_V1y,
                                                         d_curr_V2x, d_curr_V2y,
                                                         d_curr_V3x, d_curr_V3y,
                                                         t);

        cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max_lambda, local_num_elem);
        cudaMemcpy(&max_lambda, d_max_lambda, sizeof(double), cudaMemcpyDeviceToHost);

        divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, d_circles, d_lambda, local_num_elem); 
        cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_min_ratio, local_num_elem);
        cudaMemcpy(&min_ratio, d_min_ratio, sizeof(double), cudaMemcpyDeviceToHost);
        dt =  0.9*min_ratio / CFL; 

        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime  && endtime > 0) {
            dt = endtime - t;
        } 


// F1 = func(t        , y_0          );
        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


// F2 = func(t+h/4    , y_0+(h/4)*F1);
        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  1./4., dt, d_k1, d_curr_J); // evaluate intermediate solution 1


        // stage 2
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k2, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt/4., local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k2, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt/4., csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
// F3 = func(t+h/4    , y_0+(h/8)*F1+(h/8)*F2);
        intermediate2<<<n_blocks_rk, n_threads>>>(d_temp, d_c,   1./8., 1./8., d_k1,d_k2, dt,d_curr_J); // evaluate intermediate solution 2


        // stage 3
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k3, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt/4., local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k3, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt / 4., csum, num_color[k]);
                  csum+=num_color[k];
            }
        }

// F4 = func(t+h/2    , y_0-(h/2)*F2+h*F3);
        intermediate3<<<n_blocks_rk, n_threads>>>(d_temp, d_c, 0., -1./2., 1., d_k1,d_k2,d_k3, dt,d_curr_J); // evaluate intermediate solution 2

        // stage 4
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k4, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt / 2., local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k4, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt / 2., csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        // F5 = func(t+(h*3/4), y_0+(h*3/16)*F1+(h*9/16)*F4);

        intermediate4<<<n_blocks_rk, n_threads>>>(d_temp, d_c, 3./16., 0., 0., 9./16., d_k1,d_k2,d_k3,d_k4, dt,d_curr_J); // evaluate intermediate solution 2
        
        // stage 5
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k5, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt * 3./4., local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k5, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt * 3./4., csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
// F6 = func(t+h      , y_0-(h*3/7)*F1+(h*2/7)*F2+(h*12/7)*F3-(h*12/7)*F4+(h*8/7)*F5);
        intermediate5<<<n_blocks_rk, n_threads>>>(d_temp, d_c, -3./7., 2./7., 12./7., -12./7., 8./7.,  d_k1,d_k2,d_k3,d_k4,d_k5, dt,d_curr_J); // evaluate intermediate solution 2
        

        // stage 6
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k6, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k6, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        // y = y_0 + (h/90)*(7*F1+32*F3+12*F4+32*F5+7*F6);
        intermediate6<<<n_blocks_rk, n_threads>>>(d_c, d_c, 7./90., 0., 32./90., 12./90., 32./90., 7./90., d_k1,d_k2,d_k3,d_k4,d_k5,d_k6, dt,d_curr_J); // evaluate intermediate solution 2
        





        if (verbose == 1 && !convergence) {
            printf("\r(%i) t = %lf, max lambda %lf", timestep, t, max_lambda);
        } else if (convergence == 1)  {
            printf("\r(%i) t = %lf, convergence = %e", timestep, t, conv);
        }
        else {
            printf("\r(%i) t = %lf", timestep, t);
        } 

        if (video > 0) {
            if (timestep % video == 0) {
                write_U(local_num_elem, vidnum, total_timesteps,t);
                write_Um(local_num_elem, total_timesteps, total_timesteps);
                // write_physical(local_num_elem, total_timesteps, total_timesteps);
                vidnum++;
            }
        }



        timestep++;
        t+=dt;

    }
    
    if(convergence){
      fclose(out_file);
    }

    // if (benchmark) {
    //     printf("\n Time spent in limiter as %lf\n", limiting_time);
    // }
    // write_C(local_num_elem, local_n_p);

    return t;
}


double rk4(int local_num_elem, int local_num_sides, 
               int local_n, int local_n_p,
               double endtime, int total_timesteps, 
               int verbose, int convergence, int video, double tol, double tprev, int benchmark,
               int refine,int num, int refinecycles,
               int *num_color) {  
    int n_threads = 256;
    int vidnum = 0;
    double dt, t;
    double conv, max_conv;
    double max_dt, max_lambda, min_ratio, min_h; 
    FILE *out_file;


    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
    int n_blocks_rk    = (local_num_elem / n_threads) 
                       + ((local_num_elem % n_threads) ? 1 : 0);
    int n_blocks_conv    = (local_n_p*local_N*local_num_elem / n_threads) 
                       + ((local_n_p*local_N*local_num_elem % n_threads) ? 1 : 0);

    t = tprev;
    int timestep = 0;
    double CFL = (7.)*(1.+4./25.);
    conv = 1.;
    double limiting_time = 0., start, end;

    if(convergence){
        out_file  = fopen("output/conv.txt" , "w");
    }

      cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_circles, d_max, local_num_elem);
      cudaMemcpy(&min_h, d_max, sizeof(double), cudaMemcpyDeviceToHost);
      if(verbose)
        printf("\nminimum h is: %e\n", min_h);

    while(continue_while(t, endtime,
                         timestep, total_timesteps,
                         refine, refinecycles,
                         convergence, conv, tol) ){
        eval_global_lambda<<<n_blocks_elem, n_threads>>>(d_c, d_lambda, 
                                                         d_curr_V1x, d_curr_V1y,
                                                         d_curr_V2x, d_curr_V2y,
                                                         d_curr_V3x, d_curr_V3y,
                                                         t);

        cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max_lambda, local_num_elem);
        cudaMemcpy(&max_lambda, d_max_lambda, sizeof(double), cudaMemcpyDeviceToHost);

        divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, d_circles, d_lambda, local_num_elem); 
        cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_min_ratio, local_num_elem);
        cudaMemcpy(&min_ratio, d_min_ratio, sizeof(double), cudaMemcpyDeviceToHost);
        dt =  0.9*min_ratio / CFL; 

        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime  && endtime > 0) {
            dt = endtime - t;
        } 

        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  0.5, dt, d_k1, d_curr_J); // evaluate intermediate solution 1


        // stage 2
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k2, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + 0.5*dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k2, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ 0.5*dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  0.5, dt, d_k2, d_curr_J); // evaluate intermediate solution 2


        // stage 3
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k3, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + 0.5*dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k3, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ 0.5*dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }

        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  1.0 , dt, d_k3, d_curr_J); // evaluate intermediate solution 3

        // stage 4
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k4, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k4, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        intermediate4<<<n_blocks_rk, n_threads>>>(d_c, d_c, 1./6., 1./3., 1./3., 1./6., d_k1, d_k2, d_k3,d_k4,dt, d_curr_J); // k1






        if (verbose == 1 && !convergence) {
            printf("\r(%i) t = %lf, max lambda %lf", timestep, t, max_lambda);
        } else if (convergence == 1)  {
            printf("\r(%i) t = %lf, convergence = %e", timestep, t, conv);
        }
        else {
            printf("\r(%i) t = %lf", timestep, t);
        } 

        if (video > 0) {
            if (timestep % video == 0) {
                write_U(local_num_elem, vidnum, total_timesteps,t);
                write_Um(local_num_elem, total_timesteps, total_timesteps);
                // write_physical(local_num_elem, total_timesteps, total_timesteps);
                vidnum++;
            }
        }



        timestep++;
        t+=dt;

    }
    
    if(convergence){
      fclose(out_file);
    }

    // if (benchmark) {
    //     printf("\n Time spent in limiter as %lf\n", limiting_time);
    // }
    // write_C(local_num_elem, local_n_p);

    return t;
}


double rk3_ssp(int local_num_elem, int local_num_sides, 
               int local_n, int local_n_p,
               double endtime, int total_timesteps, 
               int verbose, int convergence, int video, double tol, double tprev, int benchmark,
               int refine,int num, int refinecycles,
               int *num_color) {  
    int n_threads = 256;
    int vidnum = 0;
    double dt, t;
    double conv, max_conv;
    double max_dt, max_lambda, min_ratio, min_h; 
    FILE *out_file;


    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
    int n_blocks_rk    = (local_num_elem / n_threads) 
                       + ((local_num_elem % n_threads) ? 1 : 0);
    int n_blocks_conv    = (local_n_p*local_N*local_num_elem / n_threads) 
                       + ((local_n_p*local_N*local_num_elem % n_threads) ? 1 : 0);

    t = tprev;
    int timestep = 0;
    double CFL = (5.)*(1.+4./16.);
    conv = 1.;
    double limiting_time = 0., start, end;

    if(convergence){
        out_file  = fopen("output/conv.txt" , "w");
    }

      cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_circles, d_max, local_num_elem);
      cudaMemcpy(&min_h, d_max, sizeof(double), cudaMemcpyDeviceToHost);
      if(verbose)
        printf("\nminimum h is: %e\n", min_h);

    while(continue_while(t, endtime,
                         timestep, total_timesteps,
                         refine, refinecycles,
                         convergence, conv, tol) ){
        eval_global_lambda<<<n_blocks_elem, n_threads>>>(d_c, d_lambda, 
                                                         d_curr_V1x, d_curr_V1y,
                                                         d_curr_V2x, d_curr_V2y,
                                                         d_curr_V3x, d_curr_V3y,
                                                         t);

        cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max_lambda, local_num_elem);
        cudaMemcpy(&max_lambda, d_max_lambda, sizeof(double), cudaMemcpyDeviceToHost);

        divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, d_circles, d_lambda, local_num_elem); 
        cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_min_ratio, local_num_elem);
        cudaMemcpy(&min_ratio, d_min_ratio, sizeof(double), cudaMemcpyDeviceToHost);
        dt =  0.9*min_ratio / CFL; 

        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime  && endtime > 0) {
            dt = endtime - t;
        } 

        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  1.0, dt, d_k1, d_curr_J); // evaluate intermediate solution 1


        // stage 2
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k2, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k2, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
        intermediate2<<<n_blocks_elem, n_threads>>>(d_temp, d_c, d_temp, d_k2, dt, d_curr_J);


        // stage 3
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k3, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt/2., local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k3, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt/2., csum, num_color[k]);
                  csum+=num_color[k];
            }
        }

        rk3_final<<<n_blocks_elem, n_threads>>>(d_c, d_temp, d_k3, dt, d_curr_J);






        if (verbose == 1 && !convergence) {
            printf("\r(%i) t = %lf, max lambda %lf", timestep, t, max_lambda);
        } else if (convergence == 1)  {
            printf("\r(%i) t = %lf, convergence = %e", timestep, t, conv);
        }
        else {
            printf("\r(%i) t = %lf", timestep, t);
        } 

        if (video > 0) {
            if (timestep % video == 0) {
                write_U(local_num_elem, vidnum, total_timesteps,t);
                write_Um(local_num_elem, total_timesteps, total_timesteps);
                // write_physical(local_num_elem, total_timesteps, total_timesteps);
                vidnum++;
            }
        }



        timestep++;
        t+=dt;

    }
    
    if(convergence){
      fclose(out_file);
    }

    // if (benchmark) {
    //     printf("\n Time spent in limiter as %lf\n", limiting_time);
    // }
    // write_C(local_num_elem, local_n_p);

    return t;
}


double rk2_ssp(int local_num_elem, int local_num_sides, 
               int local_n, int local_n_p,
               double endtime, int total_timesteps, 
               int verbose, int convergence, int video, double tol, double tprev, int benchmark,
               int refine,int num, int refinecycles,
               int *num_color) {  
    int n_threads = 256;
    int vidnum = 0;
    double dt, t;
    double conv, max_conv;
    double max_dt, max_lambda, min_ratio, min_h; 
    FILE *out_file;


    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
    int n_blocks_rk    = (local_num_elem / n_threads) 
                       + ((local_num_elem % n_threads) ? 1 : 0);
    int n_blocks_conv    = (local_n_p*local_N*local_num_elem / n_threads) 
                       + ((local_n_p*local_N*local_num_elem % n_threads) ? 1 : 0);

    t = tprev;
    int timestep = 0;
    // double CFL = 6./sqrt(2.);
    // double CFL = 3.;
    double CFL = 3.*(1.+4./9.);
    conv = 1.;
    double limiting_time = 0., start, end;

    if(convergence){
        out_file  = fopen("output/conv.txt" , "w");
    }

      cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_circles, d_max, local_num_elem);
      cudaMemcpy(&min_h, d_max, sizeof(double), cudaMemcpyDeviceToHost);



    while(continue_while(t, endtime,
                         timestep, total_timesteps,
                         refine, refinecycles,
                         convergence, conv, tol) ){
        eval_global_lambda<<<n_blocks_elem, n_threads>>>(d_c, d_lambda, 
                                                         d_curr_V1x, d_curr_V1y,
                                                         d_curr_V2x, d_curr_V2y,
                                                         d_curr_V3x, d_curr_V3y,
                                                         t);

        cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max_lambda, local_num_elem);
        cudaMemcpy(&max_lambda, d_max_lambda, sizeof(double), cudaMemcpyDeviceToHost);

        divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, d_circles, d_lambda, local_num_elem); 
        cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_min_ratio, local_num_elem);
        cudaMemcpy(&min_ratio, d_min_ratio, sizeof(double), cudaMemcpyDeviceToHost);
        dt =  0.9*min_ratio / CFL; 

        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime  && endtime > 0) {
            dt = endtime - t;
        } 


        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


        intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  1.0, dt, d_k1, d_curr_J); // evaluate intermediate solution 1
        if (limiter) {
          if (benchmark) {
              cudaThreadSynchronize();
              start = clock();
          }
          // if(t + dt + 1e-10 > endtime )
          //   write_activated(d_temp, local_num_elem);
          limit(d_temp, local_num_elem);

          if (benchmark) {
              cudaThreadSynchronize();
              end = clock();
              limiting_time += ((double)(end - start)) / CLOCKS_PER_SEC ;
          }
        } 


        // stage 2
        eval_volume<<<n_blocks_elem, n_threads>>>
                              (d_temp, d_k2, 
                              d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                              d_curr_V1x, d_curr_V1y,
                              d_curr_V2x, d_curr_V2y,
                              d_curr_V3x, d_curr_V3y,
                              d_curr_J, t + dt, local_num_elem);

        csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_temp, d_k2, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t+ dt, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }

        rk2_ssp_final<<<n_blocks_rk, n_threads>>>(d_c, d_c, d_temp, d_k2, 0.5, 0.5,0.5,dt,d_curr_J);
 
        if (limiter) {
          if (benchmark) {
              cudaThreadSynchronize();
              start = clock();
          }    

          limit(d_c, local_num_elem);

          if (benchmark) {
              cudaThreadSynchronize();
              end = clock();
              limiting_time += ((double)(end - start)) / CLOCKS_PER_SEC ;
          }
        } 


        if (verbose == 1 && !convergence) {
            printf("\r(%i) t = %lf, max lambda %lf", timestep, t, max_lambda);
        } else if (convergence == 1)  {
            printf("\r(%i) t = %lf, convergence = %e", timestep, t, conv);
        }
        else {
            printf("\r(%i) t = %lf", timestep, t);
        } 

        // check the convergence
        // if (convergence && timestep > 0) {
        //   check_convergence<<<get_blocks(local_num_elem,n_threads), n_threads>>>(d_c_prev, d_c, local_num_elem);
        //   conv = -1;

        //   for(int n = 0; n < local_N; n++){
        //     for(int i = 0; i < local_n_p; i++){
        //       cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, h_c_prev[n*local_n_p + i], d_max, local_num_elem);
        //       cudaMemcpy(&max_conv, d_max, sizeof(double), cudaMemcpyDeviceToHost);
        //       // printf("max is %lf\n", max_conv);
        //       conv = ( conv < max_conv ) ? max_conv : conv;
        //     }
        //   }
           
        //   if(timestep % 100 == 0)
        //     fprintf(out_file, "%i %e\n", timestep, conv);

        //   for(int n = 0; n < local_N; n++)
        //     for(int i = 0; i < local_n_p; i++)
        //       cudaMemcpy(h_c_prev[n*local_n_p + i], h_c[n*local_n_p + i], local_num_elem * sizeof(double), cudaMemcpyDeviceToDevice);
        // }

        // if (video > 0) {
        //     if ((refine == -1 && timestep % video == 0 )|| (refine == 1 && refine % video == 0)) {
        //         write_U(local_num_elem, vidnum, total_timesteps,t);
        //         // write_Um(local_num_elem, total_timesteps, total_timesteps);
        //         // write_physical(local_num_elem, total_timesteps, total_timesteps);
        //         vidnum++;
        //     }
        // }

      // print_angular_momentum(d_a_m,d_c,
      //                        d_curr_V1x, d_curr_V1y,
      //                        d_curr_V2x, d_curr_V2y,
      //                        d_curr_V3x, d_curr_V3y,
      //                        d_curr_J, 
      //                        local_num_elem);

        timestep++;
        t+=dt;

    }
    
    if(convergence){
      fclose(out_file);
    }

    // if (benchmark) {
    //     printf("\n Time spent in limiter as %lf\n", limiting_time);
    // }
    // write_C(local_num_elem, local_n_p);

    return t;
}





double rk1(int local_num_elem, int local_num_sides, 
               int local_n, int local_n_p,
               double endtime, int total_timesteps, 
               int verbose, int convergence, int video, double tol, double tprev,
               int refine,int num, int refinecycles,
               int *num_color) { 
    int n_threads = 256;
    int vidnum = 0;
    double dt, t;
    double conv, max_conv;
    double max_dt, max_lambda, min_ratio, min_h; 
    double CFL = 4.;
    double max_abs;
    FILE *out_file;



    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
    int n_blocks_rk    = (local_num_elem / n_threads) 
                       + ((local_num_elem % n_threads) ? 1 : 0);
    int n_blocks_conv    = (local_n_p*local_N*local_num_elem / n_threads) 
                       + ((local_n_p*local_N*local_num_elem % n_threads) ? 1 : 0);

    t = tprev;
    int timestep = 0;
    conv = 1.;

    if(convergence){
        out_file  = fopen("output/conv.txt" , "w");
    }


    // if(limiter == BJ || limiter == BJ_N || limiter == BJ_QUAD || limiter == BJ_QUAD_N || limiter == BJ_R || limiter == BJ_QUAD_R || limiter == MODAL|| limiter == NO_LIMITER){
      cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, get_h(), d_max, local_num_elem);
      cudaMemcpy(&min_h, d_max, sizeof(double), cudaMemcpyDeviceToHost);
      printf("\nminimum h is: %e\n", min_h);
    // }

    // printf("\nconvergence %i conv %lf bool %i, TOL %lf, total_timesteps %i\n", convergence, conv, continue_while(t, endtime,
    //                      timestep, total_timesteps,
    //                      refine, refinecycles,
    //                      convergence, conv, tol), tol, total_timesteps); 

    while(continue_while(t, endtime,
                         timestep, total_timesteps,
                         refine, refinecycles,
                         convergence, conv, tol) ){
        eval_global_lambda<<<n_blocks_elem, n_threads>>>(d_c, d_lambda, 
                                                         d_curr_V1x, d_curr_V1y,
                                                         d_curr_V2x, d_curr_V2y,
                                                         d_curr_V3x, d_curr_V3y,
                                                         t);

        cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max_lambda, local_num_elem);
        cudaMemcpy(&max_lambda, d_max_lambda, sizeof(double), cudaMemcpyDeviceToHost);

        // if(limiter == BJ || limiter == BJ_N || limiter == BJ_QUAD || limiter == BJ_QUAD_N || limiter == BJ_R || limiter == BJ_QUAD_R || limiter == MODAL){ 
          divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, get_h(), d_lambda, local_num_elem); 
          cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_min_ratio, local_num_elem);
          cudaMemcpy(&min_ratio, d_min_ratio, sizeof(double), cudaMemcpyDeviceToHost);
          dt = min_ratio / CFL;  
        // }
        // else{
        //   divide<<<get_blocks(local_num_elem, n_threads), n_threads>>>(d_lambda, d_circles, local_num_elem); 
        //   cub::DeviceReduce::Min(d_temp_storage_reduce, temp_storage_bytes_reduce, d_lambda, d_max, local_num_elem);
        //   cudaMemcpy(&max_dt, d_max, sizeof(double), cudaMemcpyDeviceToHost);
        //   dt = 0.9 * max_dt/ (2. * local_n + 1.);  
        // }


        // panic
        if (isnan(dt)) {
            printf("Error: dt is NaN. Dumping...\n");
            return t;
        }

        // get next timestep
        if (t + dt > endtime  && endtime > 0) {
            dt = endtime - t;
        } 

        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }


        // if(verbose){
        //   intermediate1<<<n_blocks_rk, n_threads>>>(d_temp, d_c,  1.0, dt, d_k1, d_curr_J); // evaluate intermediate solution 1
        //   check_lmp(d_temp, d_c, timestep, local_num_elem);
        // }

        intermediate1<<<n_blocks_rk, n_threads>>>(d_c, d_c,  1.0, dt, d_k1, d_curr_J); // evaluate intermediate solution 1
        if (limiter) {
          limit(d_c, local_num_elem);
        } 

        timestep++;
        t+=dt;

        if (video > 0) {
            if (timestep % video == 0) {
                write_U(local_num_elem, vidnum, total_timesteps,t);
                // write_Um(local_num_elem, total_timesteps, total_timesteps);
                // write_physical(local_num_elem, total_timesteps, total_timesteps);
                vidnum++;
            }
        }

        if (convergence) {
          abs<<<n_blocks_rk, n_threads>>>(d_abs, d_k1, d_curr_J, dt);
          cub::DeviceReduce::Max(d_temp_storage_reduce, temp_storage_bytes_reduce, d_abs, d_max_abs, local_num_elem);
          cudaMemcpy(&max_abs, d_max_abs, sizeof(double), cudaMemcpyDeviceToHost);
          printf("\r(%i) t = %lf, rhs = %lf max lambda %lf", timestep, t, max_abs, max_lambda);
          if(timestep % 100 == 0)
            fprintf(out_file, "%i %e\n", timestep, max_abs);
        }
        else{
          printf("\r(%i) t = %lf, max lambda %lf", timestep, t, max_lambda);
        }
    }

    if(convergence){
      fclose(out_file);
    }
    
    return t;
}





double eval_rhs(int local_num_elem, int local_num_sides, double t, int *num_color) { 
    int n_threads = 256;
    int n_blocks_elem  = (local_num_elem  / n_threads) 
                       + ((local_num_elem  % n_threads) ? 1 : 0);
    int n_blocks_sides = (local_num_sides / n_threads) 
                       + ((local_num_sides % n_threads) ? 1 : 0);
     double max_lambda;
        // stage 1
        eval_volume<<<n_blocks_elem, n_threads>>>(d_c, d_k1, 
                                                  d_curr_xr, d_curr_yr, d_curr_xs, d_curr_ys,
                                                  d_curr_V1x, d_curr_V1y,
                                                  d_curr_V2x, d_curr_V2y,
                                                  d_curr_V3x, d_curr_V3y,
                                                  d_curr_J, t, local_num_elem);

        int csum = 0;
        for(int k = 0; k < 6; k++) {
            if(num_color[k] > 0){
                  eval_surface<<<get_blocks(num_color[k], n_threads), n_threads>>>
                               (d_c, d_k1, 
                                d_s_length , 
                                d_curr_V1x, d_curr_V1y ,
                                d_curr_V2x, d_curr_V2y ,
                                d_curr_V3x, d_curr_V3y ,
                                d_curr_side, d_curr_originalside,
                                d_curr_slevel,
                                d_left_elem , d_right_elem ,
                                d_left_side_number , d_right_side_number , max_lambda,
                                d_Nx , d_Ny , t, csum, num_color[k]);
                  csum+=num_color[k];
            }
        }
}

