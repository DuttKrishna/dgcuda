void init_plot(double *in_v1x, double *in_v2x, double *in_v3x, 
               double *in_v1y, double *in_v2y, double *in_v3y,
               double *in_bv, double *in_bm,
               double **in_c,
               int in_N, int in_n_p);
void allocate_plot(memoryCounters *timesteppingCounter, int emesh_MAX);
void write_U(int curr_num_elem, int num, int total_timesteps);
void write_U(int curr_num_elem, int num, int total_timesteps, double t);
void write_Um(int curr_num_elem, int num, int total_timesteps);
void write_physical(int curr_num_elem, int num, int total_timesteps);
void write_U_p2(int curr_num_elem, int num, int total_timesteps) ;
void write_C(int local_num_elem, int local_n_p) ;
void plot_error(double t, int curr_num_elem) ;
void plot_exact(double t, int curr_num_elem) ;
