#include "../advection.cu"

/* test1.cu
 *
 * simple flow with exact boundary conditions
 *
 */

#define PI 3.14159265358979323

int limiter = NO_LIMITER;   // no limiter   

/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

 // initial condition function
 // *
 // * returns the value of the intial condition at point x,y
 

__device__ void U0(double *U, double x, double y) {
    // U[0] =  - 0.5 * x - 0.5 * y;
    // U[0] =  - 0.5 * x*x - 0.5 * y*y;
        // U[0] = -0.5 * x*x*x - 0.5 * y * y * y; 
        U[0] = -0.5 * x*x*x*x - 0.5 * y * y * y* y; 


    // U[0] = sinpi(x) * sinpi(y);  
    // U[0] = y;
    // no3 vs all
    // double x0=-0.25, y0= 0. ; 
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1. ;
    // } else {
    //     U[0] = 0.;  
    // }


    // // c2 rot
    // double x0, y0 ; 
    // double x_rot, y_rot ; 

    // x_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // y_rot =  -sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x0 = -0.25/sqrt(2.);
    // y0 = 0.25/sqrt(2.);
    // if (x_rot - x0< .25 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }



    // comparing no3 and all
    // U[0] = 0.;
    // double x0 = 0.25, y0 = 0.25;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0))  ;
    // if(r <= 0.25){ // hill
    //     U[0] = cospi(2*r) * cospi(2*r);
    // }

    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = 0.;
    // r = 0.15;
    // U[0] = exp(-(pow(x - x0,2) + pow(y - y0,2))/(2.*r*r)    );
 

    // advecting hill
    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = -0.25;
    // r = 0.15;
    // U[0] = 5*exp(-(pow(x - x0,2) + pow(y - y0,2))/(2.*r*r)    );
 

     // example rotating shapes
    // U[0] = 0.;
    // double x0 = 0.5, y0 = 0.;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0))  ;
    // if(r <= 0.25){ // cone
    //     U[0] = cospi(2*r) * cospi(2*r);
    // }

    // x0 = 0.35; y0 = 0.;
    // r =  fmax( fabs(x-x0), fabs(y-y0));
    // if(r <= 0.25){ // square
    //     U[0] = 1. ;
    // }


    // U[0] = 0.;
    // double x0 = 0.5, y0 = 0.;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0)) /0.25 ;
    // if(r <= 1.-1e-10){
    //   U[0] = exp(- 0.1 / (1-r*r));
    // }

    // c2 rot
    // double x0, y0 ; 
    // double x_rot, y_rot ; 

    // x_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // y_rot =  -sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x0 = -0.25;
    // y0 = 0.;
    // if (x_rot - x0< .25 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }


    //example 83
    // double x0=0., y0=0. ; 
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }

    // experiment 85
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.15, r ;
    // double xp = (x+1)/2., yp = (y+1.)/2.;
    // x0 = 0.5; y0 = 0.75;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(xp-x0) >= 0.025 || yp >=0.85)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.5; y0 = 0.25;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }



    // experiment 85
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.35, r ;

    // x0 = -0.45; y0 = 0.;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }
    // if(0.1 < x && x < 0.6 && -0.25 < y && y < 0.25)
    //     U[0] = 1.;


    // x0 = 0.25; y0 = 0.5;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // hump
    //     U[0] = (1+cospi(r))/4.;
    // }




    // double x0, y0 ,r; 
    // x0 = 0.;
    // y0 = 0.;
    // r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0))  ;
    // U[0] = 5*exp(-pow(r,2)/(0.01)    );


    // double x0, y0 ,r; 
    // x0 = -0.25; 
    // y0 = -0.25;
    // r = 0.25;
    // if(sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0))/r < 1.)
    //   U[0] = exp( - 1. / (1.-sqrt((x-x0)*(x-x0)+ (y-y0)*(y-y0))/r) ) + 1.;
    // else
    //   U[0] = 0.;

    // U[0] = 0.;
    // double x0 = 0.5, y0 = 0.;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0)) /0.25 ;
    // if(r <= 1.-1e-10){
    //   U[0] = exp(- .1 / (1-r*r));
    // }



    // U[0] = 0.;
    // double x0 = 0.5, y0 = 0.;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0)) /0.25 ;
    // if(r <= 1.-1e-10){
    //   U[0] = exp(- 1. / (1-r*r));
    // }


        // U[0] = 0;  
    // double r = sqrtf(x*x + y*y);
    // U[0]=cospi(r) + 2.*exp(-20*(PI*r - PI/2.)*(PI*r - PI/2.))*cospi(110*r)/10.;

    // if (x*x - y*y <= .1) { 
    //     U[0] = cospi(x*x +y*y);
    // } else {
    //     U[0] = -cospi(x*x +y*y);
    // }


        // U[0] = y;    
    // U[0] = -0.5*x-0.5*y; 
    // U[0] = sinpi(x) ; 
    // U[0] = sinpi(x) * sinpi(y); 

   // double r = sqrt(x*x + y*y);
   // double r0 = 0.75;
   //  if(0.2 < r && r < 0.4)
   //      U[0] = 1.;
   //  else if( 0.5 < r && r < 1)
   //      U[0] = 5*exp(-pow(r - r0,2)/(5*0.001)    );
   //  else
   //      U[0] = 0.;

        // U[0] = 10*y ;


    



}

/***********************
*
* INFLOW CONDITIONS
*
************************/
__device__ void get_velocity(double *A, double x, double y, double t) {
    // A[0] = -2*PI*y;
    // A[1] =  2*PI*x;

    A[0] = 1.; 
    A[1] = 1.;

    // A[0] = 1.; 
    // A[1] = 0.;

    // A[0] = 0.82903757;
    // A[1] = 0.55919290;
}

__device__ void U_inflow(double *U, double x, double y, double t) {

    // U[0] = 0;

    // double x0, y0, r;
    // x0 = 0.2;
    // y0 = 0.0;
    // r  = 0.15;
    // U[0] = 5*exp(-(pow(x*cospi(2*t) + y*sinpi(2*t) - x0,2) + 
    //                pow(-x*sinpi(2*t) + y*cospi(2*t) - y0,2))/(2*r*r));
    // U[0] = 1.;
    // U[0] = t;
    // U0(U, x, y); 
    // U[0] = sinpi(y/2.); 
    U_exact(U, x, y, t);

}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

__device__ void U_outflow(double *U, double x, double y, double t) {
    // there are no outflow boundaries in this problem 
    // U[0] = sinpi(y/2.); 
    U[0] = 0.; 
    U_exact(U, x, y, t);

}

/***********************
*
* REFLECTING CONDITIONS
*
************************/

__device__ void U_reflection(double *U_left, double *U_right, 
                             double x, double y, double t,
                             double nx, double ny) {
    // there are no reflecting boundaries in this problem
    U_right[0] = 0.; 
    U_exact(U_right, x, y, t);

}

/***********************
*
* EXACT SOLUTION
*
************************/

__device__ void U_exact(double *U, double x, double y, double t) {
    double A[2];
    get_velocity(A, x, y, t);
    U0(U, x - A[0]*t, y - A[1]*t);
    // U0(U, x , y );
    // double x0,y0,r;
    // x0 = 0.2;
    // y0 = 0.0;
    // r = 0.15;
    // U[0] = 5*exp(-(pow(x*cospi(2*t) + y*sinpi(2*t) - x0,2) + 
    //                pow(-x*sinpi(2*t) + y*cospi(2*t) - y0,2))/(2*r*r));

    // U0(U, x*cospi(2*t) + y*sinpi(2*t), -x*sinpi(2*t) + y*cospi(2*t));
}

/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

int main(int argc, char *argv[]) {
    run_dgcuda(argc, argv);
}