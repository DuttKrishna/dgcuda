// limiter options
#define NO_LIMITER 0
#define COCKBURN 1
#define BJ 2
#define MODAL 3
#define BJ_N 4
#define BJ_QUAD 5
#define BJ_QUAD_N 6
#define BJ_R 7 
#define BJ_QUAD_R 8
#define BJ_SHOCK 9
#define BJ_SECOND 10
#define MOMENT_AM 11

void allocate_limiter(memoryCounters *counter, int num_elem0, int num_sides0, int in_limiter_type);
void init_limiter(int curr_num_elem, 
			      double *basis_midpoint, double *basis_vertex, double *in_basis, double *in_weights, double *d_basis_side,
			      double *circles,
                  int *curr_s1, int *curr_s2, int *curr_s3,
                  int *left_elem, int *right_elem,
                  int *left_side_number, int *right_side_number,
                  int *in_spos, int *in_schild1, int *in_schild2,
                  double *V1x, double *V1y,
                  double *V2x, double *V2y,
                  double *V3x, double *V3y,
                  int in_N_eq, int in_N_poly, int in_n_quadrature, int in_n_quad1d,
                  char *limiter_filename);
void limit(double **C, int num_elem); 
void limit(double **C, int *d_flag, int num_elem);
void write_activated(double **C, int num_elem); 
void check_lmp(double **C, double **C_prev, int timestep, int num_elem);
double *get_h();

