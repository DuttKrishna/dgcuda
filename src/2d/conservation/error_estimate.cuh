




// gradient error estimate
__global__ void eval_gradient(double *error, double **c, 
                                    double *gxr, double *gxs,
                                    double *gyr, double *gys,
                                    double *gJ,
                                    int n_p, int n_quad,
                                    double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                                    int curr_num_elem); 

__global__ void eval_error_L1_higher_order(double **C, double *error,
                                           double *V1x, double *V1y,
                                           double *V2x, double *V2y,
                                           double *V3x, double *V3y,
                                           double *J,
                                           int n, double t,
                                           double *w, double *basis_higher_order,
                                           int n_quad_higher_order,
                                           int max) ;

__global__ void eval_error_L1_higher_order_m(double **C, double *error,
                                           double *V1x, double *V1y,
                                           double *V2x, double *V2y,
                                           double *V3x, double *V3y,
                                           double *J,
                                           int n, double t,
                                           double *w, double *basis_higher_order,
                                           int n_quad_higher_order,
                                           int n_p,
                                           int max);

void evaluate_error_l1(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *J,
                       int n, double t,
                       int max); 
void evaluate_error_l2(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *J,
                       int n, double t,
                       int max); 

void evaluate_error_l1_m(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *J,
                       int n, double t,
                       int max);
void evaluate_error_linfinity(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       int n, double t,
                       int max);

void evaluate_gradient(double *d_error_estimate, 
                       double **d_curr_c, 
                       double *d_curr_xr, double *d_curr_xs,
                       double *d_curr_yr, double *d_curr_ys,
                       double *d_curr_J,
                       int n_p, int n_quad,
                       double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                       double *d_circles,
                       int curr_num_elem);

void evaluate_error_rhs(double *d_error_estimate, 
                        double *d_length,
                        int *d_curr_s1, int *d_curr_s2, int *d_curr_s3,
                        int *d_spos, int *d_schild1, int *d_schild2,
                        int *d_soriginal,
                        int order,
                        int max);
void evaluate_max(double *d_error_estimate, 
                    double **d_curr_c, int curr_num_elem);
void evaluate_jumps(double *d_error_estimate, 
                    double **d_curr_c, double *d_J,
                    int *d_curr_side, int *d_original_side,
                    int *d_slevel,double *d_slength, 
                    int *d_left_elem, int *d_right_elem,
                    int *lsn, int *rsn,
                    int *num_color, int curr_num_elem, int curr_num_sides);

void init_error_estimate(int local_n_p, int degree1,int degree2, int estimate_power, double **in_h_k1, double *in_basis_vertex);
void allocate_error_estimate(memoryCounters *counter, int in_emesh_max);

void evaluate_gradient_trumpet(double *d_error_estimate, 
                       double **d_curr_c, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *d_curr_xr, double *d_curr_xs,
                       double *d_curr_yr, double *d_curr_ys,
                       double *d_curr_J,
                       int n_p, int n_quad,
                       double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                       double *d_circles,
                       int curr_num_elem);



void shock_detector(int *d_flag, double *d_error_estimate, double *d_circles, double order, int max) ;
void shock_detector(int *d_flag, double *detector, double *d_error_estimate, double *d_circles, double order, int max) ;

void write_limit(int curr_num_elem,
                 double *d_detector,
                 double *d_curr_V1x, double *d_curr_V1y,
                 double *d_curr_V2x, double *d_curr_V2y,
                 double *d_curr_V3x, double *d_curr_V3y) ;
