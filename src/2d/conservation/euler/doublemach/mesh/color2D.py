import time
import sys
import random


from sys import argv

def adjacent_faces(conflict_edge, left_elem, right_elem, elem_s1, elem_s2, elem_s3):
	goodside1 = elem_s1[left_elem]
	goodside2 = elem_s2[left_elem]

	if goodside1 == conflict_edge:
		goodside1 = elem_s3[left_elem]
	elif goodside2 == conflict_edge:
		goodside2 = elem_s3[left_elem]

	f1 = goodside1
	f2 = goodside2

	if right_elem > -1:
		goodside1 = elem_s1[right_elem]
		goodside2 = elem_s2[right_elem]

		if goodside1 == conflict_edge:
			goodside1 = elem_s3[right_elem]
		elif goodside2 == conflict_edge:
			goodside2 = elem_s3[right_elem]

		f3 = goodside1
		f4 = goodside2
	else:
		f3 = -1
		f4 = -1

	return (f1, f2, f3, f4)

def color(inFilename, outFilename, n_colors, order, renumber):
	inFile  = open(inFilename, "rb")
	outFile = open(outFilename, "wb")
	num_colors = int(n_colors)
	order = int(order)
	renumber = int(renumber)

	print "Reading file: %s..." % inFilename

	# the next line is the number of vertices
	num_elements = int(inFile.readline())

	elem_v1 = []
	elem_v2 = []
	elem_v3 = []
	elem_s1 = [0] * num_elements
	elem_s2 = [0] * num_elements
	elem_s3 = [0] * num_elements

	for i in xrange(num_elements):
		s = inFile.readline().split()
		elem_v1.append((float(s[0]), float(s[1])))
		elem_v2.append((float(s[2]), float(s[3])))
		elem_v3.append((float(s[4]), float(s[5])))
		elem_s1[i] = int(s[6])
		elem_s2[i] = int(s[7])
		elem_s3[i] = int(s[8])

	# next line is the number of elements
	num_sides = int(inFile.readline())
	side_v1 = []
	side_v2 = []
	left_elem = [0] * num_sides
	right_elem = [0] * num_sides
	left_side_number = [0] * num_sides
	right_side_number = [0] * num_sides
	for i in xrange(num_sides):

		s = inFile.readline().split()

		side_v1.append((float(s[0]), float(s[1])))
		side_v2.append((float(s[2]), float(s[3])))
		# store the index of the verticies
		left_elem[i] = int(s[4])
		right_elem[i] = int(s[5])
		left_side_number[i] = int(s[6])
		right_side_number[i] = int(s[7])

	print num_elements, "elements..."
	print num_sides, "num_sides..."
	##################################################
	# now that we've read in the verticies for the elements and sides,
	# we can begin creating our mesh
	##################################################


	print "Coloring edges"

	start = time.time()


	#naive graph coloring
	elem_flag = [1] * num_elements
	edge_color = [-1] * num_sides

	color_count = [0]*num_colors

	f = [-1]*4
	for edge in xrange(num_sides):
		f[0],f[1],f[2],f[3]= adjacent_faces(edge, left_elem[edge], right_elem[edge], elem_s1, elem_s2, elem_s3)
		color_hash = [0] * num_colors
		c1 = edge_color[f[0]]
		c2 = edge_color[f[1]]
		if c1 > -1:
			color_hash[c1]=color_hash[c1]+1
		if c2 > -1:
			color_hash[c2]=color_hash[c2]+1

		# this is not a boundary side
		if f[2]  > -1:
			# there are no boundary sides
			c3 = edge_color[f[2]]
			c4 = edge_color[f[3]]
			if c3 > -1:
				color_hash[c3]=color_hash[c3]+1
			if c4 > -1:
				color_hash[c4]=color_hash[c4]+1


		if 0 in color_hash:
			indices = [i for i, x in enumerate(color_hash) if x == 0]
			edge_color[edge] = random.choice(indices)




	# sanity check
	conflict_edge = [-1]*edge_color.count(-1)
	loopID = [-1]*edge_color.count(-1)

	num_conflicts = 0
	for i in xrange(num_sides):
		if edge_color[i] == -1:
			conflict_edge[num_conflicts] = i
			loopID[num_conflicts] = i
			num_conflicts += 1


	for i in xrange(num_colors):
		print edge_color.count(i)

	print 'number of conflicts ',num_conflicts

	prev_edge = [-1]*num_conflicts



	while len(conflict_edge) > 0:
		ce = conflict_edge[-1]
		f = [-1]*4
		f[0],f[1],f[2],f[3] = adjacent_faces(ce, left_elem[ce], right_elem[ce], elem_s1, elem_s2, elem_s3)
		c1 = -1
		c2 = -1

		color_hash = [0] * num_colors
		c1 = edge_color[f[0]]
		c2 = edge_color[f[1]]
		if c1 > -1:
			color_hash[c1]=color_hash[c1]+1
		if c2 > -1:
			color_hash[c2]=color_hash[c2]+1


		# this is a boundary side
		if f[2]  == -1:
			f = [f[0],f[1]]
			c_list = [c1, c2]

		# this is not a boundary side
		else:
			c3 = edge_color[f[2]]
			c4 = edge_color[f[3]]
			if c3 > -1:
				color_hash[c3]=color_hash[c3]+1
			if c4 > -1:
				color_hash[c4]=color_hash[c4]+1

			c_list = [c1, c2, c3, c4]



		#at end of loop, double
		if sum(num > 0 for num in color_hash) == num_colors and loopID[-1] in f and (loopID[-1] != prev_edge[-1]) and not (-1 in c_list):
			double_c = color_hash.index(2)
			index = c_list.index(double_c)
			edge_color[f[index]] = -1
			new_f1 = f[index]
			f.pop(index)
			c_list.pop(index)

			index = c_list.index(double_c)
			edge_color[f[index]] = -1
			new_f2 = f[index]
			f.pop(index)
			c_list.pop(index)

			# remove the color repeat
			color_hash = [0]*num_colors
			for c in c_list:
				color_hash[c] = color_hash[c]+1

			# resolve the conflict
			index = color_hash.index(0)
			edge_color[ce] = index
			conflict_edge.pop()
			prev_edge.pop()
			loopID.pop()

			conflict_edge.append(new_f1)
			loopID.append(new_f1)
			prev_edge.append(ce)

			conflict_edge.append(new_f2)
			loopID.append(new_f2)
			prev_edge.append(ce)

		# not end of loop, and cannot be resolved, swap with available sides
		elif sum(num > 0 for num in color_hash) == num_colors:
			# remove the repeated colors
			if 2 in color_hash:
				for double_c,c_count in enumerate(color_hash):
					if c_count == 2:
						for j in xrange(2):
							index = c_list.index(double_c)
							f.pop(index)
							c_list.pop(index)

			while -1 in c_list:
				index = c_list.index(-1)
				f.pop(index)
				c_list.pop(index)

			if prev_edge[-1] > -1:
				if prev_edge[-1] in f:
					index = f.index(prev_edge[-1])
					f.pop(index)
					c_list.pop(index)

			faceToSwap = random.choice(f)

			# swap
			edge_color[faceToSwap], edge_color[ce]= edge_color[ce], edge_color[faceToSwap]
			conflict_edge[-1] = faceToSwap
			prev_edge[-1] = ce



		# it can be resolved, so resolve
		elif sum(num > 0 for num in color_hash) < num_colors:
			for c_num, c_pop in enumerate(color_hash):
				if c_pop == 0:
					edge_color[ce] = c_num
					break
			conflict_edge.pop()
			prev_edge.pop()
			loopID.pop()
			# sys.stdout.write("conflicts left %5.1i/%i\r" % (len(conflict_edge), num_conflicts))
			# sys.stdout.flush()


	
	end = time.time()




	for e in xrange(num_elements): 
		if (edge_color[elem_s3[e]] == -1 or  edge_color[elem_s2[e]] == -1 or edge_color[elem_s1[e]] == -1 or edge_color[elem_s1[e]] == edge_color[elem_s2[e]] or edge_color[elem_s1[e]]==edge_color[elem_s3[e]] or edge_color[elem_s2[e]]==edge_color[elem_s3[e]]):
			print "something's wrong elem %i sides %i %i %i %i %i %i" % (e, elem_s1[e], elem_s2[e], elem_s3[e],edge_color[elem_s1[e]], edge_color[elem_s2[e]], edge_color[elem_s3[e]])



	print "\ncolor timing: %lf" % (end - start)


	count = [-1]*num_colors
	for i in range(num_colors):
		count[i] = edge_color.count(i)
		print "there are %i edges in color %i \n" %(edge_color.count(i), i)



	start = time.time()

	print "Sorting sides according to color..."
	# sort the mesh so that right element = -1 items are first, -2 second, -3 third
	j = 0 # location after the latest right element
	for N in xrange(num_colors):
		for i in xrange(num_sides):
			if edge_color[i] == N:

				# update index for left_elem[j]
				if left_side_number[j] == 0:
					elem_s1[left_elem[j]] = i
				elif left_side_number[j] == 1:
					elem_s2[left_elem[j]] = i
				elif left_side_number[j] == 2:
					elem_s3[left_elem[j]]= i

				# update index for right_elem[j]
				if right_elem[j] > -1:
					if right_side_number[j] == 0:
						elem_s1[right_elem[j]] = i
					elif right_side_number[j] == 1:
						elem_s2[right_elem[j]] = i
					elif right_side_number[j] == 2:
						elem_s3[right_elem[j]] = i

				# update index for left_elem[i]
				if left_side_number[i] == 0:
					elem_s1[left_elem[i]] = j
				if left_side_number[i] == 1:
					elem_s2[left_elem[i]] = j
				if left_side_number[i] == 2:
					elem_s3[left_elem[i]] = j

				if right_elem[i] > -1 :
					# update index for right_elem[i]
					if right_side_number[i] == 0:
						elem_s1[right_elem[i]] = j
					if right_side_number[i] == 1:
						elem_s2[right_elem[i]] = j
					if right_side_number[i] == 2:
						elem_s3[right_elem[i]] = j

				# swap sides i and j
				# sidelist[i], sidelist[j] = sidelist[j], sidelist[i]
				left_elem[i] , left_elem[j]  = left_elem[j] , left_elem[i]
				right_elem[i], right_elem[j] = right_elem[j], right_elem[i]
				left_side_number[i] , left_side_number[j]  = left_side_number[j] , left_side_number[i]
				right_side_number[i], right_side_number[j] = right_side_number[j], right_side_number[i]


				side_v1[i],side_v1[j] = side_v1[j],side_v1[i] 
				side_v2[i],side_v2[j] = side_v2[j],side_v2[i] 

				edge_color[i], edge_color[j] = edge_color[j], edge_color[i] 
				# increment j
				j += 1


	if renumber:
		print "Renaming left elements in color 1"
		for i in range(count[0]):
			# element ce
			j = left_elem[i]

			s1j = elem_s1[j]
			s2j = elem_s2[j]
			s3j = elem_s3[j]

			s1i = elem_s1[i]
			s2i = elem_s2[i]
			s3i = elem_s3[i]




			ls1j = left_elem[s1j]
			rs1j = right_elem[s1j]
			ls2j = left_elem[s2j]
			rs2j = right_elem[s2j]
			ls3j = left_elem[s3j]
			rs3j = right_elem[s3j]

			ls1i = left_elem[s1i]
			rs1i = right_elem[s1i]
			ls2i = left_elem[s2i]
			rs2i = right_elem[s2i]
			ls3i = left_elem[s3i]
			rs3i = right_elem[s3i]

			if ls1j == j:
				left_elem[s1j] = i
			elif rs1j == j:
				right_elem[s1j] = i

			if ls2j == j:
				left_elem[s2j] = i
			elif rs2j == j:
				right_elem[s2j] = i

			if ls3j == j:
				left_elem[s3j] = i
			elif rs3j == j:
				right_elem[s3j] = i

			if ls1i == i :
				left_elem[s1i] = j
			elif rs1i == i :
				right_elem[s1i] = j

			if ls2i == i :
				left_elem[s2i] = j
			elif rs2i == i :
				right_elem[s2i] = j

			if ls3i == i :
				left_elem[s3i] = j
			elif rs3i == i :
				right_elem[s3i] = j

			elem_s1[j] = s1i
			elem_s2[j] = s2i
			elem_s3[j] = s3i

			elem_s1[i] = s1j
			elem_s2[i] = s2j
			elem_s3[i] = s3j


			# swap vertices
			v1j = elem_v1[j]
			v2j = elem_v2[j]
			v3j = elem_v3[j]
			
			v1i = elem_v1[i]
			v2i = elem_v2[i]
			v3i = elem_v3[i]

			elem_v1[j] = v1i
			elem_v2[j] = v2i
			elem_v3[j] = v3i

			elem_v1[i] = v1j
			elem_v2[i] = v2j
			elem_v3[i] = v3j
	 
		print "Renaming right elements in color 1"
		i = count[0]
		# sorting elements according to color
		for d in range(count[0]):
			# element ce
			j = right_elem[d]

			if j >  -1 :
				s1j = elem_s1[j]
				s2j = elem_s2[j]
				s3j = elem_s3[j]

				s1i = elem_s1[i]
				s2i = elem_s2[i]
				s3i = elem_s3[i]


				ls1j = left_elem[s1j]
				rs1j = right_elem[s1j]
				ls2j = left_elem[s2j]
				rs2j = right_elem[s2j]
				ls3j = left_elem[s3j]
				rs3j = right_elem[s3j]

				ls1i = left_elem[s1i]
				rs1i = right_elem[s1i]
				ls2i = left_elem[s2i]
				rs2i = right_elem[s2i]
				ls3i = left_elem[s3i]
				rs3i = right_elem[s3i]

				if ls1j == j:
					left_elem[s1j] = i
				elif rs1j == j:
					right_elem[s1j] = i

				if ls2j == j:
					left_elem[s2j] = i
				elif rs2j == j:
					right_elem[s2j] = i

				if ls3j == j:
					left_elem[s3j] = i
				elif rs3j == j:
					right_elem[s3j] = i

				if ls1i == i :
					left_elem[s1i] = j
				elif rs1i == i :
					right_elem[s1i] = j

				if ls2i == i :
					left_elem[s2i] = j
				elif rs2i == i :
					right_elem[s2i] = j

				if ls3i == i :
					left_elem[s3i] = j
				elif rs3i == i :
					right_elem[s3i] = j

				

				# element i's sides now become element j's sides
				elem_s1[j] = s1i
				elem_s2[j] = s2i
				elem_s3[j] = s3i

				# element j's sides now become element i's sides
				elem_s1[i] = s1j
				elem_s2[i] = s2j
				elem_s3[i] = s3j

				# swap vertices
				v1j = elem_v1[j]
				v2j = elem_v2[j]
				v3j = elem_v3[j]
				
				v1i = elem_v1[i]
				v2i = elem_v2[i]
				v3i = elem_v3[i]

				elem_v1[j] = v1i
				elem_v2[j] = v2i
				elem_v3[j] = v3i

				elem_v1[i] = v1j
				elem_v2[i] = v2j
				elem_v3[i] = v3j 

				i = i + 1
	else:
		print "no renumbering"

	# 		i = i + 1
	# for e in xrange(num_elements): 
	#     if (edge_color[elem_s3[e]] == -1 or  edge_color[elem_s2[e]] == -1 or edge_color[elem_s1[e]] == -1 or edge_color[elem_s1[e]] == edge_color[elem_s2[e]] or edge_color[elem_s1[e]]==edge_color[elem_s3[e]] or edge_color[elem_s2[e]]==edge_color[elem_s3[e]]):
	#         print "2 something's wrong elem %i sides %i %i %i %i %i %i" % (e, elem_s1[e], elem_s2[e], elem_s3[e],edge_color[elem_s1[e]], edge_color[elem_s2[e]], edge_color[elem_s3[e]])

	if order:
		start_j = count[0]
		for k in xrange(num_colors-1):
			print "Sorting mesh according to left element in color %i..."% (k+1)
			left_dict = {}

			for pos in range(start_j, start_j + count[k+1]):
				elem = left_elem[pos]
				left_dict[elem] = pos

			# # sort the elements
			# print "Sorting mesh according to left element in color 1..."
			j = start_j # location after the latest right element

			for elem in range(num_elements):
				# sys.stdout.write("left %5.1i/%i\r" % (elem, num_elements))
				# sys.stdout.flush()
				if elem in left_dict:
					i = left_dict[elem]

					left_dict[elem] = j
					left_dict[left_elem[j]] = i

					# update index for left_elem[j]
					if left_side_number[j] == 0:
						elem_s1[left_elem[j]] = i
					elif left_side_number[j] == 1:
						elem_s2[left_elem[j]] = i
					elif left_side_number[j] == 2:
						elem_s3[left_elem[j]]= i

					# update index for right_elem[j]
					if right_elem[j] > -1:
						if right_side_number[j] == 0:
							elem_s1[right_elem[j]] = i
						elif right_side_number[j] == 1:
							elem_s2[right_elem[j]] = i
						elif right_side_number[j] == 2:
							elem_s3[right_elem[j]] = i

					# update index for left_elem[i]
					if left_side_number[i] == 0:
						elem_s1[left_elem[i]] = j
					if left_side_number[i] == 1:
						elem_s2[left_elem[i]] = j
					if left_side_number[i] == 2:
						elem_s3[left_elem[i]] = j

					if right_elem[i] > -1 :
						# update index for right_elem[i]
						if right_side_number[i] == 0:
							elem_s1[right_elem[i]] = j
						if right_side_number[i] == 1:
							elem_s2[right_elem[i]] = j
						if right_side_number[i] == 2:
							elem_s3[right_elem[i]] = j

					# swap sides i and j
					left_elem[i] , left_elem[j]  = left_elem[j] , left_elem[i]
					right_elem[i], right_elem[j] = right_elem[j], right_elem[i]
					left_side_number[i] , left_side_number[j]  = left_side_number[j] , left_side_number[i]
					right_side_number[i], right_side_number[j] = right_side_number[j], right_side_number[i]


					side_v1[i],side_v1[j] = side_v1[j],side_v1[i] 
					side_v2[i],side_v2[j] = side_v2[j],side_v2[i] 

					edge_color[i], edge_color[j] = edge_color[j], edge_color[i] 
					# increment j
					j += 1
			start_j = start_j + count[k+1]
	else:
		print "no ordering"

	end = time.time()
	print "\nsorting timing: %lf" % (end - start)




	for e in xrange(num_elements): 
		if (edge_color[elem_s3[e]] == -1 or  edge_color[elem_s2[e]] == -1 or edge_color[elem_s1[e]] == -1 or edge_color[elem_s1[e]] == edge_color[elem_s2[e]] or edge_color[elem_s1[e]]==edge_color[elem_s3[e]] or edge_color[elem_s2[e]]==edge_color[elem_s3[e]]):
			print "something's wrong elem %i sides %i %i %i %i %i %i" % (e, elem_s1[e], elem_s2[e], elem_s3[e],edge_color[elem_s1[e]], edge_color[elem_s2[e]], edge_color[elem_s3[e]])

	# write the mesh to file
	print "Writing file: %s..." % outFilename

	outFile.write(str(num_elements) + "\n")
	for v1, v2, v3, s1, s2, s3 in zip(elem_v1, elem_v2, elem_v3, elem_s1, elem_s2, elem_s3):

		outFile.write("%.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %i %i %i\n" % 
															 (v1[0], v1[1], v2[0], v2[1], v3[0], v3[1],
															  s1, s2, s3))

	outFile.write(str(num_sides) + "\n")
	for v1, v2, le, re, lsn, rsn, color in zip(side_v1, side_v2, left_elem, right_elem, left_side_number, right_side_number, edge_color):
		outFile.write("%.015lf %.015lf %.015lf %.015lf %i %i %i %i %i\n" % 
										(v1[0], v1[1],
										 v2[0], v2[1],
										 le,re,
										 lsn,rsn, color))

	for i in range(10):
		outFile.write(str(edge_color.count(i)) + "\n")

	outFile.close()





if __name__ == "__main__":
	# try:
		inFilename  = argv[1] 
		outFilename = argv[2]
		n_colors = argv[3]
		order = argv[4]
		renumber = argv[5]
		color(inFilename, outFilename, n_colors, order, renumber)
	# except Exception:
		# print "Usage: genmesh [infile] [outfile]"

