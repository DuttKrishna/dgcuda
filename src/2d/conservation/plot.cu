#include "memoryCounters.cuh"
#include "operations.cuh"
#include <stdio.h>
static double *d_curr_V1x;
static double *d_curr_V1y;
static double *d_curr_V2x;
static double *d_curr_V2y;
static double *d_curr_V3x;
static double *d_curr_V3y;
static double **d_c;
static double *basis_vertex;
static double *basis_midpoint;

// stores computed values at vertices
__device__ double *d_Uv1;
__device__ double *d_Uv2;
__device__ double *d_Uv3;
__device__ double *d_Uv4;
__device__ double *d_Uv5;
__device__ double *d_Uv6;


static int N;
static int n_p;

void allocate_plot(memoryCounters *timesteppingCounter, int emesh_MAX){
    allocate((void **) &d_Uv1, emesh_MAX * sizeof(double), &(timesteppingCounter->visualization_memory));
    allocate((void **) &d_Uv2, emesh_MAX * sizeof(double), &(timesteppingCounter->visualization_memory));
    allocate((void **) &d_Uv3, emesh_MAX * sizeof(double), &(timesteppingCounter->visualization_memory));
    allocate((void **) &d_Uv4, emesh_MAX * sizeof(double), &(timesteppingCounter->visualization_memory));
    allocate((void **) &d_Uv5, emesh_MAX * sizeof(double), &(timesteppingCounter->visualization_memory));
    allocate((void **) &d_Uv6, emesh_MAX * sizeof(double), &(timesteppingCounter->visualization_memory));
}
void init_plot(double *in_v1x, double *in_v2x, double *in_v3x, 
               double *in_v1y, double *in_v2y, double *in_v3y,
               double *in_bv, double *in_bm,
               double **in_c,
               int in_N, int in_n_p){

    d_curr_V1x = in_v1x;
    d_curr_V1y = in_v1y;
    d_curr_V2x = in_v2x;
    d_curr_V2y = in_v2y;
    d_curr_V3x = in_v3x;
    d_curr_V3y = in_v3y;
    basis_vertex = in_bv;
    basis_midpoint = in_bm;
    d_c = in_c;
    N = in_N;
    n_p = in_n_p;
}

/* evaluate u
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */

__global__ void eval_u(double **C, 
                      double *basis_vertex,
                      double *Uv1, double *Uv2, double *Uv3, 
                      int n, int n_p,
                      int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double uv1, uv2, uv3;

        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;
        uv3 = 0.;
        for (i = 0; i < n_p; i++) {
            uv1 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 0];
            uv2 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 1];
            uv3 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
        Uv3[idx] = uv3;
    }
}

__global__ void eval_rho(double **C, 
                      double *basis_vertex,
                      double *Uv1, double *Uv2, double *Uv3, 
                      int n_p,
                      int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double uv1, uv2, uv3;

        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;
        uv3 = 0.;
        for (i = 0; i < n_p; i++) {
            uv1 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 0];
            uv2 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 1];
            uv3 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
        Uv3[idx] = uv3;
    }
}

__global__ void eval_ux(double **C, 
                      double *basis_vertex,
                      double *Uv1, double *Uv2, double *Uv3, 
                       int n_p,
                      int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double u1, u2, u3;
        double v1, v2, v3;
        double rho_v1, rho_v2, rho_v3;

        // calculate values at the integration points
        rho_v1 = 0.;
        rho_v2 = 0.;
        rho_v3 = 0.;
        for (i = 0; i < n_p; i++) {
            rho_v1 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 0];
            rho_v2 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 1];
            rho_v3 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        u1 = 0.; v1 = 0.;
        u2 = 0.; v2 = 0.;
        u3 = 0.; v3 = 0.;
        for (i = 0; i < n_p; i++) {
            u1 += C[1*n_p + i][idx] * basis_vertex[i * 3 + 0];
            u2 += C[1*n_p + i][idx] * basis_vertex[i * 3 + 1];
            u3 += C[1*n_p + i][idx] * basis_vertex[i * 3 + 2];

            // v1 += C[2*n_p + i][idx] * basis_vertex[i * 3 + 0];
            // v2 += C[2*n_p + i][idx] * basis_vertex[i * 3 + 1];
            // v3 += C[2*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        // store result
        Uv1[idx] = u1/ rho_v1;
        Uv2[idx] = u2/ rho_v2;
        Uv3[idx] = u3/ rho_v3;
    }
}

__global__ void eval_p(double **C, 
                      double *basis_vertex,
                      double *Uv1, double *Uv2, double *Uv3, 
                      int n_p,
                      int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double u1, u2, u3;
        double v1, v2, v3;
        double rho_v1, rho_v2, rho_v3;
        double E_1, E_2, E_3;

        // calculate values at the integration points
        rho_v1 = 0.;
        rho_v2 = 0.;
        rho_v3 = 0.;
        for (i = 0; i < n_p; i++) {
            rho_v1 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 0];
            rho_v2 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 1];
            rho_v3 += C[0*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        u1 = 0.; v1 = 0.; E_1 = 0.;
        u2 = 0.; v2 = 0.; E_2 = 0.;
        u3 = 0.; v3 = 0.; E_3 = 0.;
        for (i = 0; i < n_p; i++) {
            u1 += C[1*n_p + i][idx] * basis_vertex[i * 3 + 0];
            u2 += C[1*n_p + i][idx] * basis_vertex[i * 3 + 1];
            u3 += C[1*n_p + i][idx] * basis_vertex[i * 3 + 2];

            v1 += C[2*n_p + i][idx] * basis_vertex[i * 3 + 0];
            v2 += C[2*n_p + i][idx] * basis_vertex[i * 3 + 1];
            v3 += C[2*n_p + i][idx] * basis_vertex[i * 3 + 2];

            E_1 += C[3*n_p + i][idx] * basis_vertex[i * 3 + 0];
            E_2 += C[3*n_p + i][idx] * basis_vertex[i * 3 + 1];
            E_3 += C[3*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        // store result
        Uv1[idx] = (1.4 - 1.) * (E_1 - (u1*u1+v1*v1) / 2. / rho_v1);
        Uv2[idx] = (1.4 - 1.) * (E_2 - (u2*u2+v2*v2) / 2. / rho_v2);
        Uv3[idx] = (1.4 - 1.) * (E_3 - (u3*u3+v3*v3) / 2. / rho_v3);
    }
}

__global__ void eval_um(double **C, 
                      double *Uv1, double *Uv2, double *Uv3, 
                      int n, int n_p,
                      int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double uv1, uv2, uv3;

        // calculate values at the integration points
        uv1 += C[n*n_p + 0][idx] * sqrt(2.);
        uv2 += C[n*n_p + 0][idx] * sqrt(2.);
        uv3 += C[n*n_p + 0][idx] * sqrt(2.);

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
        Uv3[idx] = uv3;
    }
}

__global__ void eval_u(double **C, 
                      double *basis_midpoint,double *basis_vertex,
                      double *Uv1, double *Uv2, double *Uv3, 
                      double *Uv4, double *Uv5, double *Uv6, 
                      int n, int n_p,
                      int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double uv1, uv2, uv3;
        double uv4, uv5, uv6;

        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;
        uv3 = 0.;
        uv4 = 0.;
        uv5 = 0.;
        uv6 = 0.;
        for (i = 0; i < n_p; i++) {
            uv1 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 0];
            uv2 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 1];
            uv3 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 2];

            uv4 += C[n*n_p + i][idx] * basis_midpoint[i * 3 + 0];
            uv5 += C[n*n_p + i][idx] * basis_midpoint[i * 3 + 1];
            uv6 += C[n*n_p + i][idx] * basis_midpoint[i * 3 + 2];

        }

        // store result
        Uv1[idx] = uv1;
        Uv2[idx] = uv2;
        Uv3[idx] = uv3;
        Uv4[idx] = uv4;
        Uv5[idx] = uv5;
        Uv6[idx] = uv6;
    }
}

void write_Um(int curr_num_elem, int num, int total_timesteps) {
    double *Uv1, *Uv2, *Uv3;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];
    double min, max;
    int max_index, min_index;

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // printf("ELEMENT 0'S VERTICES are (%f,%f) (%f,%f) (%f,%f)\n", V1x[0], V1y[0], V2x[0], V2y[0], V3x[0], V3y[0]);
    // evaluate and write to file
    for (n = 0; n < N; n++) {
        eval_um<<<n_blocks_elem, n_threads>>>(d_c, d_Uv1, d_Uv2, d_Uv3, n, n_p, curr_num_elem);

        cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

        if (num == total_timesteps) {
            sprintf(out_filename, "output/U%d-final_means.pos", n);
        } else {
            sprintf(out_filename, "output/video/U%d-%d.pos", n, num);
        }
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"U%i \" {\n", n);
        min = max = Uv1[0];
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                                   V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                   Uv1[i], Uv2[i], Uv3[i]);
        
            if(Uv1[i] > max){
               max =  Uv1[i];
               max_index = i;
            }
            if(Uv1[i] < min){
               min =  Uv1[i];
               min_index = i;
            }
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }


    free(Uv1);
    free(Uv2);
    free(Uv3);

    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);

    printf("  MEAN RANGE: min: %e on element %i, max: %e on element %i\n", min, min_index, max, max_index);
}

__global__ void eval_mach(double **C, 
                          double *V1x, double *V1y, 
                          double *V2x, double *V2y, 
                          double *V3x, double *V3y, 
                          double *Uv1, double *Uv2, double *Uv3, 
                          double *basis_vertex,
                          int N, int n_p,
                          int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i, n, vertex;
        double U[4];
        double c[3], vel[3];
        double u,v;
        double xi,eta, p, rho;

        double V[6];
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];


        for (vertex = 0; vertex < 3; vertex++) {
            for (n = 0; n < N; n++) {
                U[n] = 0.;
                for (i = 0; i < n_p; i++) {
                    U[n] += C[n*n_p + i][idx] * basis_vertex[i * 3 + vertex];
                }
            }






            p = (1.4 - 1.) * (U[3] - (U[1]*U[1] + U[2]*U[2]) / 2. / U[0]);
            rho = U[0];


            c[vertex] = sqrt(1.4* p / rho);
            
            xi  = V[2*vertex];
            eta = V[2*vertex + 1];

            u = U[1]/U[0];u-=xi;
            v = U[2]/U[0];v-=eta;
            vel[vertex] = sqrt(u*u + v*v);
        }


        // store result
        Uv1[idx] = vel[0] / c[0];
        Uv2[idx] = vel[1] / c[1];
        Uv3[idx] = vel[2] / c[2];
    }
}


void write_U(int curr_num_elem, int num, int total_timesteps, double t) {
    double *Uv1, *Uv2, *Uv3;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // evaluate and write to file
    for (n = 0; n < N; n++) {
        eval_u<<<n_blocks_elem, n_threads>>>(d_c, basis_vertex, d_Uv1, d_Uv2, d_Uv3, n, n_p, curr_num_elem);

        cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

        if (num == total_timesteps) {
            sprintf(out_filename, "output/U%d-final.pos", n);
        } else {
            sprintf(out_filename, "output/video/U%d-%d.pos", n, num);
        }
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"U%i - T = %lf\" {\n", n, t);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                                   V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                   Uv1[i], Uv2[i], Uv3[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }

    // eval_mach<<<n_blocks_elem, n_threads>>>(d_c, 
    //                                         d_curr_V1x,d_curr_V1y,
    //                                         d_curr_V2x,d_curr_V2y,
    //                                         d_curr_V3x,d_curr_V3y, 
    //                                         d_Uv1, d_Uv2, d_Uv3, 
    //                                         basis_vertex,
    //                                         4, n_p,
    //                                         curr_num_elem) ;
    // cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    // cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    // cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // sprintf(out_filename, "output/video/mach-%d.pos", num);
    // out_file  = fopen(out_filename , "w");
    // fprintf(out_file, "View \"mach - T = %lf\" {\n", t);
    // for (i = 0; i < curr_num_elem; i++) {
    //     fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
    //                            V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
    //                            Uv1[i], Uv2[i], Uv3[i]);
    // }
    // fprintf(out_file,"};");
    // fclose(out_file);


    free(Uv1);
    free(Uv2);
    free(Uv3);

    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}

void write_physical(int curr_num_elem, int num, int total_timesteps) {
    double *Uv1, *Uv2, *Uv3;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // printf("ELEMENT 0'S VERTICES are (%f,%f) (%f,%f) (%f,%f)\n", V1x[0], V1y[0], V2x[0], V2y[0], V3x[0], V3y[0]);
    // evaluate and write to file

    eval_rho<<<n_blocks_elem, n_threads>>>(d_c, basis_vertex, d_Uv1, d_Uv2, d_Uv3, n_p, curr_num_elem);
    cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    if (num == total_timesteps) {
        sprintf(out_filename, "output/rho-final.pos");
    } else {
        sprintf(out_filename, "output/video/rho-%d.pos", num);
    }
    out_file  = fopen(out_filename , "w");
    fprintf(out_file, "View \"rho \" {\n");
    for (i = 0; i < curr_num_elem; i++) {
        fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                               V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                               Uv1[i], Uv2[i], Uv3[i]);
    }
    fprintf(out_file,"};");
    fclose(out_file);

    eval_ux<<<n_blocks_elem, n_threads>>>(d_c, basis_vertex, d_Uv1, d_Uv2, d_Uv3, n_p, curr_num_elem);
    cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    if (num == total_timesteps) {
        sprintf(out_filename, "output/ux-final.pos");
    } else {
        sprintf(out_filename, "output/video/ux-%d.pos", num);
    }
    out_file  = fopen(out_filename , "w");
    fprintf(out_file, "View \"ux \" {\n");
    for (i = 0; i < curr_num_elem; i++) {
        fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                               V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                               Uv1[i], Uv2[i], Uv3[i]);
    }
    fprintf(out_file,"};");
    fclose(out_file);


    eval_p<<<n_blocks_elem, n_threads>>>(d_c, basis_vertex, d_Uv1, d_Uv2, d_Uv3, n_p, curr_num_elem);
    cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    if (num == total_timesteps) {
        sprintf(out_filename, "output/p-final.pos");
    } else {
        sprintf(out_filename, "output/video/p-%d.pos", num);
    }
    out_file  = fopen(out_filename , "w");
    fprintf(out_file, "View \"U \" {\n");
    for (i = 0; i < curr_num_elem; i++) {
        fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                               V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                               Uv1[i], Uv2[i], Uv3[i]);
    }
    fprintf(out_file,"};");
    fclose(out_file);




    free(Uv1);
    free(Uv2);
    free(Uv3);

    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}




void write_U_p2(int curr_num_elem, int num, int total_timesteps) {
    double *Uv1, *Uv2, *Uv3, *Uv4, *Uv5, *Uv6;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv4 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv5 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv6 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // printf("ELEMENT 0'S VERTICES are (%f,%f) (%f,%f) (%f,%f)\n", V1x[0], V1y[0], V2x[0], V2y[0], V3x[0], V3y[0]);
    // evaluate and write to file
    for (n = 0; n < 1; n++) {
        eval_u<<<n_blocks_elem, n_threads>>>(d_c, basis_midpoint, basis_vertex,d_Uv1, d_Uv2, d_Uv3, d_Uv4, d_Uv5, d_Uv6,n, n_p, curr_num_elem);

        cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv4, d_Uv4, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv5, d_Uv5, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv6, d_Uv6, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

        sprintf(out_filename, "output/U%d-final-high-order.msh", n);
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n$Nodes\n");
        fprintf(out_file, "%i\n", 6*curr_num_elem);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "%i %.015lf %.015lf 0 \n%i %.015lf %.015lf 0 \n%i %.015lf %.015lf 0 \n%i %.015lf %.015lf 0 \n%i %.015lf %.015lf 0 \n%i %.015lf %.015lf 0\n", 
                                   6*i +1 , V1x[i], V1y[i],
                                   6*i +2 , V2x[i], V2y[i],
                                   6*i +3 , V3x[i], V3y[i],
                                   6*i +4 , (V1x[i] + V2x[i])/2., (V1y[i] + V2y[i])/2.,
                                   6*i +5 , (V2x[i] + V3x[i])/2., (V2y[i] + V3y[i])/2.,
                                   6*i +6 , (V3x[i] + V1x[i])/2., (V3y[i] + V1y[i])/2.);
        }
        fprintf(out_file, "$EndNodes\n$Elements\n%i\n", curr_num_elem);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "%i 9 2 99 2 %i %i %i %i %i %i \n",i+1, 6*i + 1,6*i +2 ,6*i +3 ,6*i +4 ,6*i +5,6*i +6 );
        }
        fprintf(out_file, "$EndElements\n$NodeData\n");
        fprintf(out_file, "1\n"); // one string tag
        fprintf(out_file,  "\"U0\"\n"); //label
        fprintf(out_file, "1\n"); // one real tag
        fprintf(out_file, "0.0\n"); //time
        fprintf(out_file, "3\n"); // three integer tags
        fprintf(out_file, "0\n"); //time step
        fprintf(out_file, "1\n"); //1 component scalar field
        fprintf(out_file, "%i\n", 6*curr_num_elem); //number of nodes
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "%i %lf\n%i %lf\n%i %lf\n%i %lf\n%i %lf\n%i %lf\n",6*i +1,Uv1[i], 
            																	 6*i +2,Uv2[i],
            																	 6*i +3,Uv3[i],
            																	 6*i +4,Uv4[i],
            																	 6*i +5,Uv5[i],
            																	 6*i +6,Uv6[i]);
        }
        fprintf(out_file, "$EndNodeData\n");
        fclose(out_file);
    }


    free(Uv1);
    free(Uv2);
    free(Uv3);
    free(Uv4);
    free(Uv5);
    free(Uv6);

    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}

__device__ static void riemann_problem(double *U, double x, double t){



// stationary shock
// double rho_l, u_l, p_l, c_l; //left states
// double rho_r, u_r, p_r, c_r; //right states
// double GAMMA = 1.4;

// double p, u, rho_l_star, rho_r_star, c_l_star, c_r_star;
// double rho_out, u_out, p_out, u_temp, p_temp, rho_temp, xi;
// double ws[5];
// rho_l = 1.;
// u_l = -19.58745;
// p_l = 1000.;
// c_l = sqrt(GAMMA*p_l/rho_l);

// rho_r = 1.;
// u_r = -19.58745;
// p_r = 0.01;
// c_r = sqrt(GAMMA*p_r/rho_r);

// ws[0] = -5.7004023868e+01;
// ws[1] = -3.3487082201e+01;
// ws[2] =  1.0001388723e-02;
// ws[3] = 3.9300869669e+00;
// ws[4] = 3.9300869669e+00;

// rho_l_star=5.7506229848e-01;
// rho_r_star=5.9992407048e+00 ;
// u = 1.0001388723e-02;
// p = 4.6089378749e+02;


// if(t > 0.){
//   xi = x/t;
//     if(xi <= ws[0]){ // left state
//       rho_out = rho_l;
//       u_out = u_l;
//       p_out = p_l;
//     }
//     else if(ws[0] < xi && xi <= ws[1]){ // 1-rarefaction
//       u_temp = ((GAMMA-1.)*u_l + 2*(c_l + xi))/(GAMMA+1.);
//       rho_temp = pow((pow(rho_l,GAMMA)*(u_temp-xi)*(u_temp-xi)/(GAMMA*p_l)), 1./(GAMMA-1.) );
//       p_temp = p_l*pow((rho_temp/rho_l),GAMMA);
//       rho_out = rho_temp;
//       u_out = u_temp;
//       p_out = p_temp;
//     }
//     else if(ws[1] < xi && xi <= ws[2]){ // left-of-contact
//       rho_out = rho_l_star;
//       u_out = u;
//       p_out = p;
//     }
//     else if(ws[2] < xi && xi <= ws[3]){ // right-of-contact
//       rho_out = rho_r_star;
//       u_out = u;
//       p_out = p;
//     }
//     else if(ws[3] < xi && xi <= ws[4]){ // 3-rarefaction
//       u_temp = ((GAMMA-1.)*u_r - 2*(c_r - xi))/(GAMMA+1.); 
//       rho_temp = pow((pow(rho_r,GAMMA)*(xi-u_temp)*(xi-u_temp)/(GAMMA*p_r)), 1./(GAMMA-1.) );
//       p_temp = p_r*pow((rho_temp/rho_r),GAMMA);

//       rho_out = rho_temp;
//       u_out = u_temp;
//       p_out = p_temp;
//     }
//     else{
//       rho_out = rho_r;
//       u_out = u_r;
//       p_out = p_r;
//     }
// }
// else{
//     if(x <= 0){ // left state
//       rho_out = rho_l;
//       u_out = u_l;
//       p_out = p_l;
//     }
//     else{
//       rho_out = rho_r;
//       u_out = u_r;
//       p_out = p_r;
//     }
// }

// U[0] = rho_out;
// U[1] = rho_out * u_out;
// U[2] = 0.;
// U[3] = p_out/(GAMMA-1.) + 0.5*rho_out*u_out*u_out;

// mach 10
    double p, GAMMA = 1.4;
    if(x < 0 + 10 * t ){
        p = 116.5;
        U[0] = 8.;
        U[1] =  8.25 * U[0];
        U[2] = 0.;
        U[3] = 0.5 * U[0] * 8.25 * 8.25 + p / (GAMMA - 1.0);
    }
    else{
        U[0] = GAMMA;
        U[1] = 0.;
        U[2] = 0.;
        U[3] = 1./ (GAMMA - 1.0);
    }


}

__device__ static void U_exact(double *U, double x, double y, double t) {

    U[0] = - 0.5 * (x-t) * (x - t)* (x - t)* (x - t)  - 0.5 *  ( y-t )*  ( y-t )*  ( y-t ) * (y - t);
    // U[0] = - 0.5 * x  - 0.5 * y;
    // initial(U, x*cospi(2*t) + y*sinpi(2*t), -x*sinpi(2*t) + y*cospi(2*t));
    // double s = 1./3.;
    // if(x <=  s*t)
    //     U[0] = 1.;
    // else
    //     U[0] = 0.;
    // U0(U, x - t, y - t);
    // U0(U, x*cospi(2*t) + y*sinpi(2*t), -x*sinpi(2*t) + y*cospi(2*t));

    // double x0, y0 ; 
    // double x_rot, y_rot ; 

    // x_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // y_rot =  -sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // // x_rot =  sqrt(2.)/2. * x - sqrt(2.)/2. * y;
    // // y_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x0 = 0.;
    // y0 = 0.;
    // if (x_rot - x0< .75 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = (x_rot+0.25); 
    // } else {
    //     U[0] = 0;  
    // }



    // experiment 85
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.15, r ;
    // double xp = (x+1)/2., yp = (y+1.)/2.;
    // x0 = 0.5; y0 = 0.75;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(xp-x0) >= 0.025 || yp >=0.85)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.5; y0 = 0.25;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }



    // U[0] = 0.;
    // double x0 = 0.5, y0 = 0.;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0)) /0.25 ;
    // if(r <= 1.-1e-10){
    //   U[0] = exp(- 1. / (1-r*r));
    // }








    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.3, r ;
    // x0 = 0; y0 = 0.5;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(x-x0) >= 0.05 || y >=0.65)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.; y0 = -0.5;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // x0 = -0.5; y0 = 0.;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // hump
    //     U[0] = (1+cospi(r))/4.;
    // }
    // riemann_problem(U, x, t);


    // double x0=0., y0=0. ; 
    // if (x - x0 - t< .25 && x - x0 - t> -.25 && y -y0 - t < .25 && y -y0 -t > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }

    // experiment 1
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.35, r ;

    // x0 = -0.45; y0 = 0.;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // if(0.1 < x && x < 0.6 && -0.25 < y && y < 0.25)
    //     U[0] = 1.;

    // experiment 2
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.15, r ;
    // double xp = (x+1)/2., yp = (y+1.)/2.;
    // x0 = 0.5; y0 = 0.75;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(xp-x0) >= 0.025 || yp >=0.85)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.5; y0 = 0.25;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // x0 = 0.25; y0 = 0.5;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // hump
    //     U[0] = (1+cospi(r))/4.;
    // }
}


/* plot error
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void plot_error(double **C, 
                           double *V1x, double *V1y,
                           double *V2x, double *V2y,
                           double *V3x, double *V3y,
                           double *Uv1, double *Uv2, double *Uv3, 
                           double t, int n,
                           double *basis_vertex,
                           int n_p,
                           int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double uv1, uv2, uv3;
        double U[4];

        // calculate values at the integration points
        uv1 = 0.;
        uv2 = 0.;
        uv3 = 0.;
        for (i = 0; i < n_p; i++) {
            uv1 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 0];
            uv2 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 1];
            uv3 += C[n*n_p + i][idx] * basis_vertex[i * 3 + 2];
        }

        // store the difference of error and exact
        U_exact(U, V1x[idx], V1y[idx], t);
        Uv1[idx] = uv1 - U[n];
        U_exact(U, V2x[idx], V2y[idx], t);
        Uv2[idx] = uv2 - U[n];
        U_exact(U, V3x[idx], V3y[idx], t);
        Uv3[idx] = uv3 - U[n];
    }
}

__global__ void plot_exact(double **C, 
                           double *V1x, double *V1y,
                           double *V2x, double *V2y,
                           double *V3x, double *V3y,
                           double *Uv1, double *Uv2, double *Uv3, 
                           double t, int n,
                           double *basis_vertex,
                           int n_p,
                           int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i;
        double uv1, uv2, uv3;
        double U[4];

        // store the difference of error and exact
        U_exact(U, V1x[idx], V1y[idx], t);
        Uv1[idx] = U[n];
        U_exact(U, V2x[idx], V2y[idx], t);
        Uv2[idx] =  U[n];
        U_exact(U, V3x[idx], V3y[idx], t);
        Uv3[idx] = U[n];
    }
}

void plot_error(double t, int curr_num_elem) {
    double *Uv1, *Uv2, *Uv3, *Uv4, *Uv5, *Uv6;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv4 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv5 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv6 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);



    for (n = 0; n < N; n++) {
        plot_error<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, 
                                                 d_curr_V1x, d_curr_V1y,
                                                 d_curr_V2x, d_curr_V2y,
                                                 d_curr_V3x, d_curr_V3y,
                                                 d_Uv1, d_Uv2, d_Uv3, 
                                                 t, n,
                                                 basis_vertex,
                                                 n_p,
                                                 curr_num_elem);
        cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        sprintf(out_filename, "output/U%derror.msh", n);
        printf("Writing to %s...\n", out_filename);
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"U%i \" {\n", n);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                                   V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                   Uv1[i], Uv2[i], Uv3[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }    
}
 
void plot_exact(double t, int curr_num_elem) {
    double *Uv1, *Uv2, *Uv3, *Uv4, *Uv5, *Uv6;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv4 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv5 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv6 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    for (n = 0; n < N; n++) {
        plot_exact<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, 
                                                 d_curr_V1x, d_curr_V1y,
                                                 d_curr_V2x, d_curr_V2y,
                                                 d_curr_V3x, d_curr_V3y,
                                                 d_Uv1, d_Uv2, d_Uv3, 
                                                 t, n,
                                                 basis_vertex,
                                                 n_p,
                                                 curr_num_elem);
        cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        sprintf(out_filename, "output/U%dexact.msh", n);
        printf("Writing to %s...\n", out_filename);
        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"U%i \" {\n", n);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                                   V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                   Uv1[i], Uv2[i], Uv3[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }  
}
