#define N_MAX 4
#define NP_MAX 21
#include <stdio.h>

#include "./cub-1.5.1/cub/cub.cuh"
using namespace cub;

double tt = 0.;
#include "memoryCounters.cuh"
#include "operations.cuh"
#include "tree.cu"
#include "error_estimate.cuh"
#include "strategy.cuh"

#define GRAD 0
#define JUMPS 1
#define RHS 2
#define EXACT 3
#define GRAD_TRUMPET 4
#define MAX 5

#define TIMING 0
#define PRINT 0
#define PRINT_INTERVAL 1
// #define PRINT_INTERVAL 8979
// #define PRINT_INTERVAL 12952
// #define PRINT_INTERVAL 16827
// #define PRINT_INTERVAL 20720







// #define PRINT_INTERVAL 8979
// #define PRINT_INTERVAL 11657
// #define PRINT_INTERVAL 15144
// #define PRINT_INTERVAL 18647 




#define DECLARE_TIMING(s)  clock_t timeStart_##s; double timeDiff_##s; double total_time_##s = 0;
#define START_TIMING(s)    if(TIMING && num > -1) cudaDeviceSynchronize(); if(TIMING && num > -1) timeStart_##s = clock()
#define STOP_TIMING(s)     if(TIMING && num > -1) cudaDeviceSynchronize(); if(TIMING && num > -1) timeDiff_##s = (double)(clock() - timeStart_##s); if(TIMING && num > -1) total_time_##s += timeDiff_##s 
#define GET_TIMING(s)      (double)(timeDiff_##s /CLOCKS_PER_SEC)
#define GET_TOTAL_TIMING(s)      (double)(total_time_##s /CLOCKS_PER_SEC)
#define str(s) #s
#define PRINT_TIMING(s)      if(PRINT && num % PRINT_INTERVAL == 0) printf(#s" %lf\n", (double)(total_time_##s /CLOCKS_PER_SEC))


DECLARE_TIMING(flagElementsandSidesforAdapatation);
DECLARE_TIMING(flagElementsandSidesforAdapatation1);
DECLARE_TIMING(flagElementsandSidesforAdapatation2);
DECLARE_TIMING(enforce_nonconformity);
DECLARE_TIMING(enforce_monotonicity);
DECLARE_TIMING(coarsen);
DECLARE_TIMING(refine);
DECLARE_TIMING(resizeData);
DECLARE_TIMING(determineOrder);
DECLARE_TIMING(reorder);
DECLARE_TIMING(redetermineConnectivity);
DECLARE_TIMING(replaceCurrentMesh);
DECLARE_TIMING(grad);

__device__ int *d_temp;
__device__ int *d_pool;
__device__ int *d_internal;
__device__ int *d_interface;
__device__ int *d_to;
__device__ int *d_from;


static cub::DoubleBuffer<int> d_keys;
static cub::DoubleBuffer<int> d_values;

size_t temp_storage_bytes_sort2;
__device__ void *d_temp_storage2;

void flagElementsandSidesforAdapatation();
void coarsen();
void refine();
void resizeData_inplace();
void determineOrder();
void determineOrder_cub();
void resizeData();
void reorder();
void redetermineConnectivity();
void replaceCurrentMesh();
void printStatistics();
void set_num_elem(int value);
void set_num_sides(int value);
void eval_basis(double **basis_local, int local_n_p, int local_n_quad, double *r1_local, double *r2_local);



void write_cellwise(int curr_num_elem,
                 int *d_detector,
                 double *d_curr_V1x, double *d_curr_V1y,
                 double *d_curr_V2x, double *d_curr_V2y,
                 double *d_curr_V3x, double *d_curr_V3y,
                 int num) {
    int *limit;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/cellwise%i.pos", num);

    // evaluate at the vertex points and copy over data
    limit = (int *) malloc(curr_num_elem * sizeof(int));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // evaluate and write to file
      cudaMemcpy(limit, d_detector, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);

      out_file  = fopen(out_filename , "w");
      fprintf(out_file, "View \"U0 \" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit[i] , limit[i] , limit[i] );
      }
      fprintf(out_file,"};");
      fclose(out_file);


      // out_file  = fopen("output/limit2.pos" , "w");
      // fprintf(out_file, "View \"U0\" {\n");
      // for (i = 0; i < curr_num_elem; i++) {
      //     fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
      //                            V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
      //                            limit[i] > 10., limit[i] > 10., limit[i] > 10.);
      // }
      // fprintf(out_file,"};");
      // fclose(out_file);

    free(limit);
    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}


void write_cellwise(int curr_num_elem,
                 double *d_detector,
                 double *d_curr_V1x, double *d_curr_V1y,
                 double *d_curr_V2x, double *d_curr_V2y,
                 double *d_curr_V3x, double *d_curr_V3y,
                 int num) {
    double *limit;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/cellwise%i.pos", num);

    // evaluate at the vertex points and copy over data
    limit = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // evaluate and write to file
      cudaMemcpy(limit, d_detector, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

      out_file  = fopen(out_filename , "w");
      fprintf(out_file, "View \"U0 \" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%lf,%lf,%lf};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit[i] , limit[i] , limit[i] );
      }
      fprintf(out_file,"};");
      fclose(out_file);


      // out_file  = fopen("output/limit2.pos" , "w");
      // fprintf(out_file, "View \"U0\" {\n");
      // for (i = 0; i < curr_num_elem; i++) {
      //     fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
      //                            V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
      //                            limit[i] > 10., limit[i] > 10., limit[i] > 10.);
      // }
      // fprintf(out_file,"};");
      // fclose(out_file);

    free(limit);
    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}


/***********************
 *
 * Adaptivity variables
 *
 ***********************/
double max_error;
int new_num_sides, new_num_elem;
int num_ecoarsen = 0, num_ecadd = 0, num_scoarsen = 0, num_scadd = 0;
int num_erefine = 0, num_srefine = 0;
int n_threads = 512;
int verbose, num;
int new_num_ecadd = 0;
__device__ double *d_error_estimate;
__device__ double *d_max_error;
__device__ int *d_enum_sides;

__device__ int *edr; 
__device__ int *sdr; 

__device__ int *d_ecadd; 
__device__ int *d_erefine; 
__device__ int *d_srefine; 

__device__ int *d_inflag; 
__device__ int *d_outflag;

__device__ int *d_pos_efree;
__device__ int *d_pos_sfree;
int num_pos_efree = 0;
int num_pos_sfree = 0;

__device__ int *d_prev_edr;

__device__ int *d_ordering;
__device__ int *d_s_ordering;

__device__ double *d_min;
__device__ int *d_flag; 


/*
Current mesh pointers
*/

int curr_num_elem, curr_num_sides;
int init_sides;

// int max_level = 17 ; //4e-6  
// int max_level = 9 ; 
int max_level = 6 ;  
// int max_level = 3; 
int emesh_MAX, smesh_MAX;

int n_p, N, order;
double t;

__device__ double **d_curr_c;
__device__ double **h_curr_c;

__device__ int *d_curr_elem;
__device__ int *d_curr_side;

__device__ int *d_curr_s1, *d_curr_s2, *d_curr_s3;

__device__ int *d_curr_elevel;
__device__ int *d_curr_slevel;

__device__ int *d_curr_originalside;
__device__ double *d_slength;
__device__ int *d_curr_scolor;

__device__ int *d_curr_left1;
__device__ int *d_curr_left2;
__device__ int *d_curr_left3;

__device__ double *d_curr_V1x;
__device__ double *d_curr_V1y;
__device__ double *d_curr_V2x;
__device__ double *d_curr_V2y;
__device__ double *d_curr_V3x;
__device__ double *d_curr_V3y;


__device__ double *d_curr_xr;
__device__ double *d_curr_yr;
__device__ double *d_curr_xs;
__device__ double *d_curr_ys;

__device__ double *d_curr_J;
__device__ double *d_sdetector;

int limit;

double curr_min_r;

int *color_size;

/*
Mesh pointers common to both
*/

__device__ int *d_left_elem;
__device__ int *d_right_elem;
__device__ int *d_left_side_number;
__device__ int *d_right_side_number;

__device__ double *d_circles;


// basis pointers for computation
__device__ double *d_basis;
__device__ double *d_basis_centroid;
__device__ double *d_basis_refined;
__device__ double *d_basis_grad_x;
__device__ double *d_basis_grad_y;
__device__ double *d_w;
int n_quad;



memoryCounters *counter;

__device__ void *d_temp_storage;
size_t temp_storage_bytes_scan;
static void     *d_temp_storage_sort = NULL;
static size_t   temp_storage_bytes_sort = 0;



/********************

        KERNELS

*******************/





__global__ void all_coarsened(int *eIDbuffer, int *flag,
                             int *echild1, int *echild2, int *echild3, int *echild4,
                             int *edr, int *epos,
                             int num_parents) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int eposchild1, eposchild2, eposchild3, eposchild4;
    int refined = 0;
    double coarsen = 1;

    if (idx < num_parents) {
        eID = eIDbuffer[idx];


        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        eposchild1 = epos[eIDchild1];
        eposchild2 = epos[eIDchild2];
        eposchild3 = epos[eIDchild3];
        eposchild4 = epos[eIDchild4];

        coarsen = coarsen && (edr[eposchild1] == -1);
        coarsen = coarsen && (edr[eposchild2] == -1);
        coarsen = coarsen && (edr[eposchild3] == -1);
        coarsen = coarsen && (edr[eposchild4] == -1);

        // if all elements are in the lower_nth then coarsen
        if(coarsen)
            flag[idx] =  1 ;
        else 
            flag[idx] =  0 ;
    }
}


__global__ void s_indicator(int *edr, int *sdr, 
                            int *elevel, int *slevel,
                            int *left_elem, int *right_elem,
                            int max,
                            int num) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    int edrleft, edrright;
    int ePOSleft, ePOSright;
    int sidelevel,leftlevel, rightlevel;
    int l, r;

    if(idx < max) { 
        ePOSleft = left_elem[idx];
        ePOSright = right_elem[idx];

        sidelevel = slevel[idx];
        leftlevel = elevel[ePOSleft];
        rightlevel = (ePOSright > -1) ? elevel[ePOSright] : leftlevel;

        edrleft = edr[ePOSleft]; 
        edrright = (ePOSright > -1) ? edr[ePOSright] : edrleft;


        l = leftlevel + edrleft;
        r = rightlevel + edrright;


        sdr[idx] = (l > r) ? l - sidelevel : r - sidelevel; // take the dr from the element with the highest refinement level


    }
}





__global__ void refine_geo(double *V1x, double *V1y,
                           double *V2x, double *V2y,
                           double *V3x, double *V3y,
                           double *xr, double *yr,
                           double *xs, double *ys,
                           double *J, 
                           int *epos,
                           int curr_num_elem,
                           int *pos_efree, int num_pos_efree,
                           int *eIDlist, int num_erefine) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    
    if(idx < num_erefine) {
        double Vx[6];
        double Vy[6];
        int index[4];

        int eID = eIDlist[idx];
        int ePOS = epos[eID];


        index[0] = (idx + 0 * num_erefine < num_pos_efree) ? pos_efree[idx + 0 * num_erefine ] : curr_num_elem + 0 * num_erefine + idx - num_pos_efree;
        index[1] = (idx + 1 * num_erefine < num_pos_efree) ? pos_efree[idx + 1 * num_erefine ] : curr_num_elem + 1 * num_erefine + idx - num_pos_efree;
        index[2] = (idx + 2 * num_erefine < num_pos_efree) ? pos_efree[idx + 2 * num_erefine ] : curr_num_elem + 2 * num_erefine + idx - num_pos_efree;
        index[3] = ePOS;


        Vx[0] = V1x[ePOS]; // 0
        Vy[0] = V1y[ePOS]; // 0
        Vx[1] = 0.5 * (V1x[ePOS] + V2x[ePOS]); // 0r
        Vy[1] = 0.5 * (V1y[ePOS] + V2y[ePOS]); // 0r
        Vx[2] = V2x[ePOS]; // 1
        Vy[2] = V2y[ePOS]; // 1
        Vx[3] = 0.5 * (V2x[ePOS] + V3x[ePOS]); // 1r
        Vy[3] = 0.5 * (V2y[ePOS] + V3y[ePOS]); // 1r
        Vx[4] = V3x[ePOS]; // 2
        Vy[4] = V3y[ePOS]; // 2
        Vx[5] = 0.5 * (V3x[ePOS] + V1x[ePOS]); // 2r
        Vy[5] = 0.5 * (V3y[ePOS] + V1y[ePOS]); // 2r

        // omega 1 -> write omega 1 in the parent element's position
        V1x[index[0]] = Vx[0]; // 0
        V1y[index[0]] = Vy[0]; // 0
        V2x[index[0]] = Vx[1]; // 0r
        V2y[index[0]] = Vy[1]; // 0r
        V3x[index[0]] = Vx[5]; // 2r
        V3y[index[0]] = Vy[5]; // 2r
        
        // append the rest to the end
        // omega 2
        V1x[index[1]] = Vx[1]; // 0r
        V1y[index[1]] = Vy[1]; // 0r
        V2x[index[1]] = Vx[2]; // 1
        V2y[index[1]] = Vy[2]; // 1
        V3x[index[1]] = Vx[3]; // 1r
        V3y[index[1]] = Vy[3]; // 1r

        // omega 3
        V1x[index[2]] = Vx[5]; // 2r
        V1y[index[2]] = Vy[5]; // 2r
        V2x[index[2]] = Vx[3]; // 1r
        V2y[index[2]] = Vy[3]; // 1r
        V3x[index[2]] = Vx[4]; // 2
        V3y[index[2]] = Vy[4]; // 2

        // omega 4
        V1x[index[3]] = Vx[1]; // 0r
        V1y[index[3]] = Vy[1]; // 0r
        V2x[index[3]] = Vx[3]; // 1r
        V2y[index[3]] = Vy[3]; // 1r
        V3x[index[3]] = Vx[5]; // 2r
        V3y[index[3]] = Vy[5]; // 2r


        xr[index[0]] = Vx[1] - Vx[0] ;// V2xref[index[0]] - V1xref[index[0]];
        yr[index[0]] = Vy[1] - Vy[0] ;// V2yref[index[0]] - V1yref[index[0]];
        xs[index[0]] = Vx[5] - Vx[0] ;// V3xref[index[0]] - V1xref[index[0]];
        ys[index[0]] = Vy[5] - Vy[0] ;// V3yref[index[0]] - V1yref[index[0]];
        
        xr[index[1]] = Vx[2] - Vx[1] ; // V2xref[index[1]] - V1xref[index[1]];
        yr[index[1]] = Vy[2] - Vy[1] ; // V2yref[index[1]] - V1yref[index[1]];
        xs[index[1]] = Vx[3] - Vx[1] ; // V3xref[index[1]] - V1xref[index[1]];
        ys[index[1]] = Vy[3] - Vy[1] ; // V3yref[index[1]] - V1yref[index[1]];

        xr[index[2]] = Vx[3] - Vx[5] ; // V2xref[index[2]] - V1xref[index[2]];
        yr[index[2]] = Vy[3] - Vy[5] ; // V2yref[index[2]] - V1yref[index[2]];
        xs[index[2]] = Vx[4] - Vx[5] ; // V3xref[index[2]] - V1xref[index[2]];
        ys[index[2]] = Vy[4] - Vy[5] ; // V3yref[index[2]] - V1yref[index[2]];

        xr[index[3]] = Vx[3] - Vx[1] ; // V2xref[index[3]] - V1xref[index[3]];
        yr[index[3]] = Vy[3] - Vy[1] ; // V2yref[index[3]] - V1yref[index[3]];
        xs[index[3]] = Vx[5] - Vx[1] ; // V3xref[index[3]] - V1xref[index[3]];
        ys[index[3]] = Vy[5] - Vy[1] ; // V3yref[index[3]] - V1yref[index[3]];

        J[index[0]] = (Vx[1] - Vx[0]) *       
                         (Vy[5] - Vy[0]) -      
                         (Vx[5] - Vx[0]) *      
                         (Vy[1] - Vy[0]);      
      
        J[index[1]] = (Vx[2] - Vx[1]) *       
                         (Vy[3] - Vy[1]) -      
                         (Vx[3] - Vx[1]) *      
                         (Vy[2] - Vy[1]);      
      
        J[index[2]] = (Vx[3] - Vx[5]) *       
                         (Vy[4] - Vy[5]) -      
                         (Vx[4] - Vx[5]) *      
                         (Vy[3] - Vy[5]);      
      
        J[index[3]] = (Vx[3] - Vx[1]) *       
                         (Vy[5] - Vy[1]) -      
                         (Vx[5] - Vx[1]) *      
                         (Vy[3] - Vy[1]);         
    }
}
 

__global__ void refine_c1(double **C_in,
                         int curr_num_elem, 
                         int *epos,
                         int *eIDlist, 
                         int N, 
                         int *pos_efree, int num_pos_efree,
                         int num_erefine) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int index[4];

    if(idx < num_erefine) {
        int eID = eIDlist[idx];
        int ePOS = epos[eID];
        const int n_p = 3;
        double C[n_p]; 

        index[0] = (idx + 0 * num_erefine < num_pos_efree) ? pos_efree[idx + 0 * num_erefine] : curr_num_elem + 0 * num_erefine + idx - num_pos_efree;
        index[1] = (idx + 1 * num_erefine < num_pos_efree) ? pos_efree[idx + 1 * num_erefine] : curr_num_elem + 1 * num_erefine + idx - num_pos_efree;
        index[2] = (idx + 2 * num_erefine < num_pos_efree) ? pos_efree[idx + 2 * num_erefine] : curr_num_elem + 2 * num_erefine + idx - num_pos_efree;
        index[3] = ePOS;

        for(int n=0; n < N; n++){
            C[0] = C_in[n*n_p + 0][ePOS];
            C[1] = C_in[n*n_p + 1][ePOS];
            C[2] = C_in[n*n_p + 2][ePOS];

          C_in[n*n_p + 0][index[0]] = -sqrt(0.2e1) * C[1] / 0.2e1 + C[0] - sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1;
          C_in[n*n_p + 1][index[0]] = C[1] / 0.2e1;
          C_in[n*n_p + 2][index[0]] = C[2] / 0.2e1;

          C_in[n*n_p + 0][index[1]] = sqrt(0.2e1) * C[1] + C[0];
          C_in[n*n_p + 1][index[1]] = C[1] / 0.2e1;
          C_in[n*n_p + 2][index[1]] = C[2] / 0.2e1;

          C_in[n*n_p + 0][index[2]] = -sqrt(0.2e1) * C[1] / 0.2e1 + C[0] + sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1;
          C_in[n*n_p + 1][index[2]] = C[1] / 0.2e1;
          C_in[n*n_p + 2][index[2]] = C[2] / 0.2e1;

          C_in[n*n_p + 0][index[3]] = C[0];
          C_in[n*n_p + 1][index[3]] = C[2] * sqrt(0.3e1) / 0.4e1 + C[1] / 0.4e1;
          C_in[n*n_p + 2][index[3]] = C[2] / 0.4e1 - sqrt(0.3e1) * C[1] / 0.4e1;
        }

    }
}

__global__ void refine_c2(double **C_in,
                         int curr_num_elem, 
                         int *epos,
                         int *eIDlist, 
                         int N, 
                         int *pos_efree, int num_pos_efree,
                         int num_erefine) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    double eval[N_MAX];
    double U[N_MAX];
    int index[4];

    if(idx < num_erefine) {
        int eID = eIDlist[idx];
        int ePOS = epos[eID];
        const int n_p = 6;
        double C[n_p]; 

        index[0] = (idx + 0 * num_erefine < num_pos_efree) ? pos_efree[idx + 0 * num_erefine] : curr_num_elem + 0 * num_erefine + idx - num_pos_efree;
        index[1] = (idx + 1 * num_erefine < num_pos_efree) ? pos_efree[idx + 1 * num_erefine] : curr_num_elem + 1 * num_erefine + idx - num_pos_efree;
        index[2] = (idx + 2 * num_erefine < num_pos_efree) ? pos_efree[idx + 2 * num_erefine] : curr_num_elem + 2 * num_erefine + idx - num_pos_efree;
        index[3] = ePOS;

        for(int n=0; n < N; n++){
          for (int k = 0; k < n_p; k++) {
            C[k] = C_in[n*n_p + k][ePOS];
          }

          C_in[n*n_p + 0][index[0]] = 4.*(C[3] * sqrt(0.3e1) / 0.48e2 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + C[4] / 0.16e2 - sqrt(0.2e1) * C[1] / 0.8e1 + C[0] / 0.4e1 - sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.8e1);
          C_in[n*n_p + 1][index[0]] = 4.*(-C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.12e2 + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2 - 0.5e1 / 0.32e2 * C[4] * sqrt(0.2e1) + C[1] / 0.8e1);
          C_in[n*n_p + 2][index[0]] = 4.*(-sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.32e2 - 0.3e1 / 0.32e2 * C[5] * sqrt(0.5e1) * sqrt(0.2e1) + C[2] / 0.8e1);
          C_in[n*n_p + 3][index[0]] = 4.*(sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2);
          C_in[n*n_p + 4][index[0]] = 4.*(sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.96e2);
          C_in[n*n_p + 5][index[0]] = 4.*(sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2);

          C_in[n*n_p + 0][index[1]] = 4.*(C[3] * sqrt(0.3e1) / 0.16e2 + sqrt(0.2e1) * C[1] / 0.4e1 + C[0] / 0.4e1);
          C_in[n*n_p + 1][index[1]] = 4.*(C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.8e1 + C[1] / 0.8e1);
          C_in[n*n_p + 2][index[1]] = 4.*(sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 + C[2] / 0.8e1);
          C_in[n*n_p + 3][index[1]] = 4.*(sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2);
          C_in[n*n_p + 4][index[1]] = 4.*(sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.96e2);
          C_in[n*n_p + 5][index[1]] = 4.*(sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2);

          C_in[n*n_p + 0][index[2]] = 4.*(C[3] * sqrt(0.3e1) / 0.48e2 - C[4] / 0.16e2 + C[0] / 0.4e1 + sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.8e1 - sqrt(0.2e1) * C[1] / 0.8e1 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2);
          C_in[n*n_p + 1][index[2]] = 4.*(-C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.12e2 + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2 + 0.5e1 / 0.32e2 * C[4] * sqrt(0.2e1) + C[1] / 0.8e1);
          C_in[n*n_p + 2][index[2]] = 4.*(0.3e1 / 0.32e2 * C[5] * sqrt(0.5e1) * sqrt(0.2e1) + C[2] / 0.8e1 - sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.32e2);
          C_in[n*n_p + 3][index[2]] = 4.*(sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2);
          C_in[n*n_p + 4][index[2]] = 4.*(sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.96e2);
          C_in[n*n_p + 5][index[2]] = 4.*(sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2);

          C_in[n*n_p + 0][index[3]] = 4.*(-0.5e1 / 0.48e2 * C[3] * sqrt(0.3e1) - C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.24e2 + C[0] / 0.4e1);
          C_in[n*n_p + 1][index[3]] = 4.*(-C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.48e2 + 0.3e1 / 0.32e2 * C[4] * sqrt(0.2e1) + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.96e2 + C[2] * sqrt(0.3e1) / 0.16e2 + C[1] / 0.16e2);
          C_in[n*n_p + 2][index[3]] = 4.*(C[3] * sqrt(0.2e1) / 0.16e2 + sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.32e2 - C[5] * sqrt(0.5e1) * sqrt(0.2e1) / 0.32e2 + C[2] / 0.16e2 - sqrt(0.3e1) * C[1] / 0.16e2); 
          C_in[n*n_p + 3][index[3]] = 4.*(sqrt(0.6e1) * C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.288e3 + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.288e3 + sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.96e2);
          C_in[n*n_p + 4][index[3]] = 4.*(sqrt(0.6e1) * C[5] * sqrt(0.5e1) * sqrt(0.2e1) / 0.192e3 - sqrt(0.6e1) * C[3] * sqrt(0.2e1) / 0.96e2 - sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.192e3);
          C_in[n*n_p + 5][index[3]] = 4.*(sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.576e3 + sqrt(0.5e1) * sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.288e3 - sqrt(0.5e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.192e3);
        }
    }
}

__global__ void refine_c3(double **C_in,
                         int curr_num_elem, 
                         int *epos,
                         int *eIDlist, 
                         int N, 
                         int *pos_efree, int num_pos_efree,
                         int num_erefine) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    double eval[N_MAX];
    double U[N_MAX];
    int index[4];

    if(idx < num_erefine) {
        int eID = eIDlist[idx];
        int ePOS = epos[eID];
        const int n_p = 10;
        double C[n_p]; 

        index[0] = (idx + 0 * num_erefine < num_pos_efree) ? pos_efree[idx + 0 * num_erefine] : curr_num_elem + 0 * num_erefine + idx - num_pos_efree;
        index[1] = (idx + 1 * num_erefine < num_pos_efree) ? pos_efree[idx + 1 * num_erefine] : curr_num_elem + 1 * num_erefine + idx - num_pos_efree;
        index[2] = (idx + 2 * num_erefine < num_pos_efree) ? pos_efree[idx + 2 * num_erefine] : curr_num_elem + 2 * num_erefine + idx - num_pos_efree;
        index[3] = ePOS;

        for(int n=0; n < N; n++){
          for (int k = 0; k < n_p; k++) {
            C[k] = C_in[n*n_p + k][ePOS];
          }

          C_in[n*n_p + 0][index[0]] = C[8] * sqrt(0.5e1) / 0.8e1 + C[3] * sqrt(0.3e1) / 0.12e2 + C[4] / 0.4e1 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.12e2 + C[7] * sqrt(0.3e1) / 0.8e1 + C[6] / 0.8e1 - sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1 + C[9] * sqrt(0.7e1) / 0.8e1 - sqrt(0.2e1) * C[1] / 0.2e1 + C[0];
          C_in[n*n_p + 1][index[0]] = 0.3e1 / 0.20e2 * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.3e1 - 0.5e1 / 0.8e1 * C[4] * sqrt(0.2e1) + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + 0.3e1 / 0.10e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) + 0.3e1 / 0.8e1 * C[6] * sqrt(0.2e1) - 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + C[1] / 0.2e1;
          C_in[n*n_p + 2][index[0]] = 0.3e1 / 0.40e2 * sqrt(0.3e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 - 0.3e1 / 0.8e1 * C[5] * sqrt(0.5e1) * sqrt(0.2e1) + 0.3e1 / 0.40e2 * C[7] * sqrt(0.2e1) + C[2] / 0.2e1 + 0.3e1 / 0.20e2 * sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1);
          C_in[n*n_p + 3][index[0]] = 0.7e1 / 0.240e3 * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - 0.7e1 / 0.80e2 * sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) - 0.3e1 / 0.16e2 * sqrt(0.6e1) * C[6] * sqrt(0.2e1) - sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.240e3;
          C_in[n*n_p + 4][index[0]] = -0.7e1 / 0.120e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2 - sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.10e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3;
          C_in[n*n_p + 5][index[0]] = sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.24e2 - sqrt(0.5e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.24e2;
          C_in[n*n_p + 6][index[0]] = C[6] / 0.8e1;
          C_in[n*n_p + 7][index[0]] = C[7] / 0.8e1;
          C_in[n*n_p + 8][index[0]] = C[8] / 0.8e1;
          C_in[n*n_p + 9][index[0]] = C[9] / 0.8e1;

          C_in[n*n_p + 0][index[1]] = -C[6] / 0.2e1 + C[3] * sqrt(0.3e1) / 0.4e1 + sqrt(0.2e1) * C[1] + C[0];
          C_in[n*n_p + 1][index[1]] = 0.3e1 / 0.4e1 * C[6] * sqrt(0.2e1) + C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.2e1 + C[1] / 0.2e1;
          C_in[n*n_p + 2][index[1]] = 0.3e1 / 0.4e1 * sqrt(0.2e1) * C[7] + sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.2e1 + C[2] / 0.2e1;
          C_in[n*n_p + 3][index[1]] = sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.4e1 + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2;
          C_in[n*n_p + 4][index[1]] = sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.4e1 + sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2;
          C_in[n*n_p + 5][index[1]] = sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.4e1;
          C_in[n*n_p + 6][index[1]] = C[6] / 0.8e1;
          C_in[n*n_p + 7][index[1]] = C[7] / 0.8e1;
          C_in[n*n_p + 8][index[1]] = C[8] / 0.8e1;
          C_in[n*n_p + 9][index[1]] = C[9] / 0.8e1;

          C_in[n*n_p + 0][index[2]] = C[8] * sqrt(0.5e1) / 0.8e1 + C[3] * sqrt(0.3e1) / 0.12e2 - C[4] / 0.4e1 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.12e2 - C[7] * sqrt(0.3e1) / 0.8e1 + C[6] / 0.8e1 + sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1 - C[9] * sqrt(0.7e1) / 0.8e1 - sqrt(0.2e1) * C[1] / 0.2e1 + C[0];
          C_in[n*n_p + 1][index[2]] = 0.3e1 / 0.20e2 * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.3e1 + 0.5e1 / 0.8e1 * C[4] * sqrt(0.2e1) + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - 0.3e1 / 0.10e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) + 0.3e1 / 0.8e1 * C[6] * sqrt(0.2e1) + 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + C[1] / 0.2e1;
          C_in[n*n_p + 2][index[2]] = -0.3e1 / 0.40e2 * sqrt(0.3e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 + 0.3e1 / 0.8e1 * sqrt(0.5e1) * sqrt(0.2e1) * C[5] + 0.3e1 / 0.40e2 * sqrt(0.2e1) * C[7] + C[2] / 0.2e1 + 0.3e1 / 0.20e2 * sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1);
          C_in[n*n_p + 3][index[2]] = 0.7e1 / 0.240e3 * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + 0.7e1 / 0.80e2 * sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) - 0.3e1 / 0.16e2 * sqrt(0.6e1) * C[6] * sqrt(0.2e1) + sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.240e3;
          C_in[n*n_p + 4][index[2]] = 0.7e1 / 0.120e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2 - sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.10e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3;
          C_in[n*n_p + 5][index[2]] = sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.24e2 + sqrt(0.5e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.24e2;
          C_in[n*n_p + 6][index[2]] = C[6] / 0.8e1;
          C_in[n*n_p + 7][index[2]] = C[7] / 0.8e1;
          C_in[n*n_p + 8][index[2]] = C[8] / 0.8e1;
          C_in[n*n_p + 9][index[2]] = C[9] / 0.8e1;

          C_in[n*n_p + 0][index[3]] = C[6] / 0.4e1 - C[8] * sqrt(0.5e1) / 0.4e1 - 0.5e1 / 0.12e2 * C[3] * sqrt(0.3e1) - C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.6e1 + C[0];
          C_in[n*n_p + 1][index[3]] = -C[8] * sqrt(0.5e1) * sqrt(0.2e1) / 0.40e2 - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.12e2 + 0.3e1 / 0.8e1 * C[4] * sqrt(0.2e1) + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - 0.3e1 / 0.40e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) - C[6] * sqrt(0.2e1) / 0.8e1 + C[2] * sqrt(0.3e1) / 0.4e1 - 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + C[1] / 0.4e1;
          C_in[n*n_p + 2][index[3]] = sqrt(0.3e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) / 0.40e2 + sqrt(0.2e1) * C[3] / 0.4e1 + sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 - sqrt(0.5e1) * sqrt(0.2e1) * C[5] / 0.8e1 - 0.3e1 / 0.40e2 * sqrt(0.2e1) * C[7] + sqrt(0.3e1) * C[6] * sqrt(0.2e1) / 0.8e1 + C[2] / 0.4e1 - sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.40e2 - sqrt(0.3e1) * C[1] / 0.4e1;
          C_in[n*n_p + 3][index[3]] = 0.3e1 / 0.40e2 * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.72e2 + sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2 + sqrt(0.6e1) * C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.72e2 + sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) / 0.60e2 - sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.24e2 + sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.60e2;
          C_in[n*n_p + 4][index[3]] = sqrt(0.3e1) * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) / 0.120e3 - sqrt(0.6e1) * C[3] * sqrt(0.2e1) / 0.24e2 - sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.48e2 + sqrt(0.6e1) * C[5] * sqrt(0.5e1) * sqrt(0.2e1) / 0.48e2 - sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.40e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.24e2 - sqrt(0.3e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3;
          C_in[n*n_p + 5][index[3]] = sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.8e1 + sqrt(0.5e1) * sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.72e2 - sqrt(0.5e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.48e2 + sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.144e3 - sqrt(0.5e1) * sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) / 0.120e3 - sqrt(0.5e1) * sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.24e2 - sqrt(0.5e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3;
          C_in[n*n_p + 6][index[3]] = C[8] * sqrt(0.5e1) / 0.32e2 + C[7] * sqrt(0.3e1) / 0.32e2 + C[6] / 0.32e2 + C[9] * sqrt(0.7e1) / 0.32e2;
          C_in[n*n_p + 7][index[3]] = -sqrt(0.3e1) * C[8] * sqrt(0.5e1) / 0.160e3 - 0.11e2 / 0.160e3 * C[7] - sqrt(0.3e1) * C[6] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.3e1) * C[9] * sqrt(0.7e1);
          C_in[n*n_p + 8][index[3]] = -0.3e1 / 0.32e2 * C[8] + sqrt(0.5e1) * C[7] * sqrt(0.3e1) / 0.160e3 + sqrt(0.5e1) * C[6] / 0.32e2 + sqrt(0.5e1) * C[9] * sqrt(0.7e1) / 0.160e3;
          C_in[n*n_p + 9][index[3]] = -sqrt(0.7e1) * C[8] * sqrt(0.5e1) / 0.160e3 + 0.3e1 / 0.160e3 * sqrt(0.7e1) * C[7] * sqrt(0.3e1) - sqrt(0.7e1) * C[6] / 0.32e2 + C[9] / 0.160e3;

        }
    }
}

__global__ void refine_c4(double **C_in,
                         int curr_num_elem, 
                         int *epos,
                         int *eIDlist, 
                         int N, 
                         int *pos_efree, int num_pos_efree,
                         int num_erefine) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    double eval[N_MAX];
    double U[N_MAX];
    int index[4];

    if(idx < num_erefine) {
        int eID = eIDlist[idx];
        int ePOS = epos[eID];
        const int n_p = 15;
        double C[n_p]; 

        index[0] = (idx + 0 * num_erefine < num_pos_efree) ? pos_efree[idx + 0 * num_erefine] : curr_num_elem + 0 * num_erefine + idx - num_pos_efree;
        index[1] = (idx + 1 * num_erefine < num_pos_efree) ? pos_efree[idx + 1 * num_erefine] : curr_num_elem + 1 * num_erefine + idx - num_pos_efree;
        index[2] = (idx + 2 * num_erefine < num_pos_efree) ? pos_efree[idx + 2 * num_erefine] : curr_num_elem + 2 * num_erefine + idx - num_pos_efree;
        index[3] = ePOS;

        for(int n=0; n < N; n++){
          for (int k = 0; k < n_p; k++) {
            C[k] = C_in[n*n_p + k][ePOS];
          }
          C_in[n*n_p + 0 ][index[0]] = C[7] * sqrt(0.3e1) / 0.8e1 - sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1 + C[9] * sqrt(0.7e1) / 0.8e1 + C[8] * sqrt(0.5e1) / 0.8e1 - C[12] / 0.8e1 - C[11] * sqrt(0.3e1) * sqrt(0.5e1) / 0.40e2 + C[4] / 0.4e1 - C[10] * sqrt(0.5e1) / 0.40e2 + C[3] * sqrt(0.3e1) / 0.12e2 + C[6] / 0.8e1 - sqrt(0.2e1) * C[1] / 0.2e1 + C[0] - 0.3e1 / 0.40e2 * C[14] * sqrt(0.5e1) - C[13] * sqrt(0.7e1) * sqrt(0.5e1) / 0.40e2 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.12e2;
          C_in[n*n_p + 1 ][index[0]] = 0.3e1 / 0.10e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) - 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + 0.3e1 / 0.20e2 * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - 0.5e1 / 0.8e1 * C[4] * sqrt(0.2e1) - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.3e1 + 0.3e1 / 0.8e1 * C[6] * sqrt(0.2e1) + C[1] / 0.2e1 + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2;
          C_in[n*n_p + 2 ][index[0]] = 0.3e1 / 0.40e2 * C[7] * sqrt(0.2e1) + C[2] / 0.2e1 + 0.3e1 / 0.20e2 * sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + 0.3e1 / 0.40e2 * sqrt(0.3e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 - 0.3e1 / 0.8e1 * C[5] * sqrt(0.5e1) * sqrt(0.2e1);
          C_in[n*n_p + 3 ][index[0]] = -0.7e1 / 0.80e2 * sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) - sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.240e3 + 0.7e1 / 0.240e3 * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + 0.25e2 / 0.336e3 * sqrt(0.6e1) * C[12] * sqrt(0.2e1) + sqrt(0.6e1) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) / 0.16e2 + 0.3e1 / 0.32e2 * sqrt(0.6e1) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - 0.3e1 / 0.16e2 * sqrt(0.6e1) * C[6] * sqrt(0.2e1) + 0.3e1 / 0.224e3 * sqrt(0.6e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) - 0.13e2 / 0.672e3 * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1);
          C_in[n*n_p + 4 ][index[0]] = -sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.10e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3 - 0.7e1 / 0.120e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + 0.5e1 / 0.42e2 * sqrt(0.3e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1) + sqrt(0.6e1) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) / 0.32e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2 - 0.5e1 / 0.224e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) + 0.17e2 / 0.672e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1);
          C_in[n*n_p + 5 ][index[0]] = 0.25e2 / 0.672e3 * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) + sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - sqrt(0.5e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.24e2 - sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.24e2 + 0.75e2 / 0.224e3 * sqrt(0.6e1) * C[14] * sqrt(0.2e1) + 0.5e1 / 0.672e3 * sqrt(0.5e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1);
          C_in[n*n_p + 6 ][index[0]] = 0.9e1 / 0.56e2 * C[12] - 0.3e1 / 0.40e2 * C[11] * sqrt(0.3e1) * sqrt(0.5e1) - C[10] * sqrt(0.5e1) / 0.5e1 + C[6] / 0.8e1 + 0.3e1 / 0.1120e4 * C[14] * sqrt(0.5e1) - 0.9e1 / 0.1120e4 * C[13] * sqrt(0.7e1) * sqrt(0.5e1);
          C_in[n*n_p + 7 ][index[0]] = C[7] / 0.8e1 - 0.15e2 / 0.56e2 * sqrt(0.3e1) * C[12] - C[11] * sqrt(0.5e1) / 0.8e1 - sqrt(0.3e1) * C[14] * sqrt(0.5e1) / 0.224e3 + 0.3e1 / 0.224e3 * sqrt(0.3e1) * C[13] * sqrt(0.7e1) * sqrt(0.5e1);
          C_in[n*n_p + 8 ][index[0]] = -0.45e2 / 0.224e3 * sqrt(0.7e1) * C[13] + C[8] / 0.8e1 + 0.15e2 / 0.224e3 * C[14] - sqrt(0.5e1) * C[12] / 0.14e2;
          C_in[n*n_p + 9 ][index[0]] = -C[13] * sqrt(0.5e1) / 0.32e2 + C[9] / 0.8e1 - 0.3e1 / 0.32e2 * sqrt(0.7e1) * C[14] * sqrt(0.5e1);
          C_in[n*n_p + 10][index[0]] = sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          C_in[n*n_p + 11][index[0]] = sqrt(0.10e2) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          C_in[n*n_p + 12][index[0]] = sqrt(0.5e1) * sqrt(0.10e2) * C[12] * sqrt(0.2e1) / 0.160e3;
          C_in[n*n_p + 13][index[0]] = sqrt(0.10e2) * C[13] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          C_in[n*n_p + 14][index[0]] = sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;


          C_in[n*n_p + 0 ][index[1]] = -C[10] * sqrt(0.5e1) / 0.8e1 - C[6] / 0.2e1 + C[3] * sqrt(0.3e1) / 0.4e1 + sqrt(0.2e1) * C[1] + C[0];
          C_in[n*n_p + 1 ][index[1]] = C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.2e1 + 0.3e1 / 0.4e1 * C[6] * sqrt(0.2e1) + C[1] / 0.2e1;
          C_in[n*n_p + 2 ][index[1]] = 0.3e1 / 0.4e1 * C[7] * sqrt(0.2e1) + sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.2e1 + C[2] / 0.2e1;
          C_in[n*n_p + 3 ][index[1]] = 0.5e1 / 0.32e2 * sqrt(0.6e1) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.4e1;
          C_in[n*n_p + 4 ][index[1]] = sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.4e1 + 0.5e1 / 0.32e2 * sqrt(0.6e1) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2;
          C_in[n*n_p + 5 ][index[1]] = sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.4e1 + 0.5e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1);
          C_in[n*n_p + 6 ][index[1]] = C[10] * sqrt(0.5e1) / 0.4e1 + C[6] / 0.8e1;
          C_in[n*n_p + 7 ][index[1]] = C[7] / 0.8e1 + C[11] * sqrt(0.5e1) / 0.4e1;
          C_in[n*n_p + 8 ][index[1]] = C[8] / 0.8e1 + sqrt(0.5e1) * C[12] / 0.4e1;
          C_in[n*n_p + 9 ][index[1]] = C[13] * sqrt(0.5e1) / 0.4e1 + C[9] / 0.8e1;
          C_in[n*n_p + 10][index[1]] = sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          C_in[n*n_p + 11][index[1]] = sqrt(0.10e2) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          C_in[n*n_p + 12][index[1]] = sqrt(0.5e1) * sqrt(0.10e2) * C[12] * sqrt(0.2e1) / 0.160e3;
          C_in[n*n_p + 13][index[1]] = sqrt(0.10e2) * C[13] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          C_in[n*n_p + 14][index[1]] = sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;

          // C_in[n*n_p + 0 ][index[2]] = -C[7] * sqrt(0.3e1) / 0.8e1 + sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1 - C[9] * sqrt(0.7e1) / 0.8e1 + C[8] * sqrt(0.5e1) / 0.8e1 - C[12] / 0.8e1 + sqrt(0.5e1) * sqrt(0.3e1) * C[11] / 0.40e2 - C[4] / 0.4e1 - C[10] * sqrt(0.5e1) / 0.40e2 + C[3] * sqrt(0.3e1) / 0.12e2 + C[6] / 0.8e1 - sqrt(0.2e1) * C[1] / 0.2e1 + C[0] - 0.3e1 / 0.40e2 * C[14] * sqrt(0.5e1) + C[13] * sqrt(0.7e1) * sqrt(0.5e1) / 0.40e2 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.12e2;
          // C_in[n*n_p + 1 ][index[2]] = -0.3e1 / 0.10e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) + 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + 0.3e1 / 0.20e2 * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + 0.5e1 / 0.8e1 * C[4] * sqrt(0.2e1) - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.3e1 + 0.3e1 / 0.8e1 * C[6] * sqrt(0.2e1) + C[1] / 0.2e1 + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2;
          // C_in[n*n_p + 2 ][index[2]] = 0.3e1 / 0.40e2 * C[7] * sqrt(0.2e1) + C[2] / 0.2e1 + 0.3e1 / 0.20e2 * sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) - 0.3e1 / 0.40e2 * sqrt(0.3e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 + 0.3e1 / 0.8e1 * C[5] * sqrt(0.5e1) * sqrt(0.2e1);
          // C_in[n*n_p + 3 ][index[2]] = 0.7e1 / 0.80e2 * sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) + sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.240e3 + 0.7e1 / 0.240e3 * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + 0.25e2 / 0.336e3 * sqrt(0.6e1) * C[12] * sqrt(0.2e1) - sqrt(0.6e1) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) / 0.16e2 + 0.3e1 / 0.32e2 * sqrt(0.6e1) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 - 0.3e1 / 0.16e2 * sqrt(0.6e1) * C[6] * sqrt(0.2e1) + 0.3e1 / 0.224e3 * sqrt(0.6e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) + 0.13e2 / 0.672e3 * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1);
          // C_in[n*n_p + 4 ][index[2]] = -sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.10e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3 + 0.7e1 / 0.120e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) - 0.5e1 / 0.42e2 * sqrt(0.3e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1) + sqrt(0.6e1) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) / 0.32e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2 + 0.5e1 / 0.224e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) + 0.17e2 / 0.672e3 * sqrt(0.3e1) * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1);
          // C_in[n*n_p + 5 ][index[2]] = sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.4e1 + 0.5e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1);
          // C_in[n*n_p + 6 ][index[2]] = 0.9e1 / 0.56e2 * C[12] + 0.3e1 / 0.40e2 * C[11] * sqrt(0.3e1) * sqrt(0.5e1) - C[10] * sqrt(0.5e1) / 0.5e1 + C[6] / 0.8e1 + 0.3e1 / 0.1120e4 * C[14] * sqrt(0.5e1) + 0.9e1 / 0.1120e4 * C[13] * sqrt(0.7e1) * sqrt(0.5e1);
          // C_in[n*n_p + 7 ][index[2]] = C[7] / 0.8e1 + 0.15e2 / 0.56e2 * sqrt(0.3e1) * C[12] - C[11] * sqrt(0.5e1) / 0.8e1 + sqrt(0.3e1) * C[14] * sqrt(0.5e1) / 0.224e3 + 0.3e1 / 0.224e3 * sqrt(0.3e1) * C[13] * sqrt(0.7e1) * sqrt(0.5e1);
          // C_in[n*n_p + 8 ][index[2]] = 0.45e2 / 0.224e3 * sqrt(0.7e1) * C[13] + C[8] / 0.8e1 + 0.15e2 / 0.224e3 * C[14] - sqrt(0.5e1) * C[12] / 0.14e2;
          // C_in[n*n_p + 9 ][index[2]] = -C[13] * sqrt(0.5e1) / 0.32e2 + C[9] / 0.8e1 + 0.3e1 / 0.32e2 * sqrt(0.7e1) * C[14] * sqrt(0.5e1);
          // C_in[n*n_p + 10][index[2]] = sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          // C_in[n*n_p + 11][index[2]] = sqrt(0.10e2) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          // C_in[n*n_p + 12][index[2]] = sqrt(0.5e1) * sqrt(0.10e2) * C[12] * sqrt(0.2e1) / 0.160e3;
          // C_in[n*n_p + 13][index[2]] = sqrt(0.10e2) * C[13] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;
          // C_in[n*n_p + 14][index[2]] = sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) / 0.160e3;

          C_in[n*n_p + 0 ][index[2]] = sqrt(0.2e1) * C[2] * sqrt(0.3e1) / 0.2e1 - C[12] / 0.8e1 + C[0] - C[4] / 0.4e1 + C[3] * sqrt(0.3e1) / 0.12e2 - C[10] * sqrt(0.5e1) / 0.40e2 - 0.3e1 / 0.40e2 * C[14] * sqrt(0.5e1) + C[8] * sqrt(0.5e1) / 0.8e1 + C[11] * sqrt(0.3e1) * sqrt(0.5e1) / 0.40e2 + C[13] * sqrt(0.7e1) * sqrt(0.5e1) / 0.40e2 + C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.12e2 - C[9] * sqrt(0.7e1) / 0.8e1 - C[7] * sqrt(0.3e1) / 0.8e1 + C[6] / 0.8e1 - sqrt(0.2e1) * C[1] / 0.2e1;
          C_in[n*n_p + 1 ][index[2]] = 0.5e1 / 0.8e1 * C[4] * sqrt(0.2e1) - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.3e1 + 0.3e1 / 0.20e2 * sqrt(0.2e1) * sqrt(0.5e1) * C[8] + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2 + 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) - 0.3e1 / 0.10e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) + 0.3e1 / 0.8e1 * C[6] * sqrt(0.2e1) + C[1] / 0.2e1;
          C_in[n*n_p + 2 ][index[2]] = C[2] / 0.2e1 - sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 - 0.3e1 / 0.40e2 * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) * C[8] + 0.3e1 / 0.8e1 * C[5] * sqrt(0.5e1) * sqrt(0.2e1) + 0.3e1 / 0.20e2 * sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) + 0.3e1 / 0.40e2 * C[7] * sqrt(0.2e1);
          C_in[n*n_p + 3 ][index[2]] = 0.25e2 / 0.168e3 * sqrt(0.3e1) * C[12] + C[3] / 0.4e1 + 0.3e1 / 0.16e2 * sqrt(0.3e1) * C[10] * sqrt(0.5e1) + 0.3e1 / 0.112e3 * sqrt(0.3e1) * sqrt(0.5e1) * C[14] + 0.7e1 / 0.120e3 * sqrt(0.3e1) * sqrt(0.5e1) * C[8] - 0.3e1 / 0.8e1 * C[11] * sqrt(0.5e1) + 0.13e2 / 0.336e3 * sqrt(0.3e1) * C[13] * sqrt(0.7e1) * sqrt(0.5e1) + sqrt(0.3e1) * C[9] * sqrt(0.7e1) / 0.120e3 + 0.21e2 / 0.40e2 * C[7] - 0.3e1 / 0.8e1 * sqrt(0.3e1) * C[6];
          C_in[n*n_p + 4 ][index[2]] = -0.5e1 / 0.7e1 * C[12] + C[4] / 0.4e1 + 0.15e2 / 0.112e3 * C[14] * sqrt(0.5e1) + 0.7e1 / 0.20e2 * C[8] * sqrt(0.5e1) + C[11] * sqrt(0.3e1) * sqrt(0.5e1) / 0.16e2 + 0.17e2 / 0.112e3 * C[13] * sqrt(0.7e1) * sqrt(0.5e1) + C[9] * sqrt(0.7e1) / 0.20e2 - C[7] * sqrt(0.3e1) / 0.5e1;
          C_in[n*n_p + 5 ][index[2]] =  C[5] / 0.4e1 - 0.25e2 / 0.336e3 * sqrt(0.3e1) * C[13] * sqrt(0.7e1) - sqrt(0.3e1) * C[8] / 0.12e2 + 0.75e2 / 0.112e3 * sqrt(0.3e1) * C[14] + sqrt(0.5e1) * sqrt(0.3e1) * C[9] * sqrt(0.7e1) / 0.12e2 + 0.5e1 / 0.336e3 * sqrt(0.5e1) * sqrt(0.3e1) * C[12];
          C_in[n*n_p + 6 ][index[2]] = 0.9e1 / 0.56e2 * C[12] - C[10] * sqrt(0.5e1) / 0.5e1 + 0.3e1 / 0.1120e4 * C[14] * sqrt(0.5e1) + 0.3e1 / 0.40e2 * C[11] * sqrt(0.3e1) * sqrt(0.5e1) + 0.9e1 / 0.1120e4 * C[13] * sqrt(0.7e1) * sqrt(0.5e1) + C[6] / 0.8e1;
          C_in[n*n_p + 7 ][index[2]] = 0.15e2 / 0.56e2 * sqrt(0.3e1) * C[12] + sqrt(0.3e1) * sqrt(0.5e1) * C[14] / 0.224e3 - C[11] * sqrt(0.5e1) / 0.8e1 + 0.3e1 / 0.224e3 * sqrt(0.3e1) * C[13] * sqrt(0.7e1) * sqrt(0.5e1) + C[7] / 0.8e1;
          C_in[n*n_p + 8 ][index[2]] = 0.45e2 / 0.224e3 * C[13] * sqrt(0.7e1) + C[8] / 0.8e1 + 0.15e2 / 0.224e3 * C[14] - sqrt(0.5e1) * C[12] / 0.14e2;
          C_in[n*n_p + 9 ][index[2]] = -sqrt(0.5e1) * C[13] / 0.32e2 + 0.3e1 / 0.32e2 * sqrt(0.7e1) * sqrt(0.5e1) * C[14] + C[9] / 0.8e1;
          C_in[n*n_p + 10][index[2]] = C[10] / 0.16e2;
          C_in[n*n_p + 11][index[2]] = C[11] / 0.16e2;
          C_in[n*n_p + 12][index[2]] = C[12] / 0.16e2;
          C_in[n*n_p + 13][index[2]] = C[13] / 0.16e2;
          C_in[n*n_p + 14][index[2]] = C[14] / 0.16e2;

          C_in[n*n_p + 0 ][index[3]] = -C[8] * sqrt(0.5e1) / 0.4e1 + C[12] / 0.4e1 + 0.7e1 / 0.40e2 * C[10] * sqrt(0.5e1) - 0.5e1 / 0.12e2 * C[3] * sqrt(0.3e1) + C[6] / 0.4e1 + C[0] + 0.3e1 / 0.20e2 * C[14] * sqrt(0.5e1) - C[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.6e1;
          C_in[n*n_p + 1 ][index[3]] = -0.3e1 / 0.40e2 * C[7] * sqrt(0.3e1) * sqrt(0.2e1) + C[2] * sqrt(0.3e1) / 0.4e1 - 0.3e1 / 0.40e2 * C[9] * sqrt(0.7e1) * sqrt(0.2e1) - C[8] * sqrt(0.5e1) * sqrt(0.2e1) / 0.40e2 - C[12] * sqrt(0.2e1) / 0.8e1 - 0.3e1 / 0.40e2 * C[11] * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) + 0.3e1 / 0.8e1 * C[4] * sqrt(0.2e1) + C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.10e2 - C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.12e2 - C[6] * sqrt(0.2e1) / 0.8e1 + C[1] / 0.4e1 - 0.3e1 / 0.40e2 * C[14] * sqrt(0.2e1) * sqrt(0.5e1) - 0.3e1 / 0.40e2 * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) + C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.24e2;
          C_in[n*n_p + 2 ][index[3]] = -0.3e1 / 0.40e2 * C[7] * sqrt(0.2e1) + C[2] / 0.4e1 - sqrt(0.3e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.40e2 + sqrt(0.3e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) / 0.40e2 + sqrt(0.3e1) * C[12] * sqrt(0.2e1) / 0.8e1 - 0.3e1 / 0.40e2 * C[11] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.3e1) * C[4] * sqrt(0.2e1) / 0.8e1 - sqrt(0.3e1) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.10e2 + sqrt(0.2e1) * C[3] / 0.4e1 + sqrt(0.3e1) * C[6] * sqrt(0.2e1) / 0.8e1 - sqrt(0.3e1) * C[1] / 0.4e1 + 0.3e1 / 0.40e2 * sqrt(0.3e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) - sqrt(0.3e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) / 0.40e2 - C[5] * sqrt(0.5e1) * sqrt(0.2e1) / 0.8e1;
          C_in[n*n_p + 3 ][index[3]] = sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) / 0.60e2 + sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.60e2 + 0.3e1 / 0.40e2 * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) + 0.9e1 / 0.224e3 * sqrt(0.6e1) * C[12] * sqrt(0.2e1) - 0.7e1 / 0.480e3 * sqrt(0.6e1) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.24e2 + sqrt(0.6e1) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.480e3 + sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.72e2 - sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.24e2 - sqrt(0.6e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) / 0.140e3 + 0.13e2 / 0.1680e4 * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.6e1) * C[5] * sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) / 0.72e2;
          C_in[n*n_p + 4 ][index[3]] = -sqrt(0.6e1) * C[7] * sqrt(0.2e1) / 0.40e2 - sqrt(0.3e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3 + sqrt(0.3e1) * sqrt(0.6e1) * C[8] * sqrt(0.5e1) * sqrt(0.2e1) / 0.120e3 + 0.71e2 / 0.1344e4 * sqrt(0.3e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1) + 0.7e1 / 0.320e3 * sqrt(0.6e1) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) - sqrt(0.3e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.48e2 - sqrt(0.3e1) * sqrt(0.6e1) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.480e3 - sqrt(0.6e1) * C[3] * sqrt(0.2e1) / 0.24e2 + sqrt(0.3e1) * sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.24e2 - 0.17e2 / 0.1120e4 * sqrt(0.3e1) * sqrt(0.6e1) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) - 0.13e2 / 0.3360e4 * sqrt(0.3e1) * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.6e1) * C[5] * sqrt(0.5e1) * sqrt(0.2e1) / 0.48e2;
          C_in[n*n_p + 5 ][index[3]] = -sqrt(0.5e1) * sqrt(0.6e1) * C[7] * sqrt(0.3e1) * sqrt(0.2e1) / 0.120e3 - sqrt(0.5e1) * sqrt(0.6e1) * C[9] * sqrt(0.7e1) * sqrt(0.2e1) / 0.120e3 + sqrt(0.6e1) * C[8] * sqrt(0.2e1) / 0.8e1 - sqrt(0.5e1) * sqrt(0.6e1) * C[12] * sqrt(0.2e1) / 0.64e2 + 0.7e1 / 0.192e3 * sqrt(0.6e1) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) - sqrt(0.5e1) * sqrt(0.6e1) * C[4] * sqrt(0.2e1) / 0.48e2 + sqrt(0.6e1) * C[10] * sqrt(0.2e1) / 0.96e2 + sqrt(0.5e1) * sqrt(0.6e1) * C[3] * sqrt(0.2e1) * sqrt(0.3e1) / 0.72e2 - sqrt(0.5e1) * sqrt(0.6e1) * C[6] * sqrt(0.2e1) / 0.24e2 + sqrt(0.6e1) * C[14] * sqrt(0.2e1) / 0.32e2 - 0.13e2 / 0.672e3 * sqrt(0.6e1) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) + sqrt(0.6e1) * C[5] * sqrt(0.2e1) * sqrt(0.3e1) / 0.144e3;
          C_in[n*n_p + 6 ][index[3]] = C[7] * sqrt(0.3e1) / 0.32e2 + C[9] * sqrt(0.7e1) / 0.32e2 + C[8] * sqrt(0.5e1) / 0.32e2 + 0.3e1 / 0.14e2 * C[12] - 0.3e1 / 0.80e2 * C[10] * sqrt(0.5e1) + C[6] / 0.32e2 + 0.39e2 / 0.1120e4 * C[14] * sqrt(0.5e1) + 0.15e2 / 0.224e3 * C[13] * sqrt(0.7e1) * sqrt(0.5e1);
          C_in[n*n_p + 7 ][index[3]] = -0.11e2 / 0.160e3 * C[7] + 0.3e1 / 0.160e3 * sqrt(0.3e1) * C[9] * sqrt(0.7e1) - sqrt(0.3e1) * C[8] * sqrt(0.5e1) / 0.160e3 - 0.3e1 / 0.28e2 * sqrt(0.3e1) * C[12] + 0.3e1 / 0.80e2 * sqrt(0.3e1) * C[10] * sqrt(0.5e1) - sqrt(0.3e1) * C[6] / 0.32e2 - 0.9e1 / 0.1120e4 * sqrt(0.3e1) * C[14] * sqrt(0.5e1) + 0.3e1 / 0.224e3 * sqrt(0.3e1) * C[13] * sqrt(0.7e1) * sqrt(0.5e1);
          C_in[n*n_p + 8 ][index[3]] = sqrt(0.5e1) * C[7] * sqrt(0.3e1) / 0.160e3 + sqrt(0.5e1) * C[9] * sqrt(0.7e1) / 0.160e3 - 0.3e1 / 0.32e2 * C[8] - 0.3e1 / 0.16e2 * C[10] + sqrt(0.5e1) * C[6] / 0.32e2 - 0.3e1 / 0.32e2 * C[14] + 0.15e2 / 0.224e3 * C[13] * sqrt(0.7e1);
          C_in[n*n_p + 9 ][index[3]] = 0.3e1 / 0.160e3 * sqrt(0.7e1) * C[7] * sqrt(0.3e1) + C[9] / 0.160e3 - sqrt(0.7e1) * C[8] * sqrt(0.5e1) / 0.160e3 - 0.3e1 / 0.28e2 * sqrt(0.7e1) * C[12] + 0.3e1 / 0.80e2 * sqrt(0.7e1) * C[10] * sqrt(0.5e1) - sqrt(0.7e1) * C[6] / 0.32e2 - 0.9e1 / 0.1120e4 * sqrt(0.7e1) * C[14] * sqrt(0.5e1) + 0.3e1 / 0.32e2 * C[13] * sqrt(0.5e1);
          C_in[n*n_p + 10][index[3]] = sqrt(0.10e2) * C[12] * sqrt(0.2e1) / 0.160e3 + sqrt(0.10e2) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) / 0.800e3 + sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.800e3 + 0.3e1 / 0.800e3 * sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.10e2) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) / 0.800e3;
          C_in[n*n_p + 11][index[3]] = -sqrt(0.3e1) * sqrt(0.10e2) * C[12] * sqrt(0.2e1) / 0.320e3 - sqrt(0.10e2) * C[11] * sqrt(0.2e1) * sqrt(0.5e1) / 0.320e3 - sqrt(0.3e1) * sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.800e3 + sqrt(0.3e1) * sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) / 0.400e3;
          C_in[n*n_p + 12][index[3]] = -0.3e1 / 0.2240e4 * sqrt(0.5e1) * sqrt(0.10e2) * C[12] * sqrt(0.2e1) + sqrt(0.10e2) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) / 0.320e3 + sqrt(0.10e2) * C[10] * sqrt(0.2e1) / 0.160e3 + 0.3e1 / 0.560e3 * sqrt(0.10e2) * C[14] * sqrt(0.2e1) - sqrt(0.10e2) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) / 0.280e3;
          C_in[n*n_p + 13][index[3]] = sqrt(0.7e1) * sqrt(0.10e2) * C[12] * sqrt(0.2e1) / 0.280e3 - sqrt(0.7e1) * sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) / 0.800e3 + 0.3e1 / 0.11200e5 * sqrt(0.7e1) * sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) - sqrt(0.10e2) * C[13] * sqrt(0.2e1) * sqrt(0.5e1) / 0.320e3;
          C_in[n*n_p + 14][index[3]] = 0.3e1 / 0.560e3 * sqrt(0.10e2) * C[12] * sqrt(0.2e1) - sqrt(0.10e2) * C[11] * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) / 0.400e3 + 0.3e1 / 0.800e3 * sqrt(0.10e2) * C[10] * sqrt(0.2e1) * sqrt(0.5e1) + sqrt(0.10e2) * C[14] * sqrt(0.2e1) * sqrt(0.5e1) / 0.11200e5 - 0.3e1 / 0.11200e5 * sqrt(0.10e2) * C[13] * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1);

        }
    }
}





__global__ void refine_c(double **C,
                         int curr_num_elem, 
                         int *epos,
                         int *eIDlist, 
                         int n_p, int N, int n_quad,
                         double *d_basis_refined, double *d_w, double *d_basis,
                         int *pos_efree, int num_pos_efree,
                         int num_erefine) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, j, k, n;
    double eval[N_MAX];
    double U[N_MAX];
    int index[4];
    double C_coarse[N_MAX*NP_MAX]; 

    if(idx < num_erefine) {
        int eID = eIDlist[idx];
        int ePOS = epos[eID];

        for (k = 0; k < n_p; k++) {
            for (n = 0; n < N; n++) {
                C_coarse[n*n_p + k] = C[n*n_p + k][ePOS];

            }
        }

        index[0] = (idx + 0 * num_erefine < num_pos_efree) ? pos_efree[idx + 0 * num_erefine] : curr_num_elem + 0 * num_erefine + idx - num_pos_efree;
        index[1] = (idx + 1 * num_erefine < num_pos_efree) ? pos_efree[idx + 1 * num_erefine] : curr_num_elem + 1 * num_erefine + idx - num_pos_efree;
        index[2] = (idx + 2 * num_erefine < num_pos_efree) ? pos_efree[idx + 2 * num_erefine] : curr_num_elem + 2 * num_erefine + idx - num_pos_efree;
        index[3] = ePOS;

    // element 0
        for (i = 0; i < n_p; i++) {

            for (n = 0; n < N; n++) { 
                    U[n] = 0.;
            }

            for (j = 0; j < n_quad; j++) {

                // set eval to 0
                for (n = 0; n < N; n++) {
                    eval[n] = 0.;
                }

                // evaluate the solution at the correct point
                for (k = 0; k < n_p; k++) {
                    for (n = 0; n < N; n++) {
                        eval[n] += C_coarse[n*n_p + k] *  
                                   d_basis_refined[n_p * n_quad * 0 + k * n_quad + j];

                    }
                }


                // use the projection
                for (n = 0; n < N; n++) {
                    U[n] += d_w[j] * eval[n] * d_basis[i * n_quad + j];
                }

            }

            // store the new coefficients
            for (n = 0; n < N; n++) {
                C[n*n_p + i][index[0]] = U[n];
            }

        }



        // element 1
        for (i = 0; i < n_p; i++) {

            for (n = 0; n < N; n++) {
                    U[n] = 0.;
            }

            for (j = 0; j < n_quad; j++) {

                // set eval to 0
                for (n = 0; n < N; n++) {
                    eval[n] = 0.;
                }

                // evaluate the solution at the correct point
                for (k = 0; k < n_p; k++) {
                    for (n = 0; n < N; n++) {
                        eval[n] += C_coarse[n*n_p + k] * 
                                   d_basis_refined[n_p * n_quad * 1 + k * n_quad + j];
                    }
                }

                // use the projection
                for (n = 0; n < N; n++) {
                    U[n] += d_w[j] * eval[n] * d_basis[i * n_quad + j];
                }

            }

            // store the new coefficients
            for (n = 0; n < N; n++) {
                C[n*n_p + i][index[1]] = U[n];
            }
        }

        // element 2
        for (i = 0; i < n_p; i++) {

            for (n = 0; n < N; n++) {
                    U[n] = 0.;
            }

            for (j = 0; j < n_quad; j++) {

                // set eval to 0
                for (n = 0; n < N; n++) {
                    eval[n] = 0.;
                }

                // evaluate the solution at the correct point
                for (k = 0; k < n_p; k++) {
                    for (n = 0; n < N; n++) {
                        eval[n] += C_coarse[n*n_p + k] * 
                                   d_basis_refined[n_p * n_quad * 2 + k * n_quad + j];
                    }
                }

                // use the projection
                for (n = 0; n < N; n++) {
                    U[n] += d_w[j] * eval[n] * d_basis[i * n_quad + j];
                }

            }

            // store the new coefficients
            for (n = 0; n < N; n++) {
                C[n*n_p + i][index[2]] = U[n];
            }
        }

        // element 2
        for (i = 0; i < n_p; i++) {

            for (n = 0; n < N; n++) {
                    U[n] = 0.;
            }

            for (j = 0; j < n_quad; j++) {

                // set eval to 0
                for (n = 0; n < N; n++) {
                    eval[n] = 0.;
                } 

                // evaluate the solution at the correct point
                for (k = 0; k < n_p; k++) {
                    for (n = 0; n < N; n++) {
                        eval[n] += C_coarse[n*n_p + k] * 
                                   d_basis_refined[n_p * n_quad * 3 + k * n_quad + j];
                    }
                }

                // use the projection
                for (n = 0; n < N; n++) {
                    U[n] += d_w[j] * eval[n] * d_basis[i * n_quad + j];
                }

            }

            // store the new coefficients
            for (n = 0; n < N; n++) {
                C[n*n_p + i][index[3]] = U[n];
            }
        }
    }
}


__global__ void coarsen_c(int *ecoarsen, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          double **C, 
                          double *J,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          int *epos,
                          int n_p, int N, int n_quad,
                          double *d_basis_refined, double *d_w, double *d_basis,
                          int num_ecadd) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    double eval1[N_MAX], eval2[N_MAX], eval3[N_MAX], eval4[N_MAX], U[N_MAX*NP_MAX];
    double J1, J2, J3, J4, Jcoarse;
    double x1, x2, x3, y1, y2, y3;
    
    if(idx < num_ecadd) {
        int eIDcoarsen, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
        int ePOScoarsen, ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
        int new_pos;
        int i, n, j, k;

        eIDcoarsen = ecoarsen[idx];
        eIDchild1 = echild1[eIDcoarsen];
        eIDchild2 = echild2[eIDcoarsen];
        eIDchild3 = echild3[eIDcoarsen];
        eIDchild4 = echild4[eIDcoarsen];

        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];

        new_pos = ePOSchild4;

        // read new vertex points
        x1 = V1x[ePOSchild1];
        y1 = V1y[ePOSchild1];
        x2 = V2x[ePOSchild2];
        y2 = V2y[ePOSchild2];
        x3 = V3x[ePOSchild3];
        y3 = V3y[ePOSchild3];

        // calculate jacobian determinant
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        J1 = J[ePOSchild1];
        J2 = J[ePOSchild2];
        J3 = J[ePOSchild3];
        J4 = J[ePOSchild4];
        Jcoarse = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);

        // if(idx ==2)
        //     for(n = 0; n< 1; n++)
        //         for(i = 0 ; i < n_p; i++)
        //             printf("\nidx %i, %i %i, %lf %lf %lf %lf %lf\n",idx, n, i, C[n*n_p + i][ePOSchild1], C[n*n_p + i][ePOSchild2], C[n*n_p + i][ePOSchild3], C[n*n_p + i][ePOSchild4], J4/ Jcoarse);

        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++){
                U[n*n_p + i] = 0.;
            }
        }

        for (i = 0; i < n_p; i++) {
            for (j = 0; j < n_quad; j++) {

                // set eval to 0
                for (n = 0; n < N; n++) {
                    eval1[n] = 0.;
                    eval2[n] = 0.;
                    eval3[n] = 0.;
                    eval4[n] = 0.;
                }

                // evaluate the solution at the correct point
                for (k = 0; k < n_p; k++) {
                    for (n = 0; n < N; n++) {
                        eval1[n] += C[n*n_p + k][ePOSchild1] * 
                                    d_basis[k * n_quad + j];
                        eval2[n] += C[n*n_p + k][ePOSchild2] * 
                                    d_basis[k * n_quad + j];
                        eval3[n] += C[n*n_p + k][ePOSchild3] * 
                                    d_basis[k * n_quad + j];
                        eval4[n] += C[n*n_p + k][ePOSchild4] * 
                                    d_basis[k * n_quad + j];
                    }
                }

                // use the projection
                for (n = 0; n < N; n++) {
                    U[n*n_p + i] += d_w[j] * eval1[n] * d_basis_refined[n_p * n_quad * 0 + i * n_quad + j] * J1/ Jcoarse;
                    U[n*n_p + i] += d_w[j] * eval2[n] * d_basis_refined[n_p * n_quad * 1 + i * n_quad + j] * J2/ Jcoarse;
                    U[n*n_p + i] += d_w[j] * eval3[n] * d_basis_refined[n_p * n_quad * 2 + i * n_quad + j] * J3/ Jcoarse;
                    U[n*n_p + i] += d_w[j] * eval4[n] * d_basis_refined[n_p * n_quad * 3 + i * n_quad + j] * J4/ Jcoarse;
                }

            }
        }




            // store the new coefficients
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                C[n*n_p + i][new_pos] = U[n*n_p + i] ;
            }
        }



        // if(idx ==2)
        //     for(n = 0; n< 1; n++)
        //         for(i = 0 ; i < n_p; i++)
        //             printf("\nidx %i, %i %i, new %lf \n",idx, n, i, C[n*n_p + i][ePOSchild4]);

    }
}       


__global__ void coarsen_c1(int *ecoarsen, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          double **C,  int *epos,
                          int N, 
                          int num_ecadd) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx < num_ecadd) {
        int eIDcoarsen, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
        int ePOScoarsen, ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
        int new_pos;
        const int n_p = 3;
        eIDcoarsen = ecoarsen[idx];
        eIDchild1 = echild1[eIDcoarsen];
        eIDchild2 = echild2[eIDcoarsen];
        eIDchild3 = echild3[eIDcoarsen];
        eIDchild4 = echild4[eIDcoarsen];

        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];
        new_pos = ePOSchild4;

        double C_1[3];
        double C_2[3];
        double C_3[3];
        double C_4[3];

        for(int n = 0; n < N; n++){
          C_1[0] = C[n*n_p + 0][ePOSchild1];
          C_1[1] = C[n*n_p + 1][ePOSchild1];
          C_1[2] = C[n*n_p + 2][ePOSchild1];

          C_2[0] = C[n*n_p + 0][ePOSchild2];
          C_2[1] = C[n*n_p + 1][ePOSchild2];
          C_2[2] = C[n*n_p + 2][ePOSchild2];

          C_3[0] = C[n*n_p + 0][ePOSchild3];
          C_3[1] = C[n*n_p + 1][ePOSchild3];
          C_3[2] = C[n*n_p + 2][ePOSchild3];

          C_4[0] = C[n*n_p + 0][ePOSchild4];
          C_4[1] = C[n*n_p + 1][ePOSchild4];
          C_4[2] = C[n*n_p + 2][ePOSchild4];

          C[n*n_p + 0][new_pos] = C_1[0] / 0.4e1 + C_2[0] / 0.4e1 + C_3[0] / 0.4e1 + C_4[0] / 0.4e1;
          C[n*n_p + 1][new_pos] = C_1[1] / 0.8e1 - C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[1] / 0.8e1 + C_2[0] * sqrt(0.2e1) / 0.4e1 + C_3[1] / 0.8e1 - C_3[0] * sqrt(0.2e1) / 0.8e1 - C_4[2] * sqrt(0.3e1) / 0.16e2 + C_4[1] / 0.16e2;
          C[n*n_p + 2][new_pos] = C_1[2] / 0.8e1 - sqrt(0.3e1) * C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[2] / 0.8e1 + C_3[2] / 0.8e1 + sqrt(0.3e1) * C_3[0] * sqrt(0.2e1) / 0.8e1 + C_4[2] / 0.16e2 + sqrt(0.3e1) * C_4[1] / 0.16e2;
          
          // C_coarse[0] =  0.25*C_1[0] + 0.25*C_2[0] +0.25*C_3[0] +0.25*C_4[0]; 
          // C_coarse[1] = (-sqrt(2.)/8.) * C_1[0] + (sqrt(2.)/4.)* C_2[0] + (-sqrt(2.)/8.) * C_3[0]
          //              +(1./8.)*C_1[1]+(1./8.)*C_2[1]+(1./8.)*C_3[1]+(1./16.)*C_4[1]
          //              +(-sqrt(3.)/16.)*C_4[2];

          // C_coarse[2] = (-sqrt(6.)/8.) * C_1[0] + (sqrt(6.)/8.) * C_3[0]
          //              +(sqrt(3.)/16.)*C_4[1]
          //              +(1./8.)*C_1[2] + (1./8.)*C_2[2] + (1./8.)*C_3[2] + (1./16.)*C_4[2];
        
        }


    }
}           

__global__ void coarsen_c2(int *ecoarsen, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          double **C,  int *epos,
                          int N, 
                          int num_ecadd) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx < num_ecadd) {
        int eIDcoarsen, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
        int ePOScoarsen, ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
        int new_pos;
        const int n_p = 6;
        eIDcoarsen = ecoarsen[idx];
        eIDchild1 = echild1[eIDcoarsen];
        eIDchild2 = echild2[eIDcoarsen];
        eIDchild3 = echild3[eIDcoarsen];
        eIDchild4 = echild4[eIDcoarsen];

        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];
        new_pos = ePOSchild4;

        double C_1[6];
        double C_2[6];
        double C_3[6];
        double C_4[6];

        for(int n = 0; n < N; n++){
          C_1[0] = C[n*n_p + 0][ePOSchild1];
          C_1[1] = C[n*n_p + 1][ePOSchild1];
          C_1[2] = C[n*n_p + 2][ePOSchild1];
          C_1[3] = C[n*n_p + 3][ePOSchild1];
          C_1[4] = C[n*n_p + 4][ePOSchild1];
          C_1[5] = C[n*n_p + 5][ePOSchild1];

          C_2[0] = C[n*n_p + 0][ePOSchild2];
          C_2[1] = C[n*n_p + 1][ePOSchild2];
          C_2[2] = C[n*n_p + 2][ePOSchild2];
          C_2[3] = C[n*n_p + 3][ePOSchild2];
          C_2[4] = C[n*n_p + 4][ePOSchild2];
          C_2[5] = C[n*n_p + 5][ePOSchild2];

          C_3[0] = C[n*n_p + 0][ePOSchild3];
          C_3[1] = C[n*n_p + 1][ePOSchild3];
          C_3[2] = C[n*n_p + 2][ePOSchild3];
          C_3[3] = C[n*n_p + 3][ePOSchild3];
          C_3[4] = C[n*n_p + 4][ePOSchild3];
          C_3[5] = C[n*n_p + 5][ePOSchild3];

          C_4[0] = C[n*n_p + 0][ePOSchild4];
          C_4[1] = C[n*n_p + 1][ePOSchild4];
          C_4[2] = C[n*n_p + 2][ePOSchild4];
          C_4[3] = C[n*n_p + 3][ePOSchild4];
          C_4[4] = C[n*n_p + 4][ePOSchild4];
          C_4[5] = C[n*n_p + 5][ePOSchild4];

          C[n*n_p + 0][new_pos] = C_1[0] / 0.4e1 + C_2[0] / 0.4e1 + C_3[0] / 0.4e1 + C_4[0] / 0.4e1;
          C[n*n_p + 1][new_pos] = C_1[1] / 0.8e1 - C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[1] / 0.8e1 + C_2[0] * sqrt(0.2e1) / 0.4e1 + C_3[1] / 0.8e1 - C_3[0] * sqrt(0.2e1) / 0.8e1 - C_4[2] * sqrt(0.3e1) / 0.16e2 + C_4[1] / 0.16e2;
          C[n*n_p + 2][new_pos] = C_1[2] / 0.8e1 - sqrt(0.3e1) * C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[2] / 0.8e1 + C_3[2] / 0.8e1 + sqrt(0.3e1) * C_3[0] * sqrt(0.2e1) / 0.8e1 + C_4[2] / 0.16e2 + sqrt(0.3e1) * C_4[1] / 0.16e2;
          C[n*n_p + 3][new_pos] = C_1[3] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] / 0.12e2 + sqrt(0.3e1) * C_1[0] / 0.48e2 + C_2[3] / 0.16e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_2[1] / 0.8e1 + sqrt(0.3e1) * C_2[0] / 0.16e2 + C_3[3] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] / 0.12e2 + sqrt(0.3e1) * C_3[0] / 0.48e2 - 0.5e1 / 0.48e2 * sqrt(0.3e1) * C_4[0] + sqrt(0.2e1) * C_4[2] / 0.16e2 - sqrt(0.3e1) * C_4[4] / 0.48e2 + C_4[5] * sqrt(0.5e1) / 0.48e2 + C_4[3] / 0.48e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] / 0.48e2;
          C[n*n_p + 4][new_pos] = C_1[4] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_1[2] / 0.32e2 - 0.5e1 / 0.32e2 * sqrt(0.2e1) * C_1[1] + C_1[0] / 0.16e2 + C_2[4] / 0.16e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_2[2] / 0.8e1 + C_3[4] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_3[2] / 0.32e2 + 0.5e1 / 0.32e2 * sqrt(0.2e1) * C_3[1] - C_3[0] / 0.16e2 - C_4[4] / 0.32e2 + C_4[3] * sqrt(0.3e1) / 0.48e2 - C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.96e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_4[2] / 0.32e2 + 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_4[1];
          C[n*n_p + 5][new_pos] = C_1[5] / 0.16e2 - 0.3e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[2] + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] / 0.96e2 + sqrt(0.5e1) * sqrt(0.3e1) * C_1[0] / 0.48e2 + C_2[5] / 0.16e2 + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] / 0.96e2 + 0.3e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[2] + sqrt(0.5e1) * sqrt(0.3e1) * C_3[0] / 0.48e2 + C_3[5] / 0.16e2 + C_4[5] / 0.96e2 + sqrt(0.5e1) * C_4[3] / 0.48e2 + sqrt(0.5e1) * sqrt(0.3e1) * C_4[4] / 0.96e2 - sqrt(0.5e1) * sqrt(0.2e1) * C_4[2] / 0.32e2 + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] / 0.96e2 - sqrt(0.5e1) * sqrt(0.3e1) * C_4[0] / 0.24e2;
        }


    }
} 


__global__ void coarsen_c3(int *ecoarsen, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          double **C,  int *epos,
                          int N, 
                          int num_ecadd) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx < num_ecadd) {
        int eIDcoarsen, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
        int ePOScoarsen, ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
        int new_pos;
        const int n_p = 10;
        eIDcoarsen = ecoarsen[idx];
        eIDchild1 = echild1[eIDcoarsen];
        eIDchild2 = echild2[eIDcoarsen];
        eIDchild3 = echild3[eIDcoarsen];
        eIDchild4 = echild4[eIDcoarsen];

        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];
        new_pos = ePOSchild4;

        double C_1[10];
        double C_2[10];
        double C_3[10];
        double C_4[10];

        for(int n = 0; n < N; n++){
          for(int i = 0; i< 10; i++){
            C_1[i] = C[n*n_p + i][ePOSchild1];
            C_2[i] = C[n*n_p + i][ePOSchild2];
            C_3[i] = C[n*n_p + i][ePOSchild3];
            C_4[i] = C[n*n_p + i][ePOSchild4];
          }
            C[n*n_p + 0][new_pos] = C_1[0] / 0.4e1 + C_2[0] / 0.4e1 + C_3[0] / 0.4e1 + C_4[0] / 0.4e1;
            C[n*n_p + 1][new_pos] = C_1[1] / 0.8e1 - C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[1] / 0.8e1 + C_2[0] * sqrt(0.2e1) / 0.4e1 + C_3[1] / 0.8e1 - C_3[0] * sqrt(0.2e1) / 0.8e1 - C_4[2] * sqrt(0.3e1) / 0.16e2 + C_4[1] / 0.16e2;
            C[n*n_p + 2][new_pos] = C_1[2] / 0.8e1 - sqrt(0.3e1) * C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[2] / 0.8e1 + C_3[2] / 0.8e1 + sqrt(0.3e1) * C_3[0] * sqrt(0.2e1) / 0.8e1 + C_4[2] / 0.16e2 + sqrt(0.3e1) * C_4[1] / 0.16e2;
            C[n*n_p + 3][new_pos] = C_1[3] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] / 0.12e2 + sqrt(0.3e1) * C_1[0] / 0.48e2 + C_2[3] / 0.16e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_2[1] / 0.8e1 + sqrt(0.3e1) * C_2[0] / 0.16e2 + C_3[3] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] / 0.12e2 + sqrt(0.3e1) * C_3[0] / 0.48e2 - 0.5e1 / 0.48e2 * sqrt(0.3e1) * C_4[0] + sqrt(0.2e1) * C_4[2] / 0.16e2 - sqrt(0.3e1) * C_4[4] / 0.48e2 + C_4[5] * sqrt(0.5e1) / 0.48e2 + C_4[3] / 0.48e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] / 0.48e2;
            C[n*n_p + 4][new_pos] = C_1[4] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_1[2] / 0.32e2 - 0.5e1 / 0.32e2 * sqrt(0.2e1) * C_1[1] + C_1[0] / 0.16e2 + C_2[4] / 0.16e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_2[2] / 0.8e1 + C_3[4] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_3[2] / 0.32e2 + 0.5e1 / 0.32e2 * sqrt(0.2e1) * C_3[1] - C_3[0] / 0.16e2 - C_4[4] / 0.32e2 + C_4[3] * sqrt(0.3e1) / 0.48e2 - C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.96e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_4[2] / 0.32e2 + 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_4[1];
            C[n*n_p + 5][new_pos] = C_1[5] / 0.16e2 - 0.3e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[2] + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] / 0.96e2 + sqrt(0.5e1) * sqrt(0.3e1) * C_1[0] / 0.48e2 + C_2[5] / 0.16e2 + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] / 0.96e2 + 0.3e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[2] + sqrt(0.5e1) * sqrt(0.3e1) * C_3[0] / 0.48e2 + C_3[5] / 0.16e2 + C_4[5] / 0.96e2 + sqrt(0.5e1) * C_4[3] / 0.48e2 + sqrt(0.5e1) * sqrt(0.3e1) * C_4[4] / 0.96e2 - sqrt(0.5e1) * sqrt(0.2e1) * C_4[2] / 0.32e2 + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] / 0.96e2 - sqrt(0.5e1) * sqrt(0.3e1) * C_4[0] / 0.24e2;
            C[n*n_p + 6][new_pos] = 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_1[1] + C_1[0] / 0.32e2 + C_1[6] / 0.32e2 - 0.3e1 / 0.32e2 * C_1[3] * sqrt(0.3e1) + 0.3e1 / 0.16e2 * sqrt(0.2e1) * C_2[1] - C_2[0] / 0.8e1 + C_2[6] / 0.32e2 + C_2[3] * sqrt(0.3e1) / 0.8e1 + 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_3[1] + C_3[0] / 0.32e2 - 0.3e1 / 0.32e2 * C_3[3] * sqrt(0.3e1) + C_3[6] / 0.32e2 - C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + C_4[8] * sqrt(0.5e1) / 0.128e3 - C_4[3] * sqrt(0.3e1) / 0.48e2 + C_4[0] / 0.16e2 - sqrt(0.2e1) * C_4[1] / 0.32e2 - C_4[7] * sqrt(0.3e1) / 0.128e3 - C_4[9] * sqrt(0.7e1) / 0.128e3 + C_4[4] / 0.16e2 + sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.32e2 + C_4[6] / 0.128e3;
            C[n*n_p + 7][new_pos] = 0.3e1 / 0.40e2 * sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] + sqrt(0.3e1) * C_1[0] / 0.32e2 - 0.21e2 / 0.160e3 * C_1[3] - sqrt(0.3e1) * C_1[4] / 0.20e2 + C_1[7] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.2e1) * C_1[2] + C_2[7] / 0.32e2 + sqrt(0.3e1) * C_2[4] / 0.8e1 + 0.3e1 / 0.16e2 * sqrt(0.2e1) * C_2[2] - 0.3e1 / 0.40e2 * sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] - sqrt(0.3e1) * C_3[0] / 0.32e2 - sqrt(0.3e1) * C_3[4] / 0.20e2 + 0.21e2 / 0.160e3 * C_3[3] + C_3[7] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.2e1) * C_3[2] - C_4[5] * sqrt(0.5e1) / 0.80e2 + sqrt(0.3e1) * C_4[8] * sqrt(0.5e1) / 0.640e3 + C_4[3] / 0.40e2 - 0.3e1 / 0.160e3 * sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] - 0.11e2 / 0.640e3 * C_4[7] + 0.3e1 / 0.640e3 * sqrt(0.3e1) * C_4[9] * sqrt(0.7e1) - sqrt(0.3e1) * C_4[4] / 0.80e2 - 0.3e1 / 0.160e3 * sqrt(0.2e1) * C_4[2] + sqrt(0.3e1) * C_4[6] / 0.128e3;
            C[n*n_p + 8][new_pos] = C_3[8] / 0.32e2 + C_2[8] / 0.32e2 + C_1[8] / 0.32e2 - 0.3e1 / 0.128e3 * C_4[8] + C_2[5] * sqrt(0.3e1) / 0.8e1 + sqrt(0.5e1) * C_3[0] / 0.32e2 + 0.7e1 / 0.80e2 * sqrt(0.5e1) * C_3[4] - C_3[5] * sqrt(0.3e1) / 0.48e2 + C_4[5] * sqrt(0.3e1) / 0.16e2 - sqrt(0.5e1) * C_4[0] / 0.16e2 + sqrt(0.5e1) * C_4[4] / 0.80e2 + sqrt(0.5e1) * C_4[6] / 0.128e3 + sqrt(0.5e1) * C_1[0] / 0.32e2 - 0.7e1 / 0.80e2 * sqrt(0.5e1) * C_1[4] - C_1[5] * sqrt(0.3e1) / 0.48e2 + 0.3e1 / 0.80e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[1] + 0.7e1 / 0.480e3 * sqrt(0.5e1) * C_1[3] * sqrt(0.3e1) + 0.3e1 / 0.80e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[1] + 0.7e1 / 0.480e3 * sqrt(0.5e1) * C_3[3] * sqrt(0.3e1) + 0.3e1 / 0.80e2 * sqrt(0.5e1) * C_4[3] * sqrt(0.3e1) - sqrt(0.5e1) * sqrt(0.2e1) * C_4[1] / 0.160e3 - sqrt(0.5e1) * C_4[7] * sqrt(0.3e1) / 0.640e3 - sqrt(0.5e1) * C_4[9] * sqrt(0.7e1) / 0.640e3 - 0.3e1 / 0.160e3 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[2] * sqrt(0.3e1) + sqrt(0.5e1) * sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.160e3 + 0.3e1 / 0.160e3 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[2] * sqrt(0.3e1);
            C[n*n_p + 9][new_pos] = -0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * C_1[1] + sqrt(0.7e1) * C_1[0] / 0.32e2 + C_1[9] / 0.32e2 - sqrt(0.7e1) * C_1[3] * sqrt(0.3e1) / 0.480e3 + sqrt(0.7e1) * C_1[4] / 0.80e2 - sqrt(0.7e1) * C_1[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + 0.3e1 / 0.80e2 * sqrt(0.7e1) * sqrt(0.2e1) * C_1[2] * sqrt(0.3e1) + C_2[9] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * C_3[1] - sqrt(0.7e1) * C_3[0] / 0.32e2 + sqrt(0.7e1) * C_3[4] / 0.80e2 + sqrt(0.7e1) * C_3[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + sqrt(0.7e1) * C_3[3] * sqrt(0.3e1) / 0.480e3 + 0.3e1 / 0.80e2 * sqrt(0.7e1) * sqrt(0.2e1) * C_3[2] * sqrt(0.3e1) + C_3[9] / 0.32e2 - sqrt(0.7e1) * C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.240e3 + sqrt(0.7e1) * C_4[8] * sqrt(0.5e1) / 0.640e3 + sqrt(0.7e1) * C_4[3] * sqrt(0.3e1) / 0.120e3 - 0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * C_4[1] + 0.3e1 / 0.640e3 * sqrt(0.7e1) * C_4[7] * sqrt(0.3e1) + C_4[9] / 0.640e3 - sqrt(0.7e1) * C_4[4] / 0.80e2 - sqrt(0.7e1) * sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.160e3 + sqrt(0.7e1) * C_4[6] / 0.128e3;
        }
    }
} 

__global__ void coarsen_c4(int *ecoarsen, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          double **C,  int *epos,
                          int N, 
                          int num_ecadd) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx < num_ecadd) {
        int eIDcoarsen, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
        int ePOScoarsen, ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
        int new_pos;
        const int n_p = 15;
        eIDcoarsen = ecoarsen[idx];
        eIDchild1 = echild1[eIDcoarsen];
        eIDchild2 = echild2[eIDcoarsen];
        eIDchild3 = echild3[eIDcoarsen];
        eIDchild4 = echild4[eIDcoarsen];

        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];
        new_pos = ePOSchild4;

        double C_1[15];
        double C_2[15];
        double C_3[15];
        double C_4[15];

        for(int n = 0; n < N; n++){
          for(int i = 0; i< 15; i++){
            C_1[i] = C[n*n_p + i][ePOSchild1];
            C_2[i] = C[n*n_p + i][ePOSchild2];
            C_3[i] = C[n*n_p + i][ePOSchild3];
            C_4[i] = C[n*n_p + i][ePOSchild4];
          }
            C[n*n_p + 0][new_pos] = C_1[0] / 0.4e1 + C_2[0] / 0.4e1 + C_3[0] / 0.4e1 + C_4[0] / 0.4e1;
            C[n*n_p + 1][new_pos] = C_1[1] / 0.8e1 - C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[1] / 0.8e1 + C_2[0] * sqrt(0.2e1) / 0.4e1 + C_3[1] / 0.8e1 - C_3[0] * sqrt(0.2e1) / 0.8e1 - C_4[2] * sqrt(0.3e1) / 0.16e2 + C_4[1] / 0.16e2;
            C[n*n_p + 2][new_pos] = C_1[2] / 0.8e1 - sqrt(0.3e1) * C_1[0] * sqrt(0.2e1) / 0.8e1 + C_2[2] / 0.8e1 + C_3[2] / 0.8e1 + sqrt(0.3e1) * C_3[0] * sqrt(0.2e1) / 0.8e1 + C_4[2] / 0.16e2 + sqrt(0.3e1) * C_4[1] / 0.16e2;
            C[n*n_p + 3][new_pos] = C_1[3] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] / 0.12e2 + sqrt(0.3e1) * C_1[0] / 0.48e2 + C_2[3] / 0.16e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_2[1] / 0.8e1 + sqrt(0.3e1) * C_2[0] / 0.16e2 + C_3[3] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] / 0.12e2 + sqrt(0.3e1) * C_3[0] / 0.48e2 - 0.5e1 / 0.48e2 * sqrt(0.3e1) * C_4[0] + sqrt(0.2e1) * C_4[2] / 0.16e2 - sqrt(0.3e1) * C_4[4] / 0.48e2 + C_4[5] * sqrt(0.5e1) / 0.48e2 + C_4[3] / 0.48e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] / 0.48e2;
            C[n*n_p + 4][new_pos] = C_1[4] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_1[2] / 0.32e2 - 0.5e1 / 0.32e2 * sqrt(0.2e1) * C_1[1] + C_1[0] / 0.16e2 + C_2[4] / 0.16e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_2[2] / 0.8e1 + C_3[4] / 0.16e2 - sqrt(0.2e1) * sqrt(0.3e1) * C_3[2] / 0.32e2 + 0.5e1 / 0.32e2 * sqrt(0.2e1) * C_3[1] - C_3[0] / 0.16e2 - C_4[4] / 0.32e2 + C_4[3] * sqrt(0.3e1) / 0.48e2 - C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.96e2 + sqrt(0.2e1) * sqrt(0.3e1) * C_4[2] / 0.32e2 + 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_4[1];
            C[n*n_p + 5][new_pos] = C_1[5] / 0.16e2 - 0.3e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[2] + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] / 0.96e2 + sqrt(0.5e1) * sqrt(0.3e1) * C_1[0] / 0.48e2 + C_2[5] / 0.16e2 + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] / 0.96e2 + 0.3e1 / 0.32e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[2] + sqrt(0.5e1) * sqrt(0.3e1) * C_3[0] / 0.48e2 + C_3[5] / 0.16e2 + C_4[5] / 0.96e2 + sqrt(0.5e1) * C_4[3] / 0.48e2 + sqrt(0.5e1) * sqrt(0.3e1) * C_4[4] / 0.96e2 - sqrt(0.5e1) * sqrt(0.2e1) * C_4[2] / 0.32e2 + sqrt(0.5e1) * sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] / 0.96e2 - sqrt(0.5e1) * sqrt(0.3e1) * C_4[0] / 0.24e2;
            C[n*n_p + 6][new_pos] = 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_1[1] + C_1[0] / 0.32e2 + C_1[6] / 0.32e2 - 0.3e1 / 0.32e2 * C_1[3] * sqrt(0.3e1) + 0.3e1 / 0.16e2 * sqrt(0.2e1) * C_2[1] - C_2[0] / 0.8e1 + C_2[6] / 0.32e2 + C_2[3] * sqrt(0.3e1) / 0.8e1 + 0.3e1 / 0.32e2 * sqrt(0.2e1) * C_3[1] + C_3[0] / 0.32e2 - 0.3e1 / 0.32e2 * C_3[3] * sqrt(0.3e1) + C_3[6] / 0.32e2 - C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + C_4[8] * sqrt(0.5e1) / 0.128e3 - C_4[3] * sqrt(0.3e1) / 0.48e2 + C_4[0] / 0.16e2 - sqrt(0.2e1) * C_4[1] / 0.32e2 - C_4[7] * sqrt(0.3e1) / 0.128e3 - C_4[9] * sqrt(0.7e1) / 0.128e3 + C_4[4] / 0.16e2 + sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.32e2 + C_4[6] / 0.128e3;
            C[n*n_p + 7][new_pos] = 0.3e1 / 0.40e2 * sqrt(0.2e1) * sqrt(0.3e1) * C_1[1] + sqrt(0.3e1) * C_1[0] / 0.32e2 - 0.21e2 / 0.160e3 * C_1[3] - sqrt(0.3e1) * C_1[4] / 0.20e2 + C_1[7] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.2e1) * C_1[2] + C_2[7] / 0.32e2 + sqrt(0.3e1) * C_2[4] / 0.8e1 + 0.3e1 / 0.16e2 * sqrt(0.2e1) * C_2[2] - 0.3e1 / 0.40e2 * sqrt(0.2e1) * sqrt(0.3e1) * C_3[1] - sqrt(0.3e1) * C_3[0] / 0.32e2 - sqrt(0.3e1) * C_3[4] / 0.20e2 + 0.21e2 / 0.160e3 * C_3[3] + C_3[7] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.2e1) * C_3[2] - C_4[5] * sqrt(0.5e1) / 0.80e2 + sqrt(0.3e1) * C_4[8] * sqrt(0.5e1) / 0.640e3 + C_4[3] / 0.40e2 - 0.3e1 / 0.160e3 * sqrt(0.2e1) * sqrt(0.3e1) * C_4[1] - 0.11e2 / 0.640e3 * C_4[7] + 0.3e1 / 0.640e3 * sqrt(0.3e1) * C_4[9] * sqrt(0.7e1) - sqrt(0.3e1) * C_4[4] / 0.80e2 - 0.3e1 / 0.160e3 * sqrt(0.2e1) * C_4[2] + sqrt(0.3e1) * C_4[6] / 0.128e3;
            C[n*n_p + 8][new_pos] = C_3[8] / 0.32e2 + C_2[8] / 0.32e2 + C_1[8] / 0.32e2 - 0.3e1 / 0.128e3 * C_4[8] + C_2[5] * sqrt(0.3e1) / 0.8e1 + sqrt(0.5e1) * C_3[0] / 0.32e2 + 0.7e1 / 0.80e2 * sqrt(0.5e1) * C_3[4] - C_3[5] * sqrt(0.3e1) / 0.48e2 + C_4[5] * sqrt(0.3e1) / 0.16e2 - sqrt(0.5e1) * C_4[0] / 0.16e2 + sqrt(0.5e1) * C_4[4] / 0.80e2 + sqrt(0.5e1) * C_4[6] / 0.128e3 + sqrt(0.5e1) * C_1[0] / 0.32e2 - 0.7e1 / 0.80e2 * sqrt(0.5e1) * C_1[4] - C_1[5] * sqrt(0.3e1) / 0.48e2 + 0.3e1 / 0.80e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[1] + 0.7e1 / 0.480e3 * sqrt(0.5e1) * C_1[3] * sqrt(0.3e1) + 0.3e1 / 0.80e2 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[1] + 0.7e1 / 0.480e3 * sqrt(0.5e1) * C_3[3] * sqrt(0.3e1) + 0.3e1 / 0.80e2 * sqrt(0.5e1) * C_4[3] * sqrt(0.3e1) - sqrt(0.5e1) * sqrt(0.2e1) * C_4[1] / 0.160e3 - sqrt(0.5e1) * C_4[7] * sqrt(0.3e1) / 0.640e3 - sqrt(0.5e1) * C_4[9] * sqrt(0.7e1) / 0.640e3 - 0.3e1 / 0.160e3 * sqrt(0.5e1) * sqrt(0.2e1) * C_3[2] * sqrt(0.3e1) + sqrt(0.5e1) * sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.160e3 + 0.3e1 / 0.160e3 * sqrt(0.5e1) * sqrt(0.2e1) * C_1[2] * sqrt(0.3e1);
            C[n*n_p + 9][new_pos] = -0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * C_1[1] + sqrt(0.7e1) * C_1[0] / 0.32e2 + C_1[9] / 0.32e2 - sqrt(0.7e1) * C_1[3] * sqrt(0.3e1) / 0.480e3 + sqrt(0.7e1) * C_1[4] / 0.80e2 - sqrt(0.7e1) * C_1[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + 0.3e1 / 0.80e2 * sqrt(0.7e1) * sqrt(0.2e1) * C_1[2] * sqrt(0.3e1) + C_2[9] / 0.32e2 + 0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * C_3[1] - sqrt(0.7e1) * C_3[0] / 0.32e2 + sqrt(0.7e1) * C_3[4] / 0.80e2 + sqrt(0.7e1) * C_3[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.48e2 + sqrt(0.7e1) * C_3[3] * sqrt(0.3e1) / 0.480e3 + 0.3e1 / 0.80e2 * sqrt(0.7e1) * sqrt(0.2e1) * C_3[2] * sqrt(0.3e1) + C_3[9] / 0.32e2 - sqrt(0.7e1) * C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.240e3 + sqrt(0.7e1) * C_4[8] * sqrt(0.5e1) / 0.640e3 + sqrt(0.7e1) * C_4[3] * sqrt(0.3e1) / 0.120e3 - 0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * C_4[1] + 0.3e1 / 0.640e3 * sqrt(0.7e1) * C_4[7] * sqrt(0.3e1) + C_4[9] / 0.640e3 - sqrt(0.7e1) * C_4[4] / 0.80e2 - sqrt(0.7e1) * sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.160e3 + sqrt(0.7e1) * C_4[6] / 0.128e3;
            C[n*n_p + 10][new_pos] = -0.3e1 / 0.64e2 * C_4[8] + C_2[10] / 0.64e2 - sqrt(0.2e1) * sqrt(0.5e1) * C_4[2] * sqrt(0.3e1) / 0.40e2 - C_4[13] * sqrt(0.7e1) / 0.320e3 + C_4[5] * sqrt(0.3e1) / 0.192e3 - sqrt(0.5e1) * C_1[0] / 0.160e3 - sqrt(0.5e1) * C_1[6] / 0.20e2 - sqrt(0.5e1) * C_2[0] / 0.32e2 + sqrt(0.5e1) * C_2[6] / 0.16e2 - sqrt(0.5e1) * C_3[0] / 0.160e3 - sqrt(0.5e1) * C_3[6] / 0.20e2 + 0.7e1 / 0.160e3 * sqrt(0.5e1) * C_4[0] - sqrt(0.5e1) * C_4[4] / 0.320e3 - 0.3e1 / 0.320e3 * sqrt(0.5e1) * C_4[6] + sqrt(0.5e1) * C_4[12] / 0.320e3 - C_4[11] * sqrt(0.3e1) / 0.320e3 + C_4[10] / 0.320e3 + C_1[10] / 0.64e2 + 0.5e1 / 0.64e2 * sqrt(0.5e1) * C_2[3] * sqrt(0.3e1) + 0.3e1 / 0.64e2 * sqrt(0.5e1) * C_3[3] * sqrt(0.3e1) + sqrt(0.5e1) * C_4[3] * sqrt(0.3e1) / 0.960e3 + 0.3e1 / 0.320e3 * sqrt(0.5e1) * C_4[7] * sqrt(0.3e1) + 0.3e1 / 0.320e3 * sqrt(0.5e1) * C_4[9] * sqrt(0.7e1) + 0.3e1 / 0.64e2 * sqrt(0.5e1) * C_1[3] * sqrt(0.3e1) + sqrt(0.2e1) * sqrt(0.5e1) * C_4[1] / 0.40e2 + 0.3e1 / 0.320e3 * C_4[14] + C_3[10] / 0.64e2;
            C[n*n_p + 11][new_pos] = -sqrt(0.3e1) * sqrt(0.5e1) * C_1[0] / 0.160e3 - 0.3e1 / 0.160e3 * sqrt(0.3e1) * sqrt(0.5e1) * C_1[6] + 0.3e1 / 0.32e2 * sqrt(0.5e1) * C_1[3] + sqrt(0.3e1) * sqrt(0.5e1) * C_1[4] / 0.64e2 - sqrt(0.5e1) * C_1[7] / 0.32e2 + C_1[11] / 0.64e2 + C_2[11] / 0.64e2 + 0.5e1 / 0.64e2 * sqrt(0.3e1) * sqrt(0.5e1) * C_2[4] + sqrt(0.5e1) * C_2[7] / 0.16e2 + sqrt(0.3e1) * sqrt(0.5e1) * C_3[0] / 0.160e3 + sqrt(0.3e1) * sqrt(0.5e1) * C_3[4] / 0.64e2 - 0.3e1 / 0.32e2 * sqrt(0.5e1) * C_3[3] - sqrt(0.5e1) * C_3[7] / 0.32e2 + C_3[11] / 0.64e2 + 0.3e1 / 0.160e3 * sqrt(0.3e1) * sqrt(0.5e1) * C_3[6] + 0.7e1 / 0.128e3 * C_4[5] - 0.7e1 / 0.320e3 * sqrt(0.5e1) * C_4[3] - 0.3e1 / 0.160e3 * sqrt(0.3e1) * sqrt(0.2e1) * sqrt(0.5e1) * C_4[1] + 0.7e1 / 0.640e3 * sqrt(0.3e1) * sqrt(0.5e1) * C_4[4] - 0.3e1 / 0.160e3 * sqrt(0.2e1) * sqrt(0.5e1) * C_4[2] + sqrt(0.3e1) * C_4[10] / 0.320e3 - sqrt(0.3e1) * C_4[14] / 0.160e3 + sqrt(0.3e1) * sqrt(0.5e1) * C_4[12] / 0.640e3 - C_4[11] / 0.128e3;
            C[n*n_p + 12][new_pos] = 0.9e1 / 0.224e3 * C_3[6] - 0.5e1 / 0.28e2 * C_3[4] - C_3[0] / 0.32e2 + sqrt(0.2e1) * C_4[2] * sqrt(0.3e1) / 0.32e2 + 0.5e1 / 0.1344e4 * sqrt(0.5e1) * C_1[5] * sqrt(0.3e1) + 0.5e1 / 0.1344e4 * sqrt(0.5e1) * C_3[5] * sqrt(0.3e1) + 0.9e1 / 0.224e3 * C_1[6] + 0.5e1 / 0.28e2 * C_1[4] - C_1[0] / 0.32e2 + C_4[13] * sqrt(0.7e1) * sqrt(0.5e1) / 0.560e3 - C_4[5] * sqrt(0.5e1) * sqrt(0.3e1) / 0.128e3 - C_4[11] * sqrt(0.3e1) * sqrt(0.5e1) / 0.640e3 + 0.3e1 / 0.56e2 * C_4[6] + 0.71e2 / 0.896e3 * C_4[4] + C_4[0] / 0.16e2 + 0.25e2 / 0.672e3 * C_1[3] * sqrt(0.3e1) - 0.15e2 / 0.224e3 * C_1[7] * sqrt(0.3e1) + 0.25e2 / 0.672e3 * C_3[3] * sqrt(0.3e1) + 0.15e2 / 0.224e3 * C_3[7] * sqrt(0.3e1) - sqrt(0.5e1) * C_1[8] / 0.56e2 - sqrt(0.5e1) * C_3[8] / 0.56e2 - sqrt(0.2e1) * C_4[1] / 0.32e2 - 0.3e1 / 0.112e3 * C_4[9] * sqrt(0.7e1) + C_4[10] * sqrt(0.5e1) / 0.320e3 + 0.3e1 / 0.1120e4 * C_4[14] * sqrt(0.5e1) + 0.9e1 / 0.448e3 * C_4[3] * sqrt(0.3e1) - 0.3e1 / 0.112e3 * C_4[7] * sqrt(0.3e1) + C_2[12] / 0.64e2 + C_1[12] / 0.64e2 - 0.3e1 / 0.896e3 * C_4[12] + C_3[12] / 0.64e2 + C_2[8] * sqrt(0.5e1) / 0.16e2 + 0.5e1 / 0.64e2 * C_2[5] * sqrt(0.5e1) * sqrt(0.3e1);
            C[n*n_p + 13][new_pos] = -sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) * C_4[2] * sqrt(0.3e1) / 0.160e3 + C_1[13] / 0.64e2 - 0.3e1 / 0.160e3 * sqrt(0.7e1) * sqrt(0.2e1) * sqrt(0.5e1) * C_4[1] - 0.45e2 / 0.896e3 * sqrt(0.7e1) * C_1[8] + 0.45e2 / 0.896e3 * sqrt(0.7e1) * C_3[8] + 0.15e2 / 0.896e3 * sqrt(0.7e1) * C_4[8] + sqrt(0.7e1) * C_4[10] / 0.320e3 - 0.3e1 / 0.4480e4 * sqrt(0.7e1) * C_4[14] - sqrt(0.5e1) * C_1[9] / 0.128e3 + sqrt(0.5e1) * C_2[9] / 0.16e2 - sqrt(0.5e1) * C_3[9] / 0.128e3 + 0.3e1 / 0.128e3 * sqrt(0.5e1) * C_4[9] - C_4[13] / 0.128e3 + C_2[13] / 0.64e2 - 0.13e2 / 0.1344e4 * sqrt(0.7e1) * sqrt(0.5e1) * C_1[3] * sqrt(0.3e1) + 0.3e1 / 0.896e3 * sqrt(0.7e1) * sqrt(0.5e1) * C_1[7] * sqrt(0.3e1) + 0.13e2 / 0.1344e4 * sqrt(0.7e1) * sqrt(0.5e1) * C_3[3] * sqrt(0.3e1) + 0.3e1 / 0.896e3 * sqrt(0.7e1) * sqrt(0.5e1) * C_3[7] * sqrt(0.3e1) + 0.13e2 / 0.3360e4 * sqrt(0.7e1) * sqrt(0.5e1) * C_4[3] * sqrt(0.3e1) + 0.3e1 / 0.896e3 * sqrt(0.7e1) * sqrt(0.5e1) * C_4[7] * sqrt(0.3e1) - sqrt(0.7e1) * sqrt(0.5e1) * C_1[0] / 0.160e3 - 0.9e1 / 0.4480e4 * sqrt(0.7e1) * sqrt(0.5e1) * C_1[6] + 0.17e2 / 0.448e3 * sqrt(0.7e1) * sqrt(0.5e1) * C_1[4] + sqrt(0.7e1) * sqrt(0.5e1) * C_3[0] / 0.160e3 + 0.17e2 / 0.448e3 * sqrt(0.7e1) * sqrt(0.5e1) * C_3[4] + 0.9e1 / 0.4480e4 * sqrt(0.7e1) * sqrt(0.5e1) * C_3[6] - 0.13e2 / 0.2240e4 * sqrt(0.7e1) * sqrt(0.5e1) * C_4[4] + 0.15e2 / 0.896e3 * sqrt(0.7e1) * sqrt(0.5e1) * C_4[6] - sqrt(0.7e1) * sqrt(0.5e1) * C_4[12] / 0.560e3 + 0.25e2 / 0.1344e4 * sqrt(0.7e1) * C_1[5] * sqrt(0.3e1) - 0.25e2 / 0.1344e4 * sqrt(0.7e1) * C_3[5] * sqrt(0.3e1) - 0.13e2 / 0.1344e4 * sqrt(0.7e1) * C_4[5] * sqrt(0.3e1) + C_3[13] / 0.64e2;
            C[n*n_p + 14][new_pos] = 0.15e2 / 0.896e3 * C_3[8] + 0.15e2 / 0.896e3 * C_1[8] - 0.3e1 / 0.128e3 * C_4[8] + 0.15e2 / 0.448e3 * sqrt(0.5e1) * C_3[4] + 0.3e1 / 0.4480e4 * sqrt(0.5e1) * C_3[6] + 0.3e1 / 0.80e2 * sqrt(0.5e1) * C_4[0] - 0.51e2 / 0.2240e4 * sqrt(0.5e1) * C_4[4] + 0.39e2 / 0.4480e4 * sqrt(0.5e1) * C_4[6] + 0.3e1 / 0.1120e4 * sqrt(0.5e1) * C_4[12] + 0.75e2 / 0.448e3 * C_1[5] * sqrt(0.3e1) + 0.75e2 / 0.448e3 * C_3[5] * sqrt(0.3e1) + C_4[5] * sqrt(0.3e1) / 0.64e2 + 0.3e1 / 0.4480e4 * C_4[13] * sqrt(0.7e1) + C_4[11] * sqrt(0.3e1) / 0.160e3 - 0.3e1 / 0.160e3 * sqrt(0.5e1) * C_1[0] + 0.3e1 / 0.4480e4 * sqrt(0.5e1) * C_1[6] - 0.15e2 / 0.448e3 * sqrt(0.5e1) * C_1[4] - 0.3e1 / 0.160e3 * sqrt(0.5e1) * C_3[0] - 0.3e1 / 0.160e3 * sqrt(0.2e1) * sqrt(0.5e1) * C_4[1] - 0.9e1 / 0.4480e4 * sqrt(0.5e1) * C_4[7] * sqrt(0.3e1) - 0.9e1 / 0.4480e4 * sqrt(0.5e1) * C_4[9] * sqrt(0.7e1) - 0.3e1 / 0.128e3 * sqrt(0.5e1) * C_1[9] * sqrt(0.7e1) + 0.3e1 / 0.448e3 * sqrt(0.5e1) * C_1[3] * sqrt(0.3e1) - sqrt(0.5e1) * C_1[7] * sqrt(0.3e1) / 0.896e3 + 0.3e1 / 0.448e3 * sqrt(0.5e1) * C_3[3] * sqrt(0.3e1) + sqrt(0.5e1) * C_3[7] * sqrt(0.3e1) / 0.896e3 + 0.3e1 / 0.128e3 * sqrt(0.5e1) * C_3[9] * sqrt(0.7e1) - sqrt(0.5e1) * C_4[3] * sqrt(0.3e1) / 0.280e3 + 0.3e1 / 0.160e3 * sqrt(0.2e1) * sqrt(0.5e1) * C_4[2] * sqrt(0.3e1) + 0.3e1 / 0.320e3 * C_4[10] + C_2[14] / 0.64e2 + C_4[14] / 0.4480e4 + C_1[14] / 0.64e2 + C_3[14] / 0.64e2;
          }
    }
} 


__global__ void coarsen_geo(int *ecoarsen, 
                            int *echild1, int *echild2, int *echild3, int *echild4,
                            double *V1x, double *V1y,
                            double *V2x, double *V2y,
                            double *V3x, double *V3y,
                            double *xr, double *yr,
                            double *xs, double *ys,
                            double *J, 
                            int *epos,
                            int num_ecadd) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    
    if(idx < num_ecadd) {
        double Vx[3];
        double Vy[3];
        int eIDcoarsen, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
        int ePOScoarsen, ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
        int new_pos;


        eIDcoarsen = ecoarsen[idx];
        eIDchild1 = echild1[eIDcoarsen];
        eIDchild2 = echild2[eIDcoarsen];
        eIDchild3 = echild3[eIDcoarsen];
        eIDchild4 = echild4[eIDcoarsen];

        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];
        new_pos = ePOSchild4;


        Vx[0] = V1x[ePOSchild1] ;
        Vx[1] = V2x[ePOSchild2] ;
        Vx[2] = V3x[ePOSchild3] ;

        Vy[0] = V1y[ePOSchild1] ;
        Vy[1] = V2y[ePOSchild2] ;
        Vy[2] = V3y[ePOSchild3] ;

        V1x[new_pos] = Vx[0] ;
        V2x[new_pos] = Vx[1] ;
        V3x[new_pos] = Vx[2] ;

        V1y[new_pos] = Vy[0] ;
        V2y[new_pos] = Vy[1] ;
        V3y[new_pos] = Vy[2] ;

        xr[new_pos] = Vx[1] - Vx[0] ;
        yr[new_pos] = Vy[1] - Vy[0] ;
        xs[new_pos] = Vx[2] - Vx[0] ;
        ys[new_pos] = Vy[2] - Vy[0] ;
        
        J[new_pos] = (Vx[1] - Vx[0]) *       
                     (Vy[2] - Vy[0]) -      
                     (Vx[2] - Vx[0]) *      
                     (Vy[1] - Vy[0]);      

    }
}


__global__ void init_nl_h(double *h,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double a, b, c, k;

        //Twice the area
        k = (V2x[idx]-V1x[idx])*(V3y[idx]-V1y[idx])-(V3x[idx]-V1x[idx])*(V2y[idx]-V1y[idx]);

        //Length of each side
        a = sqrt(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrt(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2));
        c = sqrt(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        //Smallest height
        h[idx] = min(min(k/a,k/b),k/c);
    }
}

__global__ void preval_inscribed_circles(double *J,
                                         double *V1x, double *V1y,
                                         double *V2x, double *V2y,
                                         double *V3x, double *V3y,
                                         int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double a, b, c, k;
        a = sqrt(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrt(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2));
        c = sqrt(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        k = 0.5 * (a + b + c);

        // for the diameter, we multiply by 2
        J[idx] =  sqrt(k * (k - a) * (k - b) * (k - c)) / k;
        // printf("c%i (%f,%f) (%f,%f) (%f,%f) %f\n",idx,V1x[idx], V1y[idx], V2x[idx], V2y[idx], V3x[idx], V3y[idx], J[idx]);
    }
}






// assumes correct non-conformity
// after adaptivity, if number below > above + same, then refine
__global__ void refinement_fill(int *edr, int *edrtemp, int *elevel,
                                     int *elem_s1, int *elem_s2, int *elem_s3,
                                     int *spos, int *d_schild1, int *d_schild2,
                                     int *left_elem, int *right_elem,
                                     int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int ePOSlevel ;
    int sID[6], sPOS[6], neighPOS[6], ePOSleft[6], ePOSright[6], neighlevel[6];
    double above= 0 , same = 0, below = 0;
    int haschildren;
    int neigh_edr;

    if(idx  < max) { 

        if(edr[idx] == 0) {
            // load the sides of the elements
            sID[0] = elem_s1[idx];
            sID[1] = elem_s2[idx];
            sID[2] = elem_s3[idx];

            for(int i = 0; i < 3; i++) {
                haschildren = d_schild1[sID[i]];
                if(haschildren > -1){
                    sPOS[i] = spos[d_schild1[sID[i]]];
                    sPOS[i + 3] = spos[d_schild2[sID[i]]];
                }
                else{
                    sPOS[i] = spos[ sID[i] ];
                    sPOS[i + 3] = -1;
                }
            }

            ePOSlevel = elevel[idx] ;
            for(int i = 0; i < 6; i++) {
                if(sPOS[i] > -1) {
                    ePOSleft[i] = left_elem[sPOS[i] ] ;
                    ePOSright[i] = right_elem[sPOS[i] ] ;
                    ePOSright[i] = ePOSright[i]  > -1 ? ePOSright[i] : ePOSleft[i];

                    neighPOS[i]  = (idx == ePOSleft[i])  ? ePOSright[i]  : ePOSleft[i] ;
                    neigh_edr = edr[neighPOS[i]];
                    neighlevel[i]  = elevel[neighPOS[i]] + neigh_edr ;

                    if(neighlevel[i] > ePOSlevel)
                        below = below + 2*(neigh_edr == 1)+ 1*(neigh_edr == 0) ;
                    else if(neighlevel[i] == ePOSlevel)
                        same = same + 1*(neigh_edr == 1) + 1*(neigh_edr == 0) + 0.5 * (neigh_edr == -1);
                    else 
                        above = above + 1*(neigh_edr == 0) + 0.5 * (neigh_edr == -1);
                }
            }
            edrtemp[idx] = (below > same + above + 1e-10) ? 1 : 0;  
        }
        else
            edrtemp[idx] = edr[idx];


    }
}


__global__ void coarsening_fill( int *coarsenlist, int *flag,
                                           int *epos,
                                           int *elem_s1, int *elem_s2, int *elem_s3,
                                           int *spos, int *left_elem, int *right_elem,
                                           int *schild1,
                                           int *echild1, int *echild2, int *echild3, int *echild4,
                                           int *elevel, int *edr, 
                                           int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID[6], sPOS[6], neighbour[6], neighbour_level[6], mylevel;
    double same = 0, above = 0, below = 0;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int POSchild1, POSchild2, POSchild3, POSchild4;
    int neigh_edr;
    if(idx  < max) { 
        eID = coarsenlist[idx];

        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        POSchild1 = epos[eIDchild1];
        POSchild2 = epos[eIDchild2];
        POSchild3 = epos[eIDchild3];
        POSchild4 = epos[eIDchild4];

        // load the sides of the elements
        sID[0] = elem_s1[POSchild1];
        sID[1] = elem_s1[POSchild2];
        sID[2] = elem_s2[POSchild2];
        sID[3] = elem_s2[POSchild3];
        sID[4] = elem_s3[POSchild3];
        sID[5] = elem_s3[POSchild1];

        mylevel = elevel[POSchild1] -1;

        sPOS[0] = spos[sID[0]];
        sPOS[1] = spos[sID[1]];
        sPOS[2] = spos[sID[2]];
        sPOS[3] = spos[sID[3]];
        sPOS[4] = spos[sID[4]];
        sPOS[5] = spos[sID[5]];

        sPOS[0] = sPOS[0] > -1 ? sPOS[0] : spos[schild1[sID[0]]];
        sPOS[1] = sPOS[1] > -1 ? sPOS[1] : spos[schild1[sID[1]]];
        sPOS[2] = sPOS[2] > -1 ? sPOS[2] : spos[schild1[sID[2]]];
        sPOS[3] = sPOS[3] > -1 ? sPOS[3] : spos[schild1[sID[3]]];
        sPOS[4] = sPOS[4] > -1 ? sPOS[4] : spos[schild1[sID[4]]];
        sPOS[5] = sPOS[5] > -1 ? sPOS[5] : spos[schild1[sID[5]]];


        neighbour[0] = ( left_elem[sPOS[0]] == POSchild1 ) ? right_elem[sPOS[0]] : left_elem[sPOS[0]];
        neighbour[1] = ( left_elem[sPOS[1]] == POSchild2 ) ? right_elem[sPOS[1]] : left_elem[sPOS[1]];
        neighbour[2] = ( left_elem[sPOS[2]] == POSchild2 ) ? right_elem[sPOS[2]] : left_elem[sPOS[2]];
        neighbour[3] = ( left_elem[sPOS[3]] == POSchild3 ) ? right_elem[sPOS[3]] : left_elem[sPOS[3]];
        neighbour[4] = ( left_elem[sPOS[4]] == POSchild3 ) ? right_elem[sPOS[4]] : left_elem[sPOS[4]];
        neighbour[5] = ( left_elem[sPOS[5]] == POSchild1 ) ? right_elem[sPOS[5]] : left_elem[sPOS[5]];

        for(int i = 0; i < 6; i++) {
            if(neighbour[i] > -1) {
                neigh_edr = edr[neighbour[i]];
                neighbour_level[i] = elevel[neighbour[i]] + neigh_edr;
                if( neighbour_level[i] > mylevel )
                    below = below + 0.5 * (neigh_edr == -1) + 1 * (neigh_edr == 0) + 1 * (neigh_edr == 1);
                else if(neighbour_level[i] == mylevel)
                    same = same  + 0.5*(neigh_edr == 0) + 0.5 * (neigh_edr == -1);
                else
                    above = above + 1 * (neigh_edr == -1);
            }
        }


        if(below > same + above + 1e-10) {
            flag[idx] =  0 ;
            edr[POSchild1] =  0 ;
            edr[POSchild2] =  0 ;
            edr[POSchild3] =  0 ;
            edr[POSchild4] =  0 ; 
        }
        else{
            flag[idx] =  1 ;
        }

    }
}

__global__ void add_neighbors(int *pool, 
                              int *curr_s1, int *curr_s2, int *curr_s3,
                              int *spos, int *d_schild1, int *d_schild2,
                              int *left_elem, int *right_elem,
                              int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int ePOS, ePOSlevel ;
    int sID[3], sPOS[6], neighPOS[6], ePOSleft[6], ePOSright[6], neighlevel[6];
    int maxlevel, haschildren;
 
    if(idx  < max) { 
        ePOS = pool[idx];
        sID[0] = curr_s1[ePOS];
        sID[1] = curr_s2[ePOS];
        sID[2] = curr_s3[ePOS];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                sPOS[i] = spos[d_schild1[sID[i]]];
                sPOS[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                sPOS[i] = spos[ sID[i] ];
                sPOS[i + 3] = -1;
            }
        }

        for(int i = 0; i < 6; i++) {
            if(sPOS[i] > -1) {
                ePOSleft[i] = left_elem[sPOS[i] ] ;
                ePOSright[i] = right_elem[sPOS[i] ] ;
                ePOSright[i] = ePOSright[i]  > -1 ? ePOSright[i] : ePOSleft[i];
                neighPOS[i]  = (ePOS == ePOSleft[i])  ? ePOSright[i]  : ePOSleft[i] ;
            }
            else
                neighPOS[i]  = -1 ;

        }

        pool[0 * max + idx] = neighPOS[0];
        pool[1 * max + idx] = neighPOS[1];
        pool[2 * max + idx] = neighPOS[2];
        pool[3 * max + idx] = neighPOS[3];
        pool[4 * max + idx] = neighPOS[4];
        pool[5 * max + idx] = neighPOS[5];

    }

}

__global__ void enforce_nonconformity_pool(int *pool, int *inflag, 
                                           int *temp, 
                                           int *edr, int *elevel,
                                           int *curr_s1, int *curr_s2, int *curr_s3,
                                           int *spos, int *d_schild1, int *d_schild2,
                                           int *left_elem, int *right_elem,
                                           int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int ePOS, ePOSlevel ;
    int sID[3], sPOS[6], neighPOS[6], ePOSleft[6], ePOSright[6], neighlevel[6];
    int maxlevel, haschildren;
 
    if(idx  < max) { 
        ePOS = pool[idx];
        sID[0] = curr_s1[ePOS];
        sID[1] = curr_s2[ePOS];
        sID[2] = curr_s3[ePOS];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                sPOS[i] = spos[d_schild1[sID[i]]];
                sPOS[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                sPOS[i] = spos[ sID[i] ];
                sPOS[i + 3] = -1;
            }
        }

        ePOSlevel = elevel[ePOS] + edr[ePOS];
 
        maxlevel = ePOSlevel;
        for(int i = 0; i < 6; i++) {
            if(sPOS[i] > -1) {
                ePOSleft[i] = left_elem[sPOS[i] ] ;
                ePOSright[i] = right_elem[sPOS[i] ] ;
                ePOSright[i] = ePOSright[i]  > -1 ? ePOSright[i] : ePOSleft[i];

                neighPOS[i]  = (ePOS == ePOSleft[i])  ? ePOSright[i]  : ePOSleft[i] ;
                neighlevel[i]  = elevel[neighPOS[i]] + edr[neighPOS[i]] ;
                
                maxlevel = (maxlevel < neighlevel[i]) ? neighlevel[i] : maxlevel;
            }
        }

        ePOSlevel = (maxlevel - ePOSlevel > 1) ? maxlevel - 1 : ePOSlevel;
        temp[ePOS] = ePOSlevel - elevel[ePOS];

        if(edr[ePOS] != temp[ePOS]){
          inflag[idx] = 1;
        }
        else
          inflag[idx] = 0;


    }

}

__global__ void enforce_nonconformity( int *edr, int *edrtemp, int *elevel,
                                       int *curr_s1, int *curr_s2, int *curr_s3,
                                       int *spos, int *d_schild1, int *d_schild2,
                                       int *left_elem, int *right_elem,
                                       int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int ePOSlevel ;
    int sID[3], sPOS[6], neighPOS[6], ePOSleft[6], ePOSright[6], neighlevel[6];
    int maxlevel, haschildren;
 
    if(idx  < max) { 

    // load the sides of the elements
        sID[0] = curr_s1[idx];
        sID[1] = curr_s2[idx];
        sID[2] = curr_s3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                sPOS[i] = spos[d_schild1[sID[i]]];
                sPOS[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                sPOS[i] = spos[ sID[i] ];
                sPOS[i + 3] = -1;
            }
        }

        ePOSlevel = elevel[idx] + edr[idx];
 
        maxlevel = ePOSlevel;
        for(int i = 0; i < 6; i++) {
            if(sPOS[i] > -1) {
                ePOSleft[i] = left_elem[sPOS[i] ] ;
                ePOSright[i] = right_elem[sPOS[i] ] ;
                ePOSright[i] = ePOSright[i]  > -1 ? ePOSright[i] : ePOSleft[i];

                neighPOS[i]  = (idx == ePOSleft[i])  ? ePOSright[i]  : ePOSleft[i] ;
                neighlevel[i]  = elevel[neighPOS[i]] + edr[neighPOS[i]] ;
                
                maxlevel = (maxlevel < neighlevel[i]) ? neighlevel[i] : maxlevel;
            }
        }

        ePOSlevel = (maxlevel - ePOSlevel > 1) ? maxlevel - 1 : ePOSlevel;
        edrtemp[idx] = ePOSlevel - elevel[idx];

        // if(idx == 198)
        // {
        //   printf("%i %i %i \n %i %i\n", maxlevel, ePOSlevel, edrtemp[idx], sPOS[2], sPOS[5]);
        // }
    }

}



__global__ void enforce_nonconformity_list( int *list, int *edr,  
                                       int *elevel,
                                       int *left_elem, int *right_elem,
                                       int csum, int max) { 
    int index = blockDim.x * blockIdx.x + threadIdx.x + csum;
    int left_pos, right_pos, left_level, right_level, max_level, left_old_level, right_old_level;
    int idx;
    if (index < max + csum) {
        idx = list[index];
        // read left and right data
        left_pos   = left_elem[idx];
        right_pos  = right_elem[idx];

        left_old_level = elevel[left_pos];
        right_old_level = right_pos > -1 ? elevel[right_pos] : -1;

        left_level = edr[left_pos] + left_old_level;
        right_level = right_pos > -1 ? edr[right_pos] + right_old_level : -1;

        max_level = left_level > right_level ? left_level : right_level;

        if(max_level - left_level > 1)
          edr[left_pos] =  max_level - 1 - left_old_level;
        if(right_pos > -1)
          if(max_level - right_level > 1 )
            edr[right_pos] =  max_level - 1  - right_old_level;

        // if(left_pos == 39 || right_pos == 39)
        //   printf("side idx %i\n", idx);

    }

}


__global__ void enforce_nonconformity( int *edr,  
                                       int *elevel,
                                       int *left_elem, int *right_elem,
                                       int csum, int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
 
    if (idx < max + csum) {
        int left_pos, right_pos, left_level, right_level, max_level, left_old_level, right_old_level;

        // read left and right data
        left_pos   = left_elem[idx];
        right_pos  = right_elem[idx];

        left_old_level = elevel[left_pos];
        right_old_level = right_pos > -1 ? elevel[right_pos] : -1;

        left_level = edr[left_pos] + left_old_level;
        right_level = right_pos > -1 ? edr[right_pos] + right_old_level : -1;

        max_level = left_level > right_level ? left_level : right_level;

        if(max_level - left_level > 1)
          edr[left_pos] =  max_level - 1 - left_old_level;
        if(right_pos > -1)
          if(max_level - right_level > 1 )
            edr[right_pos] =  max_level - 1  - right_old_level;


    }

}


// __global__ void enforce_nonconformity( int *edr,  int *side_list, int *flag,
//                                        int *elevel,
//                                        int *left_elem, int *right_elem,
//                                        int csum, int max) { 
//     int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
 
//     if (idx < max + csum) {
//         int left_pos, right_pos, left_level, right_level, max_level, left_old_level, right_old_level;
//         int edr_l, edr_r;

//         // read left and right data
//         left_pos   = left_elem[side_list[idx]];
//         right_pos  = right_elem[side_list[idx]];

//         left_old_level = elevel[left_pos];
//         right_old_level = right_pos > -1 ? elevel[right_pos] : -1;

//         left_level = edr[left_pos] + left_old_level;
//         right_level = right_pos > -1 ? edr[right_pos] + right_old_level : -1;

//         max_level = left_level > right_level ? left_level : right_level;

//         edr_l = edr[left_pos] ;
//         edr_l_new =  max_level - left_level > 1 ? max_level - 1 - left_old_level: left_level - left_old_level;
//         if(edr_l != edr_l_new){
//           edr[left_pos] =edr_l_new;
//           flag[left_pos] = 1;
//         }

//         if(right_pos > -1){
//           edr_r = edr[left_pos] ;
//           edr_r_new = max_level - right_level > 1 ? max_level - 1  - right_old_level: right_level - right_old_level;
//           if(edr_r != edr_r_new){
//             edr[right_pos] =edr_r_new;
//             flag[right_pos] = 1;
//           }
//         }
//     }

// }




// __global__ void check_colors(int *elem_s1, int *elem_s2, int *elem_s3, 
//                              int *curr_scolor,
//                              int *elem, int *sides,  int *eparent, int *echild1, int *echild2, int *echild3, int *echild4, 
//                              int cs1, int cs2, int cs3,
//                              int curr_num_elem) {
//     int idx = blockDim.x * blockIdx.x + threadIdx.x;
//     int s[6], c[6];
//     int hash[6];
//     int bad = 0;
//     int eIDparent, eIDchild1, eIDchild2, eIDchild3, eIDchild4;

//     if (idx < curr_num_elem) {
//         for(int i = 0; i < 6; i++){
//             c[i]=-1;
//             hash[i] = 0;
//         }

//         s[0] = elem_s1[idx];
//         s[1] = elem_s2[idx];
//         s[2] = elem_s3[idx];
//         s[3] =(cs1 <= idx ) ? elem_s4[idx - cs1] : -1;
//         s[4] =(cs2 <= idx ) ? elem_s5[idx - cs2] : -1;
//         s[5] =(cs3 <= idx ) ? elem_s6[idx - cs3] : -1;

//         c[0] = curr_scolor[s[0]];
//         c[1] = curr_scolor[s[1]];
//         c[2] = curr_scolor[s[2]];

//         if(s[3] != -1)
//             c[3] = curr_scolor[s[3]];
//         if(s[4] != -1)
//             c[4] = curr_scolor[s[4]];
//         if(s[5] != -1)
//             c[5] = curr_scolor[s[5]];

//         hash[c[0]] +=1;
//         hash[c[1]] +=1;
//         hash[c[2]] +=1;
//         hash[c[3]] += (cs1 <= idx ) ? 1 : 0;
//         hash[c[4]] += (cs2 <= idx ) ? 1 : 0;
//         hash[c[5]] += (cs3 <= idx ) ? 1 : 0;

//         for(int i = 0; i < 6; i++){
//             if(hash[i] > 1)
//                 bad = 1;
//         }

//         if(bad == 1){
//             eIDparent = eparent[elem[idx]];
//             eIDchild1 = (eIDparent > -1) ?  echild1[eIDparent] : -1;
//             eIDchild2 = (eIDparent > -1) ?  echild2[eIDparent] : -1;
//             eIDchild3 = (eIDparent > -1) ?  echild3[eIDparent] : -1;
//             eIDchild4 = (eIDparent > -1) ?  echild4[eIDparent] : -1;
//             printf("%i %i %i %i %i %i %i %i %i %i %i %i\n",elem[idx], eIDparent, eIDchild1, eIDchild2, eIDchild3, eIDchild4, c[0], c[1], c[2], c[3], c[4], c[5]);
//         }


//     }
// }
__global__ void check_colors(int *elem_s1, int *elem_s2, int *elem_s3, 
                             int *curr_scolor, int *d_schild1, int *d_schild2, int *spos,
                             int curr_num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int s[6], c[6];
    int hash[6];
    int bad = 0;
    int haschildren, sID[6];

    if (idx < curr_num_elem) {
        for(int i = 0; i < 6; i++){
            c[i]=-1;
            hash[i] = 0;
        }

        sID[0] = elem_s1[idx];
        sID[1] = elem_s2[idx];
        sID[2] = elem_s3[idx];
        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                s[i] = spos[d_schild1[sID[i]]];
                s[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                s[i] = spos[ sID[i] ];
                s[i + 3] = -1;
            }
        }

        c[0] = curr_scolor[s[0]]; hash[c[0]] +=1;
        c[1] = curr_scolor[s[1]]; hash[c[1]] +=1;
        c[2] = curr_scolor[s[2]]; hash[c[2]] +=1;
        for(int i = 0; i < 3; i++){
          if(c[i]<0 || c[i] > 5)
            bad = 1;
        }

        if(s[3] != -1){
            c[3] = curr_scolor[s[3]];
            hash[c[3]] +=1;
            if(c[3]<0 || c[3] > 5)
              bad = 1;
          }
        if(s[4] != -1){
            c[4] = curr_scolor[s[4]];
            hash[c[4]] +=1;
            if(c[4]<0 || c[4] > 5)
              bad = 1;
          }
        if(s[5] != -1){
            c[5] = curr_scolor[s[5]];
            hash[c[5]] +=1;
            if(c[5]<0 || c[5] > 5)
              bad = 1;
      }
        for(int i = 0; i < 6; i++){
            if(hash[i] > 1)
                bad = 1;
        }

        if(bad == 1)
            printf("\n \n %i %i %i %i %i %i \n \n",c[0], c[1], c[2], c[3], c[4], c[5]);
        



    }
}
        // sID[0] = curr_s1[idx];
        // sID[1] = curr_s2[idx];
        // sID[2] = curr_s3[idx];

        // for(int i = 0; i < 3; i++) {
        //     haschildren = d_schild1[sID[i]];
        //     if(haschildren > -1){
        //         s[i] = spos[d_schild1[sID[i]]];
        //         s[i + 3] = spos[d_schild2[sID[i]]];
        //     }
        //     else{
        //         s[i] = spos[ sID[i] ];
        //         s[i + 3] = -1;
        //     }
        // }
        // // get element neighbor indexes
        // // and make sure we aren't on a boundary element
        // for( i = 0; i < 6; i++ ) {
        //     neighbor_idx[i] = -1;

        //     if(s[i] > -1) {
        //         if(left_elem[s[i]] == idx )  {
        //             neighbor_idx[i] = right_elem[s[i]];
        //         }
        //         else{
        //             neighbor_idx[i] = left_elem[s[i]];
        //         }
        //     }
        // }


__global__ void enforce_refine_monotonicity(double **C,
                                            double *basis_centroid,
                                            int *curr_elem, int *edr,
                                            int *curr_s1, int *curr_s2, int *curr_s3,
                                            int *spos, int *d_schild1, int *d_schild2,
                                            int *left_elem, int *right_elem,
                                            int *lsn, int *rsn,
                                            int N, int n_p,
                                            int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s[6],  sID[3];
        int n, i, side;
        int neighbor_idx[6];
        int haschildren;
        double y;

        if(edr[idx] == 1) {
        // load the sides of the elements
            sID[0] = curr_s1[idx];
            sID[1] = curr_s2[idx];
            sID[2] = curr_s3[idx];

            for(int i = 0; i < 3; i++) {
                haschildren = d_schild1[sID[i]];
                if(haschildren > -1){
                    s[i] = spos[d_schild1[sID[i]]];
                    s[i + 3] = spos[d_schild2[sID[i]]];
                }
                else{
                    s[i] = spos[ sID[i] ];
                    s[i + 3] = -1;
                }
            }

            // get element neighbor indexes
            // and make sure we aren't on a boundary element
            for( i = 0; i < 6; i++ ) {
                neighbor_idx[i] = -1;

                if(s[i] > 0) {
                    if(left_elem[s[i]] == idx )  {
                        neighbor_idx[i] = right_elem[s[i]];
                    }
                    else{
                        neighbor_idx[i] = left_elem[s[i]];
                    }
                }
            }
            
            for (n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * 1.414213562373095;
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (i = 0; i < 6; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * 1.414213562373095;
                        
                        Umin = (U_i < Umin) ? U_i : Umin;
                        Umax = (U_i > Umax) ? U_i : Umax;
                    }
                }

                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int centroid = 0; centroid < 3; centroid++) {
                    U_i = 0.;
                    //evaluate U
                    for (i = 0; i < n_p; i++) {
                        U_i += C[n*n_p + i][idx] * basis_centroid[3 * i + centroid];
                    }

                    y = 1.;
                    alpha = 1.;
                    // evaluate alpha
                    if (U_i > U_c) {
                        y = (Umax - U_c)/(U_i - U_c);
                    } else if (U_i < U_c) {
                        y = (Umin - U_c)/(U_i - U_c);
                    } 
                    alpha = min(1., y); // Barth Jesperson

                    if (alpha < min_alpha)  {
                        min_alpha = alpha;
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }

                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }

        }
    }
}



void allocateAdaptivity(int num_elem, int num_sides,
                        int in_emesh_MAX, int in_smesh_MAX,
                        int in_np, int in_N, int in_order, // in_order is p.
                        double **d_c,double **in_h_c,
                        int *d_elem, int *d_side,
                        int *d_s1, int *d_s2, int *d_s3,
                        int *d_elevel, int *d_slevel,
                        int *d_original_side, double *in_slength,
                        int *d_left1, int *d_left2, int *d_left3,
                        double *d_V1x, double *d_V1y, double *d_V2x, double *d_V2y, double *d_V3x, double *d_V3y,
                        int *in_left_elem, int *in_right_elem, int *in_left_side_number, int *in_right_side_number,
                        double *in_J,
                        double *in_xr, double *in_xs, double *in_yr, double *in_ys,
                        double *in_circles,
                        int in_quad, double *in_basis, double *in_basis_refined, double *in_basis_grad_x, double *in_basis_grad_y, double *in_w,
                        int *in_color, int *in_color_size,
                        memoryCounters *incounter,
                        double **in_h_k1, int in_limiter,
                        int in_verbose) {

    /*
    1. Set the current mesh pointers
    */ 
    curr_num_elem = num_elem, curr_num_sides = num_sides;
    emesh_MAX = in_emesh_MAX, smesh_MAX = in_smesh_MAX;



    limit = in_limiter;


    n_p = in_np, N = in_N, order = in_order;

    d_curr_c = d_c;
    h_curr_c = in_h_c;

    d_curr_elem = d_elem;
    d_curr_side = d_side;

    d_curr_s1 = d_s1, d_curr_s2 = d_s2, d_curr_s3 = d_s3;


    d_curr_elevel = d_elevel;
    d_curr_slevel = d_slevel;

    d_curr_originalside = d_original_side;
    d_slength = in_slength;

    d_curr_left1 = d_left1;
    d_curr_left2 = d_left2;
    d_curr_left3 = d_left3;

    d_curr_V1x = d_V1x;
    d_curr_V1y = d_V1y;
    d_curr_V2x = d_V2x;
    d_curr_V2y = d_V2y;
    d_curr_V3x = d_V3x;
    d_curr_V3y = d_V3y;

    color_size = in_color_size;


    d_left_elem = in_left_elem;
    d_right_elem = in_right_elem;
    d_left_side_number = in_left_side_number;
    d_right_side_number = in_right_side_number;

    d_curr_xr = in_xr;
    d_curr_xs = in_xs;
    d_curr_yr = in_yr;
    d_curr_ys = in_ys;

    d_curr_J = in_J;


    d_basis = in_basis;
    d_basis_refined = in_basis_refined;
    d_basis_grad_x = in_basis_grad_x;
    d_basis_grad_y = in_basis_grad_y;
    d_w = in_w;

    n_quad = in_quad;

    counter = incounter;

    verbose = in_verbose;

    d_circles = in_circles;

    d_curr_scolor = in_color;
    allocate((void **) &d_temp, emesh_MAX * sizeof(int), &(counter->emesh_memory));
    allocate((void **) &d_pool, emesh_MAX * sizeof(int), &(counter->emesh_memory));
    allocate((void **) &d_to, emesh_MAX * sizeof(int), &(counter->emesh_memory));
    allocate((void **) &d_from, emesh_MAX * sizeof(int), &(counter->emesh_memory));


    allocate((void **) &d_internal, emesh_MAX * sizeof(int), &(counter->emesh_memory));
    allocate((void **) &d_interface, emesh_MAX * sizeof(int), &(counter->emesh_memory));

    allocate((void **) &d_sdetector, emesh_MAX * sizeof(double), &(counter->emesh_memory));

 
    /*
    2. Allocate the refined mesh memory 
    */
    allocate((void **) &d_enum_sides, emesh_MAX * sizeof(int), &(counter->emesh_memory));
    allocate((void **) &d_flag, emesh_MAX * sizeof(int), &(counter->emesh_memory));


    /*
    3. Allocate tree
    */

    allocateTree(num_elem, num_sides, counter);


    /*
    5. Refinement module allocation
    */


    allocate((void **) &d_error_estimate, emesh_MAX * sizeof(double), &counter->emesh_memory  ); 
    allocate((void **) &d_max_error, sizeof(double), &counter->emesh_memory);   

    allocate((void **) &d_ordering, emesh_MAX * sizeof(int), &counter->emesh_memory);
    allocate((void **) &d_s_ordering, smesh_MAX * sizeof(int), &counter->smesh_memory);

    allocate((void **) &edr, emesh_MAX * sizeof(int), &counter->emesh_memory);
    allocate((void **) &sdr, smesh_MAX * sizeof(int), &counter->smesh_memory);
    allocate((void **) &d_prev_edr, emesh_MAX * sizeof(int), &counter->emesh_memory);

    allocate((void **) &d_inflag, smesh_MAX * sizeof(int), &counter->miscellaneous_memory);
    allocate((void **) &d_outflag, smesh_MAX * sizeof(int), &counter->miscellaneous_memory); 

    allocate((void **) &d_pos_efree, emesh_MAX * sizeof(int), &counter->miscellaneous_memory); 
    allocate((void **) &d_pos_sfree, smesh_MAX * sizeof(int), &counter->miscellaneous_memory); 


    allocate((void **) &d_basis_centroid, 4 * n_p * sizeof(double), &counter->emesh_memory  ); 

    /*
    6. Allocate refinement/coarsening module buffers
     */

    allocate((void **) &d_ecadd, emesh_MAX * sizeof(int), &counter->ebuffer_memory);

    allocate((void **) &d_erefine, emesh_MAX * sizeof(int), &counter->ebuffer_memory);
    allocate((void **) &d_srefine, smesh_MAX * sizeof(int), &counter->ebuffer_memory);

    /*
    6. Allocate CUDPP module
     */
    // cudppCreate(&theCudpp);
    temp_storage_bytes_scan = 0;
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes_scan, d_inflag, d_outflag, smesh_MAX));
    cudaMalloc( (void **) &d_temp_storage, temp_storage_bytes_scan);

    cub::DeviceRadixSort::SortKeys(d_temp_storage_sort, temp_storage_bytes_sort, d_pool, d_enum_sides, emesh_MAX);
    cudaMalloc(&d_temp_storage_sort, temp_storage_bytes_sort);


    cudaMalloc((void**)&d_keys.d_buffers[0], sizeof(int) * smesh_MAX);
    cudaMalloc((void**)&d_keys.d_buffers[1], sizeof(int) * smesh_MAX);
    cudaMalloc((void**)&d_values.d_buffers[0], sizeof(int) * smesh_MAX);
    cudaMalloc((void**)&d_values.d_buffers[1], sizeof(int) * smesh_MAX);
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage2, temp_storage_bytes_sort2, d_keys, d_values, smesh_MAX));
    cudaMalloc(&d_temp_storage2, temp_storage_bytes_sort2);


    allocateStrategy(emesh_MAX);
    initStrategy(order);


    // allocate_error_estimate(counter, in_emesh_MAX);
    // init_error_estimate(n_p, order+1, 5, order, in_h_k1);

}

void initAdaptivity(int resume)
{

    /*
    4. Initialize tree
    */
    if(resume == 0)
      initTree(d_curr_elem, d_curr_side,
               d_curr_s1, d_curr_s2, d_curr_s3,
               d_left_elem, d_right_elem,
               d_curr_left1, d_curr_left2, d_curr_left3,
               curr_num_elem, curr_num_sides);
    else
      resumeTree();


    // preval_inscribed_circles<<<get_blocks(curr_num_elem,n_threads), n_threads>>>(d_circles, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, curr_num_elem);
    init_nl_h<<<get_blocks(curr_num_elem, n_threads),n_threads>>>(d_circles,
                                                                      d_curr_V1x, d_curr_V1y,
                                                                      d_curr_V2x, d_curr_V2y,
                                                                      d_curr_V3x, d_curr_V3y,
                                                                      curr_num_elem);
    double basis_centroid_r[] = {1./6.,
                                 2./3.,
                                 1./6.};
    double basis_centroid_s[] = {1./6.,
                                 1./6.,
                                 2./3.};

    double *basis_centroid ;
    eval_basis(&basis_centroid, 
               n_p, 3,
               basis_centroid_r, basis_centroid_s);
    cudaMemcpy(d_basis_centroid, basis_centroid, 3*n_p*sizeof(double), cudaMemcpyHostToDevice);
    free(basis_centroid);

}



void dump_curr_mesh(int count){ 

    int *curr_elem = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s1 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s2 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s3 = (int *) malloc(curr_num_elem * sizeof(int));

    cudaMemcpy(curr_elem,d_curr_elem, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s1,d_curr_s1, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s2,d_curr_s2, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s3,d_curr_s3, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    
    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/dump/curr_emesh-%i.txt", count);
    out_file  = fopen(out_filename , "w");

    for (int idx = 0; idx < curr_num_elem; idx++) {
        // fprintf(out_file, "elemID %i : (%i %i %i) \n",curr_elem[idx], curr_s1[idx], curr_s2[idx], curr_s3[idx]);
        fprintf(out_file, "%i %i %i %i\n",curr_elem[idx], curr_s1[idx], curr_s2[idx], curr_s3[idx]);
    } 

    fclose(out_file);
    free(curr_elem);
    free(curr_s1);
    free(curr_s2);
    free(curr_s3);

    int *curr_side = (int *) malloc(curr_num_sides * sizeof(int));
    int *left_elem = (int *) malloc(curr_num_sides * sizeof(int));
    int *lsn = (int *) malloc(curr_num_sides * sizeof(int));
    int *right_elem = (int *) malloc(curr_num_sides * sizeof(int));
    int *rsn = (int *) malloc(curr_num_sides * sizeof(int));
    int *slevel = (int *) malloc(curr_num_sides * sizeof(int));
    int *scolor = (int *) malloc(curr_num_sides * sizeof(int)); 

    cudaMemcpy(curr_side,d_curr_side, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(left_elem,d_left_elem, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(lsn,d_left_side_number, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(right_elem,d_right_elem, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(rsn,d_right_side_number, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(slevel,d_curr_slevel, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(scolor,d_curr_scolor, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);

    sprintf(out_filename, "output/dump/curr_smesh-%i.txt", count);
    out_file  = fopen(out_filename, "w");

    for (int idx = 0; idx < curr_num_sides; idx++) {
        // fprintf(out_file, "sideID %i (%i %i) : (%i %i %i %i) \n",curr_side[idx], slevel[idx], scolor[idx], left_elem[idx], right_elem[idx], lsn[idx], rsn[idx]);
        fprintf(out_file, "%i %i %i %i %i %i %i\n",curr_side[idx], slevel[idx], scolor[idx], left_elem[idx], right_elem[idx], lsn[idx], rsn[idx]);
    } 

    fclose(out_file);
    free(curr_side);
    free(scolor);
    free(slevel);
    free(left_elem);
    free(lsn);
    free(right_elem);
    free(rsn);
}

void enforce_monotonicity(){

    enforce_refine_monotonicity<<<get_blocks(curr_num_elem,n_threads), n_threads>>>(d_curr_c,
                                                                                    d_basis_centroid,
                                                                                    d_curr_elem, edr,
                                                                                    d_curr_s1, d_curr_s2, d_curr_s3,
                                                                                    d_spos, d_schild1, d_schild2,
                                                                                    d_left_elem, d_right_elem,
                                                                                    d_left_side_number, d_right_side_number,
                                                                                    N, n_p,
                                                                                    curr_num_elem);
}



void adapt(double ***d_c,
           int **d_elem, int **d_side,
           int **d_s1, int **d_s2, int **d_s3,
           int **d_elevel, int **d_slevel,
           int **d_original_side,
           int **d_left1, int **d_left2, int **d_left3,
           double **d_V1x, double **d_V1y, double **d_V2x, double **d_V2y, double **d_V3x, double **d_V3y,
           double **d_J, double **d_xr, double **d_xs, double **d_yr, double **d_ys,
           int *num_elem, int *num_sides,
           int **in_color, 
           int *in_cumulative_sum,
           double in_t,
           int in_num) {
    t = in_t;
    num = in_num;
    num_ecoarsen = 0, num_ecadd = 0, num_scoarsen = 0, num_scadd = 0;
    num_erefine = 0, num_srefine = 0;
    

          
// printf("\n");

START_TIMING(flagElementsandSidesforAdapatation);flagElementsandSidesforAdapatation(); STOP_TIMING(flagElementsandSidesforAdapatation); PRINT_TIMING(flagElementsandSidesforAdapatation);
START_TIMING(enforce_monotonicity);          
                                              if(limit)
                                                 enforce_monotonicity();           
                                                                            STOP_TIMING(enforce_monotonicity); PRINT_TIMING(enforce_monotonicity);
START_TIMING(coarsen);                           coarsen();                 STOP_TIMING(coarsen); PRINT_TIMING(coarsen);
START_TIMING(refine);                            refine();                  STOP_TIMING(refine); PRINT_TIMING(refine);
          
              new_num_elem = curr_num_elem - num_erefine - num_ecoarsen + num_ecadd + 4 * num_erefine;
              new_num_sides = curr_num_sides - num_srefine - num_scoarsen + num_scadd + 2 * num_srefine + 3*num_erefine ;
          
START_TIMING(resizeData);                     if( new_num_sides < curr_num_sides ||  new_num_elem < curr_num_elem)
                                                 resizeData_inplace();              STOP_TIMING(resizeData); PRINT_TIMING(resizeData);
              	                                 // resizeData();              STOP_TIMING(resizeData); PRINT_TIMING(resizeData);
                         // remove the possible holes in the data from coarsening
          
// START_TIMING(determineOrder);                    determineOrder_cub();          STOP_TIMING(determineOrder); PRINT_TIMING(determineOrder);
START_TIMING(determineOrder);                    determineOrder();          STOP_TIMING(determineOrder); PRINT_TIMING(determineOrder);
START_TIMING(reorder);                           reorder();                 STOP_TIMING(reorder);PRINT_TIMING(reorder);
START_TIMING(redetermineConnectivity);           redetermineConnectivity(); STOP_TIMING(redetermineConnectivity);PRINT_TIMING(redetermineConnectivity);
START_TIMING(replaceCurrentMesh);                replaceCurrentMesh();      STOP_TIMING(replaceCurrentMesh); PRINT_TIMING(replaceCurrentMesh);
          


              if(verbose)
                printStatistics(); 

    *num_elem = new_num_elem;
    *num_sides = new_num_sides;


    if(verbose) {
        printf("num_erefine %i num_srefine %i num_ecoarsen %i num_scoarsen %i\n", num_erefine, num_srefine, num_ecoarsen, num_scoarsen);
        printf("num scadd %i\n", num_scadd);
        printf("number of free element/side positions %i/%i\n", freeElementCount, freeSidesCount);
        printf("new num elem %i new num sides %i\n", new_num_elem, new_num_sides);
    }

// check_colors<<<get_blocks(new_num_elem,n_threads), n_threads>>>(d_curr_s1, d_curr_s2, d_curr_s3, 
//                                                                  d_curr_scolor, d_schild1, d_schild2, d_spos,
//                                                                  new_num_elem);
}




__global__ void write_edr(int *coarsenlist, int *level,
                          int *eparent, int *echild1, int *echild2, int *echild3, int *echild4,
                          int *epos,
                          int *edr,
                          int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int POSchild1, POSchild2, POSchild3, POSchild4;
    int edr1, edr2, edr3, edr4;
 
    if(idx  < max) { 
        eID = coarsenlist[idx];
        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        POSchild1 = epos[eIDchild1];
        POSchild2 = epos[eIDchild2];
        POSchild3 = epos[eIDchild3];
        POSchild4 = epos[eIDchild4];

        edr1 = edr[POSchild1];
        edr2 = edr[POSchild2];
        edr3 = edr[POSchild3];
        edr4 = edr[POSchild4];
        if(edr1 == 0 && edr2 == 0 && edr3 == 0 && edr4 == 0) {
            edr[epos[eIDchild1]] = -1;
            edr[epos[eIDchild2]] = -1;
            edr[epos[eIDchild3]] = -1;
            edr[epos[eIDchild4]] = -1;

            level[idx] = 1;
        }
        else
            level[idx] = 0;
        
    }
}





__global__ void read_edr(int *coarsenlist, int *level,  
                         int *echild1, int *echild2, int *echild3, int *echild4,
                         int *epos, int *edr,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int POSchild1, POSchild2, POSchild3, POSchild4;
    int weight1, weight2, weight3, weight4;
 
    if(idx  < max) { 
        eID = coarsenlist[idx];

        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        POSchild1 = epos[eIDchild1];
        POSchild2 = epos[eIDchild2];
        POSchild3 = epos[eIDchild3];
        POSchild4 = epos[eIDchild4];

        weight1 = edr[POSchild1];
        weight2 = edr[POSchild2];
        weight3 = edr[POSchild3];
        weight4 = edr[POSchild4];

        if(weight1 != -1 || weight2 != -1  || weight3 != -1 || weight4 != -1 ) {
            level[idx] = 0;
            edr[POSchild1] = (weight1 == -1 ) ? 0 : weight1;
            edr[POSchild2] = (weight2 == -1 ) ? 0 : weight2;
            edr[POSchild3] = (weight3 == -1 ) ? 0 : weight3;
            edr[POSchild4] = (weight4 == -1 ) ? 0 : weight4;
        }
        else
            level[idx] = 1;
    }
}

__global__ void read_edr_pool(int *coarsenlist, int *level,  int *pool, 
                         int *echild1, int *echild2, int *echild3, int *echild4,
                         int *epos, int *edr,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int POSchild1, POSchild2, POSchild3, POSchild4;
    int weight1, weight2, weight3, weight4;
 
    if(idx  < max) { 
        eID = coarsenlist[idx];

        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        POSchild1 = epos[eIDchild1];
        POSchild2 = epos[eIDchild2];
        POSchild3 = epos[eIDchild3];
        POSchild4 = epos[eIDchild4];

        weight1 = edr[POSchild1];
        weight2 = edr[POSchild2];
        weight3 = edr[POSchild3];
        weight4 = edr[POSchild4];

        if(weight1 != -1 || weight2 != -1  || weight3 != -1 || weight4 != -1 ) {
            level[idx] = 0;
            edr[POSchild1] = (weight1 == -1 ) ? 0 : weight1;
            edr[POSchild2] = (weight2 == -1 ) ? 0 : weight2;
            edr[POSchild3] = (weight3 == -1 ) ? 0 : weight3;
            edr[POSchild4] = (weight4 == -1 ) ? 0 : weight4;
            pool[0*max + idx] = POSchild1;
            pool[1*max + idx] = POSchild2;
            pool[2*max + idx] = POSchild3;
            pool[3*max + idx] = POSchild4;
        }
        else{
            level[idx] = 1;
            pool[0*max + idx] = -1;
            pool[1*max + idx] = -1;
            pool[2*max + idx] = -1;
            pool[3*max + idx] = -1;
          }
    }
}

__global__ void read_edr_islands(int *coarsenlist, int *level,  
                         int *echild1, int *echild2, int *echild3, int *echild4,
                         int *epos, int *edr,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int POSchild1, POSchild2, POSchild3, POSchild4;
    int weight1, weight2, weight3, weight4;
 
    if(idx  < max) { 
        eID = coarsenlist[idx];

        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        POSchild1 = epos[eIDchild1];
        POSchild2 = epos[eIDchild2];
        POSchild3 = epos[eIDchild3];
        POSchild4 = epos[eIDchild4];

        weight1 = edr[POSchild1];
        weight2 = edr[POSchild2];
        weight3 = edr[POSchild3];
        weight4 = edr[POSchild4];

        if(weight1 != -1 && weight2 != -1  && weight3 != -1 ) {
            level[idx] = 0;
            edr[POSchild1] =  0 ;
            edr[POSchild2] =  0 ;
            edr[POSchild3] =  0 ;
            edr[POSchild4] =  0 ;
        }
        else{
            level[idx] = 1;
            edr[POSchild1] =  -1 ;
            edr[POSchild2] =  -1 ;
            edr[POSchild3] =  -1 ;
            edr[POSchild4] =  -1 ;
        }
    }
}

double get_tp(double t){
        double tp_tx[] = {0,  0.288045382,
                          0.012963, 0.29006,
                          0.025899, 0.29123,
                          0.038834, 0.2925,
                          0.051767, 0.29437,
                          0.064699, 0.29575,
                          0.077633, 0.29709,
                          0.090566, 0.29844,
                          0.1035,   0.3,
                          0.116436, 0.3015,
                          0.142308, 0.30457,
                          0.155243, 0.30606,
                          0.168181, 0.30735,
                          0.181118, 0.30835,
                          0.194056, 0.30982,
                          0.206994, 0.31149,
                          0.219932, 0.31256,
                          0.23287,  0.3136,
                          0.245808, 0.31474,
                          0.258746, 0.31642,
                          0.271684, 0.31744,
                          0.284622, 0.31894,
                          0.29756,  0.3201,
                          0.310497, 0.32129,
                          0.38812,  0.3279,
                          0.517502, 0.3382,
                          0.646882, 0.3473,
                          0.776264, 0.3551,
                          0.90564,  0.3626,
                          1.035012, 0.3689,
                          1.164392, 0.374,
                          1.293767, 0.3787,
                          1.423142, 0.3828,
                          1.552422, 0.3868,
                          1.810575, 0.3927,
                          2.068424, 0.3972,
                          2.326082, 0.4009,
                          2.583614, 0.4036,
                          2.969752, 0.4067,
                          3.355763, 0.4083,
                          3.741714, 0.4097,
                          4.719402, 0.4109};
      float w = 0.;
      for(int i = 0; i < 41; i++){


        if(tp_tx[ 2*i + 0] - 1e-10 < t && t < tp_tx[ 2*(i+1) + 0] + 1e-10){
          w = (t - tp_tx[ 2*i + 0])/ (tp_tx[ 2*(i+1) + 0] - tp_tx[ 2*i + 0]);
          return (1.-w)*tp_tx[ 2*i + 1]+w*tp_tx[ 2*(i+1) + 1];
        }
      }
      return 0.4109;
}


__global__ void remove_incompatible_refinements_geo(int *elem_list, int *curr_elevel,  
                                                int *edr, int max_level,
                                                double *V1x, double *V1y,
                                                double *V2x, double *V2y,
                                                double *V3x, double *V3y,
                                                double t,
                                                int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int level;
    double x, y, r;
    if(idx  < max) { 
      x = (V1x[idx] + V2x[idx] + V3x[idx])/3.;
      y = (V1y[idx] + V2y[idx] + V3y[idx])/3.;
      level = curr_elevel[idx];

      x -= t;
      r = sqrt(x*x + y*y);
      if(r < 0.25){
        edr[idx] = 1;
      }
    }
}




__global__ void remove_incompatible_refinements(int *elem_list, int *curr_elevel,  
                                                int *epos, int *edr,
                                                int max_level,
                                                int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int level;
    if(idx  < max) { 
        if(edr[idx] == 1){
            level = curr_elevel[idx];
            if(level == max_level)
                edr[idx] = 0;
        }
    }
}

__global__ void remove_incompatible_coarsening(int *elem_list, int *curr_elevel,  
                                                int *epos, int *edr,
                                                int max_level,
                                                int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int level;
    if(idx  < max) { 
        if(edr[idx] == -1){
            level = curr_elevel[idx];
            if(level == 0)
                edr[idx] = 0;
        }
    }
}

__global__ void clear_coarsening_flags(int *edr,int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int level;
    if(idx  < max) { 
        if(edr[idx] == -1)
            edr[idx] = 0;
    }
}

// void enforce_nonconformity(){

//     //remove incomplete coarsening flags
//     clear_coarsening_flags<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, curr_num_elem); 

//     // // enforce nonconformity for refinement (priority)
//     for(int i = 0; i < max_level-1; i++) {
//         enforce_nonconformity<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_enum_sides, d_curr_elevel,  
//                                                                        d_curr_s1, d_curr_s2, d_curr_s3,
//                                                                        d_spos, d_schild1, d_schild2,
//                                                                        d_left_elem, d_right_elem,
//                                                                        curr_num_elem);
//         swap(&edr, &d_enum_sides); 

//     } 
    
//     // enforce nonconformity for coarsening
//     if(num_ecadd > 0){
//         write_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
//                                                                    d_eparent, d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                    d_epos,
//                                                                    edr,
//                                                                    num_ecadd);
//         compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//     }
//     for(int i = 0; i < max_level-1; i++) {
//         if(num_ecadd > 0){
//             enforce_nonconformity<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_enum_sides, d_curr_elevel,  
//                                                                d_curr_s1, d_curr_s2, d_curr_s3,
//                                                                d_spos, d_schild1, d_schild2,
//                                                                d_left_elem, d_right_elem,
//                                                                curr_num_elem);
//             swap(&edr, &d_enum_sides);


//             read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
//                                                                         d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                         d_epos,
//                                                                         edr,
//                                                                         num_ecadd);
//             compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//         }
//     }

// }

// void enforce_nonconformity(){
//     START_TIMING(enforce_nonconformity1);                   
//     clear_coarsening_flags<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, curr_num_elem); 
//     if(num_ecadd > 0){
//         write_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
//                                                                    d_eparent, d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                    d_epos,
//                                                                    edr,
//                                                                    num_ecadd);
//         compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//     }
//     STOP_TIMING(enforce_nonconformity1); printf("enforce_nonconformity1 %lf\n", GET_TOTAL_TIMING(enforce_nonconformity1));
//     START_TIMING(enforce_nonconformity2);                   
//     for(int i = 0; i < max_level-1; i++) {
//         enforce_nonconformity<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_enum_sides, d_curr_elevel,  
//                                                            d_curr_s1, d_curr_s2, d_curr_s3,
//                                                            d_spos, d_schild1, d_schild2,
//                                                            d_left_elem, d_right_elem,
//                                                            curr_num_elem);
//         swap(&edr, &d_enum_sides);

//         if(num_ecadd > 0){
//             read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
//                                                                         d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                         d_epos,
//                                                                         edr,
//                                                                         num_ecadd);
//             compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//         }
//     }
//     STOP_TIMING(enforce_nonconformity2); printf("enforce_nonconformity2 %lf\n", GET_TOTAL_TIMING(enforce_nonconformity2));

// }

void enforce_nonconformity(){
    START_TIMING(enforce_nonconformity);     
    int csum;
    clear_coarsening_flags<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, curr_num_elem); 
    if(num_ecadd > 0){
        write_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
                                                                   d_eparent, d_echild1, d_echild2, d_echild3, d_echild4,
                                                                   d_epos,
                                                                   edr,
                                                                   num_ecadd);
        compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
    }

    for(int i = 0; i < max_level-1; i++) {
        csum = 0;
        for(int k = 0; k < 6; k++) {
          if(color_size[k] > 0){
              enforce_nonconformity<<<get_blocks(color_size[k], n_threads), n_threads>>>(edr, d_curr_elevel, 
                                                                                         d_left_elem, d_right_elem, 
                                                                                         csum, color_size[k]);
              csum+=color_size[k];
            }
        }
        if(num_ecadd > 0){
            read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
                                                                        d_echild1, d_echild2, d_echild3, d_echild4,
                                                                        d_epos,
                                                                        edr,
                                                                        num_ecadd);
            compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
        }


    }
    STOP_TIMING(enforce_nonconformity); PRINT_TIMING(enforce_nonconformity);
}


// void enforce_nonconformity_fast(){ 
//   int num_ecadd_prev = num_ecadd;
//     clear_coarsening_flags<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, curr_num_elem); 
//     if(num_ecadd > 0){
//         write_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
//                                                                    d_eparent, d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                    d_epos,
//                                                                    edr,
//                                                                    num_ecadd);
//         compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//     }

//     START_TIMING(enforce_nonconformity2);     
//     int work_size = curr_num_elem, temp;
//     //write index
//     write_idx<<<get_blocks(work_size, n_threads), n_threads>>>(d_pool,work_size);
//     while(work_size > 0) {
//       enforce_nonconformity_pool<<<get_blocks(work_size, n_threads), n_threads>>>(d_pool, d_inflag, 
//                                                                                   d_enum_sides, 
//                                                                                   edr, d_curr_elevel,
//                                                                                   d_curr_s1, d_curr_s2, d_curr_s3,
//                                                                                   d_spos, d_schild1, d_schild2,
//                                                                                   d_left_elem, d_right_elem,
//                                                                                   work_size); 
//       // update edr
//       compaction(d_pool, d_inflag, work_size, &temp); 
//       compaction(d_enum_sides, d_inflag, work_size, &work_size); 

//       if(work_size > 0){
//         move_to<<<get_blocks(work_size, n_threads), n_threads>>>(edr, d_enum_sides, d_pool, work_size); 
//       //add all neighbors
//         add_neighbors<<<get_blocks(work_size, n_threads), n_threads>>>(d_pool,d_curr_s1, d_curr_s2, d_curr_s3,
//                                                                        d_spos, d_schild1, d_schild2,
//                                                                        d_left_elem, d_right_elem,
//                                                                        work_size);
//         if(num_ecadd > 0){
//             read_edr_pool<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,d_pool + 6*work_size,
//                                                                       d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                       d_epos,
//                                                                       edr,
//                                                                       num_ecadd);
//             num_ecadd_prev = num_ecadd;
//             compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//         }
//         // sort
//         cub::DeviceRadixSort::SortKeys(d_temp_storage_sort, temp_storage_bytes_sort, d_pool, d_enum_sides, 6*work_size + 4 * num_ecadd_prev);
//         cudaMemcpy(d_pool, d_enum_sides, (6*work_size + 4 * num_ecadd_prev)*sizeof(int), cudaMemcpyDeviceToDevice);
//         // find duplicates
//         find_duplicates<<<get_blocks(6*work_size+ 4 * num_ecadd_prev, n_threads), n_threads>>>(d_pool, d_inflag, 6*work_size + 4 * num_ecadd_prev);
//         // print_array(d_pool, 6*work_size);
//         compaction(d_pool, d_inflag, 6*work_size + 4 * num_ecadd_prev, &work_size);
//       }
//     }
//     STOP_TIMING(enforce_nonconformity2); PRINT_TIMING(enforce_nonconformity2);
// }
__global__ void is_internal(int *flag,int *lsn, int *rsn, int max){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx  < max) { 
      if(lsn[idx] <= 2 && rsn[idx] <=2)
        flag[idx] = 1;
      else
        flag[idx] = 0;
    }
}

__global__ void is_interface(int *flag,int *lsn, int *rsn, int max){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx  < max) { 
      if(lsn[idx] > 2 || rsn[idx] > 2  )
        flag[idx] = 1;
      else
        flag[idx] = 0;
    }
}


void internal_edges(int *color){
  int csum = 0, offset = 0;
  for(int i = 0; i < 6; i++){
    if(color_size[i] > 0){
      is_internal<<<get_blocks(color_size[i], n_threads), n_threads>>>(d_inflag, d_left_side_number+csum, d_right_side_number+csum, color_size[i]);
      position_compaction_offset(d_internal + offset, d_inflag, csum, color_size[i], color + i);
      csum+=color_size[i];
      offset+=color[i];
    }
    else{
      color[i] = 0;
    }
  }


}

void interface_edges(int *color){
  int csum = 0, offset = 0;
  for(int i = 0; i < 6; i++){
    if(color_size[i] > 0){
      is_interface<<<get_blocks(color_size[i], n_threads), n_threads>>>(d_inflag, d_left_side_number+csum, d_right_side_number+csum, color_size[i]);
      position_compaction_offset(d_interface + offset, d_inflag, csum, color_size[i], color + i);
      csum+=color_size[i];
      offset+=color[i];
    }
    else{
      color[i] = 0;
    }
  }
}

void enforce_nonconformity_fast(){ 
    clear_coarsening_flags<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, curr_num_elem); 
    if(num_ecadd > 0){
        write_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
                                                                   d_eparent, d_echild1, d_echild2, d_echild3, d_echild4,
                                                                   d_epos,
                                                                   edr,
                                                                   num_ecadd);
        compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
    }

    // START_TIMING(enforce_nonconformity2);     
    int color_internal[6];
    int color_interface[6];
    internal_edges(color_internal);
    interface_edges(color_interface);
    int csum = 0;
    for(int k = 0; k < 6; k++) {
      if(color_size[k] > 0){
          enforce_nonconformity<<<get_blocks(color_size[k], n_threads), n_threads>>>(edr, d_curr_elevel, 
                                                                                             d_left_elem, d_right_elem, 
                                                                                             csum, color_size[k]);
          csum+=color_size[k];
        }
    }
    if(num_ecadd > 0){
        read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
                                                                    d_echild1, d_echild2, d_echild3, d_echild4,
                                                                    d_epos,
                                                                    edr,
                                                                    num_ecadd);
        compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
    }

    for(int i = 0; i < max_level; i++) { 
        // csum = 0;
        // for(int k = 0; k < 6; k++) {
        //   if(color_internal[k] > 0){
        //       enforce_nonconformity_list<<<get_blocks(color_internal[k], n_threads), n_threads>>>(d_internal,
        //                                                                                  edr, d_curr_elevel, 
        //                                                                                  d_left_elem, d_right_elem, 
        //                                                                                  csum, color_internal[k]);
        //       csum+=color_internal[k];
        //     }
        // }
        csum = 0;
        for(int k = 0; k < 6; k++) {
          if(color_interface[k] > 0){
              enforce_nonconformity_list<<<get_blocks(color_interface[k], n_threads), n_threads>>>(d_interface,
                                                                                         edr, d_curr_elevel, 
                                                                                         d_left_elem, d_right_elem, 
                                                                                         csum, color_interface[k]);
              csum+=color_interface[k];
            }
        }


        if(num_ecadd > 0){
            read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
                                                                        d_echild1, d_echild2, d_echild3, d_echild4,
                                                                        d_epos,
                                                                        edr,
                                                                        num_ecadd);
            compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
        }
    }


    csum = 0;
    for(int k = 0; k < 6; k++) {
      if(color_size[k] > 0){
          enforce_nonconformity<<<get_blocks(color_size[k], n_threads), n_threads>>>(edr, d_curr_elevel, 
                                                                                             d_left_elem, d_right_elem, 
                                                                                             csum, color_size[k]);
          csum+=color_size[k];
        }
    }
    if(num_ecadd > 0){
        read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
                                                                    d_echild1, d_echild2, d_echild3, d_echild4,
                                                                    d_epos,
                                                                    edr,
                                                                    num_ecadd);
        compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
    }

    // STOP_TIMING(enforce_nonconformity2); PRINT_TIMING(enforce_nonconformity2);
}



// __global__ void is_nonconforming(int *edr, int *left_elem, int *right_elem, 
//                                  int *lsn, int *rsn, 
//                                  int *flag, int csum, int max) {
//     int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
//     if(idx  < max + csum) { 
//       int nc = (lsn[idx] >= 3 || rsn[idx] >= 3);
//       int le = left_elem[idx]; int re = right_elem[idx];
//       int coarsen;
//       coarsen = (re > -1) &&  (edr[re] == -1) || (edr[le] == -1)  ? 1 : (edr[le] == -1);
//       flag[idx] =  coarsen || nc;
//       // flag[idx] =   nc;

//     }
// }

// __global__ void enforce_nonconformity( int *pool, int *edr,  
//                                        int *elevel,
//                                        int *left_elem, int *right_elem,
//                                        int csum, int max) { 
//     int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
 
//     if (idx < max + csum) {
//         int left_pos, right_pos, left_level, right_level, max_level, left_old_level, right_old_level;
//         int pool_idx = pool[idx];
//         // read left and right data
//         left_pos   = left_elem[pool_idx];
//         right_pos  = right_elem[pool_idx];

//         left_old_level = elevel[left_pos];
//         right_old_level = right_pos > -1 ? elevel[right_pos] : -1;

//         left_level = edr[left_pos] + left_old_level;
//         right_level = right_pos > -1 ? edr[right_pos] + right_old_level : -1;

//         max_level = left_level > right_level ? left_level : right_level;

//         edr[left_pos] = max_level - left_level > 1 ? max_level - 1 - left_old_level: left_level - left_old_level;
//         if(right_pos > -1)
//           edr[right_pos] = max_level - right_level > 1 ? max_level - 1  - right_old_level: right_level - right_old_level;
//     }

// }

// void enforce_nonconformity(){
//     int csum, offset;
//     int pool = 0;

//     START_TIMING(enforce_nonconformity1);                   
//     clear_coarsening_flags<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, curr_num_elem); 
//     if(num_ecadd > 0){
//         write_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
//                                                                    d_eparent, d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                    d_epos,
//                                                                    edr,
//                                                                    num_ecadd);
//         compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//     }
//     STOP_TIMING(enforce_nonconformity1); printf("enforce_nonconformity1 %lf\n", GET_TOTAL_TIMING(enforce_nonconformity1));
//     START_TIMING(enforce_nonconformity2);     


//     int num_nonconforming[6];

//     csum = 0;
//     offset = 0;
//     for(int i = 0; i < 6; i++){
//       if(color_size[i] > 0){
//         is_nonconforming<<<get_blocks(color_size[i], n_threads), n_threads>>>(edr, d_left_elem, d_right_elem,
//                                                                               d_left_side_number, d_right_side_number, 
//                                                                               d_inflag, csum, color_size[i]);
//         position_compaction_offset(d_pool + offset, d_inflag + csum, csum, color_size[i], &num_nonconforming[i]); // remove elements that shouln't be coarsened
//         csum += color_size[i];
//         offset += num_nonconforming[i];
//       }
//       else
//         num_nonconforming[i] = 0;
//     }

//     for(int i = 0; i < max_level-1; i++) {
//         csum = 0;
//         for(int k = 0; k < 6; k++) {
//           if(num_nonconforming[k] > 0){
//               enforce_nonconformity<<<get_blocks(num_nonconforming[k], n_threads), n_threads>>>(d_pool, edr, d_curr_elevel, 
//                                                                                          d_left_elem, d_right_elem, 
//                                                                                          csum, num_nonconforming[k]);
//               csum+=num_nonconforming[k];
//             }
//         }


//       if(num_ecadd > 0){
//           read_edr<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,d_inflag,
//                                                                       d_echild1, d_echild2, d_echild3, d_echild4,
//                                                                       d_epos,
//                                                                       edr,
//                                                                       num_ecadd);
//           compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
//       }


//       csum = 0;
//       offset = 0;
//       for(int j = 0; j < 6; j++){
//         if(color_size[j] > 0){
//           is_nonconforming<<<get_blocks(color_size[j], n_threads), n_threads>>>(edr, d_left_elem, d_right_elem,
//                                                                                 d_left_side_number, d_right_side_number, 
//                                                                                 d_inflag, csum, color_size[j]);
//           position_compaction_offset(d_pool + offset, d_inflag + csum, csum, color_size[j], &num_nonconforming[j]); // remove elements that shouln't be coarsened
//           csum += color_size[j];
//           offset += num_nonconforming[j];
//         }
//         else
//           num_nonconforming[j] = 0;
//       }

//     }
//     STOP_TIMING(enforce_nonconformity2); printf("enforce_nonconformity2 %lf\n", GET_TOTAL_TIMING(enforce_nonconformity2));

// }








void initial_preparations() {
    //post-process the flags computed by the error indicator
    // remove refinement flags for elements that cannot be refined i.e. level == max_level
  START_TIMING(flagElementsandSidesforAdapatation1);
    remove_incompatible_refinements_geo<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_curr_elevel,  
                                                                                         edr,
                                                                                         max_level,
                                                                                         d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y,
                                                                                         t,
                                                                                         curr_num_elem) ;

    remove_incompatible_refinements<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_curr_elevel,  
                                                                                         d_epos, edr,
                                                                                         max_level,
                                                                                         curr_num_elem) ;

    // remove coarsening flags for elements that cannot be coarsened i.e. level == 0
    remove_incompatible_coarsening<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_curr_elevel,  
                                                                                        d_epos, edr,
                                                                                        max_level,
                                                                                        curr_num_elem) ;

    // populate d_ecadd
    is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_inflag,-1, curr_num_elem);
    compaction(d_ecadd, d_curr_elem, d_inflag, curr_num_elem, &num_ecadd); // out of place into d_ecadd


    if(num_ecadd > 0){
        is_fourth<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
                                                                       d_eparent, 
                                                                       d_epos,
                                                                       d_echild1,d_echild2,d_echild3,d_echild4,
                                                                       num_ecadd);

        compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove incompatible elements
    }
    if(num_ecadd > 0){
        all_coarsened<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
                                                                       d_echild1, d_echild2, d_echild3, d_echild4,
                                                                       edr, d_epos,
                                                                       num_ecadd); 
        compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove incompatible elements
    }
    STOP_TIMING(flagElementsandSidesforAdapatation1); PRINT_TIMING(flagElementsandSidesforAdapatation1);
    enforce_nonconformity();
    // enforce_nonconformity_fast();
}


void refine_coarsen_postprocess(){
    int keep_going = 1, count = 0 ;
    // fill refinement islands
    // technically you should check if there are any changes in the edr and coarsen_list arrays.
    // checking if there is a change in the number of refined / coarsens doesn't mean there weren't changes.
    while(keep_going) {
        cudaMemcpy(d_prev_edr, edr, curr_num_elem*sizeof(int), cudaMemcpyDeviceToDevice);

        refinement_fill<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_enum_sides, d_curr_elevel,  
                                                                                  d_curr_s1, d_curr_s2, d_curr_s3,
                                                                                  d_spos, d_schild1, d_schild2,
                                                                                  d_left_elem, d_right_elem,
                                                                                  curr_num_elem);
        swap(&edr, &d_enum_sides); 
        enforce_nonconformity();

        if(num_ecadd > 0) {
            coarsening_fill<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_inflag,
                                                                             d_epos,
                                                                             d_curr_s1, d_curr_s2, d_curr_s3,
                                                                             d_spos, d_left_elem, d_right_elem,
                                                                             d_schild1,
                                                                             d_echild1, d_echild2, d_echild3, d_echild4,
                                                                             d_curr_elevel, edr, 
                                                                             num_ecadd);
            compaction(d_ecadd, d_inflag, num_ecadd, &num_ecadd); // remove elements that shouln't be coarsened
        }
        enforce_nonconformity();


        is_neq<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_prev_edr,edr, d_inflag, curr_num_elem);
        CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes_scan, d_inflag, d_outflag, curr_num_elem));
        keep_going = (get_num_valid(d_inflag, d_outflag, curr_num_elem) > 0);
        count++;
    }


    if(verbose)
        printf("\nnumber of postprocessing loops is: %i\n", count);
}



__global__ void remove_refinement_singletons(int *edr, int *edrtemp, int *elevel,
                                     int *elem_s1, int *elem_s2, int *elem_s3,
                                     int *spos, int *d_schild1, int *d_schild2,
                                     int *left_elem, int *right_elem,
                                     int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int ePOSlevel ;
    int sID[6], sPOS[6], neighPOS[6], ePOSleft[6], ePOSright[6], neighlevel[6];
    int above= 0 , same = 0, below = 0;
    int haschildren;
    int neigh_edr;

    if(idx  < max) { 

        if(edr[idx] == 1) {
            // load the sides of the elements
            sID[0] = elem_s1[idx];
            sID[1] = elem_s2[idx];
            sID[2] = elem_s3[idx];

            for(int i = 0; i < 3; i++) {
                haschildren = d_schild1[sID[i]];
                if(haschildren > -1){
                    sPOS[i] = spos[d_schild1[sID[i]]];
                    sPOS[i + 3] = spos[d_schild2[sID[i]]];
                }
                else{
                    sPOS[i] = spos[ sID[i] ];
                    sPOS[i + 3] = -1;
                }
            }

            ePOSlevel = elevel[idx] + 1;
            for(int i = 0; i < 6; i++) {
                if(sPOS[i] > -1) {
                    ePOSleft[i] = left_elem[sPOS[i] ] ;
                    ePOSright[i] = right_elem[sPOS[i] ] ;

                    if(ePOSright[i] > -1) {
                        neighPOS[i]  = (idx == ePOSleft[i])  ? ePOSright[i]  : ePOSleft[i] ;
                        neigh_edr = edr[neighPOS[i]];
                        neighlevel[i]  = elevel[neighPOS[i]] + neigh_edr ;

                        if(neighlevel[i] > ePOSlevel)
                            below = below + 1 ;
                        else if(neighlevel[i] == ePOSlevel)
                            same = same + 1;
                        else 
                            above = above + 1;
                    }
                }
            }

            edrtemp[idx] = (below + same == 0) ? 0 : 1;  
        }
        else
            edrtemp[idx] = edr[idx];
    }
}



__global__ void remove_coarsening_singletons( int *coarsenlist, int *flag,
                                              int *epos,
                                              int *elem_s1, int *elem_s2, int *elem_s3,
                                              int *spos, int *left_elem, int *right_elem,
                                              int *schild1,
                                              int *echild1, int *echild2, int *echild3, int *echild4,
                                              int *elevel, int *edr, 
                                              int max) { 
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID[6], sPOS[6], neighbour[6], neighbour_level[6], mylevel;
    double same = 0, above = 0, below = 0;
    int eID, eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int POSchild1, POSchild2, POSchild3, POSchild4;
    int neigh_edr;
    if(idx  < max) { 
        eID = coarsenlist[idx];

        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];

        POSchild1 = epos[eIDchild1];
        POSchild2 = epos[eIDchild2];
        POSchild3 = epos[eIDchild3];
        POSchild4 = epos[eIDchild4];

        // load the sides of the elements
        sID[0] = elem_s1[POSchild1];
        sID[1] = elem_s1[POSchild2];
        sID[2] = elem_s2[POSchild2];
        sID[3] = elem_s2[POSchild3];
        sID[4] = elem_s3[POSchild3];
        sID[5] = elem_s3[POSchild1];

        mylevel = elevel[POSchild1] -1;

        sPOS[0] = spos[sID[0]];
        sPOS[1] = spos[sID[1]];
        sPOS[2] = spos[sID[2]];
        sPOS[3] = spos[sID[3]];
        sPOS[4] = spos[sID[4]];
        sPOS[5] = spos[sID[5]];

        sPOS[0] = sPOS[0] > -1 ? sPOS[0] : spos[schild1[sID[0]]];
        sPOS[1] = sPOS[1] > -1 ? sPOS[1] : spos[schild1[sID[1]]];
        sPOS[2] = sPOS[2] > -1 ? sPOS[2] : spos[schild1[sID[2]]];
        sPOS[3] = sPOS[3] > -1 ? sPOS[3] : spos[schild1[sID[3]]];
        sPOS[4] = sPOS[4] > -1 ? sPOS[4] : spos[schild1[sID[4]]];
        sPOS[5] = sPOS[5] > -1 ? sPOS[5] : spos[schild1[sID[5]]];


        neighbour[0] = ( left_elem[sPOS[0]] == POSchild1 ) ? right_elem[sPOS[0]] : left_elem[sPOS[0]];
        neighbour[1] = ( left_elem[sPOS[1]] == POSchild2 ) ? right_elem[sPOS[1]] : left_elem[sPOS[1]];
        neighbour[2] = ( left_elem[sPOS[2]] == POSchild2 ) ? right_elem[sPOS[2]] : left_elem[sPOS[2]];
        neighbour[3] = ( left_elem[sPOS[3]] == POSchild3 ) ? right_elem[sPOS[3]] : left_elem[sPOS[3]];
        neighbour[4] = ( left_elem[sPOS[4]] == POSchild3 ) ? right_elem[sPOS[4]] : left_elem[sPOS[4]];
        neighbour[5] = ( left_elem[sPOS[5]] == POSchild1 ) ? right_elem[sPOS[5]] : left_elem[sPOS[5]];

        for(int i = 0; i < 6; i++) {
            if(neighbour[i] > -1) {
                neigh_edr = edr[neighbour[i]];
                neighbour_level[i] = elevel[neighbour[i]] + neigh_edr;
                if( neighbour_level[i] > mylevel )
                    below = below + 0.5 * (neigh_edr == -1) + 1 * (neigh_edr == 0) + 1 * (neigh_edr == 1);
                else if(neighbour_level[i] == mylevel)
                    same = same  + 0.5*(neigh_edr == 0) + 0.5 * (neigh_edr == -1);
                else
                    above = above + 1 * (neigh_edr == -1);
            }
        }


        if(below + above < 1e-10) {
            flag[idx] =  1 ;
            edr[POSchild1] =  -1 ;
            edr[POSchild2] =  -1 ;
            edr[POSchild3] =  -1 ;
            edr[POSchild4] =  -1 ; 
        }
        else{
            flag[idx] =  0 ;
        }

    }
}





void singleton_postprocess() {
// refinement singletons
    remove_refinement_singletons<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_enum_sides, d_curr_elevel,  
                                                                               d_curr_s1, d_curr_s2, d_curr_s3,
                                                                               d_spos, d_schild1, d_schild2,
                                                                               d_left_elem, d_right_elem,
                                                                               curr_num_elem);

    swap(&edr, &d_enum_sides); 

    int coarsening_singletons;
    is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_inflag,0, curr_num_elem);
    compaction(d_enum_sides, d_curr_elem, d_inflag, curr_num_elem, &coarsening_singletons); // out of place into d_ecadd

    if(coarsening_singletons > 0){
        is_fourth<<<get_blocks(coarsening_singletons, n_threads), n_threads>>>(d_enum_sides, d_inflag,
                                                                   d_eparent, 
                                                                   d_epos,
                                                                   d_echild1,d_echild2,d_echild3,d_echild4,
                                                                   coarsening_singletons);

        compaction(d_enum_sides, d_inflag, coarsening_singletons, &coarsening_singletons); // remove incompatible elements
    }

    if(coarsening_singletons > 0){
        remove_coarsening_singletons<<<get_blocks(coarsening_singletons, n_threads), n_threads>>>(d_enum_sides, d_inflag,
                                                                                                  d_epos,
                                                                                                  d_curr_s1, d_curr_s2, d_curr_s3,
                                                                                                  d_spos, d_left_elem, d_right_elem,
                                                                                                  d_schild1,
                                                                                                  d_echild1, d_echild2, d_echild3, d_echild4,
                                                                                                  d_curr_elevel, edr, 
                                                                                                  coarsening_singletons);

        compaction(d_enum_sides, d_inflag, coarsening_singletons, &coarsening_singletons); // remove incompatible elements

    }
    if(coarsening_singletons > 0){
        cudaMemcpy(d_ecadd + num_ecadd, d_enum_sides, coarsening_singletons*sizeof(int), cudaMemcpyDeviceToDevice);
        num_ecadd = num_ecadd + coarsening_singletons;
    }

    if(verbose){
        printf("number of singletons added: c %i\n", coarsening_singletons);
    }
}


void prepare_for_refine_and_coarsen() {
    initial_preparations();
    // refine_coarsen_postprocess();
    // singleton_postprocess();
}



void flagElementsandSidesforAdapatation(){
    int global_error, global_error_smooth, global_error_disc;
    int local_error_order;
    int local_error_order_disc;
    int error_indicator = GRAD;
START_TIMING(grad); 

    switch (error_indicator) {
        case MAX: evaluate_max(d_error_estimate, 
                                     d_curr_c, 
                                     curr_num_elem);
                break;
        case GRAD: evaluate_gradient(d_error_estimate, 
                                     d_curr_c, 
                                     d_curr_xr, d_curr_xs,
                                     d_curr_yr, d_curr_ys,
                                     d_curr_J,
                                     n_p,  n_quad,
                                     d_w, d_basis_grad_x, d_basis_grad_y,
                                     d_circles,  
                                     curr_num_elem);
                local_error_order = order + 3;
                break;
        case GRAD_TRUMPET: evaluate_gradient_trumpet(d_error_estimate, 
                                                     d_curr_c, 
                                                     d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y,
                                                     d_curr_xr, d_curr_xs,
                                                     d_curr_yr, d_curr_ys,
                                                     d_curr_J,
                                                     n_p,  n_quad,
                                                     d_w, d_basis_grad_x, d_basis_grad_y,
                                                     d_circles,
                                                     curr_num_elem);
                local_error_order = order + 3;
                break;
        case JUMPS: local_error_order = order + 2;
                    local_error_order_disc = 2;
                    evaluate_jumps(d_error_estimate,
                                   d_curr_c, d_curr_J,
                                   d_curr_side, d_curr_originalside,
                                   d_curr_slevel,d_slength, 
                                   d_left_elem, d_right_elem,
                                   d_left_side_number, d_right_side_number,
                                   color_size, curr_num_elem, curr_num_sides);
                    // shock_detector(d_flag,d_sdetector, d_error_estimate, d_circles, 0.5*(local_error_order+local_error_order_disc), curr_num_elem);
                break;
        case RHS:local_error_order = order + 2;
                 local_error_order_disc = 2;
                 evaluate_error_rhs(d_error_estimate, 
                                     d_slength,
                                     d_curr_s1, d_curr_s2, d_curr_s3,
                                     d_spos, d_schild1, d_schild2,
                                     d_curr_originalside,
                                     order,
                                     curr_num_elem);
                 shock_detector(d_flag, d_sdetector,d_error_estimate, d_circles, 0.5*(local_error_order+local_error_order_disc), curr_num_elem);
                break;
        case EXACT: local_error_order = order + 3; 
                    local_error_order_disc = 2;
                    evaluate_error_l1(d_curr_c, d_error_estimate, 
                                      d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
                                      0, t,
                                      curr_num_elem);
                    // shock_detector(d_flag, d_sdetector,d_error_estimate, d_circles, 0.5*(local_error_order+local_error_order_disc), curr_num_elem);
                break;
        }
        // if(num % 1000 == 0 || num <=0)
            // write_limit(curr_num_elem, d_error_estimate, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y);

STOP_TIMING(grad); PRINT_TIMING(grad);

        global_error = local_error_order_disc-1;
        global_error_smooth = local_error_order-2;
        global_error_disc = local_error_order_disc-1;
    // refine_and_coarsen_optimize(d_error_estimate, d_curr_elem,
    //                                   edr, d_epos,
    //                                   verbose,global_error_smooth,
    //                                   curr_num_elem);
        double alpha = 1.1, beta = 1e-2;
          // double alpha = 2., beta = 2.5e-3; 
        // double alpha = 2., beta = 1e-3; // max_level = 17
        // 5e-3 for tp
        // 2e-1 for dm
        // 1e-2 for vortex and KH instability
    if(num < 0){
      evaluate_jumps(d_error_estimate,
                                   d_curr_c, d_curr_J,
                                   d_curr_side, d_curr_originalside,
                                   d_curr_slevel,d_slength, 
                                   d_left_elem, d_right_elem,
                                   d_left_side_number, d_right_side_number,
                                   color_size, curr_num_elem, curr_num_sides);
        refine_and_coarsen(d_error_estimate, d_curr_elem,
                     edr, d_epos,
                     verbose,
                     -1,1e-8,
                     curr_num_elem);
    }
    else
      refine_and_coarsen(d_error_estimate, d_curr_elem,
             edr, d_epos,
             verbose,
             beta / alpha, beta * alpha,
             curr_num_elem);

      // refine_and_coarsen_optimize_parallel(d_error_estimate, d_curr_elem,
      //                                   edr, d_epos,
      //                                   verbose,global_error_smooth,
      //                                   curr_num_elem);

    // print_array(d_error_estimate, curr_num_elem);


    // refine_and_coarsen_optimize_parallel(d_error_estimate, d_curr_elem,
    //                                   edr, d_epos,
    //                                   verbose,global_error_smooth,
    //                                   curr_num_elem);
    // refine_and_coarsen_optimize_shock_A(d_error_estimate, d_curr_elem, d_flag,
    //                                     edr, d_epos, 
    //                                     verbose,
    //                                     global_error_smooth,
    //                                     global_error_disc,
    //                                     global_error,
    //                                     curr_num_elem);
    // refine_and_coarsen_optimize_shock_B(d_error_estimate, d_curr_elem, d_flag,
    //                                   edr, d_epos, 
    //                                   verbose,
    //                                   global_error_smooth,
    //                                   global_error_disc,
    //                                   global_error,
    //                                   curr_num_elem);
    // set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr,1,curr_num_elem);
    // print_array(edr, curr_num_elem);
    prepare_for_refine_and_coarsen();
    // if(num % 100 == 0)
    // write_cellwise(curr_num_elem, d_sdetector, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, 2);
      // write_cellwise(curr_num_elem, d_flag, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, 1);

    // determine now the side indicator
    s_indicator<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(edr, sdr, 
                                                                      d_curr_elevel, d_curr_slevel, 
                                                                      d_left_elem, d_right_elem,
                                                                      curr_num_sides, num);
}


void coarsen() {


    is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_inflag,-1, curr_num_elem);
    compaction(d_efree + freeElementCount, d_curr_elem, d_inflag, curr_num_elem, &num_ecoarsen); // out of place into d_sfree + freeSidesCount
    freeElementCount = freeElementCount + num_ecoarsen;

    if(num_ecadd > 0) {

      if(n_p == 3)
        coarsen_c1<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,
                                                                   d_echild1, d_echild2, d_echild3, d_echild4,
                                                                   d_curr_c, d_epos,
                                                                   N, 
                                                                   num_ecadd);
      else if(n_p == 6)
        coarsen_c2<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,
                                                           d_echild1, d_echild2, d_echild3, d_echild4,
                                                           d_curr_c, d_epos,
                                                           N, 
                                                           num_ecadd);
      else if(n_p == 10)
        coarsen_c3<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,
                                                           d_echild1, d_echild2, d_echild3, d_echild4,
                                                           d_curr_c, d_epos,
                                                           N, 
                                                           num_ecadd);
      else if(n_p == 15)
        coarsen_c4<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,
                                                           d_echild1, d_echild2, d_echild3, d_echild4,
                                                           d_curr_c, d_epos,
                                                           N, 
                                                           num_ecadd);
      else
        coarsen_c<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd,
                                                                   d_echild1, d_echild2, d_echild3, d_echild4,
                                                                   d_curr_c, 
                                                                   d_curr_J,
                                                                   d_curr_V1x, d_curr_V1y,
                                                                   d_curr_V2x, d_curr_V2y,
                                                                   d_curr_V3x, d_curr_V3y,
                                                                   d_epos,
                                                                   n_p, N, n_quad,
                                                                   d_basis_refined, d_w, d_basis,
                                                                   num_ecadd);

        coarsen_geo<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, 
                                                                     d_echild1, d_echild2, d_echild3, d_echild4,
                                                                     d_curr_V1x, d_curr_V1y,
                                                                     d_curr_V2x, d_curr_V2y,
                                                                     d_curr_V3x, d_curr_V3y,
											                         d_curr_xr, d_curr_yr,
											                         d_curr_xs, d_curr_ys,
											                         d_curr_J, 
                                                                     d_epos,
                                                                     num_ecadd);
        coarsenElementData<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_ecadd, d_curr_elem,
                                                                            d_epos,
                                                                            d_echild1, d_echild2,  d_echild3, d_echild4,
                                                                            d_curr_s1,d_curr_s2,d_curr_s3,
                                                                            d_curr_left1, d_curr_left2, d_curr_left3,
                                                                            d_curr_elevel,
                                                                            d_sparent, 
                                                                            d_efree, freeElementCount,
                                                                            curr_num_elem, num_ecadd); 


	}

    is_e<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(sdr, d_inflag,-1, curr_num_sides);
    compaction(d_sfree + freeSidesCount, d_curr_side, d_inflag, curr_num_sides, &num_scoarsen); // out of place into d_sfree + freeSidesCount
    freeSidesCount = freeSidesCount + num_scoarsen;

    if(num_scoarsen > 0){

        // determine which sides will be in the coarsened mesh
        is_scoarsenable<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_side, sdr, 
                                                                              d_sparent, d_schild1, d_inflag,
                                                                              curr_num_sides);
    	compaction(d_pos_sfree, d_curr_side, d_inflag, curr_num_sides, &num_scadd); // out of place into d_pos_sfree 


        if(num_scadd > 0) {
            coarsenSideData<<<get_blocks(num_scadd, n_threads), n_threads>>>(d_pos_sfree,
                                                                             d_curr_side, d_curr_originalside, d_curr_slevel, 
                                                                             d_spos, d_sparent,  d_schild1, d_schild2,
                                                                             d_curr_scolor,
                                                                             curr_num_sides, num_scadd);
        }


    }


} 


void refine() {
	int prev_freeElementCount = freeElementCount;
	int prev_freeSidesCount = freeSidesCount;


    is_e<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_side, d_inflag, -1, curr_num_sides);
    position_compaction(d_pos_sfree, d_inflag, curr_num_sides, &num_pos_sfree); 

    is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, -1, curr_num_elem);
    position_compaction(d_pos_efree, d_inflag, curr_num_elem, &num_pos_efree);  


    is_e<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(sdr, d_inflag,
                                                               1, curr_num_sides);
    compaction(d_srefine, d_curr_side, d_inflag, curr_num_sides, &num_srefine); 
    int sID_offset = 0;  

    if(num_srefine > 0) {
    	if(verbose)
    		printf("num_epos_free %i\n", num_pos_efree);

    	// is_e<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_side, d_inflag, -1, curr_num_sides);
    	// position_compaction(d_pos_sfree, d_inflag, curr_num_sides, &num_pos_sfree); 

        refine_sides<<<get_blocks(num_srefine,n_threads), n_threads>>>(d_srefine, d_curr_side,
                                                                      d_curr_originalside, 
                                                                      d_curr_slevel, d_curr_scolor, 
                                                                      d_spos,
                                                                      d_sparent,
                                                                      d_schild1, d_schild2,
                                                                      d_sfree, d_pos_sfree, // d_sfree is the free positions in stree and d_pos_sfree are the free positions in d_sides
                                                                      stree_size, freeSidesCount,
                                                                      curr_num_sides, num_pos_sfree,
                                                                      num_srefine); // refine those sides in the tree

        if(num_srefine * 2 >= freeSidesCount) {
            sID_offset =  2 * num_srefine - freeSidesCount; // need this because sides are being refined in refine_sides kernel
            freeSidesCount = 0;
        }
        else{
            cudaMemcpy(d_inflag, d_sfree + 2 * num_srefine, (freeSidesCount - 2*num_srefine) * sizeof(int), cudaMemcpyDeviceToDevice);
            cudaMemcpy(d_sfree, d_inflag, (freeSidesCount - 2*num_srefine) * sizeof(int), cudaMemcpyDeviceToDevice);
            freeSidesCount = freeSidesCount - 2 * num_srefine;
        }

    }

    is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, d_inflag, 1, curr_num_elem);
    compaction(d_erefine, d_curr_elem, d_inflag, curr_num_elem, &num_erefine);

    if(num_erefine > 0) { 
        // is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, -1, curr_num_elem);
        // position_compaction(d_pos_efree, d_inflag, curr_num_elem, &num_pos_efree);

        refine_geo<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_curr_V1x, d_curr_V1y,
                                                                      d_curr_V2x, d_curr_V2y,
                                                                      d_curr_V3x, d_curr_V3y,
                                                                      d_curr_xr, d_curr_yr,
                                                                      d_curr_xs, d_curr_ys,
                                                                      d_curr_J, 
                                                                      d_epos,
                                                                      curr_num_elem,
                                                                      d_pos_efree, num_pos_efree,
                                                                      d_erefine, num_erefine);

        if(n_p == 3)
          refine_c1<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_curr_c,
                                                            curr_num_elem,
                                                            d_epos,
                                                            d_erefine,
                                                            N, 
                                                            d_pos_efree, num_pos_efree,
                                                            num_erefine);
        else if(n_p == 6)
          refine_c2<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_curr_c,
                                                            curr_num_elem,
                                                            d_epos,
                                                            d_erefine,
                                                            N, 
                                                            d_pos_efree, num_pos_efree,
                                                            num_erefine);
        else if(n_p == 10)
          refine_c3<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_curr_c,
                                                            curr_num_elem,
                                                            d_epos,
                                                            d_erefine,
                                                            N, 
                                                            d_pos_efree, num_pos_efree,
                                                            num_erefine);
        else if(n_p == 15)
          refine_c4<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_curr_c,
                                                            curr_num_elem,
                                                            d_epos,
                                                            d_erefine,
                                                            N, 
                                                            d_pos_efree, num_pos_efree,
                                                            num_erefine);
        else
          refine_c<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_curr_c,
                                                                      curr_num_elem,
                                                                      d_epos,
                                                                      d_erefine,
                                                                      n_p, N, n_quad,
                                                                      d_basis_refined, d_w, d_basis,
                                                                      d_pos_efree, num_pos_efree,
                                                                      num_erefine);

        refine_elements<<<get_blocks(num_erefine, n_threads), n_threads>>>(d_erefine, d_srefine,
                                                                           d_curr_elem, d_curr_side,
                                                                           d_curr_originalside,d_curr_scolor, 
                                                                           d_curr_s1, d_curr_s2, d_curr_s3,
                                                                           d_curr_left1, d_curr_left2, d_curr_left3, 
                                                                           d_curr_elevel, d_curr_slevel,
                                                                           d_epos, d_spos,
                                                                           d_echild1, d_echild2, d_echild3, d_echild4,
                                                                           d_eparent, d_sparent,
                                                                           d_schild1, d_schild2,

                                                                           d_efree, d_sfree,
                                                                           freeElementCount, freeSidesCount,

                                                                           d_pos_efree, d_pos_sfree,
                                                                           num_pos_efree, num_pos_sfree,

                                                                           etree_size, stree_size + sID_offset, 
                                                                           curr_num_elem, curr_num_sides,

                                                                           num_srefine, num_erefine, num); // refine those elements in the tree

        if(num_erefine * 3 >= freeSidesCount) { 
            freeSidesCount = 0;
        }
        else{
            cudaMemcpy(d_inflag, d_sfree + 3 * num_erefine, (freeSidesCount - 3*num_erefine) * sizeof(int), cudaMemcpyDeviceToDevice);
            cudaMemcpy(d_sfree, d_inflag, (freeSidesCount - 3*num_erefine) * sizeof(int), cudaMemcpyDeviceToDevice);
            freeSidesCount = freeSidesCount - 3 * num_erefine;
        }

        if(num_erefine * 4 >= freeElementCount) {
            freeElementCount = 0;
        }
        else{
            cudaMemcpy(d_inflag, d_efree + 4 * num_erefine, (freeElementCount - 4*num_erefine) * sizeof(int), cudaMemcpyDeviceToDevice);
            cudaMemcpy(d_efree, d_inflag , (freeElementCount - 4*num_erefine) * sizeof(int), cudaMemcpyDeviceToDevice);
            freeElementCount = freeElementCount - 4 * num_erefine;
        }
    }
    new_etree_size = (4*num_erefine > prev_freeElementCount) ?  etree_size + 4*num_erefine - prev_freeElementCount :  etree_size;
    new_stree_size = (2*num_srefine + 3 *  num_erefine > prev_freeSidesCount) ? stree_size + 2*num_srefine + 3 *  num_erefine - prev_freeSidesCount :  stree_size;   
}




void determineOrder() { 
  
    // set_value<<<get_blocks(new_num_sides,n_threads), n_threads>>>(d_s_ordering, -1, new_num_sides);
    // new positions of all, old and new sides
    int offset = 0;
    for(int i = 0; i < 6; i++){ 
        is_e<<<get_blocks(new_num_sides, n_threads), n_threads>>>(d_curr_scolor, d_inflag,
                                                                  i, new_num_sides);

        CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes_scan, d_inflag, d_outflag, new_num_sides));
        set_ordering<<<get_blocks(new_num_sides,n_threads), n_threads>>>(d_inflag, d_outflag, d_s_ordering, offset, new_num_sides);
        color_size[i] = get_num_valid(d_inflag, d_outflag, new_num_sides) ;
        offset += color_size[i] ;


    }

}

void determineOrder_cub() { 
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_curr_scolor, sizeof(int) * new_num_sides, cudaMemcpyDeviceToDevice));
    set_value<<<get_blocks(new_num_sides,n_threads), n_threads>>>(d_values.d_buffers[d_values.selector], new_num_sides);
    // print_array(d_keys.d_buffers[d_keys.selector], new_num_sides);
    // print_array(d_values.d_buffers[d_values.selector], new_num_sides);
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage2, temp_storage_bytes_sort2, d_keys, d_values, new_num_sides));
    cudaMemcpy(d_s_ordering, d_values.Current(), sizeof(int) * new_num_sides, cudaMemcpyDeviceToDevice);
    // print_array(d_keys.d_buffers[d_keys.selector], new_num_sides);
    // print_array(d_values.d_buffers[d_values.selector], new_num_sides);
}

void reorder() {
    ordered_copy(d_curr_side, d_s_ordering, new_num_sides);
    ordered_copy(d_curr_originalside, d_s_ordering, new_num_sides);
    ordered_copy(d_curr_slevel, d_s_ordering, new_num_sides);
    ordered_copy(d_curr_scolor, d_s_ordering, new_num_sides);

    update_pos<<<get_blocks(new_num_elem, n_threads), n_threads>>>(d_curr_elem, d_epos, new_num_elem);
    update_pos<<<get_blocks(new_num_sides, n_threads), n_threads>>>(d_curr_side, d_spos, new_num_sides);

} 





void redetermineConnectivity() {
    populate_sides<<<get_blocks(new_num_elem, n_threads), n_threads>>>(d_spos,
                                                                       d_schild1, d_schild2,
                                                                       d_curr_s1, d_curr_s2, d_curr_s3,
                                                                       d_curr_left1, d_curr_left2, d_curr_left3,
                                                                       d_left_elem, d_right_elem,
                                                                       d_left_side_number, d_right_side_number,
                                                                       new_num_elem);

    update_connectivity<<<get_blocks(new_num_sides,n_threads), n_threads>>>(d_left_elem, d_right_elem,
                                                                            d_left_side_number, d_right_side_number,
                                                                            d_curr_elem, d_curr_side,
                                                                            d_curr_elevel,
                                                                            d_sparent,
                                                                            d_schild1, d_schild2,
                                                                            new_num_sides);

}

void replaceCurrentMesh() {
    etree_size = new_etree_size;
    stree_size = new_stree_size;
    curr_num_elem = new_num_elem;
    curr_num_sides = new_num_sides;
    set_num_elem(new_num_elem);
    set_num_sides(new_num_sides);


    // find the min inscribed circle
    // preval_inscribed_circles<<<get_blocks(new_num_elem,n_threads), n_threads>>>(d_circles, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, new_num_elem);
    init_nl_h<<<get_blocks(new_num_elem, n_threads),n_threads>>>(d_circles,
                                                                      d_curr_V1x, d_curr_V1y,
                                                                      d_curr_V2x, d_curr_V2y,
                                                                      d_curr_V3x, d_curr_V3y,
                                                                      new_num_elem);


    // double *min_radius = (double *) malloc(new_num_elem * sizeof(double)); 
    // cudaMemcpy(min_radius, d_circles, new_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    // curr_min_r = min_radius[0];
    // for (int i = 1; i < new_num_elem; i++) {
    //     curr_min_r = (min_radius[i] < curr_min_r) ? min_radius[i] : curr_min_r;
    //     }

    // free(min_radius);

    if(verbose){
        printf("color sizes: %i %i %i %i %i %i\n", color_size[0], color_size[1], color_size[2], color_size[3], color_size[4], color_size[5]);
    }

}

void resizeData() {
	if(new_num_sides < curr_num_sides) {
	    is_neq<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_side, d_inflag,-1, curr_num_sides);
	    compaction(d_curr_side, d_inflag, curr_num_sides, &new_num_sides); 

	    compaction(d_curr_slevel, d_inflag, curr_num_sides, &new_num_sides);
	    compaction(d_curr_originalside, d_inflag, curr_num_sides, &new_num_sides);
	    compaction(d_curr_scolor, d_inflag, curr_num_sides, &new_num_sides);
	}

	if(new_num_elem < curr_num_elem) {
	    is_neq<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag,-1, curr_num_elem);
	    compaction(d_curr_elem, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_elevel, d_inflag, curr_num_elem, &new_num_elem); 

	    compaction(d_curr_s1, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_s2, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_s3, d_inflag, curr_num_elem, &new_num_elem);

	    compaction(d_curr_left1, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_left2, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_left3, d_inflag, curr_num_elem, &new_num_elem); 

	    compaction(d_curr_V1x, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_V1y, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_V2x, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_V2y, d_inflag, curr_num_elem, &new_num_elem);
	    compaction(d_curr_V3x, d_inflag, curr_num_elem, &new_num_elem);
	    compaction(d_curr_V3y, d_inflag, curr_num_elem, &new_num_elem);

	    compaction(d_curr_xr, d_inflag, curr_num_elem, &new_num_elem); 
	    compaction(d_curr_yr, d_inflag, curr_num_elem, &new_num_elem);
	    compaction(d_curr_xs, d_inflag, curr_num_elem, &new_num_elem);
	    compaction(d_curr_ys, d_inflag, curr_num_elem, &new_num_elem);

	    compaction(d_curr_J, d_inflag, curr_num_elem, &new_num_elem);

	    for(int i = 0 ; i < N * n_p; i++)
	    	compaction(h_curr_c[i], d_inflag, curr_num_elem, &new_num_elem); 


	}

} 

void resizeData_inplace() {
  int w_size, n_size;
  if(new_num_sides < curr_num_sides) {

      is_neq<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_side, d_inflag,-1, curr_num_sides);

      in_place_compaction_init(d_inflag, d_to, d_from, curr_num_sides, &w_size, &n_size);
      in_place_compaction(d_curr_side, d_to, d_from, w_size);
      in_place_compaction(d_curr_slevel, d_to, d_from, w_size);
      in_place_compaction(d_curr_originalside, d_to, d_from, w_size);
      in_place_compaction(d_curr_scolor, d_to, d_from, w_size);


      // compaction(d_curr_side, d_inflag, curr_num_sides, &new_num_sides); 

      // compaction(d_curr_slevel, d_inflag, curr_num_sides, &new_num_sides);
      // compaction(d_curr_originalside, d_inflag, curr_num_sides, &new_num_sides);
      // compaction(d_curr_scolor, d_inflag, curr_num_sides, &new_num_sides);

      // is_eq<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_side, d_inflag,-1, curr_num_sides);
      // set_value<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_curr_scolor, d_inflag, -1, curr_num_sides);

  }

  if(new_num_elem < curr_num_elem) {
      is_neq<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag,-1, curr_num_elem);
      in_place_compaction_init(d_inflag, d_to, d_from, curr_num_elem, &w_size, &n_size);

      in_place_compaction(d_curr_elem, d_to, d_from, w_size);
      in_place_compaction(d_curr_elevel, d_to, d_from, w_size);

      in_place_compaction(d_curr_s1, d_to, d_from, w_size);
      in_place_compaction(d_curr_s2, d_to, d_from, w_size);
      in_place_compaction(d_curr_s3, d_to, d_from, w_size);

      in_place_compaction(d_curr_left1, d_to, d_from, w_size);
      in_place_compaction(d_curr_left2, d_to, d_from, w_size);
      in_place_compaction(d_curr_left3, d_to, d_from, w_size);

      in_place_compaction(d_curr_V1x, d_to, d_from, w_size);
      in_place_compaction(d_curr_V1y, d_to, d_from, w_size);
      in_place_compaction(d_curr_V2x, d_to, d_from, w_size);
      in_place_compaction(d_curr_V2y, d_to, d_from, w_size);
      in_place_compaction(d_curr_V3x, d_to, d_from, w_size);
      in_place_compaction(d_curr_V3y, d_to, d_from, w_size);

      in_place_compaction(d_curr_xr, d_to, d_from, w_size);
      in_place_compaction(d_curr_yr, d_to, d_from, w_size);
      in_place_compaction(d_curr_xs, d_to, d_from, w_size);
      in_place_compaction(d_curr_ys, d_to, d_from, w_size);

      in_place_compaction(d_curr_J, d_to, d_from, w_size);

      for(int i = 0 ; i < N * n_p; i++)
        in_place_compaction(h_curr_c[i], d_to, d_from, w_size);


      // compaction(d_curr_elem, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_elevel, d_inflag, curr_num_elem, &new_num_elem); 

      // compaction(d_curr_s1, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_s2, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_s3, d_inflag, curr_num_elem, &new_num_elem);

      // compaction(d_curr_left1, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_left2, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_left3, d_inflag, curr_num_elem, &new_num_elem); 

      // compaction(d_curr_V1x, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_V1y, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_V2x, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_V2y, d_inflag, curr_num_elem, &new_num_elem);
      // compaction(d_curr_V3x, d_inflag, curr_num_elem, &new_num_elem);
      // compaction(d_curr_V3y, d_inflag, curr_num_elem, &new_num_elem);

      // compaction(d_curr_xr, d_inflag, curr_num_elem, &new_num_elem); 
      // compaction(d_curr_yr, d_inflag, curr_num_elem, &new_num_elem);
      // compaction(d_curr_xs, d_inflag, curr_num_elem, &new_num_elem);
      // compaction(d_curr_ys, d_inflag, curr_num_elem, &new_num_elem);

      // compaction(d_curr_J, d_inflag, curr_num_elem, &new_num_elem);

      // for(int i = 0 ; i < N * n_p; i++)
      //   compaction(h_curr_c[i], d_inflag, curr_num_elem, &new_num_elem); 


  }

} 


void printStatistics() {
    double temp = 0 ;
    int levelcount[max_level];
    int smallest_level=0;
    printf("\n");
    for(int i = 0; i <= max_level; i++) {
        is_e<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elevel, d_inflag,
                                                         i, curr_num_elem);
        CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes_scan, d_inflag, d_outflag, curr_num_elem));
        levelcount[i] = get_num_valid(d_inflag, d_outflag, curr_num_elem);
        printf("level %i : %i\n ", i,  levelcount[i] );
        temp += levelcount[i]*(pow(2, i));
        if(levelcount[i] != 0)
            smallest_level = i;
    }

    printf("local timestepping would give a speed-up of %lf\n", ((double)curr_num_elem * pow(2, smallest_level))/temp );

    // cub::DeviceReduce::Min(d_temp_storage, temp_storage_bytes_scan, d_circles, d_max_error, curr_num_elem);
    // cudaMemcpy(&temp, d_max_error, sizeof(double), cudaMemcpyDeviceToHost);
    // printf("minimum inscribed circle is: %e\n", temp);
}


    // evaluate_error_l1(d_curr_c, d_error_estimate, 
    //                   d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
    //                    0, t,
    //                    curr_num_elem);
    // double *local_error = (double *) malloc(curr_num_elem*sizeof(double));
    // double tot_error =0;
    // cudaMemcpy(local_error, d_error_estimate, curr_num_elem*sizeof(double), cudaMemcpyDeviceToHost);
    // for(int i = 0; i < curr_num_elem; i++)
    //     tot_error+=local_error[i];
    // printf("\nTOTAL ERROR IS %.4e\n", tot_error);
    // free(local_error);


    // evaluate the error
    // if(num < 0)
        // evaluate_gradient(d_error_estimate, 
        //                   d_curr_c, 
        //                   d_curr_xr, d_curr_xs,
        //                   d_curr_yr, d_curr_ys,
        //                   d_curr_J,
        //                   n_p,  n_quad,
        //                   d_w, d_basis_grad_x, d_basis_grad_y,
        //                   d_circles,  
        //                   curr_num_elem);
        // local_error_order = order + 3;
        // evaluate_error_l1(d_curr_c, d_error_estimate, 
        //               d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
        //                0, t,
        //                curr_num_elem);
        // local_error_order = order + 3;
    // else
        // evaluate_error_rhs(d_error_estimate, 
        //                    d_slength,
        //                    d_curr_s1, d_curr_s2, d_curr_s3,
        //                    d_spos, d_schild1, d_schild2,
        //                    d_curr_originalside,
        //                    order,
        //                    curr_num_elem);


    // evaluate_jumps(d_error_estimate,
    //                d_curr_c, d_curr_J,
    //                d_curr_side, d_curr_originalside,
    //                d_curr_slevel,d_slength, 
    //                d_left_elem, d_right_elem,
    //                d_left_side_number, d_right_side_number,
    //                color_size, curr_num_elem, curr_num_sides);
    // local_error_order = order + 2;




    // evaluate_gradient_trumpet(d_error_estimate, 
    //                   d_curr_c, 
    //                   d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y,
    //                   d_curr_xr, d_curr_xs,
    //                   d_curr_yr, d_curr_ys,
    //                   d_curr_J,
    //                   n_p,  n_quad,
    //                   d_w, d_basis_grad_x, d_basis_grad_y,
    //                   d_circles,
    //                   curr_num_elem);


    // refine_and_coarsen_fixed_number(d_error_estimate, d_curr_elem,
 //                                    edr, d_epos, 
    //                              0.01, 0.1,
 //                                    verbose,
    //                              curr_num_elem);
