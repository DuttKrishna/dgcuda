w = .1;

Point(1) = {0,0,0,w};
Point(2) = {1,0,0,w};
Point(3) = {1,.15,0,w};
Point(4) = {0,.05,0,w};


Line(5) = {1, 2};
Line(6) = {2, 3};
Line(7) = {3, 4};
Line(8) = {4, 1};
Line Loop(9) = {5, 6, 7, 8};
Plane Surface(10) = {9};



Physical Line(10000) = {7}; // reflecting
Physical Line(20000) = {6}; // outflow
Physical Line(30000) = {8}; // inflow
Physical Line(40000) = {5}; // axis of symmetry
Physical Surface(11) = {10};
