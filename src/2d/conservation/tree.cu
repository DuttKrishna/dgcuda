__device__ int *d_epos;
__device__ int *d_eparent;
__device__ int *d_echild1;
__device__ int *d_echild2;
__device__ int *d_echild3;
__device__ int *d_echild4;

__device__ int *d_spos;
__device__ int *d_sparent;
__device__ int *d_schild1;
__device__ int *d_schild2;

__device__ int *d_efree;
__device__ int *d_sfree;


int etree_MAX =  1.1e6;
int stree_MAX = 1.5 * etree_MAX;
int freeElementCount = 0;
int freeSidesCount = 0;

int etree_size, stree_size;
int new_etree_size, new_stree_size;

int *get_spos(){return d_spos;}
int *get_schild1(){return d_schild1;}
int *get_schild2(){return d_schild2;}

int *get_epos(){return d_epos;}
int *get_eparent(){return d_eparent;}
int *get_echild1(){return d_echild1;}
int *get_echild2(){return d_echild2;}
int *get_echild3(){return d_echild3;}
int *get_echild4(){return d_echild4;}

__global__ void create_elements(int *epos,
                                int *eparent,
                                int *echild1, int *echild2, int *echild3, int *echild4,
                                int *side1, int *side2, int *side3,
                                int *left_elem, int *right_elem,
                                int *curr_left1, int *curr_left2, int *curr_left3,
                                int *curr_elem, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        epos[idx] = idx;

        eparent[idx] = -1;

        echild1[idx] = -1;
        echild2[idx] = -1;
        echild3[idx] = -1;
        echild4[idx] = -1;


        // 0 means right element
        // 1 means left element
        // 2 means left element and reflecting edge
        // 3 means left element and outflow edge
        // 4 means left element and inflow edge


        curr_left1[idx] = (left_elem[side1[idx]] == idx) + 1 * (right_elem[side1[idx]] == -1) + 2 * (right_elem[side1[idx]] == -2) + 3 * (right_elem[side1[idx]] == -3)  + 4 * (right_elem[side1[idx]] == -4);
        curr_left2[idx] = (left_elem[side2[idx]] == idx) + 1 * (right_elem[side2[idx]] == -1) + 2 * (right_elem[side2[idx]] == -2) + 3 * (right_elem[side2[idx]] == -3)  + 4 * (right_elem[side2[idx]] == -4);
        curr_left3[idx] = (left_elem[side3[idx]] == idx) + 1 * (right_elem[side3[idx]] == -1) + 2 * (right_elem[side3[idx]] == -2) + 3 * (right_elem[side3[idx]] == -3)  + 4 * (right_elem[side3[idx]] == -4);



        curr_elem[idx] = idx;

    }
}




__global__ void create_sides(int *spos,
                             int *sparent,
                             int *schild1, int *schild2,
                             int *curr_sides, int max) {


    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int l_e, r_e;

    if (idx < max) {

        spos[idx] = idx;

        sparent[idx] = -1;
        schild1[idx] = -1;
        schild2[idx] = -1; 

        curr_sides[idx] = idx;

    }
}


__global__ void disconnect_schildren( int *curr_sides, 
                                      int *schild1, int *schild2,
                                      int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID;

    if(idx  < max) { 
        sID = curr_sides[idx];
        schild1[sID] = -1;
        schild2[sID] = -1;

    }
}

__global__ void disconnect_echildren( int *curr_elem, 
                                      int *echild1, int *echild2, int *echild3, int *echild4,
                                      int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID;

    if(idx  < max) { 
        eID = curr_elem[idx];
        echild1[eID] = -1;
        echild2[eID] = -1;
        echild3[eID] = -1;
        echild4[eID] = -1;
    }
}

__global__ void update_pos(int *list, int *pos, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int s;
    if (idx < max) {
        s = list[idx];
        pos[s] = idx;
    }
}

__global__ void update_pos(int *list, int *pos, int offset, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int s;
    if (idx < max) {
        s = list[idx];
        pos[s] = idx + offset;
    }
}

__global__ void update_elemidx(int *curr_elements, int *eidx, int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int elem;
    if (idx < num_elem) {
        elem = curr_elements[idx]; 
        eidx[elem] = idx;
    }
}

void computeNewTreeSize(int elementsFromRefinementCount, int sidesFromRefinementCount) {
    new_etree_size = (elementsFromRefinementCount > freeElementCount) ?  etree_size + elementsFromRefinementCount - freeElementCount :  etree_size;
    new_stree_size = (sidesFromRefinementCount > freeSidesCount) ? stree_size + sidesFromRefinementCount - freeSidesCount :  stree_size;        
}




__global__ void is_scoarsenable(int *curr_side, int *sdr, 
                         int *sparent,
                         int *schild1,
                         int *iflag,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID, sIDparent;
    int sIDchild1;
    if (idx < max) {
        if(sdr[idx] == -1){
            sID = curr_side[idx];
            sIDparent = sparent[sID];

            sIDchild1 = (sIDparent > -1) ? schild1[sIDparent] : -1;
            iflag[idx] = (sIDchild1 == sID) ? 1 : 0;
            if(sIDchild1 != sID) // if it's not going to be in the final mesh, then set to -1
                curr_side[idx] = -1;
        }
        else
            iflag[idx] = 0;
    }
}


__global__ void is_fourth(int *curr_elem, int *indicator,
                         int *eparent,
                         int *epos,
                         int *echild1,int *echild2,int *echild3,int *echild4,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDparent;
    int inmesh =0, eIDchild4;
    if (idx < max) {
        eID = curr_elem[idx];
        eIDparent = eparent[eID];

        eIDchild4 = (eIDparent > -1) ? echild4[eIDparent] : -1;

        if(eIDchild4 == eID) {
            inmesh = (epos[echild1[eIDparent]] > -1);
            inmesh = (epos[echild2[eIDparent]] > -1) && inmesh;
            inmesh = (epos[echild3[eIDparent]] > -1) && inmesh;
        }
        curr_elem[idx] = eIDparent;
        indicator[idx] =  inmesh ? 1 : 0;
    }
} 







__global__ void determine_sparent(int *sIDbuffer, int *sdrbuffer,
                                 int *sparent, 
                                 int old_num_sides) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID, sIDparent;
    int sIDchild1, sIDchild2;
    if (idx < old_num_sides) {
        sID = sIDbuffer[idx];
        sIDparent = sparent[sID];
        sIDbuffer[idx] = sIDparent;
        sdrbuffer[idx] = (sIDparent > -1) ? sdrbuffer[idx] + 1 : 1;
    }
} 


__global__ void coarsenSideData(int *scoarsen,
                              int *curr_side, int *curr_originalside, int *curr_slevel,
                              int *spos, int *sparent, int *schild1, int *schild2,
                              int *curr_scolor,
                              int curr_num_sides, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID, sIDpos, sIDparent, sIDoriginalside, sIDlevel;
    int sIDchild1, sIDchild2;
    int new_pos, sID1pos, sID2pos;
    int c;
    if (idx < max) {
        sID = scoarsen[idx];
        sIDpos = spos[sID];
        sIDparent = sparent[sID];
        sIDoriginalside = curr_originalside[sIDpos];
        sIDlevel = curr_slevel[sIDpos];
        c = curr_scolor[sIDpos];

        sIDchild1 = schild1[sIDparent];
        sIDchild2 = schild2[sIDparent];

        sID1pos = spos[sIDchild1];
        sID2pos = spos[sIDchild2];

        new_pos = sID1pos;


        curr_side[new_pos] = sIDparent ; 
        curr_originalside[new_pos] = sIDoriginalside ; 
        curr_slevel[new_pos] = sIDlevel - 1; 
        curr_scolor[new_pos] = c; 


        curr_side[sID2pos] = -1;
        schild1[sIDparent] = -1;
        schild2[sIDparent] = -1; 
        spos[sIDparent] = new_pos;
        spos[sIDchild1] = -1;
        spos[sIDchild2] = -1;


    }
} 

__global__ void coarsenElementData(int *coarsen_list, int *curr_elem, 
                              int *epos,
                              int *echild1, int *echild2, int *echild3, int *echild4,
                              int *curr_s1, int *curr_s2, int *curr_s3,
                              int *curr_left1, int *curr_left2, int *curr_left3,
                              int *curr_elevel,
                              int *sparent, 
                              int *efree, int freeElementCount, 
                              int curr_num_elem, int num_coarsenable) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
    int new_pos;
    int s1, s2, s3;
    int original_side;
    int left1, left2, left3;
    int eID, eIDlevel;

    if (idx < num_coarsenable) {
        eID = coarsen_list[idx];

        eIDchild1 = echild1[eID];
        eIDchild2 = echild2[eID];
        eIDchild3 = echild3[eID];
        eIDchild4 = echild4[eID];
        ePOSchild1 = epos[eIDchild1];
        ePOSchild2 = epos[eIDchild2];
        ePOSchild3 = epos[eIDchild3];
        ePOSchild4 = epos[eIDchild4];

        new_pos = ePOSchild4;

        eIDlevel = curr_elevel[ePOSchild1];
        s1 = curr_s1[ePOSchild1];
        s2 = curr_s2[ePOSchild2];
        s3 = curr_s3[ePOSchild1];
        left1 = curr_left1[ePOSchild1];
        left2 = curr_left2[ePOSchild2];
        left3 = curr_left3[ePOSchild1];

        s1 = sparent[s1];
        s2 = sparent[s2];
        s3 = sparent[s3];

        // put the coarsened information in the position of the fourth child

        curr_elevel[new_pos] = eIDlevel - 1;

        curr_s1[new_pos] = s1;
        curr_s2[new_pos] = s2;
        curr_s3[new_pos] = s3;

        curr_left1[new_pos] = left1;
        curr_left2[new_pos] = left2;
        curr_left3[new_pos] = left3;


        // delete old elements
        curr_elem[ePOSchild1] = -1;
        curr_elem[ePOSchild2] = -1;
        curr_elem[ePOSchild3] = -1;
        curr_elem[new_pos] = eID; 
        // printf("%i %i %i %i\n",curr_elem[ePOSchild1], curr_elem[ePOSchild2], curr_elem[ePOSchild3], curr_elem[ePOSchild4] );


        epos[eIDchild1] = -1;
        epos[eIDchild2] = -1;
        epos[eIDchild3] = -1;
        epos[eIDchild4] = -1;
        epos[eID] = new_pos; // update position of coarsened element

        echild1[eID] = -1;  // coarsened element has no children
        echild2[eID] = -1;  // coarsened element has no children
        echild3[eID] = -1;  // coarsened element has no children
        echild4[eID] = -1;  // coarsened element has no children

    }
}



__global__ void refine_sides(int *sIDlist, int *curr_sides,
                              int *curr_originalside,
                              int *curr_slevel,int *curr_color,
                              int *spos,
                              int *sparent,
                              int *schild1, int *schild2,
                              int *sfree, int *pos_sfree,
                              int curr_mesh_num_sides, int num_sfree,
                              int curr_num_sides, int num_pos_sfree,
                              int rs){ 

    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sIDparent, snewID1, snewID2;
    int parent_pos,new_spos1, new_spos2;
    int sdr_parent, original_side ;
    int sIDlevel;
    int sIDcolor;

    if (idx < rs) {
        sIDparent = sIDlist[idx];
        snewID1 = (idx + 0 * rs < num_sfree) ? sfree[idx + 0 * rs] : curr_mesh_num_sides + 0 * rs + idx - num_sfree;
        snewID2 = (idx + 1 * rs < num_sfree) ? sfree[idx + 1 * rs] : curr_mesh_num_sides + 1 * rs + idx - num_sfree;
        parent_pos = spos[sIDparent];

        new_spos1 = parent_pos;
        new_spos2 = (idx < num_pos_sfree) ? pos_sfree[idx] : curr_num_sides + idx - num_pos_sfree;

        curr_sides[new_spos1] = snewID1;
        curr_sides[new_spos2] = snewID2;

        original_side = curr_originalside[parent_pos];
        sIDlevel = curr_slevel[parent_pos];
        sIDcolor = curr_color[parent_pos];

        // update the parent
        schild1[sIDparent] = snewID1;
        schild2[sIDparent] = snewID2;

        sparent[snewID1] = sIDparent;
        sparent[snewID2] = sIDparent;

        curr_originalside[new_spos1] = original_side;
        curr_originalside[new_spos2] = original_side;

        curr_slevel[new_spos1] = sIDlevel + 1;
        curr_slevel[new_spos2] = sIDlevel + 1; 

        curr_color[new_spos1] = (sIDcolor + 0) % 6;
        curr_color[new_spos2] = (sIDcolor + 3) % 6;

        schild1[snewID1] = -1;
        schild2[snewID1] = -1;

        schild1[snewID2] = -1;
        schild2[snewID2] = -1;

        spos[sIDparent] = -1;  
        spos[snewID1] = new_spos1; 
        spos[snewID2] = new_spos2;

        // if(sIDparent == 54)
        //     printf("new level %i sID1 %i sID2 %i, %i %i\n", sIDlevel+1, snewID1, snewID2, spos[snewID1], spos[snewID2]);
    }
}



__global__ void refine_elements(int *erefine, int *srefine, 
                                int *curr_elem, int *curr_sides,
                                int *curr_originalside, 
                                int *curr_scolor, 
                                int *curr_s1, int *curr_s2, int *curr_s3, 
                                int *curr_left1, int *curr_left2, int *curr_left3, 
                                int *curr_elevel, int *curr_slevel,
                                int *epos, int *spos,
                                int *echild1, int *echild2, int *echild3, int *echild4,
                                int *eparent, int *sparent,
                                int *schild1, int *schild2,

                                int *efree, int *sfree,
                                int num_efree, int num_sfree,

                                int *pos_efree, int *pos_sfree,
                                int num_pos_efree, int num_pos_sfree,

                                int curr_mesh_num_elem, int curr_mesh_num_sides,
                                int curr_num_elem, int curr_num_sides,

                                int rs, int re, int num){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eIDparent;
    int enewID1, enewID2, enewID3, enewID4, snewID1, snewID2, snewID3;
    int enewpos1, enewpos2, enewpos3, enewpos4, snewpos1, snewpos2, snewpos3;
    int eparentlevel;
    int edr_parent;
    int s1pos, s2pos, s3pos;
    int s1, s2, s3, eIDpos;
    int left1, left2, left3;
    int eIDlevel;
    int c1, c2, c3;

    if (idx < re) {
        eIDparent = erefine[idx];
        eIDpos = epos[eIDparent];



        enewID1 = (idx + 0 * re < num_efree) ? efree[idx + 0 * re] : curr_mesh_num_elem + 0 * re + idx - num_efree;
        enewID2 = (idx + 1 * re < num_efree) ? efree[idx + 1 * re] : curr_mesh_num_elem + 1 * re + idx - num_efree;
        enewID3 = (idx + 2 * re < num_efree) ? efree[idx + 2 * re] : curr_mesh_num_elem + 2 * re + idx - num_efree;
        enewID4 = (idx + 3 * re < num_efree) ? efree[idx + 3 * re] : curr_mesh_num_elem + 3 * re + idx - num_efree;


        snewID1 = (idx + 0 * re < num_sfree) ? sfree[idx + 0 * re] : curr_mesh_num_sides + 0 * re + idx - num_sfree;
        snewID2 = (idx + 1 * re < num_sfree) ? sfree[idx + 1 * re] : curr_mesh_num_sides + 1 * re + idx - num_sfree;
        snewID3 = (idx + 2 * re < num_sfree) ? sfree[idx + 2 * re] : curr_mesh_num_sides + 2 * re + idx - num_sfree; 

        enewpos1 = (idx + 0 * re < num_pos_efree) ? pos_efree[idx + 0 * re ] : curr_num_elem + 0 * re + idx - num_pos_efree;
        enewpos2 = (idx + 1 * re < num_pos_efree) ? pos_efree[idx + 1 * re ] : curr_num_elem + 1 * re + idx - num_pos_efree;
        enewpos3 = (idx + 2 * re < num_pos_efree) ? pos_efree[idx + 2 * re ] : curr_num_elem + 2 * re + idx - num_pos_efree;
        enewpos4 = eIDpos;
        snewpos1 = (idx + rs + 0 * re < num_pos_sfree) ? pos_sfree[idx + rs + 0 * re ] : curr_num_sides + rs + 0 * re + idx - num_pos_sfree;
        snewpos2 = (idx + rs + 1 * re < num_pos_sfree) ? pos_sfree[idx + rs + 1 * re ] : curr_num_sides + rs + 1 * re + idx - num_pos_sfree;
        snewpos3 = (idx + rs + 2 * re < num_pos_sfree) ? pos_sfree[idx + rs + 2 * re ] : curr_num_sides + rs + 2 * re + idx - num_pos_sfree; 


        s1 = curr_s1[eIDpos];
        s2 = curr_s2[eIDpos];
        s3 = curr_s3[eIDpos];

        eIDlevel = curr_elevel[eIDpos]; 

        left1 = curr_left1[eIDpos];
        left2 = curr_left2[eIDpos];
        left3 = curr_left3[eIDpos];

        curr_elevel[enewpos1] = eIDlevel + 1;
        curr_elevel[enewpos2] = eIDlevel + 1;
        curr_elevel[enewpos3] = eIDlevel + 1;
        curr_elevel[enewpos4] = eIDlevel + 1;

        echild1[eIDparent] = enewID1; //omega 0
        echild2[eIDparent] = enewID2; //omega 1
        echild3[eIDparent] = enewID3; //omega 2
        echild4[eIDparent] = enewID4; //omega 3

        curr_elem[enewpos1] = enewID1; 
        curr_elem[enewpos2] = enewID2; 
        curr_elem[enewpos3] = enewID3; 
        curr_elem[enewpos4] = enewID4; 

        curr_sides[snewpos1] = snewID1; 
        curr_sides[snewpos2] = snewID2; 
        curr_sides[snewpos3] = snewID3; 

        curr_slevel[snewpos1] = eIDlevel + 1; 
        curr_slevel[snewpos2] = eIDlevel + 1;
        curr_slevel[snewpos3] = eIDlevel + 1;

        // printf("eID %i eIDpos %i \n eIDchildren %i %i %i %i ePOSchildren %i %i %i %i \n sID %i %i %i\n \n",  enewID4, eIDpos, 
        //                                                                                 enewID1,enewID2,enewID3,enewID4,
        //                                                                                 enewpos1, enewpos2, enewpos3, enewpos4,
        //                                                                                 snewID1,snewID2,snewID3);

        // printf("\n %i spos %i %i %i\n \n", num_pos_sfree, snewpos1,snewpos2,snewpos3);


        curr_left1[enewpos1] = left1;
        curr_left2[enewpos1] = left2 > 0;
        curr_left3[enewpos1] = left3;

        curr_left1[enewpos2] = left1;
        curr_left2[enewpos2] = left2;
        curr_left3[enewpos2] = left3 > 0;

        curr_left1[enewpos3] = left1 > 0;
        curr_left2[enewpos3] = left2;
        curr_left3[enewpos3] = left3;

        curr_left1[enewpos4] = !left3;
        curr_left2[enewpos4] = !left1;
        curr_left3[enewpos4] = !left2;


        sparent[snewID1] = -1;
        sparent[snewID2] = -1;
        sparent[snewID3] = -1;

        schild1[snewID1] = -1;
        schild2[snewID1] = -1;

        schild1[snewID2] = -1;
        schild2[snewID2] = -1;

        schild1[snewID3] = -1;
        schild2[snewID3] = -1;


        spos[snewID1] = snewpos1;
        spos[snewID2] = snewpos2;
        spos[snewID3] = snewpos3;



        s1pos = (schild1[s1] > -1) ?  spos[schild1[s1]] : spos[s1] ;
        s2pos = (schild1[s2] > -1) ?  spos[schild1[s2]] : spos[s2] ;
        s3pos = (schild1[s3] > -1) ?  spos[schild1[s3]] : spos[s3] ;

        s1pos = (s1pos > -1) ?   s1pos : spos[schild1[schild1[s1]]] ;
        s2pos = (s2pos > -1) ?   s2pos : spos[schild1[schild1[s2]]] ;
        s3pos = (s3pos > -1) ?   s3pos : spos[schild1[schild1[s3]]] ;


        curr_originalside[snewpos1] = curr_originalside[s3pos]; 
        curr_originalside[snewpos2] = curr_originalside[s1pos];
        curr_originalside[snewpos3] = curr_originalside[s2pos];

        c1 = curr_scolor[s1pos];
        c2 = curr_scolor[s2pos];
        c3 = curr_scolor[s3pos];

        // if(eIDparent == 27)
            // printf("\n %i %i %i\n \n %i %i %i \n %i %i %i \n ", c1, c2, c3, s1pos, s2pos, s3pos, s3, spos[s3], spos[schild1[s3]]);


        curr_scolor[snewpos1] = c3;
        curr_scolor[snewpos2] = c1;
        curr_scolor[snewpos3] = c2;

        eparent[enewID1] = eIDparent;
        eparent[enewID2] = eIDparent;
        eparent[enewID3] = eIDparent;
        eparent[enewID4] = eIDparent;

        echild1[enewID1] = -1;
        echild2[enewID1] = -1;
        echild3[enewID1] = -1;
        echild4[enewID1] = -1;

        echild1[enewID2] = -1;
        echild2[enewID2] = -1;
        echild3[enewID2] = -1;
        echild4[enewID2] = -1;

        echild1[enewID3] = -1;
        echild2[enewID3] = -1;
        echild3[enewID3] = -1;
        echild4[enewID3] = -1;

        echild1[enewID4] = -1;
        echild2[enewID4] = -1;
        echild3[enewID4] = -1;
        echild4[enewID4] = -1;


       // side 0
        if( left1 ) {
            curr_s1[enewpos1] = schild1[s1];
            curr_s1[enewpos2] = schild2[s1];
        }
        
        else {
            curr_s1[enewpos1] = schild2[s1];
            curr_s1[enewpos2] = schild1[s1];
        }

        //side 1
        if( left2 ) {
            curr_s2[enewpos2] = schild1[s2];
            curr_s2[enewpos3] = schild2[s2];
        }
        
        else {
            curr_s2[enewpos2] = schild2[s2];
            curr_s2[enewpos3] = schild1[s2];

        }


        // side 2
        if( left3 ) {
            curr_s3[enewpos3] = schild1[s3];
            curr_s3[enewpos1] = schild2[s3];
        }
        else {
            curr_s3[enewpos3] = schild2[s3];
            curr_s3[enewpos1] = schild1[s3];
        }
 
        

        curr_s2[enewpos1] = snewID3;
        curr_s3[enewpos2] = snewID1;
        curr_s1[enewpos3] = snewID2;

        curr_s1[enewpos4] = snewID1;
        curr_s2[enewpos4] = snewID2;
        curr_s3[enewpos4] = snewID3;

        // if(eIDparent == 11 || enewpos4 == 11 )
        //     printf("%i %i %i %i %i %i %i, enewpos4 %i \n", enewID1, enewID2, enewID3, enewID4, snewID1, snewID2, snewID3, enewpos4);

        epos[eIDparent] = -1;
        epos[enewID1] = enewpos1; 
        epos[enewID2] = enewpos2; 
        epos[enewID3] = enewpos3; 
        epos[enewID4] = enewpos4; 
    }
}

__global__ void add_children(int *coarsenlist, int *efree,
                             int *echild1, int *echild2, int *echild3, int *echild4, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int eID;
    if(idx < max) {
        eID = coarsenlist[idx];

    }
}





__global__ void populate_sides(int *spos, 
                               int *schild1, int *schild2,
                               int *curr_s1, int *curr_s2, int *curr_s3,
                               int *eleft1, int *eleft2, int *eleft3,
                               int *left_elemref, int *right_elemref,
                               int *left_side_numberref, int *right_side_numberref,
                               int new_num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int haschildren;
    int sID[3], sPOS[6], side_num[6], left[3];

    if(idx < new_num_elem) {

        // load the entire tree...
        sID[0]= curr_s1[idx]; 
        sID[1]= curr_s2[idx]; 
        sID[2]= curr_s3[idx];

        // printf("%i %i %i\n", sID[0], sID[1], sID[2]);

        left[0] = eleft1[idx];
        left[1] = eleft2[idx];
        left[2] = eleft3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = schild1[sID[i]];
            if(haschildren > -1){
                sPOS[i] = spos[schild1[sID[i]]];
                sPOS[i + 3] = spos[schild2[sID[i]]];
            }
            else{
                sPOS[i] = spos[ sID[i] ];
                sPOS[i + 3] = -1;
            }
        }

        for(int i = 0; i < 6; i++) {
            if(sPOS[i] > -1) {
                if(left[i % 3]) {
                    left_elemref[sPOS[i]] = idx;
                    left_side_numberref[sPOS[i]] = i % 3;

                    if(left[ i % 3 ] > 1)
                        right_elemref[sPOS[i]] = -left[ i % 3 ] + 1;
                }
                else{
                    right_elemref[sPOS[i]] = idx;
                    right_side_numberref[sPOS[i]] = i % 3;
                }
            }
        }



    }
}



__global__ void update_connectivity(int *left_elemref, int *right_elemref,
                                    int *left_side_numberref, int *right_side_numberref,
                                    int *curr_elemref,int *curr_sideref,
                                    int *elevel,
                                    int *sparent,
                                    int *schild1, int *schild2,
                                    int new_num_sides) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int sID;
    int eleftPOS, erightPOS, eleftID, erightID, spID;
    int r = 0, lsn, rsn;
    int elevelLEFT, elevelRIGHT;

    if (idx < new_num_sides) {
        sID = curr_sideref[idx];

        eleftPOS = left_elemref[idx];
        erightPOS = right_elemref[idx];
        eleftID = curr_elemref[eleftPOS];
        erightID = (erightPOS >-1) ? curr_elemref[erightPOS] : erightPOS;
        elevelLEFT =  elevel[eleftPOS];
        elevelRIGHT =   (erightPOS >-1) ? elevel[erightPOS] : elevelLEFT;


        r = elevelLEFT - elevelRIGHT; // r < 0 right is most refined; r > 0 left is most refined

        lsn = left_side_numberref[idx];
        rsn = right_side_numberref[idx]; 

        if(r == 1){ // left element is most refined.
            spID = sparent[sID];
            if(sID == schild1[spID])
                right_side_numberref[idx] = 3 + 2 * rsn + 1; 
            else
                right_side_numberref[idx] = 3 + 2 * rsn + 0; 

        }
        else if(r == -1){ // right element is most refined.
            spID = sparent[sID];
            if(sID == schild1[spID])
                left_side_numberref[idx] = 3 + 2 * lsn + 0; 
            else
                left_side_numberref[idx] = 3 + 2 * lsn + 1; 

        }
    }
}






void allocateTree(int num_etree, int num_stree, memoryCounters *counter){ 
    allocate((void **) &d_epos, etree_MAX * sizeof(int), &(counter->etree_memory));
    allocate((void **) &d_eparent, etree_MAX * sizeof(int), &(counter->etree_memory));
    allocate((void **) &d_echild1, etree_MAX * sizeof(int), &(counter->etree_memory));
    allocate((void **) &d_echild2, etree_MAX * sizeof(int), &(counter->etree_memory));
    allocate((void **) &d_echild3, etree_MAX * sizeof(int), &(counter->etree_memory));
    allocate((void **) &d_echild4, etree_MAX * sizeof(int), &(counter->etree_memory));

    allocate((void **) &d_sparent, stree_MAX * sizeof(int), &(counter->stree_memory));
    allocate((void **) &d_schild1, stree_MAX * sizeof(int), &(counter->stree_memory));
    allocate((void **) &d_schild2, stree_MAX * sizeof(int), &(counter->stree_memory));
    allocate((void **) &d_spos, stree_MAX * sizeof(int), &(counter->stree_memory));

    set_value<<<get_blocks(etree_MAX,512),512>>>(d_epos, -1, etree_MAX);
    set_value<<<get_blocks(stree_MAX,512),512>>>(d_spos, -1, stree_MAX);

    allocate((void **) &d_efree, etree_MAX * sizeof(int), &(counter->etree_memory));
    allocate((void **) &d_sfree, stree_MAX * sizeof(int), &(counter->stree_memory));
    cudaMemset(d_efree, 0, etree_MAX * sizeof(int));
    cudaMemset(d_sfree, 0, stree_MAX * sizeof(int));

    etree_size = num_etree;
    stree_size = num_stree;


}

void initTree(int *d_curr_elem, int *d_curr_side,
              int *d_elem_s1, int *d_elem_s2, int *d_elem_s3,
              int *d_left_elem, int *d_right_elem,
              int *d_curr_left1, int *d_curr_left2, int *d_curr_left3,
              int curr_num_elem, int curr_num_sides) {

    int n_threads = 512;

    create_elements<<<get_blocks(curr_num_elem,n_threads), n_threads>>>(d_epos,
                                                                         d_eparent,
                                                                         d_echild1, d_echild2, d_echild3, d_echild4,
                                                                         d_elem_s1, d_elem_s2, d_elem_s3,
                                                                         d_left_elem, d_right_elem,
                                                                         d_curr_left1, d_curr_left2,d_curr_left3,
                                                                         d_curr_elem, curr_num_elem);

    create_sides<<<get_blocks(curr_num_sides,n_threads), n_threads>>>(d_spos,
                                                                       d_sparent,
                                                                       d_schild1, d_schild2,
                                                                       d_curr_side, curr_num_sides);
                                                
                                                
    
                                                
    // update in the tree the elements' and sides' positions in the used_edge and used_elem arrays
    update_pos<<<get_blocks(curr_num_elem,n_threads), n_threads>>>(d_curr_elem, d_epos, curr_num_elem);
    update_pos<<<get_blocks(curr_num_sides,n_threads), n_threads>>>(d_curr_side, d_spos, curr_num_sides);
}

void dump_elem_tree(int count){

    int *epos = (int *) malloc(etree_size * sizeof(int));
    int *eparent = (int *) malloc(etree_size * sizeof(int));
    int *echild1 = (int *) malloc(etree_size * sizeof(int));
    int *echild2 = (int *) malloc(etree_size * sizeof(int));
    int *echild3 = (int *) malloc(etree_size * sizeof(int));
    int *echild4 = (int *) malloc(etree_size * sizeof(int));

    cudaMemcpy(epos,d_epos, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(eparent,d_eparent, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild1,d_echild1, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild2,d_echild2, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild3,d_echild3, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild4,d_echild4, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    
    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/dump/tree-%i.txt", count);
    out_file  = fopen(out_filename , "w");

    for (int idx = 0; idx < etree_size; idx++) {
        fprintf(out_file, "%i %i %i %i %i %i %i\n",idx,epos[idx],eparent[idx], echild1[idx], echild2[idx], echild3[idx], echild4[idx]);
        // fprintf(out_file, "elemID %i elemPOS %i parent %i children %i %i %i %i \n",idx,epos[idx],eparent[idx], echild1[idx], echild2[idx], echild3[idx], echild4[idx]);
    } 

    fclose(out_file);
    free(epos);
    free(eparent);
    free(echild1);
    free(echild2);
    free(echild3);
    free(echild4);
}

void dump_side_tree(int count){

    int *spos = (int *) malloc(stree_size * sizeof(int)); 
    int *sparent = (int *) malloc(stree_size * sizeof(int));
    int *schild1 = (int *) malloc(stree_size * sizeof(int));
    int *schild2 = (int *) malloc(stree_size * sizeof(int));

    cudaMemcpy(spos,d_spos, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(sparent,d_sparent, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(schild1,d_schild1, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(schild2,d_schild2, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    
    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/dump/side_tree-%i.txt",count);
    out_file  = fopen(out_filename , "w");

    for (int idx = 0; idx < etree_size; idx++) {
        fprintf(out_file, "%i %i %i %i %i\n",idx,spos[idx],sparent[idx], schild1[idx], schild2[idx]);
        // fprintf(out_file, "sideID %i sidePOS %i parent %i children %i %i \n",idx,spos[idx],sparent[idx], schild1[idx], schild2[idx]);
    } 

    fclose(out_file);
    free(spos);
    free(sparent);
    free(schild1);
    free(schild2);
}

void dump_tree(int count){


    int *epos = (int *) malloc(etree_size * sizeof(int));
    int *eparent = (int *) malloc(etree_size * sizeof(int));
    int *echild1 = (int *) malloc(etree_size * sizeof(int));
    int *echild2 = (int *) malloc(etree_size * sizeof(int));
    int *echild3 = (int *) malloc(etree_size * sizeof(int));
    int *echild4 = (int *) malloc(etree_size * sizeof(int));
    int *efree = (int *) malloc(freeElementCount * sizeof(int));

    cudaMemcpy(epos,d_epos, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(eparent,d_eparent, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild1,d_echild1, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild2,d_echild2, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild3,d_echild3, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(echild4,d_echild4, etree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(efree,d_efree, freeElementCount * sizeof(int), cudaMemcpyDeviceToHost);
    
    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/dump/tree-%i.txt", count);
    out_file  = fopen(out_filename , "w");
    fprintf(out_file, "%i %i\n",etree_size, freeElementCount);

    for (int idx = 0; idx < etree_size; idx++) {
        fprintf(out_file, "%i %i %i %i %i %i\n",epos[idx],eparent[idx], echild1[idx], echild2[idx], echild3[idx], echild4[idx]);
    } 
    for (int idx = 0; idx < freeElementCount; idx++) {
        fprintf(out_file, "%i\n",efree[idx]);
    } 

    free(epos);free(eparent);free(echild1);free(echild2);free(echild3);free(echild4);free(efree);

    int *spos = (int *) malloc(stree_size * sizeof(int)); 
    int *sparent = (int *) malloc(stree_size * sizeof(int));
    int *schild1 = (int *) malloc(stree_size * sizeof(int));
    int *schild2 = (int *) malloc(stree_size * sizeof(int));
    int *sfree = (int *) malloc(freeSidesCount * sizeof(int));

    cudaMemcpy(spos,d_spos, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(sparent,d_sparent, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(schild1,d_schild1, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(schild2,d_schild2, stree_size * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(sfree,d_sfree, freeSidesCount * sizeof(int), cudaMemcpyDeviceToHost);
    
    fprintf(out_file, "%i %i\n",stree_size, freeSidesCount);
    for (int idx = 0; idx < stree_size; idx++) {
        fprintf(out_file, "%i %i %i %i\n",spos[idx],sparent[idx], schild1[idx], schild2[idx]);
    } 
    for (int idx = 0; idx < freeSidesCount; idx++) {
        fprintf(out_file, "%i\n",sfree[idx]);
    } 

    fclose(out_file);
    free(spos);free(sparent);free(schild1);free(schild2);free(sfree);
}

void resumeTree(){
    char line[1000];
    int items;




    char *filename = "output/resume/tree.txt";
    FILE *file = fopen(filename, "r");



    fgets(line, sizeof(line), file);
    items = sscanf(line, "%i %i\n", &etree_size, &freeElementCount);

    int *epos = (int *) malloc(etree_size * sizeof(int));
    int *eparent = (int *) malloc(etree_size * sizeof(int));
    int *echild1 = (int *) malloc(etree_size * sizeof(int));
    int *echild2 = (int *) malloc(etree_size * sizeof(int));
    int *echild3 = (int *) malloc(etree_size * sizeof(int));
    int *echild4 = (int *) malloc(etree_size * sizeof(int));
    int *efree = (int *) malloc(freeElementCount * sizeof(int));

    for (int idx = 0; idx < etree_size; idx++) {
        fgets(line, sizeof(line), file);
        items = sscanf(line, "%i %i %i %i %i %i\n",epos + idx ,eparent + idx, echild1 + idx , echild2 + idx , echild3 + idx , echild4 + idx );
    } 
    for (int idx = 0; idx < freeElementCount; idx++) {
        fgets(line, sizeof(line), file);
        items = sscanf(line, "%i\n",efree + idx );
    } 


    fgets(line, sizeof(line), file);
    items = sscanf(line, "%i %i\n", &stree_size, &freeSidesCount);

    int *spos = (int *) malloc(stree_size * sizeof(int)); 
    int *sparent = (int *) malloc(stree_size * sizeof(int));
    int *schild1 = (int *) malloc(stree_size * sizeof(int));
    int *schild2 = (int *) malloc(stree_size * sizeof(int));
    int *sfree = (int *) malloc(freeSidesCount * sizeof(int));
    for (int idx = 0; idx < stree_size; idx++) {
        fgets(line, sizeof(line), file);
        sscanf(line, "%i %i %i %i\n",spos + idx ,sparent + idx , schild1 + idx , schild2 + idx );
    } 
    for (int idx = 0; idx < freeSidesCount; idx++) {
        fgets(line, sizeof(line), file);
        sscanf(line, "%i\n",sfree + idx );
    } 
    fclose(file);


    cudaMemcpy(d_epos,epos, etree_size * sizeof(int), cudaMemcpyHostToDevice);free(epos);
    cudaMemcpy(d_eparent,eparent, etree_size * sizeof(int), cudaMemcpyHostToDevice);free(eparent);
    cudaMemcpy(d_echild1,echild1, etree_size * sizeof(int), cudaMemcpyHostToDevice);free(echild1);
    cudaMemcpy(d_echild2,echild2, etree_size * sizeof(int), cudaMemcpyHostToDevice);free(echild2);
    cudaMemcpy(d_echild3,echild3, etree_size * sizeof(int), cudaMemcpyHostToDevice);free(echild3);
    cudaMemcpy(d_echild4,echild4, etree_size * sizeof(int), cudaMemcpyHostToDevice);free(echild4);
    cudaMemcpy(d_spos,spos, stree_size * sizeof(int), cudaMemcpyHostToDevice);free(spos);
    cudaMemcpy(d_sparent,sparent, stree_size * sizeof(int), cudaMemcpyHostToDevice);free(sparent);
    cudaMemcpy(d_schild1,schild1, stree_size * sizeof(int), cudaMemcpyHostToDevice);free(schild1);
    cudaMemcpy(d_schild2,schild2, stree_size * sizeof(int), cudaMemcpyHostToDevice);free(schild2);

    if(freeElementCount>0)
        cudaMemcpy(d_efree,efree, freeElementCount * sizeof(int), cudaMemcpyHostToDevice);free(efree);
    if(freeSidesCount>0)
        cudaMemcpy(d_sfree,sfree, freeSidesCount * sizeof(int), cudaMemcpyHostToDevice);free(sfree);

    

}
