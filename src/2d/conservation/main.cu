 /* main.cu 
 *
 * Contains the main functions to read the mesh mappings and run the GPU code
 * to solve the specific problem .
 *
 * Does the following
 * 1. get user input
 * 2. read mesh mappings
 * 3. allocate memory on the GPU
 * 4. get integration points and weights
 * 5. call basis.cu to precompute basis functions at those integration points
 * 6. perform other precomputations on GPU
 * 7. compute initial projection U_0 on GPU
 * 8. run the time integrator function
 * 9. write the result to file
 */
#include <cuda_profiler_api.h>
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "memoryCounters.cuh"
#include "plot.cuh"
#include "limiter.cuh"
#include "conserv_kernels.cu"
#include "time_integrator.cu"
#include "quadrature.cu"
#include "basis.cu"
#include "operations.cuh"
#include "h-adaptivity.cuh"
#include "tree.cuh" 
#include "error_estimate.cuh" 

extern int local_N;
extern int limiter;
int time_integrator;

// time integration options
#define RK1 1
#define RK2 2
#define RK3 3
#define RK4 4
#define RK5 5
#define RK6 6

#define UPWINDING 1
#define LLF 2
#define LF 3

__device__ double *d_Uv1;
__device__ double *d_Uv2;
__device__ double *d_Uv3;

__global__ void init_inscribed_circles(double *J,
                                       double *V1x, double *V1y,
                                       double *V2x, double *V2y,
                                       double *V3x, double *V3y,
                                         int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double a, b, c, k;
        a = sqrt(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrt(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2)); 
        c = sqrt(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        k = 0.5 * (a + b + c);

        // for the diameter, we multiply by 2
        J[idx] =  sqrt(k * (k - a) * (k - b) * (k - c)) / k;


    }
}

__global__ void init_nlbj_h2(double *h,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double a, b, c, k;

        //Twice the area
        k = (V2x[idx]-V1x[idx])*(V3y[idx]-V1y[idx])-(V3x[idx]-V1x[idx])*(V2y[idx]-V1y[idx]);

        //Length of each side
        a = sqrt(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrt(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2));
        c = sqrt(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        //Smallest height
        h[idx] = min(min(k/a,k/b),k/c);
    }
}

void dump_mesh(int curr_num_elem, int curr_num_sides, int num_sides0, int count){ 

    int *curr_elem = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_elevel = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s1 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s2 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s3 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_left1 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_left2 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_left3 = (int *) malloc(curr_num_elem * sizeof(int));
    double *curr_V1x = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V1y = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V2x = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V2y = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V3x = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V3y = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_J = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(curr_elem,d_curr_elem, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_elevel,d_curr_elevel, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s1,d_curr_s1, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s2,d_curr_s2, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s3,d_curr_s3, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    
    cudaMemcpy(curr_left1,d_curr_left1, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_left2,d_curr_left2, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_left3,d_curr_left3, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);

    cudaMemcpy(curr_V1x,d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V1y,d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V2x,d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V2y,d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V3x,d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V3y,d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    cudaMemcpy(curr_J,d_curr_J, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);


    FILE *out_file; 
    char out_filename[100];
    sprintf(out_filename, "output/dump/curr_mesh-%i.txt", count);

    out_file  = fopen(out_filename , "w");
    fprintf(out_file, "%i\n",curr_num_elem);
    for (int idx = 0; idx < curr_num_elem; idx++) {
        fprintf(out_file, "%i %i %i %i %i %i %i %i %.17g %.17g %.17g %.17g %.17g %.17g %.17g\n",curr_elem[idx], curr_elevel[idx], curr_s1[idx], curr_s2[idx], curr_s3[idx], curr_left1[idx], curr_left2[idx], curr_left3[idx], curr_J[idx], curr_V1x[idx], curr_V1y[idx], curr_V2x[idx], curr_V2y[idx], curr_V3x[idx], curr_V3y[idx]);
    } 


    free(curr_elem);
    free(curr_elevel);
    free(curr_s1); 
    free(curr_s2); 
    free(curr_s3); 
    free(curr_left1); 
    free(curr_left2); 
    free(curr_left3); 
    free(curr_V1x);
    free(curr_V1y);
    free(curr_V2x);
    free(curr_V2y);
    free(curr_V3x);
    free(curr_V3y);
    free(curr_J);

    int *curr_side = (int *) malloc(curr_num_sides * sizeof(int));
    int *left_elem = (int *) malloc(curr_num_sides * sizeof(int));
    int *right_elem = (int *) malloc(curr_num_sides * sizeof(int));

    int *lsn = (int *) malloc(curr_num_sides * sizeof(int));
    int *rsn = (int *) malloc(curr_num_sides * sizeof(int));

    int *slevel = (int *) malloc(curr_num_sides * sizeof(int));
    int *scolor = (int *) malloc(curr_num_sides * sizeof(int)); 

    int *original_side = (int *) malloc(curr_num_sides * sizeof(int)); 

    cudaMemcpy(curr_side,d_curr_side, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(left_elem,d_left_elem, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(lsn,d_left_side_number, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(right_elem,d_right_elem, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(rsn,d_right_side_number, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(slevel,d_curr_slevel, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(scolor,d_curr_scolor, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(original_side,d_curr_originalside, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);

    fprintf(out_file, "%i\n",curr_num_sides);
    for (int idx = 0; idx < curr_num_sides; idx++) {
        fprintf(out_file, "%i %i %i %i %i %i %i %i\n",curr_side[idx], slevel[idx], original_side[idx], scolor[idx], left_elem[idx], right_elem[idx], lsn[idx], rsn[idx]);
    } 

    free(curr_side); 
    free(left_elem); 
    free(lsn); 
    free(right_elem);
    free(rsn); 
    free(slevel);
    free(scolor);
    free(original_side); 


    double *s_length = (double *) malloc(num_sides0 * sizeof(double));
    double *Nx = (double *) malloc(num_sides0 * sizeof(double));
    double *Ny = (double *) malloc(num_sides0 * sizeof(double));
    cudaMemcpy(s_length,d_s_length, num_sides0 * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Nx,d_Nx, num_sides0 * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Ny,d_Ny, num_sides0 * sizeof(double), cudaMemcpyDeviceToHost);

    fprintf(out_file, "%i\n",num_sides0);
    for (int idx = 0; idx < num_sides0; idx++) {
        fprintf(out_file, "%.17g %.17g %.17g\n",s_length[idx], Nx[idx], Ny[idx]);
    } 

    fclose(out_file);
    free(s_length);
    free(Nx);
    free(Ny);
}







void dump_meshbytes(int num_elem, int num_sides, int sides0, int *num_color, int count){ 
    int curr_num_elem = num_elem;
    int curr_num_sides = num_sides;
    int num_sides0 = sides0;

    int *curr_elem = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_elevel = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s1 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s2 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_s3 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_left1 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_left2 = (int *) malloc(curr_num_elem * sizeof(int));
    int *curr_left3 = (int *) malloc(curr_num_elem * sizeof(int));
    double *curr_V1x = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V1y = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V2x = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V2y = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V3x = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_V3y = (double *) malloc(curr_num_elem * sizeof(double));
    double *curr_J = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(curr_elem,d_curr_elem, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_elevel,d_curr_elevel, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s1,d_curr_s1, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s2,d_curr_s2, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_s3,d_curr_s3, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    
    cudaMemcpy(curr_left1,d_curr_left1, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_left2,d_curr_left2, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_left3,d_curr_left3, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);

    cudaMemcpy(curr_V1x,d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V1y,d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V2x,d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V2y,d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V3x,d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(curr_V3y,d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    cudaMemcpy(curr_J,d_curr_J, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);


    FILE *out_file;
    char out_filename[100];
    sprintf(out_filename, "output/dump/curr_mesh-%i.bin", count);

    out_file  = fopen(out_filename , "wb");
    fwrite (&curr_num_elem , sizeof(int), 1, out_file);
    fwrite (curr_elem , sizeof(int), curr_num_elem, out_file);free(curr_elem);
    fwrite (curr_elevel , sizeof(int), curr_num_elem, out_file);free(curr_elevel);
    fwrite (curr_s1 , sizeof(int), curr_num_elem, out_file);free(curr_s1);
    fwrite (curr_s2 , sizeof(int), curr_num_elem, out_file);free(curr_s2);
    fwrite (curr_s3 , sizeof(int), curr_num_elem, out_file);free(curr_s3);
    fwrite (curr_left1 , sizeof(int), curr_num_elem, out_file);free(curr_left1);
    fwrite (curr_left2 , sizeof(int), curr_num_elem, out_file);free(curr_left2);
    fwrite (curr_left3 , sizeof(int), curr_num_elem, out_file);free(curr_left3);
    fwrite (curr_J , sizeof(double), curr_num_elem, out_file);free(curr_J);
    fwrite (curr_V1x , sizeof(double), curr_num_elem, out_file);free(curr_V1x);
    fwrite (curr_V1y , sizeof(double), curr_num_elem, out_file);free(curr_V1y);
    fwrite (curr_V2x , sizeof(double), curr_num_elem, out_file);free(curr_V2x);
    fwrite (curr_V2y , sizeof(double), curr_num_elem, out_file);free(curr_V2y);
    fwrite (curr_V3x , sizeof(double), curr_num_elem, out_file);free(curr_V3x);
    fwrite (curr_V3y , sizeof(double), curr_num_elem, out_file);free(curr_V3y);

    int *curr_side = (int *) malloc(curr_num_sides * sizeof(int));
    int *left_elem = (int *) malloc(curr_num_sides * sizeof(int));
    int *lsn = (int *) malloc(curr_num_sides * sizeof(int));
    int *right_elem = (int *) malloc(curr_num_sides * sizeof(int));
    int *rsn = (int *) malloc(curr_num_sides * sizeof(int));
    int *slevel = (int *) malloc(curr_num_sides * sizeof(int));
    int *scolor = (int *) malloc(curr_num_sides * sizeof(int)); 
    int *original_side = (int *) malloc(curr_num_sides * sizeof(int)); 

    cudaMemcpy(curr_side,d_curr_side, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(left_elem,d_left_elem, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(lsn,d_left_side_number, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(right_elem,d_right_elem, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(rsn,d_right_side_number, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(slevel,d_curr_slevel, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(scolor,d_curr_scolor, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(original_side,d_curr_originalside, curr_num_sides * sizeof(int), cudaMemcpyDeviceToHost);

    fwrite (&curr_num_sides , sizeof(int), 1, out_file);
    fwrite (curr_side , sizeof(int), curr_num_sides, out_file); free(curr_side);
    fwrite (slevel , sizeof(int), curr_num_sides, out_file); free(slevel);
    fwrite (original_side , sizeof(int), curr_num_sides, out_file);free(original_side); 
    fwrite (scolor , sizeof(int), curr_num_sides, out_file);free(scolor);
    fwrite (left_elem , sizeof(int), curr_num_sides, out_file);free(left_elem); 
    fwrite (right_elem , sizeof(int), curr_num_sides, out_file);free(right_elem);
    fwrite (lsn , sizeof(int), curr_num_sides, out_file);free(lsn);
    fwrite (rsn , sizeof(int), curr_num_sides, out_file);free(rsn);
    fwrite (num_color , sizeof(int), 6, out_file);

    double *s_length = (double *) malloc(num_sides0 * sizeof(double));
    double *Nx = (double *) malloc(num_sides0 * sizeof(double));
    double *Ny = (double *) malloc(num_sides0 * sizeof(double));
    cudaMemcpy(s_length,d_s_length, num_sides0 * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Nx,d_Nx, num_sides0 * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(Ny,d_Ny, num_sides0 * sizeof(double), cudaMemcpyDeviceToHost);


    fwrite (&num_sides0 , sizeof(int), 1, out_file);
    fwrite (s_length , sizeof(double), num_sides0, out_file);free(s_length);
    fwrite (Nx , sizeof(double), num_sides0, out_file);free(Nx);
    fwrite (Ny , sizeof(double), num_sides0, out_file);free(Ny);

    fclose(out_file);
    
    
    
}



void dump_c(int curr_num_elem, int n_p, int N, int count){ 
    FILE *out_file;
    char out_filename[100];
    double *c = (double *) malloc(curr_num_elem * sizeof(double));

    sprintf(out_filename, "output/dump/c-%i.txt", count);
    out_file  = fopen(out_filename , "w");
    fprintf(out_file, "%i %i %i\n", curr_num_elem, n_p, N);
    for(int i = 0; i < N*n_p; i++){
        cudaMemcpy(c, h_c[i] , curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

        for (int idx = 0; idx < curr_num_elem; idx++) {
            fprintf(out_file, "%.17g\n", c[idx]);
        } 
    }
    fclose(out_file);
    free(c);
}

void dump_cbytes(int num_elem, int np, int in_N,double time, int count){ 
    int curr_num_elem = num_elem;
    int n_p = np;
    int N = in_N;

    FILE *out_file;
    char out_filename[100];
    double *c = (double *) malloc(curr_num_elem * sizeof(double));

    sprintf(out_filename, "output/dump/c-%i.bin", count);
    out_file  = fopen(out_filename , "wb");
    // fprintf(out_file, "%i %i %i\n", curr_num_elem, n_p, N);
    fwrite(&time, sizeof(double), 1, out_file);
    fwrite(&curr_num_elem, sizeof(int), 1, out_file);
    fwrite(&n_p, sizeof(int), 1, out_file);
    fwrite(&N, sizeof(int), 1, out_file);
    for(int i = 0; i < N * n_p; i++){
        cudaMemcpy(c, h_c[i] , curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        fwrite(c, sizeof(double), curr_num_elem, out_file);
    }
    fclose(out_file);
    free(c);
}


void dump(int curr_num_elem, int curr_num_sides, int n_p, int N, int num_sides0, double time, int *num_color, int count) {
    // dump_c(curr_num_elem, n_p, N, count);
    // dump_mesh(curr_num_elem, curr_num_sides, num_sides0, count);

    dump_cbytes(curr_num_elem, n_p, N, time, count);
    dump_meshbytes(curr_num_elem, curr_num_sides, num_sides0, num_color, count);
    dump_tree(count);

    // dump_cbytes(curr_num_elem, n_p, N, time, 0);
    // dump_meshbytes(curr_num_elem, curr_num_sides, num_sides0, 0);
    // dump_tree(0);
}


/* set quadrature 
 *
 * sets the 1d quadrature integration points and weights for the boundary integrals
 * and the 2d quadrature integration points and weights for the volume intergrals.
 */
void set_constant(int n,
                    double **r1_local, double **r2_local, double **w_local,
                    double **s_r, double **oned_w_local,
                    int *local_n_quad, int *local_n_quad1d,
                    int local_N, int local_n_p, int curr_num_elem, int curr_num_sides) {
    int i;
    /*
     * The sides are mapped to the canonical element, so we want the integration points
     * for the boundary integrals for sides s1, s2, and s3 as shown below:

     s (r2) |\
     ^      | \
     |      |  \
     |      |   \
     |   s3 |    \ s2
     |      |     \
     |      |      \
     |      |       \
     |      |________\
     |         s1
     |
     ------------------------> r (r1)

    *
    */
    switch (n) {
        case 0: *local_n_quad = 1;
                *local_n_quad1d = 1;
                break;
        case 1: *local_n_quad = 3;
                *local_n_quad1d = 2;
                break;
        case 2: *local_n_quad = 6;
                *local_n_quad1d = 3;
                break;
        case 3: *local_n_quad = 12 ;
                *local_n_quad1d = 4;
                break;
        case 4: *local_n_quad = 16;
                *local_n_quad1d = 5;
                break;
        case 5: *local_n_quad = 25;
                *local_n_quad1d = 6;
                break;
    }
    // allocate integration points
    *r1_local = (double *)  malloc(*local_n_quad * sizeof(double));
    *r2_local = (double *)  malloc(*local_n_quad * sizeof(double));
    *w_local  = (double *) malloc(*local_n_quad * sizeof(double));

    *s_r = (double *) malloc(*local_n_quad1d * sizeof(double));
    *oned_w_local = (double *) malloc(*local_n_quad1d * sizeof(double));

    // set 2D quadrature rules
    for (i = 0; i < *local_n_quad; i++) {
        if (n > 0) {
            (*r1_local)[i] = quad_2d[2 * n - 1][3*i];
            (*r2_local)[i] = quad_2d[2 * n - 1][3*i+1];
            (*w_local) [i] = quad_2d[2 * n - 1][3*i+2] / 2.; //weights are 2 times too big for some reason
        } else {
            (*r1_local)[i] = quad_2d[0][3*i];
            (*r2_local)[i] = quad_2d[0][3*i+1];
            (*w_local) [i] = quad_2d[0][3*i+2] / 2.; //weights are 2 times too big for some reason
        }
    }

    // set 1D quadrature rules
    for (i = 0; i < *local_n_quad1d; i++) {
        (*s_r)[i] = quad_1d[n][2*i];
        (*oned_w_local)[i] = quad_1d[n][2*i+1];
    }

        // set constant data
        set_N(local_N);
        set_n_p(local_n_p);
        set_num_elem(curr_num_elem);
        set_num_sides(curr_num_sides);
        set_n_quad(*local_n_quad);
        set_n_quad1d(*local_n_quad1d);  

}

void checkCudaError(const char *message)
{
    cudaThreadSynchronize();
    cudaError_t error = cudaGetLastError();
    if(error!=cudaSuccess) {
        fprintf(stderr,"ERROR: %s: %s\n", message, cudaGetErrorString(error) );
        exit(-1);
    }
}


void read_mesh(char *mesh_filename, 
              int *curr_num_sides,
              int *curr_num_elem,
              int *num_sides0,
              double **V1x, double **V1y,
              double **V2x, double **V2y,
              double **V3x, double **V3y,
              int **left_side_number, int **right_side_number,
              double **sides_x1, double **sides_y1,
              double **sides_x2, double **sides_y2,
              int **elem_s1,  int **elem_s2, int **elem_s3,
              int **left_elem, int **right_elem,
              int **color, int *color_size,
              int **original_side,
              int *num_sensor, int **sensor_eID, double **sensor_r, double **sensor_s,

              int restart, 
              double **c, double *time,
              int *local_n_p, int *local_N,
              int **curr_elem, int **curr_side,
              int **curr_elevel, int **slevel,
              int **curr_left1, int **curr_left2, int **curr_left3,
              int **scolor, double **Nx, double **Ny, double **s_length,
              double **J) {

    switch(restart){
        case 1:
            {
            // read the coefficient file
            char *c_filename = "output/resume/C.bin";
            FILE *c_file = fopen(c_filename, "rb");
            if (!c_file) {
                printf("\nERROR: c_file not found.\n");
                exit(-1);
            }

            fread(time,sizeof(double),1,c_file);
            fread(curr_num_elem,sizeof(int),1,c_file);
            fread(local_n_p,sizeof(int),1,c_file);
            fread(local_N,sizeof(int),1,c_file);

            *c = (double *) malloc((*curr_num_elem) * (*local_n_p) * (*local_N) * sizeof(double));

            fread(*c,sizeof(double),(*curr_num_elem)* (*local_n_p) * (*local_N) ,c_file);
            fclose(c_file);
            


            c_filename = "output/resume/curr_mesh.bin";
            c_file = fopen(c_filename, "rb");

            fread(curr_num_elem,sizeof(int),1,c_file);

            *curr_elem = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*curr_elem,sizeof(int),*curr_num_elem,c_file);

            *curr_elevel = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*curr_elevel,sizeof(int),*curr_num_elem,c_file);



            *elem_s1 = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*elem_s1,sizeof(int),*curr_num_elem,c_file);
            *elem_s2 = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*elem_s2,sizeof(int),*curr_num_elem,c_file);
            *elem_s3 = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*elem_s3,sizeof(int),*curr_num_elem,c_file);

            *curr_left1 = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*curr_left1,sizeof(int),*curr_num_elem,c_file);
            *curr_left2 = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*curr_left2,sizeof(int),*curr_num_elem,c_file);
            *curr_left3 = (int *) malloc(*curr_num_elem * sizeof(int));
            fread(*curr_left3,sizeof(int),*curr_num_elem,c_file);


            *J = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*J,sizeof(double),*curr_num_elem,c_file);

            *V1x = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*V1x,sizeof(double),*curr_num_elem,c_file);
            *V1y = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*V1y,sizeof(double),*curr_num_elem,c_file);
            *V2x = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*V2x,sizeof(double),*curr_num_elem,c_file);
            *V2y = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*V2y,sizeof(double),*curr_num_elem,c_file);
            *V3x = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*V3x,sizeof(double),*curr_num_elem,c_file);
            *V3y = (double *) malloc(*curr_num_elem * sizeof(double));
            fread(*V3y,sizeof(double),*curr_num_elem,c_file);


            fread(curr_num_sides,sizeof(int),1,c_file);

            *curr_side  = (int *)   malloc(*curr_num_sides * sizeof(int));
            fread(*curr_side,sizeof(int),*curr_num_sides,c_file);

            *slevel  = (int *) malloc(*curr_num_sides * sizeof(int));
            fread(*slevel,sizeof(int),*curr_num_sides,c_file);

            *original_side  = (int *) malloc(*curr_num_sides * sizeof(int));
            fread(*original_side,sizeof(int),*curr_num_sides,c_file);

            *scolor  = (int *) malloc(*curr_num_sides * sizeof(int));
            fread(*scolor,sizeof(int),*curr_num_sides,c_file);

            *left_elem   = (int *) malloc(*curr_num_sides * sizeof(int));
            fread(*left_elem,sizeof(int),*curr_num_sides,c_file);
            *right_elem  = (int *) malloc(*curr_num_sides * sizeof(int));
            fread(*right_elem,sizeof(int),*curr_num_sides,c_file);

            *left_side_number  = (int *)   malloc(*curr_num_sides * sizeof(int));
            fread(*left_side_number,sizeof(int),*curr_num_sides,c_file);
            *right_side_number = (int *)   malloc(*curr_num_sides * sizeof(int));
            fread(*right_side_number,sizeof(int),*curr_num_sides,c_file);

            fread(color_size,sizeof(int),6,c_file);



            fread(num_sides0,sizeof(int),1,c_file);
            *s_length  = (double *)   malloc(*num_sides0 * sizeof(double));
            fread(*s_length,sizeof(double),*num_sides0,c_file);
            *Nx  = (double *)   malloc(*num_sides0 * sizeof(double));
            fread(*Nx,sizeof(double),*num_sides0,c_file);
            *Ny = (double *)   malloc(*num_sides0 * sizeof(double));
            fread(*Ny,sizeof(double),*num_sides0,c_file);
            fclose(c_file);

            break;
        }
        default:
        {
            int i, items;
            char line[1000];
            FILE *mesh_file;

            // open the mesh to get curr_num_elem for allocations
            mesh_file = fopen(mesh_filename, "r");
            if (!mesh_file) {
                printf("\nERROR: mesh file not found.\n");
                quit();
            }
            // stores the number of sides this element has.

            fgets(line, 1000, mesh_file);
            sscanf(line, "%i", curr_num_elem);
            *V1x = (double *) malloc(*curr_num_elem * sizeof(double));
            *V1y = (double *) malloc(*curr_num_elem * sizeof(double));
            *V2x = (double *) malloc(*curr_num_elem * sizeof(double));
            *V2y = (double *) malloc(*curr_num_elem * sizeof(double));
            *V3x = (double *) malloc(*curr_num_elem * sizeof(double));
            *V3y = (double *) malloc(*curr_num_elem * sizeof(double));

            *elem_s1 = (int *) malloc(*curr_num_elem * sizeof(int));
            *elem_s2 = (int *) malloc(*curr_num_elem * sizeof(int));
            *elem_s3 = (int *) malloc(*curr_num_elem * sizeof(int));

            // read the elements from the mesh
            for (i = 0; i < *curr_num_elem; i++) {
                fgets(line, sizeof(line), mesh_file);
                items = sscanf(line, "%lf %lf %lf %lf %lf %lf %i %i %i", &(*V1x)[i], &(*V1y)[i], 
                                                 &(*V2x)[i], &(*V2y)[i], 
                                                 &(*V3x)[i], &(*V3y)[i], 
                                                 &(*elem_s1)[i], &(*elem_s2)[i], &(*elem_s3)[i]);

                if (items != 9) {
                    printf("error: not enough items (%i) while reading elements from mesh.\n", items);
                    exit(0);
                }

            }

            fgets(line, 1000, mesh_file);
            sscanf(line, "%i", curr_num_sides);
            *num_sides0 = *curr_num_sides;

            *left_side_number  = (int *)   malloc(*curr_num_sides * sizeof(int));
            *right_side_number = (int *)   malloc(*curr_num_sides * sizeof(int));

            *sides_x1    = (double *) malloc(*curr_num_sides * sizeof(double));
            *sides_x2    = (double *) malloc(*curr_num_sides * sizeof(double));
            *sides_y1    = (double *) malloc(*curr_num_sides * sizeof(double));
            *sides_y2    = (double *) malloc(*curr_num_sides * sizeof(double)); 

            *left_elem   = (int *) malloc(*curr_num_sides * sizeof(int));
            *right_elem  = (int *) malloc(*curr_num_sides * sizeof(int));

            *original_side  = (int *) malloc(*curr_num_sides * sizeof(int));
            *color  = (int *) malloc(*curr_num_sides * sizeof(int));

            // read the edges from the mesh
            for (i = 0; i < *curr_num_sides; i++) {
                fgets(line, sizeof(line), mesh_file);
                items = sscanf(line, "%lf %lf %lf %lf %i %i %i %i %i", &(*sides_x1)[i], &(*sides_y1)[i],
                                                    &(*sides_x2)[i], &(*sides_y2)[i],
                                                    &(*left_elem)[i], &(*right_elem)[i],
                                                    &(*left_side_number)[i],
                                                    &(*right_side_number)[i],
                                                    &(*color)[i]);  
                (*original_side)[i] = i;
                if (items != 9) { 
                    printf("error: not enough items (%i) while reading edges from mesh.\n", items);
                    exit(0);
                }
            }
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i", color_size + 0);  

            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i", color_size + 1);  

            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i", color_size + 2);  

            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i", color_size + 3);  

            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i", color_size + 4);  

            color_size[5] = 0;

            if(fgets(line, sizeof(line), mesh_file) != NULL) {
                items = sscanf(line, "%i", num_sensor);  

                if(num_sensor > 0) {
                    *sensor_eID = (int *) malloc(*num_sensor * sizeof(int));
                    *sensor_r = (double *) malloc(*num_sensor * sizeof(double));
                    *sensor_s = (double *) malloc(*num_sensor * sizeof(double));


                    printf("the (%i) sensors are:\n", *num_sensor);
                    for(i = 0; i < *num_sensor;  i++){
                        fgets(line, sizeof(line), mesh_file);
                        items = sscanf(line, "%i %lf %lf", &(*sensor_eID)[i], &(*sensor_r)[i], &(*sensor_s)[i]);  
                        printf("%i %lf %lf\n", (*sensor_eID)[i], (*sensor_r)[i], (*sensor_s)[i]);
                    }

                }
            }

                // close the file
            fclose(mesh_file);
        }
    }

}






void init_gpu(int curr_num_elem, int curr_num_sides, int local_n_p, int local_N,
              double *V1x, double *V1y, 
              double *V2x, double *V2y, 
              double *V3x, double *V3y, 
              int *left_side_number, int *right_side_number,
              int *original_side,
              double *sides_x1, double *sides_y1,
              double *sides_x2, double *sides_y2,
              int *elem_s1, int *elem_s2, int *elem_s3,
              int *left_elem, int *right_elem,
              int convergence, int eval_error, int video,
              int *scolor,
              int n, double **r1_local, double **r2_local, double **w_local, double **s_r,
              double **oned_w_local, int *local_n_quad, int *local_n_quad1d,
              int *sensor_eID, double *sensor_basis, int num_sensor,
              int restart,

              int *left1, int *left2, int *left3,
              double *J,
              int *curr_elem,int *curr_side,
              int *elevel,int *slevel, 

              int num_sides0, double *s_length, double *Nx, double *Ny,
              double *c) {

    checkCudaError("error after gpu malloc"); 

    cudaMemcpy(d_curr_V1x, V1x, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(V1x);
    cudaMemcpy(d_curr_V1y, V1y, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(V1y);
    cudaMemcpy(d_curr_V2x, V2x, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(V2x);
    cudaMemcpy(d_curr_V2y, V2y, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(V2y);
    cudaMemcpy(d_curr_V3x, V3x, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(V3x);
    cudaMemcpy(d_curr_V3y, V3y, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(V3y);

    cudaMemcpy(d_curr_s1, elem_s1, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(elem_s1);
    cudaMemcpy(d_curr_s2, elem_s2, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(elem_s2);
    cudaMemcpy(d_curr_s3, elem_s3, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(elem_s3);

    cudaMemcpy(d_left_side_number , left_side_number , curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(left_side_number);
    cudaMemcpy(d_right_side_number, right_side_number, curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(right_side_number);

    cudaMemcpy(d_left_elem , left_elem , curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(left_elem);
    cudaMemcpy(d_right_elem, right_elem, curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(right_elem);

    cudaMemcpy(d_curr_originalside, original_side, curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(original_side);
    cudaMemcpy(d_curr_scolor, scolor, curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(scolor);

    switch(restart){
        case 1:
            {
            cudaMemcpy(d_curr_left1, left1, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(left1);
            cudaMemcpy(d_curr_left2, left2, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(left2);
            cudaMemcpy(d_curr_left3, left3, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(left3);

            cudaMemcpy(d_curr_J, J, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);free(J);

            cudaMemcpy(d_curr_side, curr_side, curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(curr_side);
            cudaMemcpy(d_curr_elem, curr_elem, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);free(curr_elem);

            cudaMemcpy(d_curr_elevel, elevel, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice); free(elevel);
            cudaMemcpy(d_curr_slevel, slevel, curr_num_sides * sizeof(int), cudaMemcpyHostToDevice);free(slevel);

            cudaMemcpy(d_Nx, Nx, num_sides0 * sizeof(double), cudaMemcpyHostToDevice);free(Nx);
            cudaMemcpy(d_Ny, Ny, num_sides0 * sizeof(double), cudaMemcpyHostToDevice);free(Ny);
            cudaMemcpy(d_s_length, s_length, num_sides0 * sizeof(double), cudaMemcpyHostToDevice);free(s_length);

            for(int n = 0; n < local_N; n++){
                for(int i = 0; i < local_n_p; i++){
                    cudaMemcpy(h_c[n*local_n_p + i], c + n*local_n_p*curr_num_elem + i*curr_num_elem, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
                }
            }
            free(c);
            break;
            }
        default:
        {
            // copy over data
            cudaMemcpy(d_s_V1x, sides_x1, curr_num_sides * sizeof(double), cudaMemcpyHostToDevice);free(sides_x1);
            cudaMemcpy(d_s_V1y, sides_y1, curr_num_sides * sizeof(double), cudaMemcpyHostToDevice);free(sides_y1);
            cudaMemcpy(d_s_V2x, sides_x2, curr_num_sides * sizeof(double), cudaMemcpyHostToDevice);free(sides_x2);
            cudaMemcpy(d_s_V2y, sides_y2, curr_num_sides * sizeof(double), cudaMemcpyHostToDevice);free(sides_y2);

            if(num_sensor > 0){
                cudaMemcpy(d_sensor_eID, sensor_eID, num_sensor * sizeof(int), cudaMemcpyHostToDevice);
                cudaMemcpy(d_sensor_basis, sensor_basis, num_sensor*local_n_p * sizeof(double), cudaMemcpyHostToDevice);
            }
        }
    }


    checkCudaError("error after gpu copy2");


    set_constant(n, r1_local, r2_local, w_local, 
                 s_r, oned_w_local, local_n_quad, local_n_quad1d,
                 local_N, local_n_p, curr_num_elem, curr_num_sides);

}





void usage_error() {
    printf("\nUsage: dgcuda [OPTIONS] [MESH] [OUTFILE]\n");
    printf(" Options: [-p] Order of polynomial approximation.\n");
    printf("          [-t] Number of timesteps.\n");
    printf("          [-T] End time.\n");
    printf("          [-v] Verbose.\n");
    printf("          [-c] Run to convergence of tolerance TOL\n");
    printf("          [-V] Print out every N timesteps.\n");
    printf("          [-e] Evaluate the error. Requires an exact solution.\n");
    printf("          [-b] Benchmark.\n");
    printf("          [-r] Refine.\n");
    printf("          [-R] Resume computation.\n");
    printf("          [-P] Project solution from an already existing solution.\n");
    printf("          [-h] Display this message.\n");
}

int get_input(int argc, char *argv[],
               int *n, int *total_timesteps, 
               double *endtime,
               int *verbose,
               int *video,
               int *convergence,
               double *tol,
               int *benchmark,
               int *eval_error,
               int *resume,
               int *refinecycles,
               int *refine,
               int *project,
               int *sensor,
               char **mesh_filename, char **limiter_filename) {

    int i;

    *total_timesteps = -1;
    *verbose         = 0;
    *convergence     = 0;
    *eval_error      = 0;
    *benchmark       = 0;
    *video           = 0;
    *resume          = 0;
    *refine          = 0;
    *project         = 0;
    *refinecycles    = -1;
    *refine          = -1;
    *sensor          = -1;
    // read command line input
    if (argc < 5) {
        usage_error();
        return 1;
    }
    for (i = 0; i < argc; i++) {
        // order of polynomial
        if (strcmp(argv[i], "-p") == 0) {
            if (i + 1 < argc) {
                *n = atoi(argv[i+1]);
                if (*n < 0 || *n > 5) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        // number of total_timesteps
        if (strcmp(argv[i], "-t") == 0) {
            if (i + 1 < argc) {
                *total_timesteps = atoi(argv[i+1]);
                if (*total_timesteps < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-T") == 0) {
            if (i + 1 < argc) {
                *endtime = atof(argv[i+1]);
                if (*endtime < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-V") == 0) {
            if (i + 1 < argc) {
                *video = atof(argv[i+1]);
                if (*video < 0) {
                    usage_error();
                    return 1;
                }
            } else {
                usage_error();
                return 1;
            }
        }
        if (strcmp(argv[i], "-c") == 0) {
            if (i + 1 < argc) {
                *convergence = 1;
                *tol         = atof(argv[i+1]);
                if (*tol < 0) {
                    usage_error();
                    return 1;
                }
            }
        }        
        if (strcmp(argv[i], "-r") == 0) {
            if (i + 1 < argc) {
                *refinecycles = atoi(argv[i+1]);
                if (*refinecycles < 0) {
                    usage_error();
                    return 1;
                }
            }
        }

        if (strcmp(argv[i], "-S") == 0) {
            if (i + 1 < argc) {
                *sensor = atoi(argv[i+1]);
                if (*sensor < 0) {
                    usage_error();
                    return 1;
                }
            }
        }
        if (strcmp(argv[i], "-R") == 0) {
            if (i + 1 < argc) {
                *refine =1;
            }
        }
        if (strcmp(argv[i], "-b") == 0) {
            *benchmark = 1;
        }
        if (strcmp(argv[i], "-e") == 0) {
            *eval_error = 1;
        }
        if (strcmp(argv[i], "-v") == 0) {
            *verbose = 1;
        }
        if (strcmp(argv[i], "-RS") == 0) {
            *resume = 1;
        }
         if (strcmp(argv[i], "-P") == 0) {
            *project = 1;
        }
        if (strcmp(argv[i], "-h") == 0) {
                usage_error();
                return 1;
        }
        if (strcmp(argv[i], "-l") == 0) {
            if (i + 1 < argc) {
                *limiter_filename = argv[i+1];
            }
        }
    } 

    // second last argument is filename
    *mesh_filename = argv[argc - 1];

    return 0;
}











void print_memory(char *name, size_t total_memory) {
    if (total_memory < 1e3) {
        printf(" ? %s memory required: %f B\n",name, total_memory);
    } else if (total_memory >= 1e3 && total_memory < 1e6) {
        printf(" ? %s memory required: %f KB\n", name, total_memory / 1000.f);
    } else if (total_memory >= 1e6 && total_memory < 1e9) {
        printf(" ? %s memory required: %f MB\n", name, total_memory / 1000000.f);
    } else {
        printf(" ? %s memory required: %f GB\n",name,  total_memory / 1000000000.f);
    }
}



void allocate_gpu(int emesh_MAX, int smesh_MAX, 
                  memoryCounters *timesteppingCounter,
                  int num_elem0,int num_sides0,
                  int local_N, int local_n_p, int convergence,
                  int num_sensor) {
    


    /*
    3. ALLOCATE mesh characteristics
    */

    allocate((void **) &d_curr_elem, emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));

    allocate((void **) &d_curr_elevel, emesh_MAX * sizeof(int) , &(timesteppingCounter->emesh_memory));
    set_value<<<get_blocks(emesh_MAX,512),512>>>(d_curr_elevel, 0, emesh_MAX);


    allocate((void **) &d_curr_left1, emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));
    allocate((void **) &d_curr_left2, emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));
    allocate((void **) &d_curr_left3, emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));

    allocate((void **) &d_curr_s1,  emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));
    allocate((void **) &d_curr_s2,  emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));
    allocate((void **) &d_curr_s3,  emesh_MAX * sizeof(int), &(timesteppingCounter->emesh_memory));

    allocate((void **) &d_curr_side, smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);
    allocate((void **) &d_curr_originalside, smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);
    allocate((void **) &d_curr_scolor, smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);
    allocate((void **) &d_curr_slevel, smesh_MAX * sizeof(int) , &timesteppingCounter->smesh_memory);
    set_value<<<get_blocks(smesh_MAX,512),512>>>(d_curr_slevel, 0, smesh_MAX);

    allocate((void **) &d_left_side_number , smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);
    allocate((void **) &d_right_side_number, smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);

    allocate((void **) &d_left_elem , smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);
    allocate((void **) &d_right_elem, smesh_MAX * sizeof(int), &timesteppingCounter->smesh_memory);
    
    allocate((void **) &d_lambda   , emesh_MAX * sizeof(double), &timesteppingCounter->emesh_memory);
    allocate((void **) &d_circles, emesh_MAX * sizeof(double), &timesteppingCounter->emesh_memory);


    allocate((void **) &d_volume, emesh_MAX * sizeof(double), &timesteppingCounter->emesh_memory);

    /*
    4. ALLOCATE geometric information
     */

    allocate((void **) &d_curr_V1x, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_V1y, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_V2x, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_V2y, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_V3x, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_V3y, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);

    allocate((void **) &d_curr_xr, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_yr, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_xs, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_curr_ys, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);

    allocate((void **) &d_curr_J, emesh_MAX * sizeof(double), &timesteppingCounter->geometry_memory);

    allocate((void **) &d_s_length , num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);

    allocate((void **) &d_s_V1x, num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_s_V2x, num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_s_V1y, num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_s_V2y, num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);

    allocate((void **) &d_Nx, num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);
    allocate((void **) &d_Ny, num_sides0 * sizeof(double), &timesteppingCounter->geometry_memory);




    /*
    5. ALLOCATE time integration 
     */

    h_c = (double **) malloc(N_MAX*NP_MAX * sizeof(double *));
    allocate((void **) &d_c, N_MAX*NP_MAX* sizeof(double*), &timesteppingCounter->geometry_memory); 
    
    for(int n = 0; n < local_N; n ++){
      for(int i = 0; i < local_n_p; i ++){
        allocate((void **) h_c + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
      }
    }

    cudaMemcpy(d_c, h_c, N_MAX*NP_MAX* sizeof(double*), cudaMemcpyHostToDevice);


 


    switch (time_integrator) {
        case RK6: 
            h_temp=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k1 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k2 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k3 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k4 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k5 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k6 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k7 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k8 = (double **) malloc(local_N*local_n_p * sizeof(double *));


            allocate((void **) &d_temp, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k1,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k2,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k3,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k4,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k5,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k6,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k7,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k8,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            
            for(int n = 0; n < local_N; n ++){
              for(int i = 0; i < local_n_p; i ++){
                allocate((void **) h_temp + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k1 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k2 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k3 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k4 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k5 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k6 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k7 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k8 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
              }
            }

            cudaMemcpy(d_temp, h_temp, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k1, h_k1, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k2, h_k2, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k3, h_k3, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k4, h_k4, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k5, h_k5, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k6, h_k6, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k7, h_k7, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k8, h_k8, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            checkCudaError("error after gpu malloc");
            break;
        case RK5: 
            h_temp=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k1 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k2 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k3 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k4 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k5 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k6 = (double **) malloc(local_N*local_n_p * sizeof(double *));


            allocate((void **) &d_temp, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k1,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k2,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k3,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k4,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k5,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k6,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            
            for(int n = 0; n < local_N; n ++){
              for(int i = 0; i < local_n_p; i ++){
                allocate((void **) h_temp + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k1 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k2 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k3 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k4 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k5 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k6 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
              }
            }

            cudaMemcpy(d_temp, h_temp, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k1, h_k1, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k2, h_k2, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k3, h_k3, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k4, h_k4, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k5, h_k5, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k6, h_k6, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            checkCudaError("error after gpu malloc");
            break;
        case RK4: 
            h_temp=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k1 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k2 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k3 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k4 = (double **) malloc(local_N*local_n_p * sizeof(double *));


            allocate((void **) &d_temp, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k1,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k2,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k3,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k4,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            
            for(int n = 0; n < local_N; n ++){
              for(int i = 0; i < local_n_p; i ++){
                allocate((void **) h_temp + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k1 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k2 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k3 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k4 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
              }
            }

            cudaMemcpy(d_temp, h_temp, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k1, h_k1, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k2, h_k2, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k3, h_k3, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k4, h_k4, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            checkCudaError("error after gpu malloc");
            break;

        case RK3: 
            h_temp=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k1 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k2 = (double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k3 = (double **) malloc(local_N*local_n_p * sizeof(double *));


            allocate((void **) &d_temp, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k1,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k2,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k3,   local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            
            for(int n = 0; n < local_N; n ++){
              for(int i = 0; i < local_n_p; i ++){
                allocate((void **) h_temp + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k1 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k2 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k3 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
              }
            }

            cudaMemcpy(d_temp, h_temp, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k1, h_k1, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k2, h_k2, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k3, h_k3, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            checkCudaError("error after gpu malloc");



            break;
        case RK2: 
            h_temp=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_temp2=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k1 = (double **) malloc(N_MAX*NP_MAX * sizeof(double *));
            h_k2 = (double **) malloc(N_MAX*NP_MAX * sizeof(double *));
            allocate((void **) &d_temp, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_temp2, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k1, N_MAX*NP_MAX* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k2, N_MAX*NP_MAX* sizeof(double*), &timesteppingCounter->geometry_memory); 
            
            for(int n = 0; n < local_N; n ++){
              for(int i = 0; i < local_n_p; i ++){
                allocate((void **) h_temp + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_temp2 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k1 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k2 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
              }
            }

            cudaMemcpy(d_temp, h_temp, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_temp2, h_temp2, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k1, h_k1, N_MAX*NP_MAX* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k2, h_k2, N_MAX*NP_MAX* sizeof(double*), cudaMemcpyHostToDevice);
            checkCudaError("error after gpu malloc");
            break;
        case RK1: 
            h_temp=(double **) malloc(local_N*local_n_p * sizeof(double *));
            h_k1 = (double **) malloc(N_MAX*NP_MAX * sizeof(double *));
            allocate((void **) &d_temp, local_N*local_n_p* sizeof(double*), &timesteppingCounter->geometry_memory); 
            allocate((void **) &d_k1, N_MAX*NP_MAX* sizeof(double*), &timesteppingCounter->geometry_memory); 
            
            for(int n = 0; n < local_N; n ++){
              for(int i = 0; i < local_n_p; i ++){
                allocate((void **) h_temp + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
                allocate((void **) h_k1 + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
              }
            }

            cudaMemcpy(d_temp, h_temp, local_N*local_n_p* sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_k1, h_k1, N_MAX*NP_MAX* sizeof(double*), cudaMemcpyHostToDevice);
            checkCudaError("error after gpu malloc");
            break;
        default:
            printf("Error selecting time integrator.\n");
            exit(0);
    }

    if (convergence) {
      h_c_prev = (double **) malloc(N_MAX*NP_MAX * sizeof(double *));
      allocate((void **) &d_c_prev, N_MAX*NP_MAX* sizeof(double*), &timesteppingCounter->geometry_memory); 
      
      for(int n = 0; n < local_N; n ++){
        for(int i = 0; i < local_n_p; i ++){
          allocate((void **) h_c_prev + n*local_n_p + i,  emesh_MAX * sizeof(double), &timesteppingCounter->integration_memory);
        }
      }

      cudaMemcpy(d_c_prev, h_c_prev, N_MAX*NP_MAX* sizeof(double*), cudaMemcpyHostToDevice);


        // allocate((void **) &d_c_prev, local_N * emesh_MAX * local_n_p * sizeof(double), &timesteppingCounter->integration_memory);
    }
    /*
    6. ALLOCATE monotonicity preserving limiter
     */

    allocate_limiter(timesteppingCounter, num_elem0, num_sides0, limiter);



    /*
    7. ALLOCATE sensor data
     */

    allocate( (void **) &d_sensor_eID, num_sensor*sizeof(int),   &(timesteppingCounter->visualization_memory)   );
    allocate( (void **) &d_sensor_basis, num_sensor*local_n_p*sizeof(double),   &(timesteppingCounter->visualization_memory)   );
    allocate( (void **) &d_sensorbuffer, num_sensor*local_N*sizeof(double),   &(timesteppingCounter->visualization_memory)   );


    /*
    8. allocate plotting
    */
    allocate_plot(timesteppingCounter, emesh_MAX);

    /*
    9. allocate error estimate
    */
    allocate_error_estimate(timesteppingCounter, emesh_MAX);
}

void print_usage(memoryCounters *counter) {
 
    print_memory("tree",counter->etree_memory+counter->stree_memory);
    print_memory("buffer",counter->ebuffer_memory + counter->sbuffer_memory);
    print_memory("mesh",counter->emesh_memory+counter->smesh_memory);
    print_memory("geometry",counter->geometry_memory);
    print_memory("integration",counter->integration_memory);
    print_memory("visualization",counter->visualization_memory);
    print_memory("miscellaneous",counter->miscellaneous_memory);
    print_memory("total", counter->total());
    size_t free_byte ;
    size_t total_byte ;
    cudaMemGetInfo( &free_byte, &total_byte ) ;
    printf(" ? MEMORY STATUS %.2lf/%.2lf\n", 1e-9f*free_byte, 1e-9f*total_byte);


}

void preval_computations(double **basis_local, double *r1_local, double *r2_local, double *s_r, double *w_local, double *oned_w_local, 
                  		 int local_n_quad, int local_n_quad1d, int local_n_p,
                  		 int n_threads, int convergence, int eval_error,
					     int curr_num_elem, int curr_num_sides,
                         int restart) {

    init_nlbj_h2<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_circles, 
																      d_curr_V1x, d_curr_V1y, 
																      d_curr_V2x, d_curr_V2y, 
																      d_curr_V3x, d_curr_V3y, 
																      curr_num_elem);

    // pre computations
    preval_partials<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_V1x, d_curr_V1y,
                                                                         d_curr_V2x, d_curr_V2y,
                                                                         d_curr_V3x, d_curr_V3y,
                                                                         d_curr_xr,  d_curr_yr,
                                                                         d_curr_xs,  d_curr_ys,
                                                                         curr_num_elem);

    if(restart != 1){
        preval_jacobian<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_J, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, curr_num_elem); 
        preval_side_length<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_s_length, d_s_V1x, d_s_V1y, d_s_V2x, d_s_V2y, curr_num_sides);
                                                
        preval_normals<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_Nx, d_Ny, 
                                                      d_s_V1x, d_s_V1y, d_s_V2x, d_s_V2y,
                                                      d_curr_V1x, d_curr_V1y, 
                                                      d_curr_V2x, d_curr_V2y, 
                                                      d_curr_V3x, d_curr_V3y, 
                                                      d_left_side_number, 
                                                      curr_num_sides);

        preval_normals_direction<<<get_blocks(curr_num_sides, n_threads), n_threads>>>(d_Nx, d_Ny, 
                                                      d_curr_V1x, d_curr_V1y, 
                                                      d_curr_V2x, d_curr_V2y, 
                                                      d_curr_V3x, d_curr_V3y, 
                                                      d_left_elem, d_left_side_number,
                                                      curr_num_sides);
    }





    cudaThreadSynchronize();
    checkCudaError("error after prevals. 2");


    // evaluate the basis functions at those points and store on GPU
    preval_basis(basis_local, r1_local, r2_local, s_r, w_local, oned_w_local, local_n_quad, local_n_quad1d, local_n_p);
    cudaThreadSynchronize();

    free(oned_w_local);
    free(s_r);

}

// __global__ void eval_error_gradient2(double *d_basis_grad_x) {
//     int idx = blockDim.x * blockIdx.x + threadIdx.x;
//     double dx, dy;
//     double xr, xs, yr, ys, J;

//     if (idx ==0) {

//         printf("%lf %lf\n", d_basis_grad_x[3], d_basis_grad_x[4]);


//     }

// }


int run_dgcuda(int argc, char *argv[]) {
    checkCudaError("error before start.");
    int curr_num_elem, curr_num_sides;

    int i, n, local_n_p, total_timesteps, local_n_quad, local_n_quad1d, runs;
    int verbose, convergence, video, eval_error, benchmark, resume, refinecycles, refine, project;

    int Nr = 0;
    int go;
    double aRT = 0, aN = 0;


    double endtime, t;
    double tol, total_error, max_error;
    int *curr_elem, *curr_elevel;
    int *curr_left1, *curr_left2, *curr_left3;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y, *J, *xr, *xs, *yr, *ys;
    double *sides_x1, *sides_x2;
    double *sides_y1, *sides_y2;
    double *s_length, *Nx, *Ny;
    int num_sides0;

    double *basis_local, *r1_local, *r2_local, *w_local;

    double *s_r, *oned_w_local;

    int *left_elem, *right_elem;
    int *elem_s1, *elem_s2, *elem_s3;
    int *left_side_number, *right_side_number;
    int *scolor, *slevel, *curr_side; 
    int color_size[6];
    int *original_side;

    FILE *c_file, *out_file;

    char out_filename[100];
    char *c_filename, *mesh_filename, *limiter_filename;

    double *c;

    double *Uv1, *Uv2, *Uv3;
    double *error;

    clock_t start, end, r_start, r_end;
    double elapsed;

    double tprev = 0;
    int sensor = -1, num_sensor = 0;
    int *sensor_eID;
    double *sensor_basis, *sensor_r, *sensor_s;

    memoryCounters *counter = new memoryCounters();


   // set the maximum size of the trees
    // set the maximum size of the buffers
    int emesh_MAX =  1.1e6 ;
    int smesh_MAX = 1.5 * emesh_MAX;

    int n_threads          = 512;



    // get input 
    endtime = -1;

    if (get_input(argc, argv, &n, &total_timesteps, &endtime, 
                              &verbose, &video, &convergence, &tol, 
                              &benchmark, &eval_error, &resume, &refinecycles, &refine, &project,
                              &sensor,
                              &mesh_filename, &limiter_filename)) {
        return 1;
    }

    if(n == 0)
        time_integrator = RK2;
    else if(n == 1)
        time_integrator = RK2;
        // time_integrator = RK1;
    else if(n == 2)
        time_integrator = RK3;
    else if(n == 3)
        time_integrator = RK4;
    else if(n == 4)
        time_integrator = RK5;
    else if(n == 5)
        time_integrator = RK6;

    // read in the mesh 
    read_mesh(mesh_filename, &curr_num_sides, &curr_num_elem, &num_sides0,
              &V1x, &V1y, &V2x, &V2y, &V3x, &V3y,
              &left_side_number, &right_side_number,
              &sides_x1, &sides_y1, 
              &sides_x2, &sides_y2, 
              &elem_s1, &elem_s2, &elem_s3,
              &left_elem, &right_elem,
              &scolor, color_size,
              &original_side,
              &num_sensor, &sensor_eID, &sensor_r, &sensor_s,
              resume, 
              &c, &tprev,
              &local_n_p, &local_N,
              &curr_elem, &curr_side,
              &curr_elevel, &slevel,
              &curr_left1, &curr_left2, &curr_left3,
              &scolor, &Nx, &Ny, &s_length,
              &J);
    // set the order of the approximation & timestep
    local_n_p = (n + 1) * (n + 2) / 2;

    if(num_sensor > 0 && sensor > 0) {
        char sensor_filename[100];
        FILE *out_file;
        sensor_basis = (double *) malloc(num_sensor*local_n_p * sizeof(double));

        for(i = 0; i < num_sensor; i++) {
            sprintf(sensor_filename, "output/sensors/sensor%i.txt", i);
            out_file  = fopen(sensor_filename, "w"); //clear this sensor output file
            fclose(out_file);
        }

        evaluate_sensor_basis(sensor_basis, sensor_r, sensor_s, num_sensor, local_n_p);
    } 


    checkCudaError("error before init.");
    cudaDeviceReset();


    // set the maximum size of the trees
    allocate_gpu(emesh_MAX, smesh_MAX, 
                 counter,
                 curr_num_elem, curr_num_sides,
                 local_N, local_n_p,convergence, num_sensor);


    // initialize the gpu
    init_gpu(curr_num_elem, curr_num_sides, local_n_p, local_N,
             V1x, V1y, V2x, V2y, V3x, V3y,
             left_side_number, right_side_number,
             original_side,
             sides_x1, sides_y1,
             sides_x2, sides_y2, 
             elem_s1, elem_s2, elem_s3,
             left_elem, right_elem,
             convergence, eval_error,video,
             scolor,
             n, &r1_local, &r2_local, &w_local, 
             &s_r, &oned_w_local, &local_n_quad, &local_n_quad1d,
             sensor_eID, sensor_basis, num_sensor,
             resume,

             curr_left1, curr_left2, curr_left3,
             J,
             curr_elem, curr_side,
             curr_elevel, slevel, 

             num_sides0, s_length, Nx, Ny, c);



    preval_computations(&basis_local, r1_local, r2_local, s_r, w_local, oned_w_local,
                       local_n_quad, local_n_quad1d, local_n_p,
                       n_threads, convergence, eval_error,
                       curr_num_elem, curr_num_sides,
                       resume);
    init_error_estimate(local_n_p, local_N+1, 5, local_N, h_k1, d_basis_vertex);

    init_plot(d_curr_V1x, d_curr_V2x, d_curr_V3x, d_curr_V1y, d_curr_V2y, d_curr_V3y, 
              d_basis_vertex, d_basis_midpoint, d_c, local_N, local_n_p);
    allocateCompaction(smesh_MAX);    
    allocateTimestepper(emesh_MAX);



    allocateAdaptivity(curr_num_elem, curr_num_sides,
                       emesh_MAX, smesh_MAX, 
                       local_n_p, local_N, n,
                       d_c, h_c,
                       d_curr_elem, d_curr_side,
                       d_curr_s1,d_curr_s2,d_curr_s3,
                       d_curr_elevel, d_curr_slevel,
                       d_curr_originalside, d_s_length,
                       d_curr_left1,d_curr_left2,d_curr_left3,
                       d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y,
                       d_left_elem, d_right_elem, d_left_side_number, d_right_side_number,
                       d_curr_J, d_curr_xr, d_curr_xs, d_curr_yr, d_curr_ys,
                       d_circles,
                       local_n_quad, d_basis, d_basis_refined, d_basis_grad_x, d_basis_grad_y, d_w, 
                       d_curr_scolor, color_size,
                       counter,
                       h_k1,limiter,
                       verbose);

    initAdaptivity(resume);

    init_limiter(curr_num_elem, 
                 d_basis_midpoint, d_basis_vertex, d_basis, d_w, d_basis_side,
                 d_circles,
                 d_curr_s1, d_curr_s2, d_curr_s3,
                 d_left_elem, d_right_elem,
                 d_left_side_number, d_right_side_number,
                 get_spos(), get_schild1(), get_schild2(),
                 d_curr_V1x, d_curr_V1y,
                 d_curr_V2x, d_curr_V2y,
                 d_curr_V3x, d_curr_V3y,
                 local_N, local_n_p, local_n_quad, local_n_quad1d,
                 limiter_filename);


    print_usage(counter);
 





    free(basis_local);
    free(r1_local);
    free(r2_local);
    free(w_local);

    printf(" ? %i degree polynomial interpolation (local_n_p = %i)\n", n, local_n_p);
    printf(" ? %i precomputed basis points\n", local_n_quad * local_n_p);
    printf(" ? %i elements\n", curr_num_elem);
    printf(" ? %i sides\n", curr_num_sides);
    printf(" ? refinecycles = %i\n", refinecycles);
    printf(" ? refine = %i\n", refine);
    printf(" ? resume = %i\n", resume);
    printf(" ? endtime = %lf\n", endtime);

    if (endtime == -1 && convergence != 1) {
        printf(" ? total_timesteps = %i\n", total_timesteps);
    } else if (endtime != -1 && convergence != 1) {
        printf(" ? endtime = %lf\n", endtime);
    } else if (convergence == 1) {
        printf(" ? convergence = %.14lf\n", tol);
    }
    
    cudaThreadSynchronize();
    checkCudaError("error before time integration.");

    printf("creating connectivity tree...\n");


 

    if(!resume) {
        init_conditions<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, d_curr_J, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, curr_num_elem);
        if (limiter) {
            limit(d_c, curr_num_elem);
        }
		eval_rhs(curr_num_elem, curr_num_sides, tprev, color_size);
        checkCudaError("error after initial conditions.");

        int count = 20; 
        go = 1; 
        int prev ;

       if(refinecycles > 0 || refine == 1) {  
        while(go && count > 0) {  

            prev = curr_num_elem;
                adapt(&d_c,
                      &d_curr_elem, &d_curr_side,
                      &d_curr_s1, &d_curr_s2, &d_curr_s3,
                      &d_curr_elevel, &d_curr_slevel,
                      &d_curr_originalside,
                      &d_curr_left1, &d_curr_left2, &d_curr_left3,
                      &d_curr_V1x, &d_curr_V1y, &d_curr_V2x, &d_curr_V2y, &d_curr_V3x, &d_curr_V3y,
                      &d_curr_J, &d_curr_xr, &d_curr_xs, &d_curr_yr, &d_curr_ys,
                      &curr_num_elem, &curr_num_sides,
                      &d_curr_scolor,
                      cumulative_sum, 0., 
                      -count);

                cudaThreadSynchronize();
                if(verbose)
                    printf("refinement %i num_elem is %i num_sides is %i\n", count, curr_num_elem, curr_num_sides);

                init_conditions<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, d_curr_J, d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, curr_num_elem);
    	        if (limiter) {
                    limit(d_c, curr_num_elem);
    	        }
				eval_rhs(curr_num_elem, curr_num_sides,tprev, color_size); 

                count--;

            }
        }
    }





    if (benchmark) {
        start = clock();
    }
    printf("\n \n");
    int vidnum=0;
    int cumulative_timestep = 0;
    aN = curr_num_elem;



    if(refine == -1 && refinecycles == -1) {
        switch (time_integrator) {
            // case RK1:
            //     tprev = rk1(curr_num_elem, curr_num_sides,  
            //                         n, local_n_p,
            //                         endtime, total_timesteps, 
            //                         verbose, convergence, video, tol, tprev, 
            //                         refine,  runs, refinecycles,
            //                         color_size); 
            //     break;
            case RK2: 
                tprev = rk2_ssp(curr_num_elem, curr_num_sides,  
                                n, local_n_p,
                                endtime, total_timesteps, 
                                verbose, convergence, video, tol, tprev, benchmark,
                                refine,  runs, refinecycles,
                                color_size); 
                break;
            case RK3:
                tprev = rk3_ssp(curr_num_elem, curr_num_sides,  
                                n, local_n_p,
                                endtime, total_timesteps, 
                                verbose, convergence, video, tol, tprev, benchmark,
                                refine,  runs, refinecycles,
                                color_size); 

                break;
            case RK4:
                tprev = rk4(curr_num_elem, curr_num_sides,  
                                n, local_n_p,
                                endtime, total_timesteps, 
                                verbose, convergence, video, tol, tprev, benchmark,
                                refine,  runs, refinecycles,
                                color_size); 
                break;
            case RK5:
                tprev = rk5(curr_num_elem, curr_num_sides,  
                                n, local_n_p,
                                endtime, total_timesteps, 
                                verbose, convergence, video, tol, tprev, benchmark,
                                refine,  runs, refinecycles,
                                color_size); 
                break;
            case RK6:
                tprev = rk5(curr_num_elem, curr_num_sides,  
                                n, local_n_p,
                                endtime, total_timesteps, 
                                verbose, convergence, video, tol, tprev, benchmark,
                                refine,  runs, refinecycles,
                                color_size); 
                break;
            default:
                printf("Error: no time integrator selected.\n");
                exit(0);
        }       
    }
    else{
        for (runs = 0; (runs < refinecycles) || (tprev < endtime); runs++) {
            switch (time_integrator) {
                // case RK1:
                //     tprev = rk1_colored_jump(curr_num_elem, curr_num_sides,  
                //                         n, local_n_p,
                //                         endtime, total_timesteps, 
                //                         verbose, convergence, video, tol, tprev, 
                //                         refine,  runs, refinecycles,
                //                         color_size); 
                case RK2: 
                // tprev = rk2_detector(curr_num_elem, curr_num_sides,  
                //                 n, local_n_p,
                //                 endtime, total_timesteps, 
                //                 verbose, convergence, video, tol, tprev, benchmark,
                //                 refine,  runs, refinecycles,
                //                 color_size); 
                    tprev = rk2_ssp(curr_num_elem, curr_num_sides,  
                                    n, local_n_p,
                                    endtime, total_timesteps, 
                                    verbose, convergence, video, tol, tprev, benchmark,
                                    refine,  runs, refinecycles,
                                    color_size); 
                    break;
                case RK3:
                    tprev = rk3_ssp(curr_num_elem, curr_num_sides,  
                                    n, local_n_p,
                                    endtime, total_timesteps, 
                                    verbose, convergence, video, tol, tprev, benchmark,
                                    refine,  runs, refinecycles,
                                    color_size); 
                    break;
                case RK4:
                    tprev = rk4(curr_num_elem, curr_num_sides,  
                                    n, local_n_p,
                                    endtime, total_timesteps, 
                                    verbose, convergence, video, tol, tprev, benchmark,
                                    refine,  runs, refinecycles,
                                    color_size); 

                    break;
                case RK5:
                    tprev = rk5(curr_num_elem, curr_num_sides,  
                                    n, local_n_p,
                                    endtime, total_timesteps, 
                                    verbose, convergence, video, tol, tprev, benchmark,
                                    refine,  runs, refinecycles,
                                    color_size); 

                    break;
                case RK6:
                    tprev = rk5(curr_num_elem, curr_num_sides,  
                                    n, local_n_p,
                                    endtime, total_timesteps, 
                                    verbose, convergence, video, tol, tprev, benchmark,
                                    refine,  runs, refinecycles,
                                    color_size); 
                    break;
                default:
                    printf("Error: no time integrator selected.\n");
                    exit(0);
            } 

            if (video > 0) {
                if (runs % video == 0) {
                    dump(curr_num_elem, curr_num_sides, local_n_p, local_N, num_sides0, tprev, color_size,runs);
                    write_U(curr_num_elem, vidnum, total_timesteps, tprev);
                    vidnum++;
                }
            }       

            if ( (runs < refinecycles) || (refine == 1 && tprev < endtime) ) {
            // if ( (runs +1< refinecycles) || (refine == 1 && tprev < endtime) ) {
                cudaThreadSynchronize();
                r_start = clock();
                adapt(&d_c,
                      &d_curr_elem, &d_curr_side,
                      &d_curr_s1, &d_curr_s2, &d_curr_s3,
                      &d_curr_elevel, &d_curr_slevel,
                      &d_curr_originalside,
                      &d_curr_left1, &d_curr_left2, &d_curr_left3,
                      &d_curr_V1x, &d_curr_V1y, &d_curr_V2x, &d_curr_V2y, &d_curr_V3x, &d_curr_V3y,
                      &d_curr_J, &d_curr_xr, &d_curr_xs, &d_curr_yr, &d_curr_ys,
                      &curr_num_elem, &curr_num_sides,
                      &d_curr_scolor,
                      cumulative_sum, tprev,
                      runs);
                cudaThreadSynchronize();
                r_end = clock();

                aRT += ( (double)(r_end - r_start) )/CLOCKS_PER_SEC;
                aN = (aN * Nr + curr_num_elem)/(Nr + 1);
                Nr++;
                if(verbose)
                    printf("refinement %i num_elem is %i num_sides is %i\n", runs, curr_num_elem, curr_num_sides);

            }
        }
        printf("\n \nRefine time %lf \nAverage number of active elements %lf \nnumber of refinement cycles %i\n\n", aRT, aN, Nr);
    }
    t = tprev;

    printf("\nFinal time T = %lf\n", t);


    printf("\n \n");
    if (benchmark) {
        end = clock();
        elapsed = ((double)(end - start)) / CLOCKS_PER_SEC;
        printf("Runtime: %lf seconds\n", elapsed);
    }


    // // evaluate the error
    if (eval_error) {
        cudaMalloc((void **) &d_error, curr_num_elem * sizeof(double));
        error = (double *) malloc(curr_num_elem * sizeof(double));
        // L_2
        for (n = 0; n < local_N; n++) {
            // eval_error_L2<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, d_error, 
            //                 d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
            //                 n, t,
            //                 curr_num_elem);
            evaluate_error_l2(d_c, d_error, 
                              d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
                              n, t,
                              curr_num_elem);
            // copy over error
            cudaMemcpy(error, d_error, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

            // sum total L2 error
            total_error = 0.;
            max_error   = -1;
            for (i = 0; i < curr_num_elem; i++) {
                total_error += error[i];
                max_error = (error[i] > max_error) ? error[i] : max_error;
            }
            printf("L2 U%i error     = %.4e\n", n, sqrt(total_error));
            // printf("inf L2 U%i error = %.019lf\n", n, max_error);
        }

        // L_1
        for (n = 0; n < local_N; n++) {
            // eval_error_L1<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, d_error, 
            //                 d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
            //                 n, t, curr_num_elem);
            evaluate_error_l1(d_c, d_error, 
                              d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
                              n, t,
                              curr_num_elem);
            // copy over error
            cudaMemcpy(error, d_error, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

            // sum total L1 error
            total_error = 0.;
            max_error   = -1;
            for (i = 0; i < curr_num_elem; i++) {
                total_error += error[i];
                max_error = (error[i] > max_error) ? error[i] : max_error;
                // printf("final error %i %.4e\n", i, error[i]);
            }
            printf("L1 U%i error = %.4e\n", n, total_error);
            // printf("inf L1 U%i error = %.019lf\n", n, max_error);
        }

        // free(error);
    // }

        // L_1_m
        for (n = 0; n < local_N; n++) {
            // eval_error_L1<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, d_error, 
            //                 d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
            //                 n, t, curr_num_elem);
            evaluate_error_l1_m(d_c, d_error, 
                              d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y, d_curr_J,
                              n, t,
                              curr_num_elem);
            // copy over error
            cudaMemcpy(error, d_error, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

            // sum total L1 error
            total_error = 0.;
            max_error   = -1;
            for (i = 0; i < curr_num_elem; i++) {
                total_error += error[i];
                max_error = (error[i] > max_error) ? error[i] : max_error;
                // printf("final error %i %.4e\n", i, error[i]);
            }
            printf("L1m U%i error = %.4e\n", n, total_error);
            // printf("inf L1 U%i error = %.019lf\n", n, max_error);
        }

        // L_infinity
        for (n = 0; n < local_N; n++) {
            evaluate_error_linfinity(d_c, d_error, 
                              d_curr_V1x, d_curr_V1y, d_curr_V2x, d_curr_V2y, d_curr_V3x, d_curr_V3y,
                              n, t,
                              curr_num_elem);
            // copy over error
            cudaMemcpy(error, d_error, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

            // sum total L1 error
            max_error   = -1;
            for (i = 0; i < curr_num_elem; i++) {
                max_error = (error[i] > max_error) ? error[i] : max_error;
            }
            printf("inf U%i error = %.4e\n", n, max_error);
        }

        free(error);
    }

    // evaluate and write U to file
    printf("outputting U now...\n");
    write_U(curr_num_elem, total_timesteps, total_timesteps, tprev);
    dump(curr_num_elem, curr_num_sides, local_n_p, local_N, num_sides0, tprev, color_size,-1);
    // write_Um(curr_num_elem, total_timesteps, total_timesteps);
    // write_U_p2(curr_num_elem, total_timesteps, total_timesteps);
    // write_physical(curr_num_elem, total_timesteps, total_timesteps);


    Uv1 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv2 = (double *) malloc(curr_num_elem * sizeof(double));
    Uv3 = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    cudaMalloc((double **)&d_Uv1, curr_num_elem * sizeof(double));
    cudaMalloc((double **)&d_Uv2, curr_num_elem * sizeof(double));
    cudaMalloc((double **)&d_Uv3, curr_num_elem * sizeof(double));


    // evaluate and write to file
    if (eval_error) {
        plot_error(t,  curr_num_elem) ;
        plot_exact(t,  curr_num_elem) ;
        // for (n = 0; n < local_N; n++) {
        //     plot_error_l2<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_c, 
        //                                              d_curr_V1x, d_curr_V1y,
        //                                              d_curr_V2x, d_curr_V2y,
        //                                              d_curr_V3x, d_curr_V3y,
        //                                              d_Uv1, d_Uv2, d_Uv3, d_curr_J,
        //                                              t, n,
        //                                              curr_num_elem);
        //     cudaMemcpy(Uv1, d_Uv1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        //     cudaMemcpy(Uv2, d_Uv2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        //     cudaMemcpy(Uv3, d_Uv3, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        //     sprintf(out_filename, "output/U%derror_l2.msh", n);
        //     printf("Writing to %s...\n", out_filename);
        //     out_file  = fopen(out_filename , "w");
        //     fprintf(out_file, "View \"U%i \" {\n", n);
        //     for (i = 0; i < curr_num_elem; i++) {
        //         fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
        //                                V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
        //                                Uv1[i], Uv2[i], Uv3[i]);
        //     }
        //     fprintf(out_file,"};");
        //     fclose(out_file);
        // }       
        

    }
    
    // free everything else

    // free CPU variables
    free(Uv1);
    free(Uv2);
    free(Uv3);

    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
    
    return 0;
}




    // CODE FOR RESUME...
    // }
    // else{



    //     char line[1000];
    //     int items;

    //     // read the coefficient file
    //     c_filename = "output/resume/C.bin";
    //     c_file = fopen(c_filename, "rb");
    //     if (!c_file) {
    //         printf("\nERROR: c_file not found.\n");
    //         return 1;
    //     }

    //     fread(&tprev,sizeof(double),1,c_file);
    //     fread(&curr_num_elem,sizeof(int),1,c_file);
    //     fread(&local_n_p,sizeof(int),1,c_file);
    //     fread(&local_N,sizeof(int),1,c_file);

    //     double *c = (double *) malloc(curr_num_elem * local_n_p * local_N * sizeof(double));

    //     fread(c,sizeof(double),curr_num_elem * local_n_p * local_N,c_file);
    //     fclose(c_file);










    //     c_filename = "output/resume/curr_mesh.bin";
    //     c_file = fopen(c_filename, "rb");

    //     fread(&curr_num_elem,sizeof(int),1,c_file);

    //     curr_elem = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(curr_elem,sizeof(int),curr_num_elem,c_file);

    //     curr_elevel = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(curr_elevel,sizeof(int),curr_num_elem,c_file);



    //     elem_s1 = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(elem_s1,sizeof(int),curr_num_elem,c_file);
    //     elem_s2 = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(elem_s2,sizeof(int),curr_num_elem,c_file);
    //     elem_s3 = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(elem_s3,sizeof(int),curr_num_elem,c_file);

    //     curr_left1 = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(curr_left1,sizeof(int),curr_num_elem,c_file);
    //     curr_left2 = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(curr_left2,sizeof(int),curr_num_elem,c_file);
    //     curr_left3 = (int *) malloc(curr_num_elem * sizeof(int));
    //     fread(curr_left3,sizeof(int),curr_num_elem,c_file);


    //     J = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(J,sizeof(double),curr_num_elem,c_file);

    //     V1x = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(V1x,sizeof(double),curr_num_elem,c_file);
    //     V1y = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(V1y,sizeof(double),curr_num_elem,c_file);
    //     V2x = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(V2x,sizeof(double),curr_num_elem,c_file);
    //     V2y = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(V2y,sizeof(double),curr_num_elem,c_file);
    //     V3x = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(V3x,sizeof(double),curr_num_elem,c_file);
    //     V3y = (double *) malloc(curr_num_elem * sizeof(double));
    //     fread(V3y,sizeof(double),curr_num_elem,c_file);




    //     fread(&curr_num_sides,sizeof(int),1,c_file);

    //     curr_side  = (int *)   malloc(curr_num_sides * sizeof(int));
    //     fread(curr_side,sizeof(int),curr_num_sides,c_file);

    //     slevel  = (int *) malloc(curr_num_sides * sizeof(int));
    //     fread(slevel,sizeof(int),curr_num_sides,c_file);

    //     original_side  = (int *) malloc(curr_num_sides * sizeof(int));
    //     fread(original_side,sizeof(int),curr_num_sides,c_file);
    //     scolor  = (int *) malloc(curr_num_sides * sizeof(int));
    //     fread(scolor,sizeof(int),curr_num_sides,c_file);

    //     left_elem   = (int *) malloc(curr_num_sides * sizeof(int));
    //     fread(left_elem,sizeof(int),curr_num_sides,c_file);
    //     right_elem  = (int *) malloc(curr_num_sides * sizeof(int));
    //     fread(right_elem,sizeof(int),curr_num_sides,c_file);

    //     left_side_number  = (int *)   malloc(curr_num_sides * sizeof(int));
    //     fread(left_side_number,sizeof(int),curr_num_sides,c_file);
    //     right_side_number = (int *)   malloc(curr_num_sides * sizeof(int));
    //     fread(right_side_number,sizeof(int),curr_num_sides,c_file);






    //     fread(&num_sides0,sizeof(int),1,c_file);
    //     s_length  = (double *)   malloc(num_sides0 * sizeof(double));
    //     fread(s_length,sizeof(double),num_sides0,c_file);
    //     Nx  = (double *)   malloc(num_sides0 * sizeof(double));
    //     fread(Nx,sizeof(double),num_sides0,c_file);
    //     Ny = (double *)   malloc(num_sides0 * sizeof(double));
    //     fread(Ny,sizeof(double),num_sides0,c_file);

    //     fclose(c_file);



    //     cudaDeviceReset();
    //     // set the maximum size of the trees
    //     allocate_gpu(emesh_MAX, smesh_MAX, 
    //                  counter,
    //                  curr_num_elem, curr_num_sides,
    //                  local_N, local_n_p,convergence, 0);

    //     // initialize the gpu
    //     init_gpu_restart(c,
    //                      curr_num_elem, curr_num_sides, local_n_p, local_N,
    //                      curr_elem,
    //                      curr_elevel,
    //                      elem_s1, elem_s2, elem_s3,
    //                      curr_left1, curr_left2, curr_left3,
    //                      V1x, V1y, V2x, V2y, V3x, V3y, J,

    //                      curr_side,
    //                      left_side_number, right_side_number,
    //                      original_side,
    //                      slevel, scolor, 
    //                      left_elem, right_elem,

    //                      num_sides0, s_length, Nx, Ny,

    //                      convergence, eval_error,video,
    //                      n, &r1_local, &r2_local, &w_local, 
    //                      &s_r, &oned_w_local, &local_n_quad, &local_n_quad1d);

    //     preval_partials<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_V1x, d_curr_V1y,
    //                                                   d_curr_V2x, d_curr_V2y,
    //                                                   d_curr_V3x, d_curr_V3y,
    //                                                   d_curr_xr,  d_curr_yr,
    //                                                   d_curr_xs,  d_curr_ys,
    //                                                   curr_num_elem);
    //     preval_basis(&basis_local, r1_local, r2_local, s_r, w_local, oned_w_local, local_n_quad, local_n_quad1d, local_n_p);

    //     allocateCompaction(smesh_MAX);

    //     for(int i = 0; i < 6; i++){
    //         count(d_curr_scolor, i, curr_num_sides, color_size+i);
    //     }


    //     free(c);
    //     free(curr_elem);
    //     free(elem_s1);
    //     free(elem_s2);
    //     free(elem_s3);
    //     free(V1x);
    //     free(V1y);
    //     free(V2x);
    //     free(V2y);
    //     free(V3x);
    //     free(V3y);
    //     free(J);
    //     free(curr_side);
    //     free(left_side_number );
    //     free(right_side_number);
    //     free(left_elem );
    //     free(right_elem);
    //     free(original_side);
    //     free(scolor);
    //     free(slevel);
    //     free(Nx);
    //     free(Ny);
    //     free(s_length);

    // }
