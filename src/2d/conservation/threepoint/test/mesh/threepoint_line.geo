w = 1;

Point(1) = {0,0,0};
Point(2) = {1.9318516525781366, 0.5176380902050415, 0, w};
Point(3) = {1.9318516525781366, 1.8, 0, w};
Point(4) = {-2, 0, 0, w};
Point(5) = { -2,  1.8, 0, w};
Point(6) = { 1.075,  1.8, 0, w};
Point(7) = { 1.075,  0.28804538186, 0, w};


Line(1) = {4, 1};
Line(2) = {1, 7};
Line(3) = {7, 2};
Line(4) = {2, 3};
Line(5) = {3, 6};
Line(6) = {6, 5};
Line(7) = {5, 4};
Line(8) = {6, 7};
Line Loop(9) = {7, 1, 2, -8, 6};
Plane Surface(10) = {9};
Line Loop(11) = {8, 3, 4, 5};
Plane Surface(12) = {11};



Physical Line(10000) = {1, 2, 3};
Physical Line(30000) = {4, 5, 6, 7};
Physical Surface(1) = {10, 12};
