#include "../main.cu"
#define FLUX LLF

/* threepoint.cu
 *
 * This file contains the relevant information for making a system to solve
 *
 */

__device__ double get_GAMMA();

__device__ void U0(double *, double, double);

__device__ void U_inflow(double *, double, double, double);

__device__ void U_outflow(double *, double, double, double);

/* size of the system */
int local_N = 4;

/***********************
 *
 * EULER DEVICE FUNCTIONS
 *
 ***********************/

__device__ void evalU0(double *U, double *V, int i) {
    int j;
    double u0[4];
    double X[2];

    U[0] = 0.;
    U[1] = 0.;
    U[2] = 0.;
    U[3] = 0.;

    for (j = 0; j < n_quad; j++) {

        // get the 2d point on the mesh
        get_coordinates_2d(X, V, j);

        // evaluate U0 here
        U0(u0, X[0], X[1]);

        // evaluate U at the integration point
        U[0] += w[j] * u0[0] * basis[i * n_quad + j];
        U[1] += w[j] * u0[1] * basis[i * n_quad + j];
        U[2] += w[j] * u0[2] * basis[i * n_quad + j];
        U[3] += w[j] * u0[3] * basis[i * n_quad + j];
    }
}

/* evaluate pressure
 *
 * evaluates the pressure for U
 */
__device__ double pressure(double *U) {

    return (get_GAMMA() - 1.) * (U[3] - (U[1]*U[1] + U[2]*U[2]) / 2. / U[0]);
}

/* is physical
 *
 * returns true if the density and pressure at U make physical sense
 */
__device__ bool is_physical(double *U) {
    if (U[0] < 0) {
        return false;
    }

    if (pressure(U) < 0) {
        return false;
    }

    return true;
}

/* check physical
 *
 * if U isn't physical, replace the solution with the constant average value
 */
__device__ void check_physical(double **C_global, double *C, double *U, int idx) {
    int i;

    // check to see if U is physical
    if (!is_physical(U)) {
        // set C[1] to C[n_p] to zero
        for (i = 1; i < n_p; i++) {
            C_global[0*n_p + i][idx] = 0.;
            C_global[1*n_p + i][idx] = 0.;
            C_global[2*n_p + i][idx] = 0.;
            C_global[3*n_p + i][idx] = 0.;

            C[n_p * 0 + i] = 0.;
            C[n_p * 1 + i] = 0.;
            C[n_p * 2 + i] = 0.;
            C[n_p * 3 + i] = 0.;
        }

        // rebuild the solution as simply the average value
        U[0] = C[n_p * 0 + 0] * basis[0];
        U[1] = C[n_p * 1 + 0] * basis[0];
        U[2] = C[n_p * 2 + 0] * basis[0];
        U[3] = C[n_p * 3 + 0] * basis[0];
    }
}

/* evaluate c
 *
 * evaulates the speed of sound c
 */
__device__ double eval_c(double *U) {
    double p = pressure(U);
    double rho = U[0];

    return sqrt(get_GAMMA() * p / rho);
}    

/***********************
 *
 * EULER FLUX
 *
 ***********************/
/* takes the actual values of rho, u, v, and E and returns the flux 
 * x and y components. 
 * NOTE: this needs the ACTUAL values for u and v, NOT rho * u, rho * v.
 */
__device__ void eval_flux(double *U, double *flux_x, double *flux_y,
                          double *V, double t, int j, int left_side) {

    double rho, rhou, rhov, E;
    double xi, eta, p;
    //double xi_r, eta_r;
    double X[2];
    //double theta = 15.;
    //double F[4], G[4];

    //double costheta = cospi(theta / 180.);
    //double sintheta = sinpi(theta / 180.);

    // evaluate pressure
    p = pressure(U);
    
    // get variables
    rho  = U[0];
    rhou = U[1];
    rhov = U[2];
    E    = U[3];

    // get the grid points on the mesh
    if (left_side >= 0) {
        // in case we're in eval_surface
        get_coordinates_1d(X, V, j, left_side);
    } else {
        // otherwise we're in eval_volume
        get_coordinates_2d(X, V, j);
    }
    xi  = X[0];
    eta = X[1];

    /*
    // rotate the coordinates
    xi_r  =  xi * costheta + eta * sintheta;
    eta_r = -xi * sintheta + eta * costheta;

    // flux_x 
    F[0] = rhou - U[0] * xi_r;
    F[1] = rhou * rhou / rho + p - U[1] * xi_r;
    F[2] = rhou * rhov / rho - U[2] * xi_r;
    F[3] = rhou * (E + p) / rho - U[3] * xi_r;

    // flux_y
    G[0] = rhov - U[0] * eta_r;
    G[1] = rhou * rhov / rho - U[1] * eta_r;
    G[2] = rhov * rhov / rho + p - U[2] * eta_r;
    G[3] = rhov * (E + p) / rho - U[3] * eta_r;

    flux_x[0] = costheta * F[0] - sintheta * G[0];
    flux_x[1] = costheta * F[1] - sintheta * G[1];
    flux_x[2] = costheta * F[2] - sintheta * G[2];
    flux_x[3] = costheta * F[3] - sintheta * G[3];

    flux_y[0] = sintheta * F[0] + costheta * G[0];
    flux_y[1] = sintheta * F[1] + costheta * G[1];
    flux_y[2] = sintheta * F[2] + costheta * G[2];
    flux_y[3] = sintheta * F[3] + costheta * G[3];
    */

    // flux_x 
    flux_x[0] = rhou - U[0] * xi;
    flux_x[1] = rhou * rhou / rho + p - U[1] * xi;
    flux_x[2] = rhou * rhov / rho - U[2] * xi;
    flux_x[3] = rhou * (E + p) / rho - U[3] * xi;

    // flux_y
    flux_y[0] = rhov - U[0] * eta;
    flux_y[1] = rhou * rhov / rho - U[1] * eta;
    flux_y[2] = rhov * rhov / rho + p - U[2] * eta;
    flux_y[3] = rhov * (E + p) / rho - U[3] * eta;
}

/***********************
 *
 * RIEMAN SOLVER
 *
 ***********************/
/* finds the max absolute value of the jacobian for F(u).
 *  |u - c|, |u|, |u + c|
 */
__device__ double eval_lambda(double *U_left, double *U_right,
                              double *V,      double t,
                              double nx,      double ny,
                              int j, int left_side) {
                              
    double left_max, right_max;
    double c_left, c_right;
    double X[2];
    double xibar;

    // get c for both sides
    c_left  = eval_c(U_left);
    c_right = eval_c(U_right);

    // get xibar
    get_coordinates_1d(X, V, j, left_side);
    xibar = nx * X[0] + ny * X[1];
    
    // if speed is positive, want s + c, else s - c
    if (xibar > 0.) {
        left_max = xibar + c_left;
    } else {
        left_max = -xibar + c_left;
    }

    // if speed is positive, want s + c, else s - c
    if (xibar > 0.) {
        right_max = xibar + c_right;
    } else {
        right_max = -xibar + c_right;
    }

    // return the max absolute value of | s +- c |
    if (abs(left_max) > abs(right_max)) {
        return abs(left_max);
    } else { 
        return abs(right_max);
    }
}

/* local lax-friedrichs riemann solver
 */
__device__ void riemann_solver(double *F_n, double *U_left, double *U_right,
                               double *V, double t,
                               double nx, double ny,
                               int j, int left_side, double global_lambda) {
    int n;

    double flux_x_l[4], flux_x_r[4];
    double flux_y_l[4], flux_y_r[4];

    // calculate the left and right fluxes
    eval_flux(U_left, flux_x_l, flux_y_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);

    double lambda = eval_lambda(U_left, U_right, V, t, nx, ny, j, left_side);

    // local lax-friedrichs
    for (n = 0; n < N; n++) {
        F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx + (flux_y_l[n] + flux_y_r[n]) * ny 
                    + lambda * (U_left[n] - U_right[n]));
    }
}


/* roe riemann solver
__device__ void riemann_solver(double *F_n, double *U_left, double *U_right,
                               double *V, double t,
                               double nx, double ny,
                               int j, int left_side) {
    int n;

    double flux_x_l[4], flux_x_r[4];
    double flux_y_l[4], flux_y_r[4];

    double rho_A, u_A, v_A, H_A;
    // density
    double rho_0 = U_left[0], rho_1 = U_right[0];
    double sqrt_rho_0 = sqrt(rho_0), sqrt_rho_1 = sqrt(rho_1);
    double inv_rho_0 = 1./rho_0, inv_rho_1 = 1./rho_1;
 
    rho_A = sqrt(U_left[0] * U_right[0]);

    // u velocity
    double inv_denom = 1./(sqrt_rho_0 + sqrt_rho_1);
    double u_0 = U_left[1]*inv_rho_0; 
    double u_1 = U_right[1]*inv_rho_1;
    u_A = (u_0 * sqrt_rho_0 + u_1 * sqrt_rho_1)*inv_denom;

    // v velocity
    double v_0 = U_left[2]*inv_rho_0, v_1 = U_right[2]*inv_rho_1;
    v_A = (v_0 * sqrt_rho_0 + v_1 * sqrt_rho_1)*inv_denom;
    double v_n_0 = u_0 * nx + v_0 * ny;
    double v_n_1 = u_1 * nx + v_1 * ny;

    // pressures
    //double p_0 = (GAMMA-1.)*(U_left[3] - 0.5*(U_left[1]*U_left[1]+U_left[2]*U_left[2])*inv_rho_0);
    //double p_1 = (GAMMA-1.)*(U_right[3] - 0.5*(U_right[1]*U_right[1]+U_right[2]*U_right[2])*inv_rho_1);
    double p_0 = pressure(U_left);
    double p_1 = pressure(U_right);

    // H value
    double H_0 = (U_left[3]+p_0)*inv_rho_0, H_1 = (U_right[3]+p_1)*inv_rho_1;
    H_A = (H_0 * sqrt_rho_0 + H_1 * sqrt_rho_1)*inv_denom;

    // speed of sound
    double qq = H_A - 0.5*(u_A * u_A + v_A * v_A);

    // for the solver
    double roe[4];
    roe[0] = 0.;
    roe[1] = 0.;
    roe[2] = 0.;
    roe[3] = 0.;

    double c_A = sqrt((get_GAMMA()-1.0) * qq);
    // normal velocity
    double vn = u_A * nx + v_A * ny;

    double evec[4][4];
    double eval[4];

    // eigen values
    eval[0] = abs(vn);
    eval[1] = abs(vn);
    eval[2] = abs(vn+c_A);
    eval[3] = abs(vn-c_A);

    // eigen vectors
    evec[0][0] = 1.;
    evec[0][1] = u_A;
    evec[0][2] = v_A;
    evec[0][3] = 0.5*(u_A*u_A+v_A*v_A);//H_A-qq

    evec[1][0] = 0.;
    evec[1][1] = ny * rho_A;
    evec[1][2] = -nx * rho_A;
    evec[1][3] = rho_A * (u_A * ny - v_A * nx);

    double k = 0.5*rho_A/c_A;
    evec[2][0] = k;
    evec[2][1] = k*(u_A + c_A * nx);
    evec[2][2] = k*(v_A + c_A * ny);
    evec[2][3] = k*(H_A + c_A * vn);

    evec[3][0] = k;
    evec[3][1] = k*(u_A - c_A * nx);
    evec[3][2] = k*(v_A - c_A * ny);
    evec[3][3] = k*(H_A - c_A * vn);

    // characteristic variables

    double charv[4];
	double inv_c_A = 1./c_A;
    double drho = rho_1-rho_0;
    double dp = p_1-p_0;
    double du = u_1-u_0;
    double dv = v_1-v_0;
    charv[0] = drho - dp*inv_c_A*inv_c_A;
    charv[1] = ny * du - nx * dv;
    charv[2] = nx * du + ny * dv + dp/rho_A * inv_c_A;
    charv[3] = -nx*du - ny * dv + dp/rho_A * inv_c_A;

    // finally, the flux
    for(int i = 0; i < 4; i++) {
	    for(int k = 0; j < 4; j++) {
	        roe[k] += eval[i] * charv[i] * evec[i][k];
        }
    }
	

    // calculate the left and right fluxes
    eval_flux(U_left, flux_x_l, flux_y_l, V, t, j, left_side);
    eval_flux(U_right, flux_x_r, flux_y_r, V, t, j, left_side);


    // set the flux
    for (n = 0; n < 4; n++) {
        F_n[n] = 0.5 * ((flux_x_l[n] + flux_x_r[n]) * nx 
                      + (flux_y_l[n] + flux_y_r[n]) * ny  - roe[n]);
    }
}
 */

// returns the source term S
__device__ void source_term(double *S, double *U, double *V, double t, int j) {
    int n;

    // set the source term to 2U
    for (n = 0; n < N; n++) {
        S[n] = -2 * U[n];
    }
}

/***********************
 *
 * CFL CONDITION
 *
 ***********************/
/* global lambda evaluation
 *
 * computes the max eigenvalue of |u + c|, |u|, |u - c|.
 */
__global__ void eval_global_lambda(double **C, double *lambda, 
                                   double *V1x, double *V1y,
                                   double *V2x, double *V2y,
                                   double *V3x, double *V3y,
                                   double t) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double c, xibar;
        double X[2];
        double U[4];
        double V[6];

        // get location
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        // get cell averages
        U[0] = C[n_p * 0 + 0][idx] * basis[0];
        U[1] = C[n_p * 1 + 0][idx] * basis[0];
        U[2] = C[n_p * 2 + 0][idx] * basis[0];
        U[3] = C[n_p * 3 + 0][idx] * basis[0];


        // evaluate c
        c = eval_c(U);

        // get xibar
        X[0] = V[2] * 1./3 + V[4] * 1./3 + V[0] * 1./3;
        X[1] = V[3] * 1./3 + V[5] * 1./3 + V[1] * 1./3;

        xibar = sqrt(X[0]*X[0] + X[1]*X[1]);

        // return the max eigenvalue
        if (xibar > 0) {
            lambda[idx] = xibar + c;
        } else {
            lambda[idx] = -xibar + c;
        }
    }
}

/* evaluate pressure
 * 
 * evaluates pressure at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void eval_pressure(double *C, double *Uv, int vertex) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) {
        int i, n;
        double U[4];

        // calculate values at the integration points
        for (n = 0; n < N; n++) {
            U[n] = 0.;
            for (i = 0; i < n_p; i++) {
                U[n] += C[num_elem * n_p * n + i * num_elem + idx] * basis_vertex[i * 3 + vertex];
            }
        }

        // store result
        Uv[idx] = pressure(U);
    }
}

/* evaluate mach number
 * 
 * evaluates mach number at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void eval_mach(double *C, 
                          double *V1x, double *V1y, 
                          double *V2x, double *V2y, 
                          double *V3x, double *V3y, 
                          double *Uv1, double *Uv2, double *Uv3) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) {
        int i, n, vertex;
        double U[4];
        double c[3], vel[3];
        double u,v;
        double xi,eta;

        double V[6];
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];



        // calculate values at the integration points
        for (vertex = 0; vertex < 3; vertex++) {
            for (n = 0; n < N; n++) {
                U[n] = 0.;
                for (i = 0; i < n_p; i++) {
                    U[n] += C[num_elem * n_p * n + i * num_elem + idx] * basis_vertex[i * 3 + vertex];
                }

                u = U[1]/U[0];
                v = U[2]/U[0];

                xi  = V[2*vertex];
                eta = V[2*vertex + 1];

                //u -= xi;
                //v -= eta;
                
                c[vertex] = eval_c(U);
                vel[vertex] = sqrt(u*u + v*v);
            }
        }


        // store result
        Uv1[idx] = vel[0] / c[0];
        Uv2[idx] = vel[1] / c[1];
        Uv3[idx] = vel[2] / c[2];
    }
}
