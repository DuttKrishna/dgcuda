#include "operations.cuh"
#include <stdio.h>
#include <stdlib.h>

int num_elem, num_sides;
int init_sides;
int max_level, max_refine;
int emesh_MAX, smesh_MAX;

int n_p, N;

double *d_c;

__device__ int *d_elem;
__device__ int *d_side;

__device__ int *d_s1, *d_s2, *d_s3;

__device__ int *d_elevel;
__device__ int *d_slevel;

__device__ int *d_originalside;

__device__ int *d_left1;
__device__ int *d_left2;
__device__ int *d_left3;

__device__ int *d_left_elem;
__device__ int *d_right_elem;
__device__ int *d_left_side_number;
__device__ int *d_right_side_number;

__device__ double *d_V1x;
__device__ double *d_V1y;
__device__ double *d_V2x;
__device__ double *d_V2y;
__device__ double *d_V3x;
__device__ double *d_V3y;


__device__ int *d_circles;
__device__ int *d_lambda;

__device__ double *d_J;

__device__ double *d_xr;
__device__ double *d_yr;
__device__ double *d_xs;
__device__ double *d_ys;

__global__ void preval_inscribed_circles(double *J,
                                         double *V1x, double *V1y,
                                         double *V2x, double *V2y,
                                         double *V3x, double *V3y,
                                         int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double a, b, c, k;
        a = sqrtf(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrtf(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2));
        c = sqrtf(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        k = 0.5 * (a + b + c);

        // for the diameter, we multiply by 2
        J[idx] = 2 * sqrtf(k * (k - a) * (k - b) * (k - c)) / k;
        // printf("c%i (%f,%f) (%f,%f) (%f,%f) %f\n",idx,V1x[idx], V1y[idx], V2x[idx], V2y[idx], V3x[idx], V3y[idx], J[idx]);
    }
}
__global__ void preval_side_length(double *s_length, 
                              double *s_V1x, double *s_V1y, 
                              double *s_V2x, double *s_V2y,
                              int num_sides) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_sides) {
        // compute and store the length of the side
        s_length[idx] = sqrtf(pow(s_V1x[idx] - s_V2x[idx],2) + pow(s_V1y[idx] - s_V2y[idx],2));
    }
}
__global__ void preval_jacobian(double *J, 
                           double *V1x, double *V1y, 
                           double *V2x, double *V2y, 
                           double *V3x, double *V3y,
                           int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double x1, y1, x2, y2, x3, y3;

        // read vertex points
        x1 = V1x[idx];
        y1 = V1y[idx];
        x2 = V2x[idx];
        y2 = V2y[idx];
        x3 = V3x[idx];
        y3 = V3y[idx];

        // calculate jacobian determinant
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        J[idx] = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
    }
}
__global__ void preval_normals(double *Nx, double *Ny, 
                          double *s_V1x, double *s_V1y, 
                          double *s_V2x, double *s_V2y,
                          double *V1x, double *V1y, 
                          double *V2x, double *V2y, 
                          double *V3x, double *V3y,
                          int *left_side_number,
                          int num_sides) {

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_sides) {
        double x, y, length;
        double sv1x, sv1y, sv2x, sv2y;
    
        sv1x = s_V1x[idx];
        sv1y = s_V1y[idx];
        sv2x = s_V2x[idx];
        sv2y = s_V2y[idx];
    
        // lengths of the vector components
        x = sv2x - sv1x;
        y = sv2y - sv1y;
    
        // normalize
        length = sqrtf(pow(x, 2) + pow(y, 2));

        // store the result
        Nx[idx] = -y / length;
        Ny[idx] =  x / length;
    }
}
__global__ void preval_normals_direction(double *Nx, double *Ny, 
                          double *V1x, double *V1y, 
                          double *V2x, double *V2y, 
                          double *V3x, double *V3y,
                          int *left_elem, int *left_side_number,
                          int num_sides) {

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_sides) {
        double new_x, new_y, dot;
        double initial_x, initial_y, target_x, target_y;
        double x, y;
        int left_idx, side;

        // get left side's vertices
        left_idx = left_elem[idx];
        side     = left_side_number[idx];

        // get the normal vector
        x = Nx[idx];
        y = Ny[idx];
    
        // make it point the correct direction by learning the third vertex point
        switch (side) {
            case 0: 
                target_x = V3x[left_idx];
                target_y = V3y[left_idx];
                initial_x = (V1x[left_idx] + V2x[left_idx]) / 2.;
                initial_y = (V1y[left_idx] + V2y[left_idx]) / 2.;
                break;
            case 1:
                target_x = V1x[left_idx];
                target_y = V1y[left_idx];
                initial_x = (V2x[left_idx] + V3x[left_idx]) / 2.;
                initial_y = (V2y[left_idx] + V3y[left_idx]) / 2.;
                break;
            case 2:
                target_x = V2x[left_idx];
                target_y = V2y[left_idx];
                initial_x = (V1x[left_idx] + V3x[left_idx]) / 2.;
                initial_y = (V1y[left_idx] + V3y[left_idx]) / 2.;
                break;
        }

        // create the vector pointing towards the third vertex point
        new_x = target_x - initial_x;
        new_y = target_y - initial_y;

        // find the dot product between the normal and new vectors
        dot = x * new_x + y * new_y;
        
        if (dot > 0) {
            Nx[idx] *= -1;
            Ny[idx] *= -1;
        }
    }
}

__global__ void preval_partials(double *V1x, double *V1y,
                                double *V2x, double *V2y,
                                double *V3x, double *V3y,
                                double *xr,  double *yr,
                                double *xs,  double *ys,
                                int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < num_elem) {
        // evaulate the jacobians of the mappings for the chain rule
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        xr[idx] = V2x[idx] - V1x[idx];
        yr[idx] = V2y[idx] - V1y[idx];
        xs[idx] = V3x[idx] - V1x[idx];
        ys[idx] = V3y[idx] - V1y[idx];
    }
}

void Mesh::readMesh(char *mesh_filename) {
        int i, items;
        char line[1000];
        FILE *mesh_file;

        // open the mesh to get num_elem for allocations
        mesh_file = fopen(mesh_filename, "r");
        if (!mesh_file) {
            printf("\nERROR: mesh file not found.\n");
            quit();
        }
        // stores the number of sides this element has.

        fgets(line, 1000, mesh_file);
        sscanf(line, "%i", &num_elem);
        V1x = (double *) malloc(num_elem * sizeof(double));
        V1y = (double *) malloc(num_elem * sizeof(double));
        V2x = (double *) malloc(num_elem * sizeof(double));
        V2y = (double *) malloc(num_elem * sizeof(double));
        V3x = (double *) malloc(num_elem * sizeof(double));
        V3y = (double *) malloc(num_elem * sizeof(double));

        elem_s1 = (int *) malloc(num_elem * sizeof(int));
        elem_s2 = (int *) malloc(num_elem * sizeof(int));
        elem_s3 = (int *) malloc(num_elem * sizeof(int));

        // read the elements from the mesh
        for (i = 0; i < num_elem; i++) {
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%lf %lf %lf %lf %lf %lf %i %i %i", &V1x[i], &V1y[i], 
                                             &V2x[i], &V2y[i], 
                                             &V3x[i], &V3y[i], 
                                             &elem_s1[i], &elem_s2[i], &elem_s3[i]);

            if (items != 9) {
                printf("error: not enough items (%i) while reading elements from mesh.\n", items);
                exit(0);
            }

        }

        fgets(line, 1000, mesh_file);
        sscanf(line, "%i", &num_sides);

        left_side_number  = (int *)   malloc(num_sides * sizeof(int));
        right_side_number = (int *)   malloc(num_sides * sizeof(int));

        sides_x1    = (double *) malloc(num_sides * sizeof(double));
        sides_x2    = (double *) malloc(num_sides * sizeof(double));
        sides_y1    = (double *) malloc(num_sides * sizeof(double));
        sides_y2    = (double *) malloc(num_sides * sizeof(double)); 

        left_elem   = (int *) malloc(num_sides * sizeof(int));
        right_elem  = (int *) malloc(num_sides * sizeof(int));

        original_side  = (int *) malloc(num_sides * sizeof(int));

        // read the edges from the mesh
        for (i = 0; i < num_sides; i++) {
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%lf %lf %lf %lf %i %i %i %i", &sides_x1[i], &sides_y1[i],
                                                &sides_x2[i], &sides_y2[i],
                                                &left_elem[i], &right_elem[i],
                                                &left_side_number[i],
                                                &right_side_number[i]);
            original_side[i] = i;
            if (items != 8) {
                printf("error: not enough items (%i) while reading edges from mesh.\n", items);
                exit(0);
            }
        }
            // close the file
        fclose(mesh_file);
    }

void Mesh::allocateGPUMesh() {
    allocate((void **) &d_c, N * emesh_MAX * n_p * sizeof(double), &counter.integration_memory);

    allocate((void **) &d_elem, emesh_MAX * sizeof(int), &counter.emesh_memory);
    allocate((void **) &d_side, smesh_MAX * sizeof(int), &counter.smesh_memory);
    
    allocate((void **) &d_s1,  emesh_MAX * sizeof(int), &counter.emesh_memory);
    allocate((void **) &d_s2,  emesh_MAX * sizeof(int), &counter.emesh_memory);
    allocate((void **) &d_s3,  emesh_MAX * sizeof(int), &counter.emesh_memory);

    allocate((void **) &d_left1, emesh_MAX * sizeof(int), &counter.emesh_memory);
    allocate((void **) &d_left2, emesh_MAX * sizeof(int), &counter.emesh_memory);
    allocate((void **) &d_left3, emesh_MAX * sizeof(int), &counter.emesh_memory);

    allocate((void **) &d_originalside, smesh_MAX * sizeof(int), &counter.smesh_memory);

    allocate((void **) &d_elevel, emesh_MAX * sizeof(int) , &counter.emesh_memory);
    allocate((void **) &d_slevel, smesh_MAX * sizeof(int) , &counter.smesh_memory);
    set_value<<<get_blocks(emesh_MAX,512),512>>>(d_elevel, 0, emesh_MAX);
    set_value<<<get_blocks(smesh_MAX,512),512>>>(d_slevel, 0, smesh_MAX);


    allocate((void **) &d_left_side_number , smesh_MAX * sizeof(int), &counter.smesh_memory);
    allocate((void **) &d_right_side_number, smesh_MAX * sizeof(int), &counter.smesh_memory);

    allocate((void **) &d_left_elem , smesh_MAX * sizeof(int), &counter.smesh_memory);
    allocate((void **) &d_right_elem, smesh_MAX * sizeof(int), &counter.smesh_memory);

    allocate((void **) &d_J, emesh_MAX * sizeof(double), &counter.geometry_memory);

    allocate((void **) &d_xr, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_yr, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_xs, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_ys, emesh_MAX * sizeof(double), &counter.geometry_memory);

    allocate((void **) &d_V1x, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_V1y, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_V2x, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_V2y, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_V3x, emesh_MAX * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_V3y, emesh_MAX * sizeof(double), &counter.geometry_memory);

    allocate((void **) &d_s_length , num_sides * sizeof(double), &counter.geometry_memory);

    allocate((void **) &d_s_V1x, num_sides * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_s_V2x, num_sides * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_s_V1y, num_sides * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_s_V2y, num_sides * sizeof(double), &counter.geometry_memory);

    allocate((void **) &d_Nx, num_sides * sizeof(double), &counter.geometry_memory);
    allocate((void **) &d_Ny, num_sides * sizeof(double), &counter.geometry_memory);

    allocate((void **) &d_lambda , emesh_MAX * sizeof(double), &counter.emesh_memory);
    allocate((void **) &d_circles, emesh_MAX * sizeof(double), &counter.emesh_memory);

}

void Mesh::initGPUMesh() {
        cudaMemcpy(d_s_V1x, sides_x1, num_sides * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_s_V1y, sides_y1, num_sides * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_s_V2x, sides_x2, num_sides * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_s_V2y, sides_y2, num_sides * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_elem_s1, elem_s1, num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_elem_s2, elem_s2, num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_elem_s3, elem_s3, num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_s1, elem_s1, num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_s2, elem_s2, num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_s3, elem_s3, num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_V1x, V1x, num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_V1y, V1y, num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_V2x, V2x, num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_V2y, V2y, num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_V3x, V3x, num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_V3y, V3y, num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_left_side_number , left_side_number , num_sides * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_right_side_number, right_side_number, num_sides * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_left_elem , left_elem , num_sides * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_right_elem, right_elem, num_sides * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_originalside, original_side, num_sides * sizeof(int), cudaMemcpyHostToDevice);

        cs1 = num_elem;
    }

void Mesh::freeCPUMesh(){
    free(elem_s1);
    free(elem_s2);
    free(elem_s3);
    free(sides_x1);
    free(sides_x2);
    free(sides_y1);
    free(sides_y2);
    free(left_elem);
    free(right_elem);
    free(left_side_number);
    free(right_side_number);
}

void Mesh::precomputations(){
    double *min_radius;

    // find the min inscribed circle
    preval_inscribed_circles<<<get_blocks(num_elem, n_threads), n_threads>>>(d_J, 
                                                                             d_V1x, d_V1y, 
                                                                             d_V2x, d_V2y, 
                                                                             d_V3x, d_V3y, 
                                                                             num_elem);
    min_radius = (double *) malloc(num_elem * sizeof(double));

    /*
    // find the min inscribed circle. do it on the gpu if there are at least 256 elements
    if (num_elem >= 256) {
        //min_reduction<<<n_blocks_reduction, 256>>>(d_J, d_reduction, num_elem);
        cudaThreadSynchronize();
        checkCudaError("error after min_jacobian.");

        // each block finds the smallest value, so need to sort through n_blocks_reduction
        min_radius = (double *) malloc(n_blocks_reduction * sizeof(double));
        cudaMemcpy(min_radius, d_reduction, n_blocks_reduction * sizeof(double), cudaMemcpyDeviceToHost);
        min_r = min_radius[0];
        for (i = 1; i < n_blocks_reduction; i++) {
            min_r = (min_radius[i] < min_r) ? min_radius[i] : min_r;
        }
        free(min_radius);

    } else {
        */
        // just grab all the radii and sort them sidesFromRefinemente there are so few of them
        min_radius = (double *) malloc(num_elem * sizeof(double));
        cudaMemcpy(min_radius, d_J, num_elem * sizeof(double), cudaMemcpyDeviceToHost);
        *min_r = min_radius[0];
        for (int i = 1; i < num_elem; i++) {
            *min_r = (min_radius[i] < *min_r) ? min_radius[i] : *min_r;
        }
        free(min_radius);
    //}

    // pre computations
    preval_jacobian<<<get_blocks(num_elem, n_threads), n_threads>>>(d_J, d_V1x, d_V1y, d_V2x, d_V2y, d_V3x, d_V3y, num_elem); 
    preval_side_length<<<get_blocks(num_sides, n_threads), n_threads>>>(d_s_length, d_s_V1x, d_s_V1y, d_s_V2x, d_s_V2y, num_sides);
    preval_normals<<<get_blocks(num_sides, n_threads), n_threads>>>(d_Nx, d_Ny, 
                                                                    d_s_V1x, d_s_V1y, d_s_V2x, d_s_V2y,
                                                                    d_V1x, d_V1y, 
                                                                    d_V2x, d_V2y, 
                                                                    d_V3x, d_V3y, 
                                                                    d_left_side_number, 
                                                                    num_sides);
    preval_normals_direction<<<get_blocks(num_sides, n_threads), n_threads>>>(d_Nx, d_Ny, 
                                                                              d_V1x, d_V1y, 
                                                                              d_V2x, d_V2y, 
                                                                              d_V3x, d_V3y, 
                                                                              d_left_elem, d_left_side_number,
                                                                              num_sides);
    preval_partials<<<get_blocks(num_elem, n_threads), n_threads>>>(d_V1x, d_V1y,
                                                                    d_V2x, d_V2y,
                                                                    d_V3x, d_V3y,
                                                                    d_xr,  d_yr,
                                                                    d_xs,  d_ys,
                                                                    num_elem);
    // cudaThreadSynchronize();
    // checkCudaError("error after prevals. 2");
    // // free computed variables
    // free_gpu(1, convergence, eval_error);


    // // evaluate the basis functions at those points and store on GPU
    // preval_basis(basis_local, r1_local, r2_local, s_r, w_local, oned_w_local, local_n_quad, local_n_quad1d, local_n_p);
    // cudaThreadSynchronize();

    // free(oned_w_local);
    // free(s_r);
}


Mesh::Mesh(char *mesh_filename, int in_n_p, int in_N ){
            n_p = in_n_p;
            N = in_N;
            readMesh(mesh_filename);
            allocateGPUMesh();
            initGPUMesh();
            freeCPUMesh();

            precomputations();
}