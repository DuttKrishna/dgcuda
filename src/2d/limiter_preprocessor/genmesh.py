
# MIT License

# Copyright (c) 2017 Andrew Giuliani

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import sys
import time
from sys import argv
import random
import copy
import math


class Face(object):
	def __init__(self, in_type,in_left_elem,in_right_elem,in_vertices):
		self.type = in_type
		self.vertices_list = copy.deepcopy(in_vertices) #order of vertices matters for PDE solver!
		self.vertices = frozenset(in_vertices) 
		self.pos = -1
		self.left_elem = in_left_elem
		self.right_elem = in_right_elem
		self.lsn = 0
		self.rsn = 0
		self.color = -1
		self.neigh_faces = None

	def set_position(self,in_num):
		self.pos = in_num

class Element(object):
	def __init__(self, in_type,in_vertices):
		self.type = in_type
		self.vertices = in_vertices
		self.pos = -1
		self.faces = set()
		self.neighbors = set()

	def set_position(self,in_num):
		self.pos = in_num

def get_neigh_faces(face):
	le = face.left_elem
	re = face.right_elem
	if re.type >-1:
		neigh_faces = le.faces.union(re.faces ).difference(set([face]))
	else:
		neigh_faces = le.faces.difference(set([face]))

	return list(neigh_faces)

def find_swappable(face,prev_face, loop_face, num_colors):
	Ek = [f for f in face.neigh_faces if not(f == prev_face)] # cannot go backwards

	c_list = [f.color for f in Ek if f.color>-1]
	color_hash = [0]*num_colors
	for c in c_list:
		color_hash[c] = color_hash[c]+1

	single_color = [index for index in xrange(num_colors) if color_hash[index]==1]
	possible_swaps = [f for f in Ek if f.color in single_color]

	candidate = random.choice(possible_swaps)
	return [color_hash,candidate]

def update_current_conflict(conflict_face):
	if len(conflict_face) > 0:
		random_swap = random.randint(0, len(conflict_face)-1)
		conflict_face[-1], conflict_face[random_swap] = conflict_face[random_swap],conflict_face[-1]
	return [conflict_face[-1]]*3 if len(conflict_face) > 0 else [-1]*3

def update_loop_face(loop_face,conflict_face):
	c_list = [f.color for f in conflict_face.neigh_faces if f.color>-1]
	color_hash = [0]*num_colors
	for c in c_list:
		color_hash[c] = color_hash[c]+1
	return conflict_face if not 2 in color_hash else loop_face

def same_color(face, num_colors):
	c_list = [f.color for f in face.neigh_faces if f.color>-1]
	color_hash = [0]*num_colors
	for c in c_list:
		color_hash[c] = color_hash[c]+1
	double_color = color_hash.index(2)
	return[f for f in face.neigh_faces if f.color == double_color]

def get_action(c_face, count, num_colors):
	Ck = set([f.color for f in c_face.neigh_faces if f.color>-1])
	C = set(range(num_colors)).difference(Ck)
	if(len(C) > 0): 
		return [0, C, None, -1]

	[chash,swap_with] = find_swappable(c_face, prev_face,loop_face,num_colors)

	if count == -1:
		if not (swap_with == loop_face): #swap
			return [1, None, swap_with, -1]
		elif swap_with == loop_face:  #swap
			return [1, None, swap_with, random.randint(1, 100)]

	elif count >0 and count <=100: #swap
		return [1, None, swap_with, count -1]
	elif count ==0 and 2 in chash: # can be doubled
		return [2, None, swap_with, -1]
	else: #cannot be doubled swap
		return [1, None, swap_with, 0]







if __name__ == "__main__":
	inFilename  = argv[1] 
	outFilename = argv[2]
	space_dim = int(argv[3])
	elem_dim = int(argv[4])

	print "? this is a %iD mesh" % elem_dim
	inFile  = open(inFilename, "rb")
	outFile = open(outFilename, "wb")

	print "? reading file: %s..." % inFilename
	line = inFile.readline()
	while "$Nodes" not in line:
	    line = inFile.readline()

	# the next line is the number of vertices
	num_vertices = int(inFile.readline())

	vertex_list = []
	for i in xrange(0,num_vertices):
	    s = inFile.readline().split()
	    if space_dim == 3:
	    	vertex_list.append((float(s[1]), float(s[2]), float(s[3])))
	    else:
	    	vertex_list.append((float(s[1]), float(s[2])))

	# next two lines are just filler
	inFile.readline()
	inFile.readline()

	# next line is the number of elements and faces
	num_contents = int(inFile.readline())
	elem_list=[]
	face_list=[]
	face_dict = {}
	inflow = 0
	outflow = 0
	reflecting = 0
	axisym = 0

	for i in xrange(num_contents):
	    s = inFile.readline().split()

	    shift = 3 + int(s[2])

	    if int(s[3]) == 1 or int(s[3]) == 10000 :
	    	reflecting +=1
	    elif int(s[3]) == 2 or int(s[3]) == 20000 :
	    	outflow+=1
	    elif int(s[3]) == 3 or int(s[3]) == 30000 :
	    	inflow+=1
	    elif int(s[3]) == 4 or int(s[3]) == 40000 :
	    	axisym+=1

	    # line
	    if int(s[1]) == 1 :
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        if elem_dim-1 == 1:
	        	face_list.append(Face(1,None,Element(-int(s[3]),-1),[v1,v2]))
	        	face_dict[frozenset([v1,v2])] = face_list[-1]

		# triangle.
	    elif int(s[1]) == 2:
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        v3 = int(s[shift + 2]) - 1

	        if elem_dim == 2:
	        	elem_list.append(Element(2,(v1,v2,v3)))
	        elif elem_dim-1 == 2:
	        	face_list.append(Face(2,None,Element(-int(s[3]),-1),[v1,v2,v3]))
	        	face_dict[frozenset([v1,v2,v3])] = face_list[-1]

		# quadrangle.
	    elif int(s[1]) == 3:
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        v3 = int(s[shift + 2]) - 1
	        v4 = int(s[shift + 3]) - 1

	        if elem_dim == 2:
	        	elem_list.append(Element(3,(v1,v2,v3,v4)))
	        elif elem_dim-1 == 2:
	        	face_list.append(Face(3,None,Element(-int(s[3]),-1),[v1,v2,v3,v4]))
	        	face_dict[frozenset([v1,v2,v3,v4])] = face_list[-1]

		# tetrahedron.
	    elif int(s[1]) == 4:
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        v3 = int(s[shift + 2]) - 1
	        v4 = int(s[shift + 3]) - 1
        	elem_list.append(Element(4,(v1,v2,v3,v4)))

		# hexahedron.
	    elif int(s[1]) == 5:
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        v3 = int(s[shift + 2]) - 1
	        v4 = int(s[shift + 3]) - 1
	        v5 = int(s[shift + 4]) - 1
	        v6 = int(s[shift + 5]) - 1
	        v7 = int(s[shift + 6]) - 1
	        v8 = int(s[shift + 7]) - 1
        	elem_list.append(Element(5,(v1,v2,v3,v4,v5,v6,v7,v8)))

        # prism
	    elif int(s[1]) == 6:
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        v3 = int(s[shift + 2]) - 1
	        v4 = int(s[shift + 3]) - 1
	        v5 = int(s[shift + 4]) - 1
	        v6 = int(s[shift + 5]) - 1
        	elem_list.append(Element(6,(v1,v2,v3,v4,v5,v6)))
        # pyramid
	    elif int(s[1]) == 7:
	        v1 = int(s[shift + 0]) - 1
	        v2 = int(s[shift + 1]) - 1
	        v3 = int(s[shift + 2]) - 1
	        v4 = int(s[shift + 3]) - 1
	        v5 = int(s[shift + 4]) - 1
        	elem_list.append(Element(7,(v1,v2,v3,v4,v5)))

	inFile.close()

	num_elem = len(elem_list)
	print "  There are (%i) elements." % num_elem
	print "  There are (%i) boundary faces." % len(face_list)
	num_boundary = len(face_list)
	print "? finding interior faces."
	for elem in elem_list:
		# add the sides of each element to face_list if the faces are not in the face dictionary
		if elem.type == 2: # triangles
			v0 = elem.vertices[0]
			v1 = elem.vertices[1]
			v2 = elem.vertices[2]
			list_of_faces = [[v0,v1], [v1,v2], [v2,v0]]
			face_type = [1]*len(list_of_faces)
		elif elem.type == 3: # quadrilateral
			v0 = elem.vertices[0]
			v1 = elem.vertices[1]
			v2 = elem.vertices[2]
			v3 = elem.vertices[3]
			list_of_faces = [[v0,v1], [v1,v2], [v2,v3], [v3,v0]]
			face_type = [1]*len(list_of_faces)
		elif elem.type == 4: # tetrahedron
			v0 = elem.vertices[0]
			v1 = elem.vertices[1]
			v2 = elem.vertices[2]
			v3 = elem.vertices[3]
			list_of_faces = [[v0,v1,v3], [v1,v2,v3], [v2,v0,v1], [v3,v0,v2]]
			face_type = [2]*len(list_of_faces)
		elif elem.type == 5: # hexahedron
			v0 = elem.vertices[0]
			v1 = elem.vertices[1]
			v2 = elem.vertices[2]
			v3 = elem.vertices[3]
			v4 = elem.vertices[4]
			v5 = elem.vertices[5]
			v6 = elem.vertices[6]
			v7 = elem.vertices[7]
			list_of_faces = [[v3,v7,v6,v2], [v0,v4,v7,v3], [v4,v5,v6,v7], [v5,v1,v2,v6], [v1,v0,v3,v2], [v1,v5,v4,v0]]
			face_type = [3]*len(list_of_faces)
		elif elem.type == 6: # prism
			v0 = elem.vertices[0]
			v1 = elem.vertices[1]
			v2 = elem.vertices[2]
			v3 = elem.vertices[3]
			v4 = elem.vertices[4]
			v5 = elem.vertices[5]
			list_of_faces = [[v3,v4,v5], [v2,v0,v1], [v1,v4,v5,v2], [v2,v5,v3,v0], [v0,v3,v4,v1]]
			face_type = [2,2,3,3,3]
		elif elem.type == 7: # pyramids
			v0 = elem.vertices[0]
			v1 = elem.vertices[1]
			v2 = elem.vertices[2]
			v3 = elem.vertices[3]
			v4 = elem.vertices[4]
			list_of_faces = [[v0,v1,v4], [v1,v2,v4], [v2,v3,v4], [v0,v3,v4], [v0,v1,v2,v3]]
			face_type = [2,2,2,2,3]
		else:
			print "Error element type not found (%i)\n" % elem.type
			quit()
		for face_num, test_face in enumerate(list_of_faces):
			set_test_face = frozenset(test_face)
			if (set_test_face in face_dict):
				elem.faces.add(face_dict[set_test_face])

				if(face_dict[set_test_face].left_elem == None ):
					face_dict[set_test_face].left_elem = elem
					face_dict[set_test_face].lsn = face_num

				elif(face_dict[set_test_face].right_elem == None):
					face_dict[set_test_face].right_elem = elem
					face_dict[set_test_face].rsn = face_num
				else:
					print "This mesh has non-manifold faces i.e. more than two elements incident on a face.  Not allowed."
					quit()

			else:
				face_list.append(Face(face_type[face_num],elem,None,test_face))
				elem.faces.add(face_list[-1])
				face_dict[set_test_face] = face_list[-1]
				face_list[-1].lsn = face_num

	for i,face in enumerate(face_list):
		#populate left and right element neighbor lists
		le = face.left_elem
		re = face.right_elem
		le.neighbors.add(re)
		if(not re == None):
			re.neighbors.add(le)
		else:
			print "  Problem with msh file: there is a boundary edge not accounted for!", i, face.vertices
			quit()
		face.neigh_faces = get_neigh_faces(face)

	#how many colors are needed?
	degree = 0
	for elem in elem_list:
		if len(elem.neighbors) > degree:
			degree = len(elem.neighbors)
		for neigh in elem.neighbors:
			if not elem in neigh.neighbors:
				print "connectivity problem."


	#number of colors needed is the degree of the graph
	num_colors = degree
	print "  The degree of the mesh dual graph is (%i).\n" % degree
	if elem_dim == 2 and num_boundary == 0 :
		ksi = len(vertex_list) - len(face_list) + len(elem_list)
		print "  *This is a surface mesh without any boundaries!*"
		print "  The Euler characteristic is (%i)." % (ksi)
		print "  This is a surface without boundaries, there are (%i) holes." % ((2.- ksi)/2.)
		print "  If this is an unexpected number of holes, check the mesh file."
	num_faces = len(face_list)

	print "  There are (%i) faces total in the mesh.\n" % num_faces

	face_types = [face.type for face in face_list]
	elem_types = [elem.type for elem in elem_list]
	print "  BC STATISTICS:\n  (%i) inflow\n  (%i) outflow\n  (%i) reflecting\n  (%i) axisymmetric\n " %(inflow, outflow, reflecting, axisym)
	print "  FACE STATISTICS:\n  (%i) lines\n  (%i) tri\n  (%i) quad\n " %(face_types.count(1),face_types.count(2),face_types.count(3))
	print "  ELEM STATISTICS:\n  (%i) tri\n  (%i) quads \n  (%i) tets \n  (%i) hexes\n  (%i) prisms\n  (%i) pyramids\n " %(elem_types.count(2),elem_types.count(3),elem_types.count(4),elem_types.count(5), elem_types.count(6), elem_types.count(7))
	
	for i,face in enumerate(face_list):
		face.pos = i

	print "? coloring the faces"
	start = time.time()

	conflict_face = []
    #greedy graph coloring
	for face in face_list:
		#colors of neighboring faces
		Ck = set([f.color for f in face.neigh_faces])
		C = set(xrange(num_colors)).difference(Ck)

		if(len(C) > 0):
			face.color = random.choice(list(C))
		else:
			conflict_face.append(face)

	print '  initial number of conflicts ',len(conflict_face), "\n"
	original_num_conflicts = len(conflict_face)

	c_face = conflict_face[-1]
	prev_face = conflict_face[-1]
	loop_face = conflict_face[-1]
	count = -1

	while len(conflict_face) > 0:
		[action, C, swap_with, count] = get_action(c_face, count, num_colors)
		# print action, count, len(conflict_face), c_face.pos
		if action == 0: # RESOLVE
			c_face.color = random.choice(list(C))
			conflict_face.pop()
			[c_face,prev_face,loop_face] = update_current_conflict(conflict_face)
		elif action == 1: # SWAP
			c_face.color = swap_with.color
			prev_face = c_face
			swap_with.color = -1
			conflict_face[-1] = swap_with

			c_face = conflict_face[-1]
			# loop_face = update_loop_face(loop_face,c_face)
		else: # DOUBLE
			[face1, face2] = same_color(c_face, num_colors) #double the conflict on two faces that have the same color
			c_face.color = face1.color

			face1.color=-1
			face2.color=-1

			conflict_face[-1] = face1
			conflict_face.append(face2)
			[c_face,prev_face,loop_face] = update_current_conflict(conflict_face)


		# sys.stdout.write("  conflicts left %5.1i/%i\r" % (len(conflict_face), original_num_conflicts))
		# sys.stdout.flush()
	end = time.time()
	print "  color timing: %lf" % (end - start)






	elem_check = [0]*num_elem
	color_set = set(range(num_colors))
	for elem in elem_list:
		colors = [f.color for f in elem.faces]
		if len(colors) != len(set(colors)) or not set(colors).issubset(color_set):
			print "problem with element %i\n" % elem.pos, colors, [f.pos for f in elem.faces]

	color_count = [0] * num_colors
	for f in face_list:
		color_count[f.color]+=1

	print "\n\n  COLOR STATISTICS: "
	for i,c in enumerate(color_count):
		print "  %i: " %i + str(c)

	start = time.time()

	current_ID = 0
	face_pos_dict = {}
	face_list_ordered = []
	#reorder the edges based on edge colors
	for c in xrange(num_colors):
		print "? ordering based on edge color ", c
		for f in face_list:
			if f.color == c:
				f.pos = current_ID
				face_pos_dict[current_ID] = f
				current_ID+=1

	for fID in xrange(num_faces):
		face_list_ordered.append(face_pos_dict[fID]) # faces are now ordered by color



	print "? renumbering left elements in color 1"
	#renumbering left elements in color 1
	for current_ID in xrange(color_count[0]):
		face_list_ordered[current_ID].left_elem.pos = current_ID 

	current_ID = color_count[0]
	print "? renumbering right elements in color 1"
	#renumbering right elements in color 1
	for i in xrange(color_count[0]):
		if(face_list_ordered[i].right_elem.type > -1):
			face_list_ordered[i].right_elem.pos = current_ID
			current_ID+=1

	for elem in elem_list: # for hybrid meshes, not all elements may have a face of color 1
		if elem.pos == -1:
			elem.pos = current_ID
			current_ID+=1







	#reordering edges by left element in colors [1..num_colors]
	l_range = color_count[0]
	u_range = color_count[0]+color_count[1]
	for c in xrange(1,num_colors):
		print "? reordering by left elements in color ",c
		#populate the dictionary
		left_dict = {} #returns the face with left element [i]
		for i in range(l_range, u_range):
			f = face_list_ordered[i]
			left_dict[f.left_elem.pos] = f

		count = l_range
		for i in xrange(num_elem):
			if(i in left_dict):
				left_dict[i].pos = count # let the face be at the position count
				count+=1

		if(c + 1 < num_colors):
			l_range = u_range
			u_range += color_count[c+1]



	# all elements and edges now have prescribed positions
	face_pos_dict = {}
	face_list_ordered = []
	for f in face_list:
		face_pos_dict[f.pos] = f

	for fID in xrange(num_faces):
		face_list_ordered.append(face_pos_dict[fID]) # ordered by color



	elem_pos_dict = {}
	elem_list_ordered = []
	#reorder the edges based on edge colors
	for e in elem_list:
		elem_pos_dict[e.pos] = e

	for eID in xrange(num_elem):
		elem_list_ordered.append(elem_pos_dict[eID]) # ordered by color


	end = time.time()
	print "  renumbering and reordering timing: %lf" % (end - start)


	elem_check = [0]*num_elem
	color_set = set(range(num_colors))
	for elem in elem_list:
		colors = [f.color for f in elem.faces]
		if len(colors) != len(set(colors)) or not set(colors).issubset(color_set):
			print "problem with element %i\n" % elem.pos, colors, [f.pos for f in elem.faces]

	color_count = [0] * num_colors
	for f in face_list:
		color_count[f.color]+=1

	print "\n\n  COLOR STATISTICS: "
	for i,c in enumerate(color_count):
		print "  %i: " %i + str(c)


	# if space_dim == 2:
		# import matplotlib.pyplot as plt
	# 	for f in face_list:
	# 		v = f.vertices_list
	# 		x_coords = [ vertex_list[temp][0] for temp in v ]
	# 		y_coords = [ vertex_list[temp][1] for temp in v ]


	# 		if f.color == 0:
	# 			plt.plot(x_coords, y_coords, 'g-', linewidth= 2)
	# 		elif f.color == 1:
	# 			plt.plot(x_coords, y_coords, 'b-', linewidth= 2)
	# 		elif f.color == 2:
	# 			plt.plot(x_coords, y_coords, 'r-', linewidth= 2)
	# 		elif f.color == 3:
	# 			plt.plot(x_coords, y_coords, 'k-', linewidth= 2)
	# 	plt.axes().set_aspect('equal', 'datalim')
	# 	plt.axis('off')
	# 	plt.show()
	# else:
		# import matplotlib.pyplot as plt
	# 	from mpl_toolkits.mplot3d import Axes3D
	# 	fig = plt.figure()
	# 	ax = plt.axes(projection='3d')
	# 	for f in face_list:
	# 		v = f.vertices_list
	# 		x_coords = [ vertex_list[temp][0] for temp in v ]
	# 		y_coords = [ vertex_list[temp][1] for temp in v ]
	# 		z_coords = [ vertex_list[temp][2] for temp in v ]


	# 		if f.color == 0:
	# 			ax.plot(x_coords, y_coords, z_coords, 'g-', linewidth= 2)
	# 		elif f.color == 1:
	# 			ax.plot(x_coords, y_coords, z_coords, 'b-', linewidth= 2)
	# 		elif f.color == 2:
	# 			ax.plot(x_coords, y_coords, z_coords, 'r-', linewidth= 2)
	# 		elif f.color == 3:
	# 			ax.plot(x_coords, y_coords, z_coords, 'k-', linewidth= 2)
	# 	plt.axes().set_aspect('equal', 'datalim')
	# 	plt.show()






    # write the mesh to file
	print "? writing file: %s..." % outFilename
	outFile.write(str(num_elem) + "\n")
	for elem in elem_list_ordered:
		coords = [vertex_list[vert] for vert in elem.vertices]
		coords = [str(x) for t in coords for x in t]
		coords = " ".join(coords)

		fac = [str(t.pos) for t in elem.faces]
		fac = " ".join(fac)
		# coords = ""
		outFile.write("%s\n" % (coords+" "+fac) )

	outFile.write(str(num_faces) + "\n")
	for face in face_list_ordered:
		coords = [vertex_list[vert] for vert in face.vertices_list]
		coords = [str(x) for t in coords for x in t]
		coords = " ".join(coords)

		lr_elements = str(face.left_elem.pos) + " " + str(face.right_elem.pos)
		lrsn = str(face.lsn) + " " + str(face.rsn)
		color = str(face.color)
		# coords = ""
		outFile.write("%s\n" % (coords+" "+lr_elements+" " + lrsn + " " +color) )

	for i in range(num_colors):
		outFile.write(str(color_count[i]) + "\n")
	if num_colors < 10:
		for i in range(num_colors, 10):
			outFile.write(str(0) + "\n")


	outFile.close()

